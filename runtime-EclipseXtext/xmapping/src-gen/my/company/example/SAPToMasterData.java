package my.company.example;

import io.FileInput;
import xpath.ReturnValue;
import io.DocumentOutput;
import my.company.example.statement.saptomasterdata.MacroFirstE1WBB12Node;
import xpath.XPathResolver;
import java.util.List;
import java.util.ArrayList;

/**
* This class was created: Mon Apr 26 13:06:22 CEST 2021
*/	
public class SAPToMasterData {
	
	/** Input file which represents Input **/
	public static FileInput input;
	/** Output file which represents Output **/
	public static DocumentOutput output;
	/** ArrayList for saving return values from routines **/
	private static ArrayList<ReturnValue> routineReturns;
				
	public static void main(String[] args){
		if(args.length <= 0){
			System.out.println("No args found - ending program.");
			System.out.println("To see manual, write arg: help");
			return;
		}
		if(args[0].equals("help")){
			System.out.println("Available args:");
			   System.out.println("1) Import input file: <path_to_input_file_xml>");
			   System.out.println("2) Import output file name: <output_file_name>");
			   System.out.println("----------------------------------------------");
			   System.out.println("<path_to_input_file_xml> - must contains suffix \".xml\"");
			   System.out.println("<output_file_name> - can not contains suffix \".xml\"");
			   return;
		}
		
		//checks input args
		if(!checkArgs(args)){
			System.out.println("Bad arguments - please check arguments");
			System.out.println("To see manual, write arg: help");
			return;
		}
		
		//main block
		int variable_jan = 5;
		int variable_integer = 10;
		int variable_e1wbb12node = 5;
		String variable_dasdasd = "String";
		DocumentOutput variable_jj = my.company.example.SAPToMasterData.output.getPartOfDocument("XPATH");
		int variable_dd = 5;
		XPathResolver.queryInputOutput(my.company.example.SAPToMasterData.input, "dsad", my.company.example.SAPToMasterData.output, "Dsad");
		XPathResolver.queryValueOutput(variable_integer, my.company.example.SAPToMasterData.output, "Dsad");
		XPathResolver.queryValueOutput(variable_integer, variable_jj, "DDASD");
		 		
		routineReturns = MacroFirstE1WBB12Node.macro_firste1wbb12node(variable_dd); 		
		variable_jan = (Integer) routineReturns.get(0).getValue(); 		
		variable_e1wbb12node = (Integer) routineReturns.get(1).getValue(); 		
		
		//generating output XML files
		try{
			output.generateDocument(false);
		}catch(Exception e){
			e.getStackTrace();
		}
	}
	
	public static boolean checkArgs(String[] args){
		List<FileInput> inputs = io.FileLoader.initInputFiles(args);
		List<DocumentOutput> outputs = io.FileLoader.initOutputFiles(args);
		
		int inputFilesSize = 1;
		int outputFilesSize = 1;
		
		if(inputs != null && outputs != null) {
			if (inputs.size() != inputFilesSize) {
				System.err.println("Number of input files does not match with files from XMapping code.");
				  	return false;
				  }
				  if (outputs.size() != outputFilesSize) {
				  	System.err.println("Number of output files does not match with files from XMapping code.");
				  	return false;
				  }
				  
			input = inputs.get(0);
			output = outputs.get(0);
			output.fillPath("source/masterData_Item.xsd");
			return true;
		}else{
			System.err.println("Error in input or output files.");
		}
		return false;
	}
	
	
}

