package my.company.example.statement.saptomasterdata;

import xpath.ReturnValue;
import io.DocumentOutput;
import my.company.example.statement.saptomasterdata.MapperE1WBB01_Structure;
import java.util.ArrayList;

/**
* This routine class was created: Mon Apr 26 13:06:22 CEST 2021
*/	
public class MacroFirstE1WBB12Node {

	/** Collection routineReturns to save other routines returns **/
	private static ArrayList<ReturnValue> routineReturns;
		
	public static ArrayList<ReturnValue> macro_firste1wbb12node(int input0) {
		DocumentOutput variable_jen = my.company.example.SAPToMasterData.output.getPartOfDocument("DASD");
		 		
		routineReturns = MapperE1WBB01_Structure.mapper_e1wbb01_structure(input0); 		
		ReturnValue return_0 = new ReturnValue<Integer>((Integer) routineReturns.get(0).getValue()); 		
		 		
		routineReturns = MapperE1WBB01_Structure.mapper_e1wbb01_structure(input0); 		
		ReturnValue return_1 = new ReturnValue<Integer>((Integer) routineReturns.get(0).getValue()); 		
		int variable_janek = 5;
		//returns
		ArrayList<ReturnValue> returns = new ArrayList<ReturnValue>();
		returns.add(return_0);
		return returns;
	}
	
}

