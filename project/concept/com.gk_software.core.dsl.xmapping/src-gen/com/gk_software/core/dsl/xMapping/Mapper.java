/**
 * generated by Xtext 2.24.0
 */
package com.gk_software.core.dsl.xMapping;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mapper</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.gk_software.core.dsl.xMapping.XMappingPackage#getMapper()
 * @model
 * @generated
 */
public interface Mapper extends Routine
{
} // Mapper
