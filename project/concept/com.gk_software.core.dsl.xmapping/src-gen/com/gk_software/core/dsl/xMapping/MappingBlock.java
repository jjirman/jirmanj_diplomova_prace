/**
 * generated by Xtext 2.24.0
 */
package com.gk_software.core.dsl.xMapping;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Mapping Block</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.gk_software.core.dsl.xMapping.XMappingPackage#getMappingBlock()
 * @model
 * @generated
 */
public interface MappingBlock extends EObject
{
} // MappingBlock
