package com.gk_software.core.dsl.parser.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.parser.antlr.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalXMappingLexer extends Lexer {
    public static final int RULE_VARIABLE_KEY=12;
    public static final int RULE_FUNCTION_KEY=8;
    public static final int RULE_OUTPUT_KEY=11;
    public static final int RULE_STRING=5;
    public static final int RULE_MACRO_KEY=6;
    public static final int RULE_SL_COMMENT=17;
    public static final int RULE_RETURN_KEY=14;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_MAPPER_KEY=9;
    public static final int RULE_ID=15;
    public static final int RULE_WS=18;
    public static final int RULE_FILTER_KEY=7;
    public static final int RULE_ANY_OTHER=19;
    public static final int RULE_INPUT_KEY=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=4;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=16;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int RULE_PARAM_KEY=13;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators

    public InternalXMappingLexer() {;} 
    public InternalXMappingLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalXMappingLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "InternalXMapping.g"; }

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:11:7: ( 'MappingClass' )
            // InternalXMapping.g:11:9: 'MappingClass'
            {
            match("MappingClass"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:12:7: ( ',' )
            // InternalXMapping.g:12:9: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:13:7: ( 'package' )
            // InternalXMapping.g:13:9: 'package'
            {
            match("package"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:14:7: ( ';' )
            // InternalXMapping.g:14:9: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:15:7: ( 'import' )
            // InternalXMapping.g:15:9: 'import'
            {
            match("import"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:16:7: ( 'map' )
            // InternalXMapping.g:16:9: 'map'
            {
            match("map"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:17:7: ( '->' )
            // InternalXMapping.g:17:9: '->'
            {
            match("->"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:18:7: ( '=' )
            // InternalXMapping.g:18:9: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:19:7: ( 'call' )
            // InternalXMapping.g:19:9: 'call'
            {
            match("call"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__28"

    // $ANTLR start "T__29"
    public final void mT__29() throws RecognitionException {
        try {
            int _type = T__29;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:20:7: ( 'new' )
            // InternalXMapping.g:20:9: 'new'
            {
            match("new"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__29"

    // $ANTLR start "T__30"
    public final void mT__30() throws RecognitionException {
        try {
            int _type = T__30;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:21:7: ( 'Null' )
            // InternalXMapping.g:21:9: 'Null'
            {
            match("Null"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__30"

    // $ANTLR start "T__31"
    public final void mT__31() throws RecognitionException {
        try {
            int _type = T__31;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:22:7: ( '.' )
            // InternalXMapping.g:22:9: '.'
            {
            match('.'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__31"

    // $ANTLR start "T__32"
    public final void mT__32() throws RecognitionException {
        try {
            int _type = T__32;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:23:7: ( ':' )
            // InternalXMapping.g:23:9: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__32"

    // $ANTLR start "T__33"
    public final void mT__33() throws RecognitionException {
        try {
            int _type = T__33;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:24:7: ( '-' )
            // InternalXMapping.g:24:9: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__33"

    // $ANTLR start "T__34"
    public final void mT__34() throws RecognitionException {
        try {
            int _type = T__34;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:25:7: ( '.*' )
            // InternalXMapping.g:25:9: '.*'
            {
            match(".*"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__34"

    // $ANTLR start "T__35"
    public final void mT__35() throws RecognitionException {
        try {
            int _type = T__35;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:26:7: ( 'def' )
            // InternalXMapping.g:26:9: 'def'
            {
            match("def"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__35"

    // $ANTLR start "RULE_VARIABLE_KEY"
    public final void mRULE_VARIABLE_KEY() throws RecognitionException {
        try {
            int _type = RULE_VARIABLE_KEY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:3143:19: ( 'Variable' )
            // InternalXMapping.g:3143:21: 'Variable'
            {
            match("Variable"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_VARIABLE_KEY"

    // $ANTLR start "RULE_MACRO_KEY"
    public final void mRULE_MACRO_KEY() throws RecognitionException {
        try {
            int _type = RULE_MACRO_KEY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:3145:16: ( 'Macro' )
            // InternalXMapping.g:3145:18: 'Macro'
            {
            match("Macro"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_MACRO_KEY"

    // $ANTLR start "RULE_FILTER_KEY"
    public final void mRULE_FILTER_KEY() throws RecognitionException {
        try {
            int _type = RULE_FILTER_KEY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:3147:17: ( 'Filter' )
            // InternalXMapping.g:3147:19: 'Filter'
            {
            match("Filter"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_FILTER_KEY"

    // $ANTLR start "RULE_FUNCTION_KEY"
    public final void mRULE_FUNCTION_KEY() throws RecognitionException {
        try {
            int _type = RULE_FUNCTION_KEY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:3149:19: ( 'Function' )
            // InternalXMapping.g:3149:21: 'Function'
            {
            match("Function"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_FUNCTION_KEY"

    // $ANTLR start "RULE_MAPPER_KEY"
    public final void mRULE_MAPPER_KEY() throws RecognitionException {
        try {
            int _type = RULE_MAPPER_KEY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:3151:17: ( 'Mapper' )
            // InternalXMapping.g:3151:19: 'Mapper'
            {
            match("Mapper"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_MAPPER_KEY"

    // $ANTLR start "RULE_INPUT_KEY"
    public final void mRULE_INPUT_KEY() throws RecognitionException {
        try {
            int _type = RULE_INPUT_KEY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:3153:16: ( 'Input' )
            // InternalXMapping.g:3153:18: 'Input'
            {
            match("Input"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INPUT_KEY"

    // $ANTLR start "RULE_OUTPUT_KEY"
    public final void mRULE_OUTPUT_KEY() throws RecognitionException {
        try {
            int _type = RULE_OUTPUT_KEY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:3155:17: ( 'Output' )
            // InternalXMapping.g:3155:19: 'Output'
            {
            match("Output"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_OUTPUT_KEY"

    // $ANTLR start "RULE_PARAM_KEY"
    public final void mRULE_PARAM_KEY() throws RecognitionException {
        try {
            int _type = RULE_PARAM_KEY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:3157:16: ( 'Param' )
            // InternalXMapping.g:3157:18: 'Param'
            {
            match("Param"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_PARAM_KEY"

    // $ANTLR start "RULE_RETURN_KEY"
    public final void mRULE_RETURN_KEY() throws RecognitionException {
        try {
            int _type = RULE_RETURN_KEY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:3159:17: ( 'Return' )
            // InternalXMapping.g:3159:19: 'Return'
            {
            match("Return"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_RETURN_KEY"

    // $ANTLR start "RULE_ID"
    public final void mRULE_ID() throws RecognitionException {
        try {
            int _type = RULE_ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:3161:9: ( ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )* )
            // InternalXMapping.g:3161:11: ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            {
            // InternalXMapping.g:3161:11: ( '^' )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0=='^') ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalXMapping.g:3161:11: '^'
                    {
                    match('^'); 

                    }
                    break;

            }

            if ( (input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // InternalXMapping.g:3161:40: ( 'a' .. 'z' | 'A' .. 'Z' | '_' | '0' .. '9' )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( ((LA2_0>='0' && LA2_0<='9')||(LA2_0>='A' && LA2_0<='Z')||LA2_0=='_'||(LA2_0>='a' && LA2_0<='z')) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalXMapping.g:
            	    {
            	    if ( (input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ID"

    // $ANTLR start "RULE_INT"
    public final void mRULE_INT() throws RecognitionException {
        try {
            int _type = RULE_INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:3163:10: ( ( '0' .. '9' )+ )
            // InternalXMapping.g:3163:12: ( '0' .. '9' )+
            {
            // InternalXMapping.g:3163:12: ( '0' .. '9' )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0>='0' && LA3_0<='9')) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalXMapping.g:3163:13: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INT"

    // $ANTLR start "RULE_STRING"
    public final void mRULE_STRING() throws RecognitionException {
        try {
            int _type = RULE_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:3165:13: ( ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' ) )
            // InternalXMapping.g:3165:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            {
            // InternalXMapping.g:3165:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"' | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\'' )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0=='\"') ) {
                alt6=1;
            }
            else if ( (LA6_0=='\'') ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalXMapping.g:3165:16: '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* '\"'
                    {
                    match('\"'); 
                    // InternalXMapping.g:3165:20: ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )*
                    loop4:
                    do {
                        int alt4=3;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0=='\\') ) {
                            alt4=1;
                        }
                        else if ( ((LA4_0>='\u0000' && LA4_0<='!')||(LA4_0>='#' && LA4_0<='[')||(LA4_0>=']' && LA4_0<='\uFFFF')) ) {
                            alt4=2;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // InternalXMapping.g:3165:21: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalXMapping.g:3165:28: ~ ( ( '\\\\' | '\"' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop4;
                        }
                    } while (true);

                    match('\"'); 

                    }
                    break;
                case 2 :
                    // InternalXMapping.g:3165:48: '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* '\\''
                    {
                    match('\''); 
                    // InternalXMapping.g:3165:53: ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )*
                    loop5:
                    do {
                        int alt5=3;
                        int LA5_0 = input.LA(1);

                        if ( (LA5_0=='\\') ) {
                            alt5=1;
                        }
                        else if ( ((LA5_0>='\u0000' && LA5_0<='&')||(LA5_0>='(' && LA5_0<='[')||(LA5_0>=']' && LA5_0<='\uFFFF')) ) {
                            alt5=2;
                        }


                        switch (alt5) {
                    	case 1 :
                    	    // InternalXMapping.g:3165:54: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalXMapping.g:3165:61: ~ ( ( '\\\\' | '\\'' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop5;
                        }
                    } while (true);

                    match('\''); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRING"

    // $ANTLR start "RULE_ML_COMMENT"
    public final void mRULE_ML_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:3167:17: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // InternalXMapping.g:3167:19: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 

            // InternalXMapping.g:3167:24: ( options {greedy=false; } : . )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0=='*') ) {
                    int LA7_1 = input.LA(2);

                    if ( (LA7_1=='/') ) {
                        alt7=2;
                    }
                    else if ( ((LA7_1>='\u0000' && LA7_1<='.')||(LA7_1>='0' && LA7_1<='\uFFFF')) ) {
                        alt7=1;
                    }


                }
                else if ( ((LA7_0>='\u0000' && LA7_0<=')')||(LA7_0>='+' && LA7_0<='\uFFFF')) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalXMapping.g:3167:52: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            match("*/"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ML_COMMENT"

    // $ANTLR start "RULE_SL_COMMENT"
    public final void mRULE_SL_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:3169:17: ( '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )? )
            // InternalXMapping.g:3169:19: '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )?
            {
            match("//"); 

            // InternalXMapping.g:3169:24: (~ ( ( '\\n' | '\\r' ) ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( ((LA8_0>='\u0000' && LA8_0<='\t')||(LA8_0>='\u000B' && LA8_0<='\f')||(LA8_0>='\u000E' && LA8_0<='\uFFFF')) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalXMapping.g:3169:24: ~ ( ( '\\n' | '\\r' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            // InternalXMapping.g:3169:40: ( ( '\\r' )? '\\n' )?
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0=='\n'||LA10_0=='\r') ) {
                alt10=1;
            }
            switch (alt10) {
                case 1 :
                    // InternalXMapping.g:3169:41: ( '\\r' )? '\\n'
                    {
                    // InternalXMapping.g:3169:41: ( '\\r' )?
                    int alt9=2;
                    int LA9_0 = input.LA(1);

                    if ( (LA9_0=='\r') ) {
                        alt9=1;
                    }
                    switch (alt9) {
                        case 1 :
                            // InternalXMapping.g:3169:41: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SL_COMMENT"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:3171:9: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
            // InternalXMapping.g:3171:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            {
            // InternalXMapping.g:3171:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            int cnt11=0;
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( ((LA11_0>='\t' && LA11_0<='\n')||LA11_0=='\r'||LA11_0==' ') ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalXMapping.g:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt11 >= 1 ) break loop11;
                        EarlyExitException eee =
                            new EarlyExitException(11, input);
                        throw eee;
                }
                cnt11++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_ANY_OTHER"
    public final void mRULE_ANY_OTHER() throws RecognitionException {
        try {
            int _type = RULE_ANY_OTHER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:3173:16: ( . )
            // InternalXMapping.g:3173:18: .
            {
            matchAny(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ANY_OTHER"

    public void mTokens() throws RecognitionException {
        // InternalXMapping.g:1:8: ( T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | RULE_VARIABLE_KEY | RULE_MACRO_KEY | RULE_FILTER_KEY | RULE_FUNCTION_KEY | RULE_MAPPER_KEY | RULE_INPUT_KEY | RULE_OUTPUT_KEY | RULE_PARAM_KEY | RULE_RETURN_KEY | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER )
        int alt12=32;
        alt12 = dfa12.predict(input);
        switch (alt12) {
            case 1 :
                // InternalXMapping.g:1:10: T__20
                {
                mT__20(); 

                }
                break;
            case 2 :
                // InternalXMapping.g:1:16: T__21
                {
                mT__21(); 

                }
                break;
            case 3 :
                // InternalXMapping.g:1:22: T__22
                {
                mT__22(); 

                }
                break;
            case 4 :
                // InternalXMapping.g:1:28: T__23
                {
                mT__23(); 

                }
                break;
            case 5 :
                // InternalXMapping.g:1:34: T__24
                {
                mT__24(); 

                }
                break;
            case 6 :
                // InternalXMapping.g:1:40: T__25
                {
                mT__25(); 

                }
                break;
            case 7 :
                // InternalXMapping.g:1:46: T__26
                {
                mT__26(); 

                }
                break;
            case 8 :
                // InternalXMapping.g:1:52: T__27
                {
                mT__27(); 

                }
                break;
            case 9 :
                // InternalXMapping.g:1:58: T__28
                {
                mT__28(); 

                }
                break;
            case 10 :
                // InternalXMapping.g:1:64: T__29
                {
                mT__29(); 

                }
                break;
            case 11 :
                // InternalXMapping.g:1:70: T__30
                {
                mT__30(); 

                }
                break;
            case 12 :
                // InternalXMapping.g:1:76: T__31
                {
                mT__31(); 

                }
                break;
            case 13 :
                // InternalXMapping.g:1:82: T__32
                {
                mT__32(); 

                }
                break;
            case 14 :
                // InternalXMapping.g:1:88: T__33
                {
                mT__33(); 

                }
                break;
            case 15 :
                // InternalXMapping.g:1:94: T__34
                {
                mT__34(); 

                }
                break;
            case 16 :
                // InternalXMapping.g:1:100: T__35
                {
                mT__35(); 

                }
                break;
            case 17 :
                // InternalXMapping.g:1:106: RULE_VARIABLE_KEY
                {
                mRULE_VARIABLE_KEY(); 

                }
                break;
            case 18 :
                // InternalXMapping.g:1:124: RULE_MACRO_KEY
                {
                mRULE_MACRO_KEY(); 

                }
                break;
            case 19 :
                // InternalXMapping.g:1:139: RULE_FILTER_KEY
                {
                mRULE_FILTER_KEY(); 

                }
                break;
            case 20 :
                // InternalXMapping.g:1:155: RULE_FUNCTION_KEY
                {
                mRULE_FUNCTION_KEY(); 

                }
                break;
            case 21 :
                // InternalXMapping.g:1:173: RULE_MAPPER_KEY
                {
                mRULE_MAPPER_KEY(); 

                }
                break;
            case 22 :
                // InternalXMapping.g:1:189: RULE_INPUT_KEY
                {
                mRULE_INPUT_KEY(); 

                }
                break;
            case 23 :
                // InternalXMapping.g:1:204: RULE_OUTPUT_KEY
                {
                mRULE_OUTPUT_KEY(); 

                }
                break;
            case 24 :
                // InternalXMapping.g:1:220: RULE_PARAM_KEY
                {
                mRULE_PARAM_KEY(); 

                }
                break;
            case 25 :
                // InternalXMapping.g:1:235: RULE_RETURN_KEY
                {
                mRULE_RETURN_KEY(); 

                }
                break;
            case 26 :
                // InternalXMapping.g:1:251: RULE_ID
                {
                mRULE_ID(); 

                }
                break;
            case 27 :
                // InternalXMapping.g:1:259: RULE_INT
                {
                mRULE_INT(); 

                }
                break;
            case 28 :
                // InternalXMapping.g:1:268: RULE_STRING
                {
                mRULE_STRING(); 

                }
                break;
            case 29 :
                // InternalXMapping.g:1:280: RULE_ML_COMMENT
                {
                mRULE_ML_COMMENT(); 

                }
                break;
            case 30 :
                // InternalXMapping.g:1:296: RULE_SL_COMMENT
                {
                mRULE_SL_COMMENT(); 

                }
                break;
            case 31 :
                // InternalXMapping.g:1:312: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 32 :
                // InternalXMapping.g:1:320: RULE_ANY_OTHER
                {
                mRULE_ANY_OTHER(); 

                }
                break;

        }

    }


    protected DFA12 dfa12 = new DFA12(this);
    static final String DFA12_eotS =
        "\1\uffff\1\36\1\uffff\1\36\1\uffff\2\36\1\45\1\uffff\3\36\1\53\1\uffff\7\36\1\34\2\uffff\3\34\2\uffff\1\36\2\uffff\1\36\1\uffff\2\36\3\uffff\3\36\3\uffff\10\36\5\uffff\4\36\1\116\1\36\1\120\1\36\1\122\13\36\1\uffff\1\137\1\uffff\1\140\1\uffff\11\36\1\152\2\36\2\uffff\3\36\1\160\1\36\1\162\2\36\1\165\1\uffff\1\36\1\167\1\36\1\171\1\36\1\uffff\1\173\1\uffff\1\174\1\36\1\uffff\1\176\1\uffff\1\36\1\uffff\1\36\2\uffff\1\36\1\uffff\1\u0082\1\u0083\1\36\2\uffff\2\36\1\u0087\1\uffff";
    static final String DFA12_eofS =
        "\u0088\uffff";
    static final String DFA12_minS =
        "\1\0\1\141\1\uffff\1\141\1\uffff\1\155\1\141\1\76\1\uffff\1\141\1\145\1\165\1\52\1\uffff\1\145\1\141\1\151\1\156\1\165\1\141\1\145\1\101\2\uffff\2\0\1\52\2\uffff\1\143\2\uffff\1\143\1\uffff\2\160\3\uffff\1\154\1\167\1\154\3\uffff\1\146\1\162\1\154\1\156\1\160\1\164\1\162\1\164\5\uffff\1\160\1\162\1\153\1\157\1\60\1\154\1\60\1\154\1\60\1\151\1\164\1\143\1\165\1\160\1\141\1\165\1\145\1\157\1\141\1\162\1\uffff\1\60\1\uffff\1\60\1\uffff\1\141\1\145\2\164\1\165\1\155\1\162\1\156\1\162\1\60\1\147\1\164\2\uffff\1\142\1\162\1\151\1\60\1\164\1\60\1\156\1\147\1\60\1\uffff\1\145\1\60\1\154\1\60\1\157\1\uffff\1\60\1\uffff\1\60\1\103\1\uffff\1\60\1\uffff\1\145\1\uffff\1\156\2\uffff\1\154\1\uffff\2\60\1\141\2\uffff\2\163\1\60\1\uffff";
    static final String DFA12_maxS =
        "\1\uffff\1\141\1\uffff\1\141\1\uffff\1\155\1\141\1\76\1\uffff\1\141\1\145\1\165\1\52\1\uffff\1\145\1\141\1\165\1\156\1\165\1\141\1\145\1\172\2\uffff\2\uffff\1\57\2\uffff\1\160\2\uffff\1\143\1\uffff\2\160\3\uffff\1\154\1\167\1\154\3\uffff\1\146\1\162\1\154\1\156\1\160\1\164\1\162\1\164\5\uffff\1\160\1\162\1\153\1\157\1\172\1\154\1\172\1\154\1\172\1\151\1\164\1\143\1\165\1\160\1\141\1\165\1\151\1\157\1\141\1\162\1\uffff\1\172\1\uffff\1\172\1\uffff\1\141\1\145\2\164\1\165\1\155\1\162\1\156\1\162\1\172\1\147\1\164\2\uffff\1\142\1\162\1\151\1\172\1\164\1\172\1\156\1\147\1\172\1\uffff\1\145\1\172\1\154\1\172\1\157\1\uffff\1\172\1\uffff\1\172\1\103\1\uffff\1\172\1\uffff\1\145\1\uffff\1\156\2\uffff\1\154\1\uffff\2\172\1\141\2\uffff\2\163\1\172\1\uffff";
    static final String DFA12_acceptS =
        "\2\uffff\1\2\1\uffff\1\4\3\uffff\1\10\4\uffff\1\15\10\uffff\1\32\1\33\3\uffff\1\37\1\40\1\uffff\1\32\1\2\1\uffff\1\4\2\uffff\1\7\1\16\1\10\3\uffff\1\17\1\14\1\15\10\uffff\1\33\1\34\1\35\1\36\1\37\24\uffff\1\6\1\uffff\1\12\1\uffff\1\20\14\uffff\1\11\1\13\11\uffff\1\22\5\uffff\1\26\1\uffff\1\30\2\uffff\1\25\1\uffff\1\5\1\uffff\1\23\1\uffff\1\27\1\31\1\uffff\1\3\3\uffff\1\21\1\24\3\uffff\1\1";
    static final String DFA12_specialS =
        "\1\0\27\uffff\1\1\1\2\156\uffff}>";
    static final String[] DFA12_transitionS = {
            "\11\34\2\33\2\34\1\33\22\34\1\33\1\34\1\30\4\34\1\31\4\34\1\2\1\7\1\14\1\32\12\27\1\15\1\4\1\34\1\10\3\34\5\26\1\20\2\26\1\21\3\26\1\1\1\13\1\22\1\23\1\26\1\24\3\26\1\17\4\26\3\34\1\25\1\26\1\34\2\26\1\11\1\16\4\26\1\5\3\26\1\6\1\12\1\26\1\3\12\26\uff85\34",
            "\1\35",
            "",
            "\1\40",
            "",
            "\1\42",
            "\1\43",
            "\1\44",
            "",
            "\1\47",
            "\1\50",
            "\1\51",
            "\1\52",
            "",
            "\1\55",
            "\1\56",
            "\1\57\13\uffff\1\60",
            "\1\61",
            "\1\62",
            "\1\63",
            "\1\64",
            "\32\36\4\uffff\1\36\1\uffff\32\36",
            "",
            "",
            "\0\66",
            "\0\66",
            "\1\67\4\uffff\1\70",
            "",
            "",
            "\1\73\14\uffff\1\72",
            "",
            "",
            "\1\74",
            "",
            "\1\75",
            "\1\76",
            "",
            "",
            "",
            "\1\77",
            "\1\100",
            "\1\101",
            "",
            "",
            "",
            "\1\102",
            "\1\103",
            "\1\104",
            "\1\105",
            "\1\106",
            "\1\107",
            "\1\110",
            "\1\111",
            "",
            "",
            "",
            "",
            "",
            "\1\112",
            "\1\113",
            "\1\114",
            "\1\115",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\117",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\121",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\123",
            "\1\124",
            "\1\125",
            "\1\126",
            "\1\127",
            "\1\130",
            "\1\131",
            "\1\133\3\uffff\1\132",
            "\1\134",
            "\1\135",
            "\1\136",
            "",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "",
            "\1\141",
            "\1\142",
            "\1\143",
            "\1\144",
            "\1\145",
            "\1\146",
            "\1\147",
            "\1\150",
            "\1\151",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\153",
            "\1\154",
            "",
            "",
            "\1\155",
            "\1\156",
            "\1\157",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\161",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\163",
            "\1\164",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "",
            "\1\166",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\170",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\172",
            "",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\175",
            "",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "",
            "\1\177",
            "",
            "\1\u0080",
            "",
            "",
            "\1\u0081",
            "",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            "\1\u0084",
            "",
            "",
            "\1\u0085",
            "\1\u0086",
            "\12\36\7\uffff\32\36\4\uffff\1\36\1\uffff\32\36",
            ""
    };

    static final short[] DFA12_eot = DFA.unpackEncodedString(DFA12_eotS);
    static final short[] DFA12_eof = DFA.unpackEncodedString(DFA12_eofS);
    static final char[] DFA12_min = DFA.unpackEncodedStringToUnsignedChars(DFA12_minS);
    static final char[] DFA12_max = DFA.unpackEncodedStringToUnsignedChars(DFA12_maxS);
    static final short[] DFA12_accept = DFA.unpackEncodedString(DFA12_acceptS);
    static final short[] DFA12_special = DFA.unpackEncodedString(DFA12_specialS);
    static final short[][] DFA12_transition;

    static {
        int numStates = DFA12_transitionS.length;
        DFA12_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA12_transition[i] = DFA.unpackEncodedString(DFA12_transitionS[i]);
        }
    }

    class DFA12 extends DFA {

        public DFA12(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 12;
            this.eot = DFA12_eot;
            this.eof = DFA12_eof;
            this.min = DFA12_min;
            this.max = DFA12_max;
            this.accept = DFA12_accept;
            this.special = DFA12_special;
            this.transition = DFA12_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | RULE_VARIABLE_KEY | RULE_MACRO_KEY | RULE_FILTER_KEY | RULE_FUNCTION_KEY | RULE_MAPPER_KEY | RULE_INPUT_KEY | RULE_OUTPUT_KEY | RULE_PARAM_KEY | RULE_RETURN_KEY | RULE_ID | RULE_INT | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA12_0 = input.LA(1);

                        s = -1;
                        if ( (LA12_0=='M') ) {s = 1;}

                        else if ( (LA12_0==',') ) {s = 2;}

                        else if ( (LA12_0=='p') ) {s = 3;}

                        else if ( (LA12_0==';') ) {s = 4;}

                        else if ( (LA12_0=='i') ) {s = 5;}

                        else if ( (LA12_0=='m') ) {s = 6;}

                        else if ( (LA12_0=='-') ) {s = 7;}

                        else if ( (LA12_0=='=') ) {s = 8;}

                        else if ( (LA12_0=='c') ) {s = 9;}

                        else if ( (LA12_0=='n') ) {s = 10;}

                        else if ( (LA12_0=='N') ) {s = 11;}

                        else if ( (LA12_0=='.') ) {s = 12;}

                        else if ( (LA12_0==':') ) {s = 13;}

                        else if ( (LA12_0=='d') ) {s = 14;}

                        else if ( (LA12_0=='V') ) {s = 15;}

                        else if ( (LA12_0=='F') ) {s = 16;}

                        else if ( (LA12_0=='I') ) {s = 17;}

                        else if ( (LA12_0=='O') ) {s = 18;}

                        else if ( (LA12_0=='P') ) {s = 19;}

                        else if ( (LA12_0=='R') ) {s = 20;}

                        else if ( (LA12_0=='^') ) {s = 21;}

                        else if ( ((LA12_0>='A' && LA12_0<='E')||(LA12_0>='G' && LA12_0<='H')||(LA12_0>='J' && LA12_0<='L')||LA12_0=='Q'||(LA12_0>='S' && LA12_0<='U')||(LA12_0>='W' && LA12_0<='Z')||LA12_0=='_'||(LA12_0>='a' && LA12_0<='b')||(LA12_0>='e' && LA12_0<='h')||(LA12_0>='j' && LA12_0<='l')||LA12_0=='o'||(LA12_0>='q' && LA12_0<='z')) ) {s = 22;}

                        else if ( ((LA12_0>='0' && LA12_0<='9')) ) {s = 23;}

                        else if ( (LA12_0=='\"') ) {s = 24;}

                        else if ( (LA12_0=='\'') ) {s = 25;}

                        else if ( (LA12_0=='/') ) {s = 26;}

                        else if ( ((LA12_0>='\t' && LA12_0<='\n')||LA12_0=='\r'||LA12_0==' ') ) {s = 27;}

                        else if ( ((LA12_0>='\u0000' && LA12_0<='\b')||(LA12_0>='\u000B' && LA12_0<='\f')||(LA12_0>='\u000E' && LA12_0<='\u001F')||LA12_0=='!'||(LA12_0>='#' && LA12_0<='&')||(LA12_0>='(' && LA12_0<='+')||LA12_0=='<'||(LA12_0>='>' && LA12_0<='@')||(LA12_0>='[' && LA12_0<=']')||LA12_0=='`'||(LA12_0>='{' && LA12_0<='\uFFFF')) ) {s = 28;}

                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA12_24 = input.LA(1);

                        s = -1;
                        if ( ((LA12_24>='\u0000' && LA12_24<='\uFFFF')) ) {s = 54;}

                        else s = 28;

                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA12_25 = input.LA(1);

                        s = -1;
                        if ( ((LA12_25>='\u0000' && LA12_25<='\uFFFF')) ) {s = 54;}

                        else s = 28;

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 12, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}