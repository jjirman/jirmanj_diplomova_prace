/**
 * generated by Xtext 2.24.0
 */
package com.gk_software.core.dsl.xMapping;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Output Section</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.gk_software.core.dsl.xMapping.OutputSection#getOutputAttributes <em>Output Attributes</em>}</li>
 *   <li>{@link com.gk_software.core.dsl.xMapping.OutputSection#getAttributes <em>Attributes</em>}</li>
 * </ul>
 *
 * @see com.gk_software.core.dsl.xMapping.XMappingPackage#getOutputSection()
 * @model
 * @generated
 */
public interface OutputSection extends EObject
{
  /**
   * Returns the value of the '<em><b>Output Attributes</b></em>' containment reference list.
   * The list contents are of type {@link com.gk_software.core.dsl.xMapping.OutputAttribute}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Output Attributes</em>' containment reference list.
   * @see com.gk_software.core.dsl.xMapping.XMappingPackage#getOutputSection_OutputAttributes()
   * @model containment="true"
   * @generated
   */
  EList<OutputAttribute> getOutputAttributes();

  /**
   * Returns the value of the '<em><b>Attributes</b></em>' containment reference list.
   * The list contents are of type {@link com.gk_software.core.dsl.xMapping.Attribute}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Attributes</em>' containment reference list.
   * @see com.gk_software.core.dsl.xMapping.XMappingPackage#getOutputSection_Attributes()
   * @model containment="true"
   * @generated
   */
  EList<Attribute> getAttributes();

} // OutputSection
