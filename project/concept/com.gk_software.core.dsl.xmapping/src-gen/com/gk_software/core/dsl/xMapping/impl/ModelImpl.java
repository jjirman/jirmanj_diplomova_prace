/**
 * generated by Xtext 2.24.0
 */
package com.gk_software.core.dsl.xMapping.impl;

import com.gk_software.core.dsl.xMapping.MappingClassBlock;
import com.gk_software.core.dsl.xMapping.Model;
import com.gk_software.core.dsl.xMapping.NamespaceImport;
import com.gk_software.core.dsl.xMapping.XMappingPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.gk_software.core.dsl.xMapping.impl.ModelImpl#getPackageName <em>Package Name</em>}</li>
 *   <li>{@link com.gk_software.core.dsl.xMapping.impl.ModelImpl#getNamespaceImports <em>Namespace Imports</em>}</li>
 *   <li>{@link com.gk_software.core.dsl.xMapping.impl.ModelImpl#getMappingClass <em>Mapping Class</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ModelImpl extends MinimalEObjectImpl.Container implements Model
{
  /**
   * The cached value of the '{@link #getPackageName() <em>Package Name</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPackageName()
   * @generated
   * @ordered
   */
  protected com.gk_software.core.dsl.xMapping.Package packageName;

  /**
   * The cached value of the '{@link #getNamespaceImports() <em>Namespace Imports</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNamespaceImports()
   * @generated
   * @ordered
   */
  protected EList<NamespaceImport> namespaceImports;

  /**
   * The cached value of the '{@link #getMappingClass() <em>Mapping Class</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMappingClass()
   * @generated
   * @ordered
   */
  protected MappingClassBlock mappingClass;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ModelImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return XMappingPackage.Literals.MODEL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public com.gk_software.core.dsl.xMapping.Package getPackageName()
  {
    return packageName;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetPackageName(com.gk_software.core.dsl.xMapping.Package newPackageName, NotificationChain msgs)
  {
    com.gk_software.core.dsl.xMapping.Package oldPackageName = packageName;
    packageName = newPackageName;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, XMappingPackage.MODEL__PACKAGE_NAME, oldPackageName, newPackageName);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setPackageName(com.gk_software.core.dsl.xMapping.Package newPackageName)
  {
    if (newPackageName != packageName)
    {
      NotificationChain msgs = null;
      if (packageName != null)
        msgs = ((InternalEObject)packageName).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - XMappingPackage.MODEL__PACKAGE_NAME, null, msgs);
      if (newPackageName != null)
        msgs = ((InternalEObject)newPackageName).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - XMappingPackage.MODEL__PACKAGE_NAME, null, msgs);
      msgs = basicSetPackageName(newPackageName, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, XMappingPackage.MODEL__PACKAGE_NAME, newPackageName, newPackageName));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<NamespaceImport> getNamespaceImports()
  {
    if (namespaceImports == null)
    {
      namespaceImports = new EObjectContainmentEList<NamespaceImport>(NamespaceImport.class, this, XMappingPackage.MODEL__NAMESPACE_IMPORTS);
    }
    return namespaceImports;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public MappingClassBlock getMappingClass()
  {
    return mappingClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetMappingClass(MappingClassBlock newMappingClass, NotificationChain msgs)
  {
    MappingClassBlock oldMappingClass = mappingClass;
    mappingClass = newMappingClass;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, XMappingPackage.MODEL__MAPPING_CLASS, oldMappingClass, newMappingClass);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setMappingClass(MappingClassBlock newMappingClass)
  {
    if (newMappingClass != mappingClass)
    {
      NotificationChain msgs = null;
      if (mappingClass != null)
        msgs = ((InternalEObject)mappingClass).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - XMappingPackage.MODEL__MAPPING_CLASS, null, msgs);
      if (newMappingClass != null)
        msgs = ((InternalEObject)newMappingClass).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - XMappingPackage.MODEL__MAPPING_CLASS, null, msgs);
      msgs = basicSetMappingClass(newMappingClass, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, XMappingPackage.MODEL__MAPPING_CLASS, newMappingClass, newMappingClass));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case XMappingPackage.MODEL__PACKAGE_NAME:
        return basicSetPackageName(null, msgs);
      case XMappingPackage.MODEL__NAMESPACE_IMPORTS:
        return ((InternalEList<?>)getNamespaceImports()).basicRemove(otherEnd, msgs);
      case XMappingPackage.MODEL__MAPPING_CLASS:
        return basicSetMappingClass(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case XMappingPackage.MODEL__PACKAGE_NAME:
        return getPackageName();
      case XMappingPackage.MODEL__NAMESPACE_IMPORTS:
        return getNamespaceImports();
      case XMappingPackage.MODEL__MAPPING_CLASS:
        return getMappingClass();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case XMappingPackage.MODEL__PACKAGE_NAME:
        setPackageName((com.gk_software.core.dsl.xMapping.Package)newValue);
        return;
      case XMappingPackage.MODEL__NAMESPACE_IMPORTS:
        getNamespaceImports().clear();
        getNamespaceImports().addAll((Collection<? extends NamespaceImport>)newValue);
        return;
      case XMappingPackage.MODEL__MAPPING_CLASS:
        setMappingClass((MappingClassBlock)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case XMappingPackage.MODEL__PACKAGE_NAME:
        setPackageName((com.gk_software.core.dsl.xMapping.Package)null);
        return;
      case XMappingPackage.MODEL__NAMESPACE_IMPORTS:
        getNamespaceImports().clear();
        return;
      case XMappingPackage.MODEL__MAPPING_CLASS:
        setMappingClass((MappingClassBlock)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case XMappingPackage.MODEL__PACKAGE_NAME:
        return packageName != null;
      case XMappingPackage.MODEL__NAMESPACE_IMPORTS:
        return namespaceImports != null && !namespaceImports.isEmpty();
      case XMappingPackage.MODEL__MAPPING_CLASS:
        return mappingClass != null;
    }
    return super.eIsSet(featureID);
  }

} //ModelImpl
