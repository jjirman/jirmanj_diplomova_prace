/**
 * generated by Xtext 2.24.0
 */
package com.gk_software.core.dsl.xMapping.impl;

import com.gk_software.core.dsl.xMapping.Assignment;
import com.gk_software.core.dsl.xMapping.AssignmentNew;
import com.gk_software.core.dsl.xMapping.AssignmentReference;
import com.gk_software.core.dsl.xMapping.AssignmentSection;
import com.gk_software.core.dsl.xMapping.AssignmentSource;
import com.gk_software.core.dsl.xMapping.AssignmentStatement;
import com.gk_software.core.dsl.xMapping.AssignmentStructure;
import com.gk_software.core.dsl.xMapping.Attribute;
import com.gk_software.core.dsl.xMapping.Block;
import com.gk_software.core.dsl.xMapping.CallStatement;
import com.gk_software.core.dsl.xMapping.Declaration;
import com.gk_software.core.dsl.xMapping.Filter;
import com.gk_software.core.dsl.xMapping.Function;
import com.gk_software.core.dsl.xMapping.Initialization;
import com.gk_software.core.dsl.xMapping.Input;
import com.gk_software.core.dsl.xMapping.InputAttribute;
import com.gk_software.core.dsl.xMapping.InputSection;
import com.gk_software.core.dsl.xMapping.InputXsd;
import com.gk_software.core.dsl.xMapping.Macro;
import com.gk_software.core.dsl.xMapping.MapForwardStatement;
import com.gk_software.core.dsl.xMapping.MapStatement;
import com.gk_software.core.dsl.xMapping.Mapper;
import com.gk_software.core.dsl.xMapping.MappingBlock;
import com.gk_software.core.dsl.xMapping.MappingClassBlock;
import com.gk_software.core.dsl.xMapping.Model;
import com.gk_software.core.dsl.xMapping.NamespaceImport;
import com.gk_software.core.dsl.xMapping.Output;
import com.gk_software.core.dsl.xMapping.OutputAttribute;
import com.gk_software.core.dsl.xMapping.OutputSection;
import com.gk_software.core.dsl.xMapping.OutputXsd;
import com.gk_software.core.dsl.xMapping.Param;
import com.gk_software.core.dsl.xMapping.ParamSection;
import com.gk_software.core.dsl.xMapping.Return;
import com.gk_software.core.dsl.xMapping.Routine;
import com.gk_software.core.dsl.xMapping.RoutineBlock;
import com.gk_software.core.dsl.xMapping.RoutineSection;
import com.gk_software.core.dsl.xMapping.RoutineSource;
import com.gk_software.core.dsl.xMapping.Statement;
import com.gk_software.core.dsl.xMapping.Variable;
import com.gk_software.core.dsl.xMapping.XMappingFactory;
import com.gk_software.core.dsl.xMapping.XMappingPackage;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class XMappingFactoryImpl extends EFactoryImpl implements XMappingFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static XMappingFactory init()
  {
    try
    {
      XMappingFactory theXMappingFactory = (XMappingFactory)EPackage.Registry.INSTANCE.getEFactory(XMappingPackage.eNS_URI);
      if (theXMappingFactory != null)
      {
        return theXMappingFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new XMappingFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public XMappingFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case XMappingPackage.MODEL: return createModel();
      case XMappingPackage.MAPPING_CLASS_BLOCK: return createMappingClassBlock();
      case XMappingPackage.ROUTINE_BLOCK: return createRoutineBlock();
      case XMappingPackage.PARAM_SECTION: return createParamSection();
      case XMappingPackage.MAPPING_BLOCK: return createMappingBlock();
      case XMappingPackage.BLOCK: return createBlock();
      case XMappingPackage.PACKAGE: return createPackage();
      case XMappingPackage.NAMESPACE_IMPORT: return createNamespaceImport();
      case XMappingPackage.INPUT_XSD: return createInputXsd();
      case XMappingPackage.OUTPUT_XSD: return createOutputXsd();
      case XMappingPackage.STATEMENT: return createStatement();
      case XMappingPackage.MAP_STATEMENT: return createMapStatement();
      case XMappingPackage.MAP_FORWARD_STATEMENT: return createMapForwardStatement();
      case XMappingPackage.ASSIGNMENT_STATEMENT: return createAssignmentStatement();
      case XMappingPackage.DECLARATION: return createDeclaration();
      case XMappingPackage.INITIALIZATION: return createInitialization();
      case XMappingPackage.CALL_STATEMENT: return createCallStatement();
      case XMappingPackage.ASSIGNMENT: return createAssignment();
      case XMappingPackage.ASSIGNMENT_STRUCTURE: return createAssignmentStructure();
      case XMappingPackage.ASSIGNMENT_SECTION: return createAssignmentSection();
      case XMappingPackage.ASSIGNMENT_SOURCE: return createAssignmentSource();
      case XMappingPackage.ASSIGNMENT_NEW: return createAssignmentNew();
      case XMappingPackage.ASSIGNMENT_REFERENCE: return createAssignmentReference();
      case XMappingPackage.INPUT_SECTION: return createInputSection();
      case XMappingPackage.ROUTINE_SECTION: return createRoutineSection();
      case XMappingPackage.OUTPUT_SECTION: return createOutputSection();
      case XMappingPackage.ROUTINE: return createRoutine();
      case XMappingPackage.ROUTINE_SOURCE: return createRoutineSource();
      case XMappingPackage.INPUT_ATTRIBUTE: return createInputAttribute();
      case XMappingPackage.OUTPUT_ATTRIBUTE: return createOutputAttribute();
      case XMappingPackage.ATTRIBUTE: return createAttribute();
      case XMappingPackage.INPUT: return createInput();
      case XMappingPackage.OUTPUT: return createOutput();
      case XMappingPackage.VARIABLE: return createVariable();
      case XMappingPackage.PARAM: return createParam();
      case XMappingPackage.RETURN: return createReturn();
      case XMappingPackage.MACRO: return createMacro();
      case XMappingPackage.FILTER: return createFilter();
      case XMappingPackage.FUNCTION: return createFunction();
      case XMappingPackage.MAPPER: return createMapper();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Model createModel()
  {
    ModelImpl model = new ModelImpl();
    return model;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public MappingClassBlock createMappingClassBlock()
  {
    MappingClassBlockImpl mappingClassBlock = new MappingClassBlockImpl();
    return mappingClassBlock;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public RoutineBlock createRoutineBlock()
  {
    RoutineBlockImpl routineBlock = new RoutineBlockImpl();
    return routineBlock;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public ParamSection createParamSection()
  {
    ParamSectionImpl paramSection = new ParamSectionImpl();
    return paramSection;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public MappingBlock createMappingBlock()
  {
    MappingBlockImpl mappingBlock = new MappingBlockImpl();
    return mappingBlock;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Block createBlock()
  {
    BlockImpl block = new BlockImpl();
    return block;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public com.gk_software.core.dsl.xMapping.Package createPackage()
  {
    PackageImpl package_ = new PackageImpl();
    return package_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NamespaceImport createNamespaceImport()
  {
    NamespaceImportImpl namespaceImport = new NamespaceImportImpl();
    return namespaceImport;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public InputXsd createInputXsd()
  {
    InputXsdImpl inputXsd = new InputXsdImpl();
    return inputXsd;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OutputXsd createOutputXsd()
  {
    OutputXsdImpl outputXsd = new OutputXsdImpl();
    return outputXsd;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Statement createStatement()
  {
    StatementImpl statement = new StatementImpl();
    return statement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public MapStatement createMapStatement()
  {
    MapStatementImpl mapStatement = new MapStatementImpl();
    return mapStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public MapForwardStatement createMapForwardStatement()
  {
    MapForwardStatementImpl mapForwardStatement = new MapForwardStatementImpl();
    return mapForwardStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public AssignmentStatement createAssignmentStatement()
  {
    AssignmentStatementImpl assignmentStatement = new AssignmentStatementImpl();
    return assignmentStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Declaration createDeclaration()
  {
    DeclarationImpl declaration = new DeclarationImpl();
    return declaration;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Initialization createInitialization()
  {
    InitializationImpl initialization = new InitializationImpl();
    return initialization;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public CallStatement createCallStatement()
  {
    CallStatementImpl callStatement = new CallStatementImpl();
    return callStatement;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Assignment createAssignment()
  {
    AssignmentImpl assignment = new AssignmentImpl();
    return assignment;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public AssignmentStructure createAssignmentStructure()
  {
    AssignmentStructureImpl assignmentStructure = new AssignmentStructureImpl();
    return assignmentStructure;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public AssignmentSection createAssignmentSection()
  {
    AssignmentSectionImpl assignmentSection = new AssignmentSectionImpl();
    return assignmentSection;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public AssignmentSource createAssignmentSource()
  {
    AssignmentSourceImpl assignmentSource = new AssignmentSourceImpl();
    return assignmentSource;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public AssignmentNew createAssignmentNew()
  {
    AssignmentNewImpl assignmentNew = new AssignmentNewImpl();
    return assignmentNew;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public AssignmentReference createAssignmentReference()
  {
    AssignmentReferenceImpl assignmentReference = new AssignmentReferenceImpl();
    return assignmentReference;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public InputSection createInputSection()
  {
    InputSectionImpl inputSection = new InputSectionImpl();
    return inputSection;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public RoutineSection createRoutineSection()
  {
    RoutineSectionImpl routineSection = new RoutineSectionImpl();
    return routineSection;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OutputSection createOutputSection()
  {
    OutputSectionImpl outputSection = new OutputSectionImpl();
    return outputSection;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Routine createRoutine()
  {
    RoutineImpl routine = new RoutineImpl();
    return routine;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public RoutineSource createRoutineSource()
  {
    RoutineSourceImpl routineSource = new RoutineSourceImpl();
    return routineSource;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public InputAttribute createInputAttribute()
  {
    InputAttributeImpl inputAttribute = new InputAttributeImpl();
    return inputAttribute;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OutputAttribute createOutputAttribute()
  {
    OutputAttributeImpl outputAttribute = new OutputAttributeImpl();
    return outputAttribute;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Attribute createAttribute()
  {
    AttributeImpl attribute = new AttributeImpl();
    return attribute;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Input createInput()
  {
    InputImpl input = new InputImpl();
    return input;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Output createOutput()
  {
    OutputImpl output = new OutputImpl();
    return output;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Variable createVariable()
  {
    VariableImpl variable = new VariableImpl();
    return variable;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Param createParam()
  {
    ParamImpl param = new ParamImpl();
    return param;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Return createReturn()
  {
    ReturnImpl return_ = new ReturnImpl();
    return return_;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Macro createMacro()
  {
    MacroImpl macro = new MacroImpl();
    return macro;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Filter createFilter()
  {
    FilterImpl filter = new FilterImpl();
    return filter;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Function createFunction()
  {
    FunctionImpl function = new FunctionImpl();
    return function;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Mapper createMapper()
  {
    MapperImpl mapper = new MapperImpl();
    return mapper;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public XMappingPackage getXMappingPackage()
  {
    return (XMappingPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static XMappingPackage getPackage()
  {
    return XMappingPackage.eINSTANCE;
  }

} //XMappingFactoryImpl
