/*
 * generated by Xtext 2.24.0
 */
package com.gk_software.core.dsl.parser.antlr;

import java.io.InputStream;
import org.eclipse.xtext.parser.antlr.IAntlrTokenFileProvider;

public class XMappingAntlrTokenFileProvider implements IAntlrTokenFileProvider {

	@Override
	public InputStream getAntlrTokenFile() {
		ClassLoader classLoader = getClass().getClassLoader();
		return classLoader.getResourceAsStream("com/gk_software/core/dsl/parser/antlr/internal/InternalXMapping.tokens");
	}
}
