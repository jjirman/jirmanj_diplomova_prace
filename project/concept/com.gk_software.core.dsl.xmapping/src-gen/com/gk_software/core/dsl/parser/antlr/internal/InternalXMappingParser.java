package com.gk_software.core.dsl.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import com.gk_software.core.dsl.services.XMappingGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalXMappingParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_INT", "RULE_STRING", "RULE_MACRO_KEY", "RULE_FILTER_KEY", "RULE_FUNCTION_KEY", "RULE_MAPPER_KEY", "RULE_INPUT_KEY", "RULE_OUTPUT_KEY", "RULE_VARIABLE_KEY", "RULE_PARAM_KEY", "RULE_RETURN_KEY", "RULE_ID", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'MappingClass'", "','", "'package'", "';'", "'import'", "'map'", "'->'", "'='", "'call'", "'new'", "'Null'", "'.'", "':'", "'-'", "'.*'", "'def'"
    };
    public static final int RULE_VARIABLE_KEY=12;
    public static final int RULE_FUNCTION_KEY=8;
    public static final int RULE_OUTPUT_KEY=11;
    public static final int RULE_STRING=5;
    public static final int RULE_MACRO_KEY=6;
    public static final int RULE_SL_COMMENT=17;
    public static final int RULE_RETURN_KEY=14;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_MAPPER_KEY=9;
    public static final int RULE_ID=15;
    public static final int RULE_WS=18;
    public static final int RULE_FILTER_KEY=7;
    public static final int RULE_ANY_OTHER=19;
    public static final int RULE_INPUT_KEY=10;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=4;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=16;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int RULE_PARAM_KEY=13;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalXMappingParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalXMappingParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalXMappingParser.tokenNames; }
    public String getGrammarFileName() { return "InternalXMapping.g"; }



    /*
      This grammar contains a lot of empty actions to work around a bug in ANTLR.
      Otherwise the ANTLR tool will create synpreds that cannot be compiled in some rare cases.
    */

     	private XMappingGrammarAccess grammarAccess;

        public InternalXMappingParser(TokenStream input, XMappingGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Model";
       	}

       	@Override
       	protected XMappingGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleModel"
    // InternalXMapping.g:70:1: entryRuleModel returns [EObject current=null] : iv_ruleModel= ruleModel EOF ;
    public final EObject entryRuleModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModel = null;


        try {
            // InternalXMapping.g:70:46: (iv_ruleModel= ruleModel EOF )
            // InternalXMapping.g:71:2: iv_ruleModel= ruleModel EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getModelRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleModel=ruleModel();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleModel; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // InternalXMapping.g:77:1: ruleModel returns [EObject current=null] : ( ( (lv_packageName_0_0= rulePackage ) ) ( (lv_namespaceImports_1_0= ruleNamespaceImport ) )* ( (lv_mappingClass_2_0= ruleMappingClassBlock ) ) ) ;
    public final EObject ruleModel() throws RecognitionException {
        EObject current = null;

        EObject lv_packageName_0_0 = null;

        EObject lv_namespaceImports_1_0 = null;

        EObject lv_mappingClass_2_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:83:2: ( ( ( (lv_packageName_0_0= rulePackage ) ) ( (lv_namespaceImports_1_0= ruleNamespaceImport ) )* ( (lv_mappingClass_2_0= ruleMappingClassBlock ) ) ) )
            // InternalXMapping.g:84:2: ( ( (lv_packageName_0_0= rulePackage ) ) ( (lv_namespaceImports_1_0= ruleNamespaceImport ) )* ( (lv_mappingClass_2_0= ruleMappingClassBlock ) ) )
            {
            // InternalXMapping.g:84:2: ( ( (lv_packageName_0_0= rulePackage ) ) ( (lv_namespaceImports_1_0= ruleNamespaceImport ) )* ( (lv_mappingClass_2_0= ruleMappingClassBlock ) ) )
            // InternalXMapping.g:85:3: ( (lv_packageName_0_0= rulePackage ) ) ( (lv_namespaceImports_1_0= ruleNamespaceImport ) )* ( (lv_mappingClass_2_0= ruleMappingClassBlock ) )
            {
            // InternalXMapping.g:85:3: ( (lv_packageName_0_0= rulePackage ) )
            // InternalXMapping.g:86:4: (lv_packageName_0_0= rulePackage )
            {
            // InternalXMapping.g:86:4: (lv_packageName_0_0= rulePackage )
            // InternalXMapping.g:87:5: lv_packageName_0_0= rulePackage
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getModelAccess().getPackageNamePackageParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_3);
            lv_packageName_0_0=rulePackage();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getModelRule());
              					}
              					set(
              						current,
              						"packageName",
              						lv_packageName_0_0,
              						"com.gk_software.core.dsl.XMapping.Package");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalXMapping.g:104:3: ( (lv_namespaceImports_1_0= ruleNamespaceImport ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==RULE_ID||LA1_0==22||(LA1_0>=24 && LA1_0<=25)||LA1_0==28||LA1_0==35) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalXMapping.g:105:4: (lv_namespaceImports_1_0= ruleNamespaceImport )
            	    {
            	    // InternalXMapping.g:105:4: (lv_namespaceImports_1_0= ruleNamespaceImport )
            	    // InternalXMapping.g:106:5: lv_namespaceImports_1_0= ruleNamespaceImport
            	    {
            	    if ( state.backtracking==0 ) {

            	      					newCompositeNode(grammarAccess.getModelAccess().getNamespaceImportsNamespaceImportParserRuleCall_1_0());
            	      				
            	    }
            	    pushFollow(FOLLOW_3);
            	    lv_namespaceImports_1_0=ruleNamespaceImport();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      					if (current==null) {
            	      						current = createModelElementForParent(grammarAccess.getModelRule());
            	      					}
            	      					add(
            	      						current,
            	      						"namespaceImports",
            	      						lv_namespaceImports_1_0,
            	      						"com.gk_software.core.dsl.XMapping.NamespaceImport");
            	      					afterParserOrEnumRuleCall();
            	      				
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // InternalXMapping.g:123:3: ( (lv_mappingClass_2_0= ruleMappingClassBlock ) )
            // InternalXMapping.g:124:4: (lv_mappingClass_2_0= ruleMappingClassBlock )
            {
            // InternalXMapping.g:124:4: (lv_mappingClass_2_0= ruleMappingClassBlock )
            // InternalXMapping.g:125:5: lv_mappingClass_2_0= ruleMappingClassBlock
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getModelAccess().getMappingClassMappingClassBlockParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_mappingClass_2_0=ruleMappingClassBlock();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getModelRule());
              					}
              					set(
              						current,
              						"mappingClass",
              						lv_mappingClass_2_0,
              						"com.gk_software.core.dsl.XMapping.MappingClassBlock");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleMappingClassBlock"
    // InternalXMapping.g:146:1: entryRuleMappingClassBlock returns [EObject current=null] : iv_ruleMappingClassBlock= ruleMappingClassBlock EOF ;
    public final EObject entryRuleMappingClassBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMappingClassBlock = null;


        try {
            // InternalXMapping.g:146:58: (iv_ruleMappingClassBlock= ruleMappingClassBlock EOF )
            // InternalXMapping.g:147:2: iv_ruleMappingClassBlock= ruleMappingClassBlock EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMappingClassBlockRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleMappingClassBlock=ruleMappingClassBlock();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMappingClassBlock; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMappingClassBlock"


    // $ANTLR start "ruleMappingClassBlock"
    // InternalXMapping.g:153:1: ruleMappingClassBlock returns [EObject current=null] : (otherlv_0= 'MappingClass' ( (lv_name_1_0= ruleExtendedID ) ) ) ;
    public final EObject ruleMappingClassBlock() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:159:2: ( (otherlv_0= 'MappingClass' ( (lv_name_1_0= ruleExtendedID ) ) ) )
            // InternalXMapping.g:160:2: (otherlv_0= 'MappingClass' ( (lv_name_1_0= ruleExtendedID ) ) )
            {
            // InternalXMapping.g:160:2: (otherlv_0= 'MappingClass' ( (lv_name_1_0= ruleExtendedID ) ) )
            // InternalXMapping.g:161:3: otherlv_0= 'MappingClass' ( (lv_name_1_0= ruleExtendedID ) )
            {
            otherlv_0=(Token)match(input,20,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getMappingClassBlockAccess().getMappingClassKeyword_0());
              		
            }
            // InternalXMapping.g:165:3: ( (lv_name_1_0= ruleExtendedID ) )
            // InternalXMapping.g:166:4: (lv_name_1_0= ruleExtendedID )
            {
            // InternalXMapping.g:166:4: (lv_name_1_0= ruleExtendedID )
            // InternalXMapping.g:167:5: lv_name_1_0= ruleExtendedID
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getMappingClassBlockAccess().getNameExtendedIDParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_name_1_0=ruleExtendedID();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getMappingClassBlockRule());
              					}
              					set(
              						current,
              						"name",
              						lv_name_1_0,
              						"com.gk_software.core.dsl.XMapping.ExtendedID");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMappingClassBlock"


    // $ANTLR start "entryRuleParamSection"
    // InternalXMapping.g:188:1: entryRuleParamSection returns [EObject current=null] : iv_ruleParamSection= ruleParamSection EOF ;
    public final EObject entryRuleParamSection() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParamSection = null;


        try {
            // InternalXMapping.g:188:53: (iv_ruleParamSection= ruleParamSection EOF )
            // InternalXMapping.g:189:2: iv_ruleParamSection= ruleParamSection EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getParamSectionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleParamSection=ruleParamSection();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleParamSection; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParamSection"


    // $ANTLR start "ruleParamSection"
    // InternalXMapping.g:195:1: ruleParamSection returns [EObject current=null] : ( ( (lv_paramSection_0_0= ruleParam ) ) (otherlv_1= ',' ( (lv_paramSection_2_0= ruleParam ) ) )* ) ;
    public final EObject ruleParamSection() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_paramSection_0_0 = null;

        EObject lv_paramSection_2_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:201:2: ( ( ( (lv_paramSection_0_0= ruleParam ) ) (otherlv_1= ',' ( (lv_paramSection_2_0= ruleParam ) ) )* ) )
            // InternalXMapping.g:202:2: ( ( (lv_paramSection_0_0= ruleParam ) ) (otherlv_1= ',' ( (lv_paramSection_2_0= ruleParam ) ) )* )
            {
            // InternalXMapping.g:202:2: ( ( (lv_paramSection_0_0= ruleParam ) ) (otherlv_1= ',' ( (lv_paramSection_2_0= ruleParam ) ) )* )
            // InternalXMapping.g:203:3: ( (lv_paramSection_0_0= ruleParam ) ) (otherlv_1= ',' ( (lv_paramSection_2_0= ruleParam ) ) )*
            {
            // InternalXMapping.g:203:3: ( (lv_paramSection_0_0= ruleParam ) )
            // InternalXMapping.g:204:4: (lv_paramSection_0_0= ruleParam )
            {
            // InternalXMapping.g:204:4: (lv_paramSection_0_0= ruleParam )
            // InternalXMapping.g:205:5: lv_paramSection_0_0= ruleParam
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getParamSectionAccess().getParamSectionParamParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_5);
            lv_paramSection_0_0=ruleParam();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getParamSectionRule());
              					}
              					add(
              						current,
              						"paramSection",
              						lv_paramSection_0_0,
              						"com.gk_software.core.dsl.XMapping.Param");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalXMapping.g:222:3: (otherlv_1= ',' ( (lv_paramSection_2_0= ruleParam ) ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==21) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalXMapping.g:223:4: otherlv_1= ',' ( (lv_paramSection_2_0= ruleParam ) )
            	    {
            	    otherlv_1=(Token)match(input,21,FOLLOW_6); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_1, grammarAccess.getParamSectionAccess().getCommaKeyword_1_0());
            	      			
            	    }
            	    // InternalXMapping.g:227:4: ( (lv_paramSection_2_0= ruleParam ) )
            	    // InternalXMapping.g:228:5: (lv_paramSection_2_0= ruleParam )
            	    {
            	    // InternalXMapping.g:228:5: (lv_paramSection_2_0= ruleParam )
            	    // InternalXMapping.g:229:6: lv_paramSection_2_0= ruleParam
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getParamSectionAccess().getParamSectionParamParserRuleCall_1_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_5);
            	    lv_paramSection_2_0=ruleParam();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getParamSectionRule());
            	      						}
            	      						add(
            	      							current,
            	      							"paramSection",
            	      							lv_paramSection_2_0,
            	      							"com.gk_software.core.dsl.XMapping.Param");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParamSection"


    // $ANTLR start "entryRuleBlock"
    // InternalXMapping.g:251:1: entryRuleBlock returns [EObject current=null] : iv_ruleBlock= ruleBlock EOF ;
    public final EObject entryRuleBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBlock = null;


        try {
            // InternalXMapping.g:251:46: (iv_ruleBlock= ruleBlock EOF )
            // InternalXMapping.g:252:2: iv_ruleBlock= ruleBlock EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBlockRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleBlock=ruleBlock();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBlock; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBlock"


    // $ANTLR start "ruleBlock"
    // InternalXMapping.g:258:1: ruleBlock returns [EObject current=null] : ( () ( (lv_statement_1_0= ruleStatement ) )* ) ;
    public final EObject ruleBlock() throws RecognitionException {
        EObject current = null;

        EObject lv_statement_1_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:264:2: ( ( () ( (lv_statement_1_0= ruleStatement ) )* ) )
            // InternalXMapping.g:265:2: ( () ( (lv_statement_1_0= ruleStatement ) )* )
            {
            // InternalXMapping.g:265:2: ( () ( (lv_statement_1_0= ruleStatement ) )* )
            // InternalXMapping.g:266:3: () ( (lv_statement_1_0= ruleStatement ) )*
            {
            // InternalXMapping.g:266:3: ()
            // InternalXMapping.g:267:4: 
            {
            if ( state.backtracking==0 ) {

              				/* */
              			
            }
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getBlockAccess().getBlockAction_0(),
              					current);
              			
            }

            }

            // InternalXMapping.g:276:3: ( (lv_statement_1_0= ruleStatement ) )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( ((LA3_0>=RULE_INPUT_KEY && LA3_0<=RULE_ID)||LA3_0==22||(LA3_0>=24 && LA3_0<=25)||LA3_0==28||LA3_0==35) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalXMapping.g:277:4: (lv_statement_1_0= ruleStatement )
            	    {
            	    // InternalXMapping.g:277:4: (lv_statement_1_0= ruleStatement )
            	    // InternalXMapping.g:278:5: lv_statement_1_0= ruleStatement
            	    {
            	    if ( state.backtracking==0 ) {

            	      					newCompositeNode(grammarAccess.getBlockAccess().getStatementStatementParserRuleCall_1_0());
            	      				
            	    }
            	    pushFollow(FOLLOW_7);
            	    lv_statement_1_0=ruleStatement();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      					if (current==null) {
            	      						current = createModelElementForParent(grammarAccess.getBlockRule());
            	      					}
            	      					add(
            	      						current,
            	      						"statement",
            	      						lv_statement_1_0,
            	      						"com.gk_software.core.dsl.XMapping.Statement");
            	      					afterParserOrEnumRuleCall();
            	      				
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBlock"


    // $ANTLR start "entryRulePackage"
    // InternalXMapping.g:299:1: entryRulePackage returns [EObject current=null] : iv_rulePackage= rulePackage EOF ;
    public final EObject entryRulePackage() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePackage = null;


        try {
            // InternalXMapping.g:299:48: (iv_rulePackage= rulePackage EOF )
            // InternalXMapping.g:300:2: iv_rulePackage= rulePackage EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getPackageRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_rulePackage=rulePackage();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_rulePackage; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePackage"


    // $ANTLR start "rulePackage"
    // InternalXMapping.g:306:1: rulePackage returns [EObject current=null] : (otherlv_0= 'package' ( (lv_name_1_0= ruleQualifiedName ) ) otherlv_2= ';' ) ;
    public final EObject rulePackage() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:312:2: ( (otherlv_0= 'package' ( (lv_name_1_0= ruleQualifiedName ) ) otherlv_2= ';' ) )
            // InternalXMapping.g:313:2: (otherlv_0= 'package' ( (lv_name_1_0= ruleQualifiedName ) ) otherlv_2= ';' )
            {
            // InternalXMapping.g:313:2: (otherlv_0= 'package' ( (lv_name_1_0= ruleQualifiedName ) ) otherlv_2= ';' )
            // InternalXMapping.g:314:3: otherlv_0= 'package' ( (lv_name_1_0= ruleQualifiedName ) ) otherlv_2= ';'
            {
            otherlv_0=(Token)match(input,22,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getPackageAccess().getPackageKeyword_0());
              		
            }
            // InternalXMapping.g:318:3: ( (lv_name_1_0= ruleQualifiedName ) )
            // InternalXMapping.g:319:4: (lv_name_1_0= ruleQualifiedName )
            {
            // InternalXMapping.g:319:4: (lv_name_1_0= ruleQualifiedName )
            // InternalXMapping.g:320:5: lv_name_1_0= ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getPackageAccess().getNameQualifiedNameParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_8);
            lv_name_1_0=ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getPackageRule());
              					}
              					set(
              						current,
              						"name",
              						lv_name_1_0,
              						"com.gk_software.core.dsl.XMapping.QualifiedName");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,23,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getPackageAccess().getSemicolonKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePackage"


    // $ANTLR start "entryRuleNamespaceImport"
    // InternalXMapping.g:345:1: entryRuleNamespaceImport returns [EObject current=null] : iv_ruleNamespaceImport= ruleNamespaceImport EOF ;
    public final EObject entryRuleNamespaceImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNamespaceImport = null;


        try {
            // InternalXMapping.g:345:56: (iv_ruleNamespaceImport= ruleNamespaceImport EOF )
            // InternalXMapping.g:346:2: iv_ruleNamespaceImport= ruleNamespaceImport EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNamespaceImportRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleNamespaceImport=ruleNamespaceImport();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNamespaceImport; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNamespaceImport"


    // $ANTLR start "ruleNamespaceImport"
    // InternalXMapping.g:352:1: ruleNamespaceImport returns [EObject current=null] : ( (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) otherlv_2= ';' ) | ( ( ruleQualifiedNameWithWildcard ) ) ) ;
    public final EObject ruleNamespaceImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        AntlrDatatypeRuleToken lv_importedNamespace_1_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:358:2: ( ( (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) otherlv_2= ';' ) | ( ( ruleQualifiedNameWithWildcard ) ) ) )
            // InternalXMapping.g:359:2: ( (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) otherlv_2= ';' ) | ( ( ruleQualifiedNameWithWildcard ) ) )
            {
            // InternalXMapping.g:359:2: ( (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) otherlv_2= ';' ) | ( ( ruleQualifiedNameWithWildcard ) ) )
            int alt4=2;
            alt4 = dfa4.predict(input);
            switch (alt4) {
                case 1 :
                    // InternalXMapping.g:360:3: (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) otherlv_2= ';' )
                    {
                    // InternalXMapping.g:360:3: (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) otherlv_2= ';' )
                    // InternalXMapping.g:361:4: otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) otherlv_2= ';'
                    {
                    otherlv_0=(Token)match(input,24,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_0, grammarAccess.getNamespaceImportAccess().getImportKeyword_0_0());
                      			
                    }
                    // InternalXMapping.g:365:4: ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) )
                    // InternalXMapping.g:366:5: (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard )
                    {
                    // InternalXMapping.g:366:5: (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard )
                    // InternalXMapping.g:367:6: lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getNamespaceImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_0_1_0());
                      					
                    }
                    pushFollow(FOLLOW_8);
                    lv_importedNamespace_1_0=ruleQualifiedNameWithWildcard();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getNamespaceImportRule());
                      						}
                      						set(
                      							current,
                      							"importedNamespace",
                      							lv_importedNamespace_1_0,
                      							"com.gk_software.core.dsl.XMapping.QualifiedNameWithWildcard");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    otherlv_2=(Token)match(input,23,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getNamespaceImportAccess().getSemicolonKeyword_0_2());
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalXMapping.g:390:3: ( ( ruleQualifiedNameWithWildcard ) )
                    {
                    // InternalXMapping.g:390:3: ( ( ruleQualifiedNameWithWildcard ) )
                    // InternalXMapping.g:391:4: ( ruleQualifiedNameWithWildcard )
                    {
                    // InternalXMapping.g:391:4: ( ruleQualifiedNameWithWildcard )
                    // InternalXMapping.g:392:5: ruleQualifiedNameWithWildcard
                    {
                    if ( state.backtracking==0 ) {

                      					/* */
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getNamespaceImportRule());
                      					}
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getNamespaceImportAccess().getSourceMappingClassBlockCrossReference_1_0());
                      				
                    }
                    pushFollow(FOLLOW_2);
                    ruleQualifiedNameWithWildcard();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNamespaceImport"


    // $ANTLR start "entryRuleStatement"
    // InternalXMapping.g:413:1: entryRuleStatement returns [EObject current=null] : iv_ruleStatement= ruleStatement EOF ;
    public final EObject entryRuleStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStatement = null;


        try {
            // InternalXMapping.g:413:50: (iv_ruleStatement= ruleStatement EOF )
            // InternalXMapping.g:414:2: iv_ruleStatement= ruleStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleStatement=ruleStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStatement"


    // $ANTLR start "ruleStatement"
    // InternalXMapping.g:420:1: ruleStatement returns [EObject current=null] : (this_MapStatement_0= ruleMapStatement | this_MapForwardStatement_1= ruleMapForwardStatement | this_AssignmentStatement_2= ruleAssignmentStatement | this_CallStatement_3= ruleCallStatement ) ;
    public final EObject ruleStatement() throws RecognitionException {
        EObject current = null;

        EObject this_MapStatement_0 = null;

        EObject this_MapForwardStatement_1 = null;

        EObject this_AssignmentStatement_2 = null;

        EObject this_CallStatement_3 = null;



        	enterRule();

        try {
            // InternalXMapping.g:426:2: ( (this_MapStatement_0= ruleMapStatement | this_MapForwardStatement_1= ruleMapForwardStatement | this_AssignmentStatement_2= ruleAssignmentStatement | this_CallStatement_3= ruleCallStatement ) )
            // InternalXMapping.g:427:2: (this_MapStatement_0= ruleMapStatement | this_MapForwardStatement_1= ruleMapForwardStatement | this_AssignmentStatement_2= ruleAssignmentStatement | this_CallStatement_3= ruleCallStatement )
            {
            // InternalXMapping.g:427:2: (this_MapStatement_0= ruleMapStatement | this_MapForwardStatement_1= ruleMapForwardStatement | this_AssignmentStatement_2= ruleAssignmentStatement | this_CallStatement_3= ruleCallStatement )
            int alt5=4;
            alt5 = dfa5.predict(input);
            switch (alt5) {
                case 1 :
                    // InternalXMapping.g:428:3: this_MapStatement_0= ruleMapStatement
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getStatementAccess().getMapStatementParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_MapStatement_0=ruleMapStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_MapStatement_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalXMapping.g:440:3: this_MapForwardStatement_1= ruleMapForwardStatement
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getStatementAccess().getMapForwardStatementParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_MapForwardStatement_1=ruleMapForwardStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_MapForwardStatement_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalXMapping.g:452:3: this_AssignmentStatement_2= ruleAssignmentStatement
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getStatementAccess().getAssignmentStatementParserRuleCall_2());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_AssignmentStatement_2=ruleAssignmentStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_AssignmentStatement_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalXMapping.g:464:3: this_CallStatement_3= ruleCallStatement
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getStatementAccess().getCallStatementParserRuleCall_3());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_CallStatement_3=ruleCallStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_CallStatement_3;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStatement"


    // $ANTLR start "entryRuleMapStatement"
    // InternalXMapping.g:479:1: entryRuleMapStatement returns [EObject current=null] : iv_ruleMapStatement= ruleMapStatement EOF ;
    public final EObject entryRuleMapStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMapStatement = null;


        try {
            // InternalXMapping.g:479:53: (iv_ruleMapStatement= ruleMapStatement EOF )
            // InternalXMapping.g:480:2: iv_ruleMapStatement= ruleMapStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMapStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleMapStatement=ruleMapStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMapStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMapStatement"


    // $ANTLR start "ruleMapStatement"
    // InternalXMapping.g:486:1: ruleMapStatement returns [EObject current=null] : (otherlv_0= 'map' ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->' ( (lv_routineSection_3_0= ruleRoutineSource ) ) otherlv_4= '->' ( (lv_outputSection_5_0= ruleOutputSection ) ) otherlv_6= ';' ) ;
    public final EObject ruleMapStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_inputSection_1_0 = null;

        EObject lv_routineSection_3_0 = null;

        EObject lv_outputSection_5_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:492:2: ( (otherlv_0= 'map' ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->' ( (lv_routineSection_3_0= ruleRoutineSource ) ) otherlv_4= '->' ( (lv_outputSection_5_0= ruleOutputSection ) ) otherlv_6= ';' ) )
            // InternalXMapping.g:493:2: (otherlv_0= 'map' ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->' ( (lv_routineSection_3_0= ruleRoutineSource ) ) otherlv_4= '->' ( (lv_outputSection_5_0= ruleOutputSection ) ) otherlv_6= ';' )
            {
            // InternalXMapping.g:493:2: (otherlv_0= 'map' ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->' ( (lv_routineSection_3_0= ruleRoutineSource ) ) otherlv_4= '->' ( (lv_outputSection_5_0= ruleOutputSection ) ) otherlv_6= ';' )
            // InternalXMapping.g:494:3: otherlv_0= 'map' ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->' ( (lv_routineSection_3_0= ruleRoutineSource ) ) otherlv_4= '->' ( (lv_outputSection_5_0= ruleOutputSection ) ) otherlv_6= ';'
            {
            otherlv_0=(Token)match(input,25,FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getMapStatementAccess().getMapKeyword_0());
              		
            }
            // InternalXMapping.g:498:3: ( (lv_inputSection_1_0= ruleInputSection ) )
            // InternalXMapping.g:499:4: (lv_inputSection_1_0= ruleInputSection )
            {
            // InternalXMapping.g:499:4: (lv_inputSection_1_0= ruleInputSection )
            // InternalXMapping.g:500:5: lv_inputSection_1_0= ruleInputSection
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getMapStatementAccess().getInputSectionInputSectionParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_10);
            lv_inputSection_1_0=ruleInputSection();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getMapStatementRule());
              					}
              					set(
              						current,
              						"inputSection",
              						lv_inputSection_1_0,
              						"com.gk_software.core.dsl.XMapping.InputSection");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,26,FOLLOW_11); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getMapStatementAccess().getHyphenMinusGreaterThanSignKeyword_2());
              		
            }
            // InternalXMapping.g:521:3: ( (lv_routineSection_3_0= ruleRoutineSource ) )
            // InternalXMapping.g:522:4: (lv_routineSection_3_0= ruleRoutineSource )
            {
            // InternalXMapping.g:522:4: (lv_routineSection_3_0= ruleRoutineSource )
            // InternalXMapping.g:523:5: lv_routineSection_3_0= ruleRoutineSource
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getMapStatementAccess().getRoutineSectionRoutineSourceParserRuleCall_3_0());
              				
            }
            pushFollow(FOLLOW_10);
            lv_routineSection_3_0=ruleRoutineSource();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getMapStatementRule());
              					}
              					set(
              						current,
              						"routineSection",
              						lv_routineSection_3_0,
              						"com.gk_software.core.dsl.XMapping.RoutineSource");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_4=(Token)match(input,26,FOLLOW_12); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getMapStatementAccess().getHyphenMinusGreaterThanSignKeyword_4());
              		
            }
            // InternalXMapping.g:544:3: ( (lv_outputSection_5_0= ruleOutputSection ) )
            // InternalXMapping.g:545:4: (lv_outputSection_5_0= ruleOutputSection )
            {
            // InternalXMapping.g:545:4: (lv_outputSection_5_0= ruleOutputSection )
            // InternalXMapping.g:546:5: lv_outputSection_5_0= ruleOutputSection
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getMapStatementAccess().getOutputSectionOutputSectionParserRuleCall_5_0());
              				
            }
            pushFollow(FOLLOW_8);
            lv_outputSection_5_0=ruleOutputSection();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getMapStatementRule());
              					}
              					set(
              						current,
              						"outputSection",
              						lv_outputSection_5_0,
              						"com.gk_software.core.dsl.XMapping.OutputSection");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,23,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getMapStatementAccess().getSemicolonKeyword_6());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMapStatement"


    // $ANTLR start "entryRuleMapForwardStatement"
    // InternalXMapping.g:571:1: entryRuleMapForwardStatement returns [EObject current=null] : iv_ruleMapForwardStatement= ruleMapForwardStatement EOF ;
    public final EObject entryRuleMapForwardStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMapForwardStatement = null;


        try {
            // InternalXMapping.g:571:60: (iv_ruleMapForwardStatement= ruleMapForwardStatement EOF )
            // InternalXMapping.g:572:2: iv_ruleMapForwardStatement= ruleMapForwardStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMapForwardStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleMapForwardStatement=ruleMapForwardStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMapForwardStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMapForwardStatement"


    // $ANTLR start "ruleMapForwardStatement"
    // InternalXMapping.g:578:1: ruleMapForwardStatement returns [EObject current=null] : ( ( (lv_inputSection_0_0= ruleInputSection ) ) otherlv_1= '->' ( (lv_outputSection_2_0= ruleOutputSection ) ) otherlv_3= ';' ) ;
    public final EObject ruleMapForwardStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_inputSection_0_0 = null;

        EObject lv_outputSection_2_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:584:2: ( ( ( (lv_inputSection_0_0= ruleInputSection ) ) otherlv_1= '->' ( (lv_outputSection_2_0= ruleOutputSection ) ) otherlv_3= ';' ) )
            // InternalXMapping.g:585:2: ( ( (lv_inputSection_0_0= ruleInputSection ) ) otherlv_1= '->' ( (lv_outputSection_2_0= ruleOutputSection ) ) otherlv_3= ';' )
            {
            // InternalXMapping.g:585:2: ( ( (lv_inputSection_0_0= ruleInputSection ) ) otherlv_1= '->' ( (lv_outputSection_2_0= ruleOutputSection ) ) otherlv_3= ';' )
            // InternalXMapping.g:586:3: ( (lv_inputSection_0_0= ruleInputSection ) ) otherlv_1= '->' ( (lv_outputSection_2_0= ruleOutputSection ) ) otherlv_3= ';'
            {
            // InternalXMapping.g:586:3: ( (lv_inputSection_0_0= ruleInputSection ) )
            // InternalXMapping.g:587:4: (lv_inputSection_0_0= ruleInputSection )
            {
            // InternalXMapping.g:587:4: (lv_inputSection_0_0= ruleInputSection )
            // InternalXMapping.g:588:5: lv_inputSection_0_0= ruleInputSection
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getMapForwardStatementAccess().getInputSectionInputSectionParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_10);
            lv_inputSection_0_0=ruleInputSection();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getMapForwardStatementRule());
              					}
              					add(
              						current,
              						"inputSection",
              						lv_inputSection_0_0,
              						"com.gk_software.core.dsl.XMapping.InputSection");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_1=(Token)match(input,26,FOLLOW_12); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getMapForwardStatementAccess().getHyphenMinusGreaterThanSignKeyword_1());
              		
            }
            // InternalXMapping.g:609:3: ( (lv_outputSection_2_0= ruleOutputSection ) )
            // InternalXMapping.g:610:4: (lv_outputSection_2_0= ruleOutputSection )
            {
            // InternalXMapping.g:610:4: (lv_outputSection_2_0= ruleOutputSection )
            // InternalXMapping.g:611:5: lv_outputSection_2_0= ruleOutputSection
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getMapForwardStatementAccess().getOutputSectionOutputSectionParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_8);
            lv_outputSection_2_0=ruleOutputSection();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getMapForwardStatementRule());
              					}
              					set(
              						current,
              						"outputSection",
              						lv_outputSection_2_0,
              						"com.gk_software.core.dsl.XMapping.OutputSection");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_3=(Token)match(input,23,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getMapForwardStatementAccess().getSemicolonKeyword_3());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMapForwardStatement"


    // $ANTLR start "entryRuleAssignmentStatement"
    // InternalXMapping.g:636:1: entryRuleAssignmentStatement returns [EObject current=null] : iv_ruleAssignmentStatement= ruleAssignmentStatement EOF ;
    public final EObject entryRuleAssignmentStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssignmentStatement = null;


        try {
            // InternalXMapping.g:636:60: (iv_ruleAssignmentStatement= ruleAssignmentStatement EOF )
            // InternalXMapping.g:637:2: iv_ruleAssignmentStatement= ruleAssignmentStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAssignmentStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAssignmentStatement=ruleAssignmentStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAssignmentStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssignmentStatement"


    // $ANTLR start "ruleAssignmentStatement"
    // InternalXMapping.g:643:1: ruleAssignmentStatement returns [EObject current=null] : (this_Declaration_0= ruleDeclaration | this_Initialization_1= ruleInitialization ) ;
    public final EObject ruleAssignmentStatement() throws RecognitionException {
        EObject current = null;

        EObject this_Declaration_0 = null;

        EObject this_Initialization_1 = null;



        	enterRule();

        try {
            // InternalXMapping.g:649:2: ( (this_Declaration_0= ruleDeclaration | this_Initialization_1= ruleInitialization ) )
            // InternalXMapping.g:650:2: (this_Declaration_0= ruleDeclaration | this_Initialization_1= ruleInitialization )
            {
            // InternalXMapping.g:650:2: (this_Declaration_0= ruleDeclaration | this_Initialization_1= ruleInitialization )
            int alt6=2;
            alt6 = dfa6.predict(input);
            switch (alt6) {
                case 1 :
                    // InternalXMapping.g:651:3: this_Declaration_0= ruleDeclaration
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getAssignmentStatementAccess().getDeclarationParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Declaration_0=ruleDeclaration();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Declaration_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalXMapping.g:663:3: this_Initialization_1= ruleInitialization
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getAssignmentStatementAccess().getInitializationParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_Initialization_1=ruleInitialization();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_Initialization_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssignmentStatement"


    // $ANTLR start "entryRuleDeclaration"
    // InternalXMapping.g:678:1: entryRuleDeclaration returns [EObject current=null] : iv_ruleDeclaration= ruleDeclaration EOF ;
    public final EObject entryRuleDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDeclaration = null;


        try {
            // InternalXMapping.g:678:52: (iv_ruleDeclaration= ruleDeclaration EOF )
            // InternalXMapping.g:679:2: iv_ruleDeclaration= ruleDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDeclarationRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleDeclaration=ruleDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDeclaration; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDeclaration"


    // $ANTLR start "ruleDeclaration"
    // InternalXMapping.g:685:1: ruleDeclaration returns [EObject current=null] : ( ( (lv_assignmentSection_0_0= ruleAssignmentNew ) ) (otherlv_1= ',' ( (lv_assignmentSection_2_0= ruleAssignmentNew ) ) )* otherlv_3= ';' ) ;
    public final EObject ruleDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_assignmentSection_0_0 = null;

        EObject lv_assignmentSection_2_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:691:2: ( ( ( (lv_assignmentSection_0_0= ruleAssignmentNew ) ) (otherlv_1= ',' ( (lv_assignmentSection_2_0= ruleAssignmentNew ) ) )* otherlv_3= ';' ) )
            // InternalXMapping.g:692:2: ( ( (lv_assignmentSection_0_0= ruleAssignmentNew ) ) (otherlv_1= ',' ( (lv_assignmentSection_2_0= ruleAssignmentNew ) ) )* otherlv_3= ';' )
            {
            // InternalXMapping.g:692:2: ( ( (lv_assignmentSection_0_0= ruleAssignmentNew ) ) (otherlv_1= ',' ( (lv_assignmentSection_2_0= ruleAssignmentNew ) ) )* otherlv_3= ';' )
            // InternalXMapping.g:693:3: ( (lv_assignmentSection_0_0= ruleAssignmentNew ) ) (otherlv_1= ',' ( (lv_assignmentSection_2_0= ruleAssignmentNew ) ) )* otherlv_3= ';'
            {
            // InternalXMapping.g:693:3: ( (lv_assignmentSection_0_0= ruleAssignmentNew ) )
            // InternalXMapping.g:694:4: (lv_assignmentSection_0_0= ruleAssignmentNew )
            {
            // InternalXMapping.g:694:4: (lv_assignmentSection_0_0= ruleAssignmentNew )
            // InternalXMapping.g:695:5: lv_assignmentSection_0_0= ruleAssignmentNew
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getDeclarationAccess().getAssignmentSectionAssignmentNewParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_13);
            lv_assignmentSection_0_0=ruleAssignmentNew();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getDeclarationRule());
              					}
              					add(
              						current,
              						"assignmentSection",
              						lv_assignmentSection_0_0,
              						"com.gk_software.core.dsl.XMapping.AssignmentNew");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalXMapping.g:712:3: (otherlv_1= ',' ( (lv_assignmentSection_2_0= ruleAssignmentNew ) ) )*
            loop7:
            do {
                int alt7=2;
                int LA7_0 = input.LA(1);

                if ( (LA7_0==21) ) {
                    alt7=1;
                }


                switch (alt7) {
            	case 1 :
            	    // InternalXMapping.g:713:4: otherlv_1= ',' ( (lv_assignmentSection_2_0= ruleAssignmentNew ) )
            	    {
            	    otherlv_1=(Token)match(input,21,FOLLOW_14); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_1, grammarAccess.getDeclarationAccess().getCommaKeyword_1_0());
            	      			
            	    }
            	    // InternalXMapping.g:717:4: ( (lv_assignmentSection_2_0= ruleAssignmentNew ) )
            	    // InternalXMapping.g:718:5: (lv_assignmentSection_2_0= ruleAssignmentNew )
            	    {
            	    // InternalXMapping.g:718:5: (lv_assignmentSection_2_0= ruleAssignmentNew )
            	    // InternalXMapping.g:719:6: lv_assignmentSection_2_0= ruleAssignmentNew
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getDeclarationAccess().getAssignmentSectionAssignmentNewParserRuleCall_1_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_13);
            	    lv_assignmentSection_2_0=ruleAssignmentNew();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getDeclarationRule());
            	      						}
            	      						add(
            	      							current,
            	      							"assignmentSection",
            	      							lv_assignmentSection_2_0,
            	      							"com.gk_software.core.dsl.XMapping.AssignmentNew");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop7;
                }
            } while (true);

            otherlv_3=(Token)match(input,23,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getDeclarationAccess().getSemicolonKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDeclaration"


    // $ANTLR start "entryRuleInitialization"
    // InternalXMapping.g:745:1: entryRuleInitialization returns [EObject current=null] : iv_ruleInitialization= ruleInitialization EOF ;
    public final EObject entryRuleInitialization() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInitialization = null;


        try {
            // InternalXMapping.g:745:55: (iv_ruleInitialization= ruleInitialization EOF )
            // InternalXMapping.g:746:2: iv_ruleInitialization= ruleInitialization EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInitializationRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleInitialization=ruleInitialization();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInitialization; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInitialization"


    // $ANTLR start "ruleInitialization"
    // InternalXMapping.g:752:1: ruleInitialization returns [EObject current=null] : ( ( (lv_assignmentSection_0_0= ruleAssignmentSource ) ) (otherlv_1= ',' ( (lv_assignmentSection_2_0= ruleAssignmentSource ) ) )* otherlv_3= '=' ( (lv_assignment_4_0= ruleAssignment ) ) otherlv_5= ';' ) ;
    public final EObject ruleInitialization() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        EObject lv_assignmentSection_0_0 = null;

        EObject lv_assignmentSection_2_0 = null;

        EObject lv_assignment_4_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:758:2: ( ( ( (lv_assignmentSection_0_0= ruleAssignmentSource ) ) (otherlv_1= ',' ( (lv_assignmentSection_2_0= ruleAssignmentSource ) ) )* otherlv_3= '=' ( (lv_assignment_4_0= ruleAssignment ) ) otherlv_5= ';' ) )
            // InternalXMapping.g:759:2: ( ( (lv_assignmentSection_0_0= ruleAssignmentSource ) ) (otherlv_1= ',' ( (lv_assignmentSection_2_0= ruleAssignmentSource ) ) )* otherlv_3= '=' ( (lv_assignment_4_0= ruleAssignment ) ) otherlv_5= ';' )
            {
            // InternalXMapping.g:759:2: ( ( (lv_assignmentSection_0_0= ruleAssignmentSource ) ) (otherlv_1= ',' ( (lv_assignmentSection_2_0= ruleAssignmentSource ) ) )* otherlv_3= '=' ( (lv_assignment_4_0= ruleAssignment ) ) otherlv_5= ';' )
            // InternalXMapping.g:760:3: ( (lv_assignmentSection_0_0= ruleAssignmentSource ) ) (otherlv_1= ',' ( (lv_assignmentSection_2_0= ruleAssignmentSource ) ) )* otherlv_3= '=' ( (lv_assignment_4_0= ruleAssignment ) ) otherlv_5= ';'
            {
            // InternalXMapping.g:760:3: ( (lv_assignmentSection_0_0= ruleAssignmentSource ) )
            // InternalXMapping.g:761:4: (lv_assignmentSection_0_0= ruleAssignmentSource )
            {
            // InternalXMapping.g:761:4: (lv_assignmentSection_0_0= ruleAssignmentSource )
            // InternalXMapping.g:762:5: lv_assignmentSection_0_0= ruleAssignmentSource
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getInitializationAccess().getAssignmentSectionAssignmentSourceParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_15);
            lv_assignmentSection_0_0=ruleAssignmentSource();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getInitializationRule());
              					}
              					add(
              						current,
              						"assignmentSection",
              						lv_assignmentSection_0_0,
              						"com.gk_software.core.dsl.XMapping.AssignmentSource");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalXMapping.g:779:3: (otherlv_1= ',' ( (lv_assignmentSection_2_0= ruleAssignmentSource ) ) )*
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==21) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalXMapping.g:780:4: otherlv_1= ',' ( (lv_assignmentSection_2_0= ruleAssignmentSource ) )
            	    {
            	    otherlv_1=(Token)match(input,21,FOLLOW_16); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_1, grammarAccess.getInitializationAccess().getCommaKeyword_1_0());
            	      			
            	    }
            	    // InternalXMapping.g:784:4: ( (lv_assignmentSection_2_0= ruleAssignmentSource ) )
            	    // InternalXMapping.g:785:5: (lv_assignmentSection_2_0= ruleAssignmentSource )
            	    {
            	    // InternalXMapping.g:785:5: (lv_assignmentSection_2_0= ruleAssignmentSource )
            	    // InternalXMapping.g:786:6: lv_assignmentSection_2_0= ruleAssignmentSource
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getInitializationAccess().getAssignmentSectionAssignmentSourceParserRuleCall_1_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_15);
            	    lv_assignmentSection_2_0=ruleAssignmentSource();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getInitializationRule());
            	      						}
            	      						add(
            	      							current,
            	      							"assignmentSection",
            	      							lv_assignmentSection_2_0,
            	      							"com.gk_software.core.dsl.XMapping.AssignmentSource");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop8;
                }
            } while (true);

            otherlv_3=(Token)match(input,27,FOLLOW_17); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getInitializationAccess().getEqualsSignKeyword_2());
              		
            }
            // InternalXMapping.g:808:3: ( (lv_assignment_4_0= ruleAssignment ) )
            // InternalXMapping.g:809:4: (lv_assignment_4_0= ruleAssignment )
            {
            // InternalXMapping.g:809:4: (lv_assignment_4_0= ruleAssignment )
            // InternalXMapping.g:810:5: lv_assignment_4_0= ruleAssignment
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getInitializationAccess().getAssignmentAssignmentParserRuleCall_3_0());
              				
            }
            pushFollow(FOLLOW_8);
            lv_assignment_4_0=ruleAssignment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getInitializationRule());
              					}
              					set(
              						current,
              						"assignment",
              						lv_assignment_4_0,
              						"com.gk_software.core.dsl.XMapping.Assignment");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_5=(Token)match(input,23,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_5, grammarAccess.getInitializationAccess().getSemicolonKeyword_4());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInitialization"


    // $ANTLR start "entryRuleCallStatement"
    // InternalXMapping.g:835:1: entryRuleCallStatement returns [EObject current=null] : iv_ruleCallStatement= ruleCallStatement EOF ;
    public final EObject entryRuleCallStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCallStatement = null;


        try {
            // InternalXMapping.g:835:54: (iv_ruleCallStatement= ruleCallStatement EOF )
            // InternalXMapping.g:836:2: iv_ruleCallStatement= ruleCallStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCallStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleCallStatement=ruleCallStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCallStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCallStatement"


    // $ANTLR start "ruleCallStatement"
    // InternalXMapping.g:842:1: ruleCallStatement returns [EObject current=null] : (otherlv_0= 'call' ( ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->' )? ( (lv_routineSection_3_0= ruleQualifiedName ) ) otherlv_4= '->' ( (lv_outputSection_5_0= ruleOutputSection ) ) otherlv_6= ';' ) ;
    public final EObject ruleCallStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_inputSection_1_0 = null;

        AntlrDatatypeRuleToken lv_routineSection_3_0 = null;

        EObject lv_outputSection_5_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:848:2: ( (otherlv_0= 'call' ( ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->' )? ( (lv_routineSection_3_0= ruleQualifiedName ) ) otherlv_4= '->' ( (lv_outputSection_5_0= ruleOutputSection ) ) otherlv_6= ';' ) )
            // InternalXMapping.g:849:2: (otherlv_0= 'call' ( ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->' )? ( (lv_routineSection_3_0= ruleQualifiedName ) ) otherlv_4= '->' ( (lv_outputSection_5_0= ruleOutputSection ) ) otherlv_6= ';' )
            {
            // InternalXMapping.g:849:2: (otherlv_0= 'call' ( ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->' )? ( (lv_routineSection_3_0= ruleQualifiedName ) ) otherlv_4= '->' ( (lv_outputSection_5_0= ruleOutputSection ) ) otherlv_6= ';' )
            // InternalXMapping.g:850:3: otherlv_0= 'call' ( ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->' )? ( (lv_routineSection_3_0= ruleQualifiedName ) ) otherlv_4= '->' ( (lv_outputSection_5_0= ruleOutputSection ) ) otherlv_6= ';'
            {
            otherlv_0=(Token)match(input,28,FOLLOW_9); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getCallStatementAccess().getCallKeyword_0());
              		
            }
            // InternalXMapping.g:854:3: ( ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->' )?
            int alt9=2;
            alt9 = dfa9.predict(input);
            switch (alt9) {
                case 1 :
                    // InternalXMapping.g:855:4: ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->'
                    {
                    // InternalXMapping.g:855:4: ( (lv_inputSection_1_0= ruleInputSection ) )
                    // InternalXMapping.g:856:5: (lv_inputSection_1_0= ruleInputSection )
                    {
                    // InternalXMapping.g:856:5: (lv_inputSection_1_0= ruleInputSection )
                    // InternalXMapping.g:857:6: lv_inputSection_1_0= ruleInputSection
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getCallStatementAccess().getInputSectionInputSectionParserRuleCall_1_0_0());
                      					
                    }
                    pushFollow(FOLLOW_10);
                    lv_inputSection_1_0=ruleInputSection();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getCallStatementRule());
                      						}
                      						set(
                      							current,
                      							"inputSection",
                      							lv_inputSection_1_0,
                      							"com.gk_software.core.dsl.XMapping.InputSection");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    otherlv_2=(Token)match(input,26,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getCallStatementAccess().getHyphenMinusGreaterThanSignKeyword_1_1());
                      			
                    }

                    }
                    break;

            }

            // InternalXMapping.g:879:3: ( (lv_routineSection_3_0= ruleQualifiedName ) )
            // InternalXMapping.g:880:4: (lv_routineSection_3_0= ruleQualifiedName )
            {
            // InternalXMapping.g:880:4: (lv_routineSection_3_0= ruleQualifiedName )
            // InternalXMapping.g:881:5: lv_routineSection_3_0= ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getCallStatementAccess().getRoutineSectionQualifiedNameParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_10);
            lv_routineSection_3_0=ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getCallStatementRule());
              					}
              					set(
              						current,
              						"routineSection",
              						lv_routineSection_3_0,
              						"com.gk_software.core.dsl.XMapping.QualifiedName");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_4=(Token)match(input,26,FOLLOW_12); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getCallStatementAccess().getHyphenMinusGreaterThanSignKeyword_3());
              		
            }
            // InternalXMapping.g:902:3: ( (lv_outputSection_5_0= ruleOutputSection ) )
            // InternalXMapping.g:903:4: (lv_outputSection_5_0= ruleOutputSection )
            {
            // InternalXMapping.g:903:4: (lv_outputSection_5_0= ruleOutputSection )
            // InternalXMapping.g:904:5: lv_outputSection_5_0= ruleOutputSection
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getCallStatementAccess().getOutputSectionOutputSectionParserRuleCall_4_0());
              				
            }
            pushFollow(FOLLOW_8);
            lv_outputSection_5_0=ruleOutputSection();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getCallStatementRule());
              					}
              					set(
              						current,
              						"outputSection",
              						lv_outputSection_5_0,
              						"com.gk_software.core.dsl.XMapping.OutputSection");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,23,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getCallStatementAccess().getSemicolonKeyword_5());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCallStatement"


    // $ANTLR start "entryRuleAssignment"
    // InternalXMapping.g:929:1: entryRuleAssignment returns [EObject current=null] : iv_ruleAssignment= ruleAssignment EOF ;
    public final EObject entryRuleAssignment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssignment = null;


        try {
            // InternalXMapping.g:929:51: (iv_ruleAssignment= ruleAssignment EOF )
            // InternalXMapping.g:930:2: iv_ruleAssignment= ruleAssignment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAssignmentRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAssignment=ruleAssignment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAssignment; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssignment"


    // $ANTLR start "ruleAssignment"
    // InternalXMapping.g:936:1: ruleAssignment returns [EObject current=null] : ( (otherlv_0= 'new' this_AssignmentStructure_1= ruleAssignmentStructure ) | ( () this_INT_3= RULE_INT ) | ( () ruleDOUBLE ) | ( () this_STRING_7= RULE_STRING ) ) ;
    public final EObject ruleAssignment() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token this_INT_3=null;
        Token this_STRING_7=null;
        EObject this_AssignmentStructure_1 = null;



        	enterRule();

        try {
            // InternalXMapping.g:942:2: ( ( (otherlv_0= 'new' this_AssignmentStructure_1= ruleAssignmentStructure ) | ( () this_INT_3= RULE_INT ) | ( () ruleDOUBLE ) | ( () this_STRING_7= RULE_STRING ) ) )
            // InternalXMapping.g:943:2: ( (otherlv_0= 'new' this_AssignmentStructure_1= ruleAssignmentStructure ) | ( () this_INT_3= RULE_INT ) | ( () ruleDOUBLE ) | ( () this_STRING_7= RULE_STRING ) )
            {
            // InternalXMapping.g:943:2: ( (otherlv_0= 'new' this_AssignmentStructure_1= ruleAssignmentStructure ) | ( () this_INT_3= RULE_INT ) | ( () ruleDOUBLE ) | ( () this_STRING_7= RULE_STRING ) )
            int alt10=4;
            switch ( input.LA(1) ) {
            case 29:
                {
                alt10=1;
                }
                break;
            case RULE_INT:
                {
                int LA10_2 = input.LA(2);

                if ( (LA10_2==31) ) {
                    alt10=3;
                }
                else if ( (LA10_2==EOF||LA10_2==23) ) {
                    alt10=2;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 10, 2, input);

                    throw nvae;
                }
                }
                break;
            case RULE_STRING:
                {
                alt10=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }

            switch (alt10) {
                case 1 :
                    // InternalXMapping.g:944:3: (otherlv_0= 'new' this_AssignmentStructure_1= ruleAssignmentStructure )
                    {
                    // InternalXMapping.g:944:3: (otherlv_0= 'new' this_AssignmentStructure_1= ruleAssignmentStructure )
                    // InternalXMapping.g:945:4: otherlv_0= 'new' this_AssignmentStructure_1= ruleAssignmentStructure
                    {
                    otherlv_0=(Token)match(input,29,FOLLOW_16); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_0, grammarAccess.getAssignmentAccess().getNewKeyword_0_0());
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				/* */
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				newCompositeNode(grammarAccess.getAssignmentAccess().getAssignmentStructureParserRuleCall_0_1());
                      			
                    }
                    pushFollow(FOLLOW_2);
                    this_AssignmentStructure_1=ruleAssignmentStructure();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = this_AssignmentStructure_1;
                      				afterParserOrEnumRuleCall();
                      			
                    }

                    }


                    }
                    break;
                case 2 :
                    // InternalXMapping.g:962:3: ( () this_INT_3= RULE_INT )
                    {
                    // InternalXMapping.g:962:3: ( () this_INT_3= RULE_INT )
                    // InternalXMapping.g:963:4: () this_INT_3= RULE_INT
                    {
                    // InternalXMapping.g:963:4: ()
                    // InternalXMapping.g:964:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					/* */
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElement(
                      						grammarAccess.getAssignmentAccess().getAssignmentAction_1_0(),
                      						current);
                      				
                    }

                    }

                    this_INT_3=(Token)match(input,RULE_INT,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(this_INT_3, grammarAccess.getAssignmentAccess().getINTTerminalRuleCall_1_1());
                      			
                    }

                    }


                    }
                    break;
                case 3 :
                    // InternalXMapping.g:979:3: ( () ruleDOUBLE )
                    {
                    // InternalXMapping.g:979:3: ( () ruleDOUBLE )
                    // InternalXMapping.g:980:4: () ruleDOUBLE
                    {
                    // InternalXMapping.g:980:4: ()
                    // InternalXMapping.g:981:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					/* */
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElement(
                      						grammarAccess.getAssignmentAccess().getAssignmentAction_2_0(),
                      						current);
                      				
                    }

                    }

                    if ( state.backtracking==0 ) {

                      				/* */
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				newCompositeNode(grammarAccess.getAssignmentAccess().getDOUBLEParserRuleCall_2_1());
                      			
                    }
                    pushFollow(FOLLOW_2);
                    ruleDOUBLE();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				afterParserOrEnumRuleCall();
                      			
                    }

                    }


                    }
                    break;
                case 4 :
                    // InternalXMapping.g:1002:3: ( () this_STRING_7= RULE_STRING )
                    {
                    // InternalXMapping.g:1002:3: ( () this_STRING_7= RULE_STRING )
                    // InternalXMapping.g:1003:4: () this_STRING_7= RULE_STRING
                    {
                    // InternalXMapping.g:1003:4: ()
                    // InternalXMapping.g:1004:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					/* */
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElement(
                      						grammarAccess.getAssignmentAccess().getAssignmentAction_3_0(),
                      						current);
                      				
                    }

                    }

                    this_STRING_7=(Token)match(input,RULE_STRING,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(this_STRING_7, grammarAccess.getAssignmentAccess().getSTRINGTerminalRuleCall_3_1());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssignment"


    // $ANTLR start "entryRuleAssignmentStructure"
    // InternalXMapping.g:1022:1: entryRuleAssignmentStructure returns [EObject current=null] : iv_ruleAssignmentStructure= ruleAssignmentStructure EOF ;
    public final EObject entryRuleAssignmentStructure() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssignmentStructure = null;


        try {
            // InternalXMapping.g:1022:60: (iv_ruleAssignmentStructure= ruleAssignmentStructure EOF )
            // InternalXMapping.g:1023:2: iv_ruleAssignmentStructure= ruleAssignmentStructure EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAssignmentStructureRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAssignmentStructure=ruleAssignmentStructure();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAssignmentStructure; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssignmentStructure"


    // $ANTLR start "ruleAssignmentStructure"
    // InternalXMapping.g:1029:1: ruleAssignmentStructure returns [EObject current=null] : ( (lv_source_0_0= ruleOutputAttribute ) ) ;
    public final EObject ruleAssignmentStructure() throws RecognitionException {
        EObject current = null;

        EObject lv_source_0_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:1035:2: ( ( (lv_source_0_0= ruleOutputAttribute ) ) )
            // InternalXMapping.g:1036:2: ( (lv_source_0_0= ruleOutputAttribute ) )
            {
            // InternalXMapping.g:1036:2: ( (lv_source_0_0= ruleOutputAttribute ) )
            // InternalXMapping.g:1037:3: (lv_source_0_0= ruleOutputAttribute )
            {
            // InternalXMapping.g:1037:3: (lv_source_0_0= ruleOutputAttribute )
            // InternalXMapping.g:1038:4: lv_source_0_0= ruleOutputAttribute
            {
            if ( state.backtracking==0 ) {

              				newCompositeNode(grammarAccess.getAssignmentStructureAccess().getSourceOutputAttributeParserRuleCall_0());
              			
            }
            pushFollow(FOLLOW_2);
            lv_source_0_0=ruleOutputAttribute();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				if (current==null) {
              					current = createModelElementForParent(grammarAccess.getAssignmentStructureRule());
              				}
              				set(
              					current,
              					"source",
              					lv_source_0_0,
              					"com.gk_software.core.dsl.XMapping.OutputAttribute");
              				afterParserOrEnumRuleCall();
              			
            }

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssignmentStructure"


    // $ANTLR start "entryRuleAssignmentSource"
    // InternalXMapping.g:1058:1: entryRuleAssignmentSource returns [EObject current=null] : iv_ruleAssignmentSource= ruleAssignmentSource EOF ;
    public final EObject entryRuleAssignmentSource() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssignmentSource = null;


        try {
            // InternalXMapping.g:1058:57: (iv_ruleAssignmentSource= ruleAssignmentSource EOF )
            // InternalXMapping.g:1059:2: iv_ruleAssignmentSource= ruleAssignmentSource EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAssignmentSourceRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAssignmentSource=ruleAssignmentSource();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAssignmentSource; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssignmentSource"


    // $ANTLR start "ruleAssignmentSource"
    // InternalXMapping.g:1065:1: ruleAssignmentSource returns [EObject current=null] : (this_AssignmentNew_0= ruleAssignmentNew | this_AssignmentReference_1= ruleAssignmentReference ) ;
    public final EObject ruleAssignmentSource() throws RecognitionException {
        EObject current = null;

        EObject this_AssignmentNew_0 = null;

        EObject this_AssignmentReference_1 = null;



        	enterRule();

        try {
            // InternalXMapping.g:1071:2: ( (this_AssignmentNew_0= ruleAssignmentNew | this_AssignmentReference_1= ruleAssignmentReference ) )
            // InternalXMapping.g:1072:2: (this_AssignmentNew_0= ruleAssignmentNew | this_AssignmentReference_1= ruleAssignmentReference )
            {
            // InternalXMapping.g:1072:2: (this_AssignmentNew_0= ruleAssignmentNew | this_AssignmentReference_1= ruleAssignmentReference )
            int alt11=2;
            alt11 = dfa11.predict(input);
            switch (alt11) {
                case 1 :
                    // InternalXMapping.g:1073:3: this_AssignmentNew_0= ruleAssignmentNew
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getAssignmentSourceAccess().getAssignmentNewParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_AssignmentNew_0=ruleAssignmentNew();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_AssignmentNew_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalXMapping.g:1085:3: this_AssignmentReference_1= ruleAssignmentReference
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getAssignmentSourceAccess().getAssignmentReferenceParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_AssignmentReference_1=ruleAssignmentReference();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_AssignmentReference_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssignmentSource"


    // $ANTLR start "entryRuleAssignmentNew"
    // InternalXMapping.g:1100:1: entryRuleAssignmentNew returns [EObject current=null] : iv_ruleAssignmentNew= ruleAssignmentNew EOF ;
    public final EObject entryRuleAssignmentNew() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssignmentNew = null;


        try {
            // InternalXMapping.g:1100:54: (iv_ruleAssignmentNew= ruleAssignmentNew EOF )
            // InternalXMapping.g:1101:2: iv_ruleAssignmentNew= ruleAssignmentNew EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAssignmentNewRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAssignmentNew=ruleAssignmentNew();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAssignmentNew; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssignmentNew"


    // $ANTLR start "ruleAssignmentNew"
    // InternalXMapping.g:1107:1: ruleAssignmentNew returns [EObject current=null] : ( ( (lv_source_0_0= ruleVariable ) ) | ( (lv_source_1_0= ruleReturn ) ) ) ;
    public final EObject ruleAssignmentNew() throws RecognitionException {
        EObject current = null;

        EObject lv_source_0_0 = null;

        EObject lv_source_1_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:1113:2: ( ( ( (lv_source_0_0= ruleVariable ) ) | ( (lv_source_1_0= ruleReturn ) ) ) )
            // InternalXMapping.g:1114:2: ( ( (lv_source_0_0= ruleVariable ) ) | ( (lv_source_1_0= ruleReturn ) ) )
            {
            // InternalXMapping.g:1114:2: ( ( (lv_source_0_0= ruleVariable ) ) | ( (lv_source_1_0= ruleReturn ) ) )
            int alt12=2;
            alt12 = dfa12.predict(input);
            switch (alt12) {
                case 1 :
                    // InternalXMapping.g:1115:3: ( (lv_source_0_0= ruleVariable ) )
                    {
                    // InternalXMapping.g:1115:3: ( (lv_source_0_0= ruleVariable ) )
                    // InternalXMapping.g:1116:4: (lv_source_0_0= ruleVariable )
                    {
                    // InternalXMapping.g:1116:4: (lv_source_0_0= ruleVariable )
                    // InternalXMapping.g:1117:5: lv_source_0_0= ruleVariable
                    {
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getAssignmentNewAccess().getSourceVariableParserRuleCall_0_0());
                      				
                    }
                    pushFollow(FOLLOW_2);
                    lv_source_0_0=ruleVariable();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElementForParent(grammarAccess.getAssignmentNewRule());
                      					}
                      					set(
                      						current,
                      						"source",
                      						lv_source_0_0,
                      						"com.gk_software.core.dsl.XMapping.Variable");
                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalXMapping.g:1135:3: ( (lv_source_1_0= ruleReturn ) )
                    {
                    // InternalXMapping.g:1135:3: ( (lv_source_1_0= ruleReturn ) )
                    // InternalXMapping.g:1136:4: (lv_source_1_0= ruleReturn )
                    {
                    // InternalXMapping.g:1136:4: (lv_source_1_0= ruleReturn )
                    // InternalXMapping.g:1137:5: lv_source_1_0= ruleReturn
                    {
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getAssignmentNewAccess().getSourceReturnParserRuleCall_1_0());
                      				
                    }
                    pushFollow(FOLLOW_2);
                    lv_source_1_0=ruleReturn();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElementForParent(grammarAccess.getAssignmentNewRule());
                      					}
                      					set(
                      						current,
                      						"source",
                      						lv_source_1_0,
                      						"com.gk_software.core.dsl.XMapping.Return");
                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssignmentNew"


    // $ANTLR start "entryRuleAssignmentReference"
    // InternalXMapping.g:1158:1: entryRuleAssignmentReference returns [EObject current=null] : iv_ruleAssignmentReference= ruleAssignmentReference EOF ;
    public final EObject entryRuleAssignmentReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssignmentReference = null;


        try {
            // InternalXMapping.g:1158:60: (iv_ruleAssignmentReference= ruleAssignmentReference EOF )
            // InternalXMapping.g:1159:2: iv_ruleAssignmentReference= ruleAssignmentReference EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAssignmentReferenceRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAssignmentReference=ruleAssignmentReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAssignmentReference; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssignmentReference"


    // $ANTLR start "ruleAssignmentReference"
    // InternalXMapping.g:1165:1: ruleAssignmentReference returns [EObject current=null] : ( ( (lv_source_0_0= ruleOutputAttribute ) ) | ( (lv_source_1_0= ruleAttribute ) ) ) ;
    public final EObject ruleAssignmentReference() throws RecognitionException {
        EObject current = null;

        EObject lv_source_0_0 = null;

        EObject lv_source_1_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:1171:2: ( ( ( (lv_source_0_0= ruleOutputAttribute ) ) | ( (lv_source_1_0= ruleAttribute ) ) ) )
            // InternalXMapping.g:1172:2: ( ( (lv_source_0_0= ruleOutputAttribute ) ) | ( (lv_source_1_0= ruleAttribute ) ) )
            {
            // InternalXMapping.g:1172:2: ( ( (lv_source_0_0= ruleOutputAttribute ) ) | ( (lv_source_1_0= ruleAttribute ) ) )
            int alt13=2;
            alt13 = dfa13.predict(input);
            switch (alt13) {
                case 1 :
                    // InternalXMapping.g:1173:3: ( (lv_source_0_0= ruleOutputAttribute ) )
                    {
                    // InternalXMapping.g:1173:3: ( (lv_source_0_0= ruleOutputAttribute ) )
                    // InternalXMapping.g:1174:4: (lv_source_0_0= ruleOutputAttribute )
                    {
                    // InternalXMapping.g:1174:4: (lv_source_0_0= ruleOutputAttribute )
                    // InternalXMapping.g:1175:5: lv_source_0_0= ruleOutputAttribute
                    {
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getAssignmentReferenceAccess().getSourceOutputAttributeParserRuleCall_0_0());
                      				
                    }
                    pushFollow(FOLLOW_2);
                    lv_source_0_0=ruleOutputAttribute();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElementForParent(grammarAccess.getAssignmentReferenceRule());
                      					}
                      					set(
                      						current,
                      						"source",
                      						lv_source_0_0,
                      						"com.gk_software.core.dsl.XMapping.OutputAttribute");
                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalXMapping.g:1193:3: ( (lv_source_1_0= ruleAttribute ) )
                    {
                    // InternalXMapping.g:1193:3: ( (lv_source_1_0= ruleAttribute ) )
                    // InternalXMapping.g:1194:4: (lv_source_1_0= ruleAttribute )
                    {
                    // InternalXMapping.g:1194:4: (lv_source_1_0= ruleAttribute )
                    // InternalXMapping.g:1195:5: lv_source_1_0= ruleAttribute
                    {
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getAssignmentReferenceAccess().getSourceAttributeParserRuleCall_1_0());
                      				
                    }
                    pushFollow(FOLLOW_2);
                    lv_source_1_0=ruleAttribute();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElementForParent(grammarAccess.getAssignmentReferenceRule());
                      					}
                      					set(
                      						current,
                      						"source",
                      						lv_source_1_0,
                      						"com.gk_software.core.dsl.XMapping.Attribute");
                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssignmentReference"


    // $ANTLR start "entryRuleInputSection"
    // InternalXMapping.g:1216:1: entryRuleInputSection returns [EObject current=null] : iv_ruleInputSection= ruleInputSection EOF ;
    public final EObject entryRuleInputSection() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInputSection = null;


        try {
            // InternalXMapping.g:1216:53: (iv_ruleInputSection= ruleInputSection EOF )
            // InternalXMapping.g:1217:2: iv_ruleInputSection= ruleInputSection EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInputSectionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleInputSection=ruleInputSection();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInputSection; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInputSection"


    // $ANTLR start "ruleInputSection"
    // InternalXMapping.g:1223:1: ruleInputSection returns [EObject current=null] : ( ( ( (lv_inputAttributes_0_0= ruleInputAttribute ) ) | ( (lv_attributes_1_0= ruleAttribute ) ) ) (otherlv_2= ',' ( ( (lv_inputAttributes_3_0= ruleInputAttribute ) ) | ( (lv_attributes_4_0= ruleAttribute ) ) ) )* ) ;
    public final EObject ruleInputSection() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject lv_inputAttributes_0_0 = null;

        EObject lv_attributes_1_0 = null;

        EObject lv_inputAttributes_3_0 = null;

        EObject lv_attributes_4_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:1229:2: ( ( ( ( (lv_inputAttributes_0_0= ruleInputAttribute ) ) | ( (lv_attributes_1_0= ruleAttribute ) ) ) (otherlv_2= ',' ( ( (lv_inputAttributes_3_0= ruleInputAttribute ) ) | ( (lv_attributes_4_0= ruleAttribute ) ) ) )* ) )
            // InternalXMapping.g:1230:2: ( ( ( (lv_inputAttributes_0_0= ruleInputAttribute ) ) | ( (lv_attributes_1_0= ruleAttribute ) ) ) (otherlv_2= ',' ( ( (lv_inputAttributes_3_0= ruleInputAttribute ) ) | ( (lv_attributes_4_0= ruleAttribute ) ) ) )* )
            {
            // InternalXMapping.g:1230:2: ( ( ( (lv_inputAttributes_0_0= ruleInputAttribute ) ) | ( (lv_attributes_1_0= ruleAttribute ) ) ) (otherlv_2= ',' ( ( (lv_inputAttributes_3_0= ruleInputAttribute ) ) | ( (lv_attributes_4_0= ruleAttribute ) ) ) )* )
            // InternalXMapping.g:1231:3: ( ( (lv_inputAttributes_0_0= ruleInputAttribute ) ) | ( (lv_attributes_1_0= ruleAttribute ) ) ) (otherlv_2= ',' ( ( (lv_inputAttributes_3_0= ruleInputAttribute ) ) | ( (lv_attributes_4_0= ruleAttribute ) ) ) )*
            {
            // InternalXMapping.g:1231:3: ( ( (lv_inputAttributes_0_0= ruleInputAttribute ) ) | ( (lv_attributes_1_0= ruleAttribute ) ) )
            int alt14=2;
            alt14 = dfa14.predict(input);
            switch (alt14) {
                case 1 :
                    // InternalXMapping.g:1232:4: ( (lv_inputAttributes_0_0= ruleInputAttribute ) )
                    {
                    // InternalXMapping.g:1232:4: ( (lv_inputAttributes_0_0= ruleInputAttribute ) )
                    // InternalXMapping.g:1233:5: (lv_inputAttributes_0_0= ruleInputAttribute )
                    {
                    // InternalXMapping.g:1233:5: (lv_inputAttributes_0_0= ruleInputAttribute )
                    // InternalXMapping.g:1234:6: lv_inputAttributes_0_0= ruleInputAttribute
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getInputSectionAccess().getInputAttributesInputAttributeParserRuleCall_0_0_0());
                      					
                    }
                    pushFollow(FOLLOW_5);
                    lv_inputAttributes_0_0=ruleInputAttribute();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getInputSectionRule());
                      						}
                      						add(
                      							current,
                      							"inputAttributes",
                      							lv_inputAttributes_0_0,
                      							"com.gk_software.core.dsl.XMapping.InputAttribute");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalXMapping.g:1252:4: ( (lv_attributes_1_0= ruleAttribute ) )
                    {
                    // InternalXMapping.g:1252:4: ( (lv_attributes_1_0= ruleAttribute ) )
                    // InternalXMapping.g:1253:5: (lv_attributes_1_0= ruleAttribute )
                    {
                    // InternalXMapping.g:1253:5: (lv_attributes_1_0= ruleAttribute )
                    // InternalXMapping.g:1254:6: lv_attributes_1_0= ruleAttribute
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getInputSectionAccess().getAttributesAttributeParserRuleCall_0_1_0());
                      					
                    }
                    pushFollow(FOLLOW_5);
                    lv_attributes_1_0=ruleAttribute();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getInputSectionRule());
                      						}
                      						add(
                      							current,
                      							"attributes",
                      							lv_attributes_1_0,
                      							"com.gk_software.core.dsl.XMapping.Attribute");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalXMapping.g:1272:3: (otherlv_2= ',' ( ( (lv_inputAttributes_3_0= ruleInputAttribute ) ) | ( (lv_attributes_4_0= ruleAttribute ) ) ) )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==21) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalXMapping.g:1273:4: otherlv_2= ',' ( ( (lv_inputAttributes_3_0= ruleInputAttribute ) ) | ( (lv_attributes_4_0= ruleAttribute ) ) )
            	    {
            	    otherlv_2=(Token)match(input,21,FOLLOW_9); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_2, grammarAccess.getInputSectionAccess().getCommaKeyword_1_0());
            	      			
            	    }
            	    // InternalXMapping.g:1277:4: ( ( (lv_inputAttributes_3_0= ruleInputAttribute ) ) | ( (lv_attributes_4_0= ruleAttribute ) ) )
            	    int alt15=2;
            	    alt15 = dfa15.predict(input);
            	    switch (alt15) {
            	        case 1 :
            	            // InternalXMapping.g:1278:5: ( (lv_inputAttributes_3_0= ruleInputAttribute ) )
            	            {
            	            // InternalXMapping.g:1278:5: ( (lv_inputAttributes_3_0= ruleInputAttribute ) )
            	            // InternalXMapping.g:1279:6: (lv_inputAttributes_3_0= ruleInputAttribute )
            	            {
            	            // InternalXMapping.g:1279:6: (lv_inputAttributes_3_0= ruleInputAttribute )
            	            // InternalXMapping.g:1280:7: lv_inputAttributes_3_0= ruleInputAttribute
            	            {
            	            if ( state.backtracking==0 ) {

            	              							newCompositeNode(grammarAccess.getInputSectionAccess().getInputAttributesInputAttributeParserRuleCall_1_1_0_0());
            	              						
            	            }
            	            pushFollow(FOLLOW_5);
            	            lv_inputAttributes_3_0=ruleInputAttribute();

            	            state._fsp--;
            	            if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              							if (current==null) {
            	              								current = createModelElementForParent(grammarAccess.getInputSectionRule());
            	              							}
            	              							add(
            	              								current,
            	              								"inputAttributes",
            	              								lv_inputAttributes_3_0,
            	              								"com.gk_software.core.dsl.XMapping.InputAttribute");
            	              							afterParserOrEnumRuleCall();
            	              						
            	            }

            	            }


            	            }


            	            }
            	            break;
            	        case 2 :
            	            // InternalXMapping.g:1298:5: ( (lv_attributes_4_0= ruleAttribute ) )
            	            {
            	            // InternalXMapping.g:1298:5: ( (lv_attributes_4_0= ruleAttribute ) )
            	            // InternalXMapping.g:1299:6: (lv_attributes_4_0= ruleAttribute )
            	            {
            	            // InternalXMapping.g:1299:6: (lv_attributes_4_0= ruleAttribute )
            	            // InternalXMapping.g:1300:7: lv_attributes_4_0= ruleAttribute
            	            {
            	            if ( state.backtracking==0 ) {

            	              							newCompositeNode(grammarAccess.getInputSectionAccess().getAttributesAttributeParserRuleCall_1_1_1_0());
            	              						
            	            }
            	            pushFollow(FOLLOW_5);
            	            lv_attributes_4_0=ruleAttribute();

            	            state._fsp--;
            	            if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              							if (current==null) {
            	              								current = createModelElementForParent(grammarAccess.getInputSectionRule());
            	              							}
            	              							add(
            	              								current,
            	              								"attributes",
            	              								lv_attributes_4_0,
            	              								"com.gk_software.core.dsl.XMapping.Attribute");
            	              							afterParserOrEnumRuleCall();
            	              						
            	            }

            	            }


            	            }


            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInputSection"


    // $ANTLR start "entryRuleRoutineSection"
    // InternalXMapping.g:1323:1: entryRuleRoutineSection returns [EObject current=null] : iv_ruleRoutineSection= ruleRoutineSection EOF ;
    public final EObject entryRuleRoutineSection() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRoutineSection = null;


        try {
            // InternalXMapping.g:1323:55: (iv_ruleRoutineSection= ruleRoutineSection EOF )
            // InternalXMapping.g:1324:2: iv_ruleRoutineSection= ruleRoutineSection EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRoutineSectionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleRoutineSection=ruleRoutineSection();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRoutineSection; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRoutineSection"


    // $ANTLR start "ruleRoutineSection"
    // InternalXMapping.g:1330:1: ruleRoutineSection returns [EObject current=null] : ( ( (lv_source_0_0= ruleMacro ) ) | ( (lv_source_1_0= ruleFilter ) ) | ( (lv_source_2_0= ruleFunction ) ) | ( (lv_source_3_0= ruleMapper ) ) ) ;
    public final EObject ruleRoutineSection() throws RecognitionException {
        EObject current = null;

        EObject lv_source_0_0 = null;

        EObject lv_source_1_0 = null;

        EObject lv_source_2_0 = null;

        EObject lv_source_3_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:1336:2: ( ( ( (lv_source_0_0= ruleMacro ) ) | ( (lv_source_1_0= ruleFilter ) ) | ( (lv_source_2_0= ruleFunction ) ) | ( (lv_source_3_0= ruleMapper ) ) ) )
            // InternalXMapping.g:1337:2: ( ( (lv_source_0_0= ruleMacro ) ) | ( (lv_source_1_0= ruleFilter ) ) | ( (lv_source_2_0= ruleFunction ) ) | ( (lv_source_3_0= ruleMapper ) ) )
            {
            // InternalXMapping.g:1337:2: ( ( (lv_source_0_0= ruleMacro ) ) | ( (lv_source_1_0= ruleFilter ) ) | ( (lv_source_2_0= ruleFunction ) ) | ( (lv_source_3_0= ruleMapper ) ) )
            int alt17=4;
            switch ( input.LA(1) ) {
            case RULE_MACRO_KEY:
                {
                alt17=1;
                }
                break;
            case RULE_FILTER_KEY:
                {
                alt17=2;
                }
                break;
            case RULE_FUNCTION_KEY:
                {
                alt17=3;
                }
                break;
            case RULE_MAPPER_KEY:
                {
                alt17=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 17, 0, input);

                throw nvae;
            }

            switch (alt17) {
                case 1 :
                    // InternalXMapping.g:1338:3: ( (lv_source_0_0= ruleMacro ) )
                    {
                    // InternalXMapping.g:1338:3: ( (lv_source_0_0= ruleMacro ) )
                    // InternalXMapping.g:1339:4: (lv_source_0_0= ruleMacro )
                    {
                    // InternalXMapping.g:1339:4: (lv_source_0_0= ruleMacro )
                    // InternalXMapping.g:1340:5: lv_source_0_0= ruleMacro
                    {
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getRoutineSectionAccess().getSourceMacroParserRuleCall_0_0());
                      				
                    }
                    pushFollow(FOLLOW_2);
                    lv_source_0_0=ruleMacro();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElementForParent(grammarAccess.getRoutineSectionRule());
                      					}
                      					set(
                      						current,
                      						"source",
                      						lv_source_0_0,
                      						"com.gk_software.core.dsl.XMapping.Macro");
                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalXMapping.g:1358:3: ( (lv_source_1_0= ruleFilter ) )
                    {
                    // InternalXMapping.g:1358:3: ( (lv_source_1_0= ruleFilter ) )
                    // InternalXMapping.g:1359:4: (lv_source_1_0= ruleFilter )
                    {
                    // InternalXMapping.g:1359:4: (lv_source_1_0= ruleFilter )
                    // InternalXMapping.g:1360:5: lv_source_1_0= ruleFilter
                    {
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getRoutineSectionAccess().getSourceFilterParserRuleCall_1_0());
                      				
                    }
                    pushFollow(FOLLOW_2);
                    lv_source_1_0=ruleFilter();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElementForParent(grammarAccess.getRoutineSectionRule());
                      					}
                      					set(
                      						current,
                      						"source",
                      						lv_source_1_0,
                      						"com.gk_software.core.dsl.XMapping.Filter");
                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalXMapping.g:1378:3: ( (lv_source_2_0= ruleFunction ) )
                    {
                    // InternalXMapping.g:1378:3: ( (lv_source_2_0= ruleFunction ) )
                    // InternalXMapping.g:1379:4: (lv_source_2_0= ruleFunction )
                    {
                    // InternalXMapping.g:1379:4: (lv_source_2_0= ruleFunction )
                    // InternalXMapping.g:1380:5: lv_source_2_0= ruleFunction
                    {
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getRoutineSectionAccess().getSourceFunctionParserRuleCall_2_0());
                      				
                    }
                    pushFollow(FOLLOW_2);
                    lv_source_2_0=ruleFunction();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElementForParent(grammarAccess.getRoutineSectionRule());
                      					}
                      					set(
                      						current,
                      						"source",
                      						lv_source_2_0,
                      						"com.gk_software.core.dsl.XMapping.Function");
                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalXMapping.g:1398:3: ( (lv_source_3_0= ruleMapper ) )
                    {
                    // InternalXMapping.g:1398:3: ( (lv_source_3_0= ruleMapper ) )
                    // InternalXMapping.g:1399:4: (lv_source_3_0= ruleMapper )
                    {
                    // InternalXMapping.g:1399:4: (lv_source_3_0= ruleMapper )
                    // InternalXMapping.g:1400:5: lv_source_3_0= ruleMapper
                    {
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getRoutineSectionAccess().getSourceMapperParserRuleCall_3_0());
                      				
                    }
                    pushFollow(FOLLOW_2);
                    lv_source_3_0=ruleMapper();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElementForParent(grammarAccess.getRoutineSectionRule());
                      					}
                      					set(
                      						current,
                      						"source",
                      						lv_source_3_0,
                      						"com.gk_software.core.dsl.XMapping.Mapper");
                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRoutineSection"


    // $ANTLR start "entryRuleOutputSection"
    // InternalXMapping.g:1421:1: entryRuleOutputSection returns [EObject current=null] : iv_ruleOutputSection= ruleOutputSection EOF ;
    public final EObject entryRuleOutputSection() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOutputSection = null;


        try {
            // InternalXMapping.g:1421:54: (iv_ruleOutputSection= ruleOutputSection EOF )
            // InternalXMapping.g:1422:2: iv_ruleOutputSection= ruleOutputSection EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOutputSectionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleOutputSection=ruleOutputSection();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOutputSection; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutputSection"


    // $ANTLR start "ruleOutputSection"
    // InternalXMapping.g:1428:1: ruleOutputSection returns [EObject current=null] : ( ( ( ( (lv_outputAttributes_0_0= ruleOutputAttribute ) ) | ( (lv_attributes_1_0= ruleAttribute ) ) ) (otherlv_2= ',' ( ( (lv_outputAttributes_3_0= ruleOutputAttribute ) ) | ( (lv_attributes_4_0= ruleAttribute ) ) ) )* ) | ( () otherlv_6= 'Null' ) ) ;
    public final EObject ruleOutputSection() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_6=null;
        EObject lv_outputAttributes_0_0 = null;

        EObject lv_attributes_1_0 = null;

        EObject lv_outputAttributes_3_0 = null;

        EObject lv_attributes_4_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:1434:2: ( ( ( ( ( (lv_outputAttributes_0_0= ruleOutputAttribute ) ) | ( (lv_attributes_1_0= ruleAttribute ) ) ) (otherlv_2= ',' ( ( (lv_outputAttributes_3_0= ruleOutputAttribute ) ) | ( (lv_attributes_4_0= ruleAttribute ) ) ) )* ) | ( () otherlv_6= 'Null' ) ) )
            // InternalXMapping.g:1435:2: ( ( ( ( (lv_outputAttributes_0_0= ruleOutputAttribute ) ) | ( (lv_attributes_1_0= ruleAttribute ) ) ) (otherlv_2= ',' ( ( (lv_outputAttributes_3_0= ruleOutputAttribute ) ) | ( (lv_attributes_4_0= ruleAttribute ) ) ) )* ) | ( () otherlv_6= 'Null' ) )
            {
            // InternalXMapping.g:1435:2: ( ( ( ( (lv_outputAttributes_0_0= ruleOutputAttribute ) ) | ( (lv_attributes_1_0= ruleAttribute ) ) ) (otherlv_2= ',' ( ( (lv_outputAttributes_3_0= ruleOutputAttribute ) ) | ( (lv_attributes_4_0= ruleAttribute ) ) ) )* ) | ( () otherlv_6= 'Null' ) )
            int alt21=2;
            int LA21_0 = input.LA(1);

            if ( ((LA21_0>=RULE_OUTPUT_KEY && LA21_0<=RULE_VARIABLE_KEY)||(LA21_0>=RULE_RETURN_KEY && LA21_0<=RULE_ID)||LA21_0==22||(LA21_0>=24 && LA21_0<=25)||LA21_0==28||LA21_0==35) ) {
                alt21=1;
            }
            else if ( (LA21_0==30) ) {
                alt21=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 21, 0, input);

                throw nvae;
            }
            switch (alt21) {
                case 1 :
                    // InternalXMapping.g:1436:3: ( ( ( (lv_outputAttributes_0_0= ruleOutputAttribute ) ) | ( (lv_attributes_1_0= ruleAttribute ) ) ) (otherlv_2= ',' ( ( (lv_outputAttributes_3_0= ruleOutputAttribute ) ) | ( (lv_attributes_4_0= ruleAttribute ) ) ) )* )
                    {
                    // InternalXMapping.g:1436:3: ( ( ( (lv_outputAttributes_0_0= ruleOutputAttribute ) ) | ( (lv_attributes_1_0= ruleAttribute ) ) ) (otherlv_2= ',' ( ( (lv_outputAttributes_3_0= ruleOutputAttribute ) ) | ( (lv_attributes_4_0= ruleAttribute ) ) ) )* )
                    // InternalXMapping.g:1437:4: ( ( (lv_outputAttributes_0_0= ruleOutputAttribute ) ) | ( (lv_attributes_1_0= ruleAttribute ) ) ) (otherlv_2= ',' ( ( (lv_outputAttributes_3_0= ruleOutputAttribute ) ) | ( (lv_attributes_4_0= ruleAttribute ) ) ) )*
                    {
                    // InternalXMapping.g:1437:4: ( ( (lv_outputAttributes_0_0= ruleOutputAttribute ) ) | ( (lv_attributes_1_0= ruleAttribute ) ) )
                    int alt18=2;
                    alt18 = dfa18.predict(input);
                    switch (alt18) {
                        case 1 :
                            // InternalXMapping.g:1438:5: ( (lv_outputAttributes_0_0= ruleOutputAttribute ) )
                            {
                            // InternalXMapping.g:1438:5: ( (lv_outputAttributes_0_0= ruleOutputAttribute ) )
                            // InternalXMapping.g:1439:6: (lv_outputAttributes_0_0= ruleOutputAttribute )
                            {
                            // InternalXMapping.g:1439:6: (lv_outputAttributes_0_0= ruleOutputAttribute )
                            // InternalXMapping.g:1440:7: lv_outputAttributes_0_0= ruleOutputAttribute
                            {
                            if ( state.backtracking==0 ) {

                              							newCompositeNode(grammarAccess.getOutputSectionAccess().getOutputAttributesOutputAttributeParserRuleCall_0_0_0_0());
                              						
                            }
                            pushFollow(FOLLOW_5);
                            lv_outputAttributes_0_0=ruleOutputAttribute();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              							if (current==null) {
                              								current = createModelElementForParent(grammarAccess.getOutputSectionRule());
                              							}
                              							add(
                              								current,
                              								"outputAttributes",
                              								lv_outputAttributes_0_0,
                              								"com.gk_software.core.dsl.XMapping.OutputAttribute");
                              							afterParserOrEnumRuleCall();
                              						
                            }

                            }


                            }


                            }
                            break;
                        case 2 :
                            // InternalXMapping.g:1458:5: ( (lv_attributes_1_0= ruleAttribute ) )
                            {
                            // InternalXMapping.g:1458:5: ( (lv_attributes_1_0= ruleAttribute ) )
                            // InternalXMapping.g:1459:6: (lv_attributes_1_0= ruleAttribute )
                            {
                            // InternalXMapping.g:1459:6: (lv_attributes_1_0= ruleAttribute )
                            // InternalXMapping.g:1460:7: lv_attributes_1_0= ruleAttribute
                            {
                            if ( state.backtracking==0 ) {

                              							newCompositeNode(grammarAccess.getOutputSectionAccess().getAttributesAttributeParserRuleCall_0_0_1_0());
                              						
                            }
                            pushFollow(FOLLOW_5);
                            lv_attributes_1_0=ruleAttribute();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              							if (current==null) {
                              								current = createModelElementForParent(grammarAccess.getOutputSectionRule());
                              							}
                              							add(
                              								current,
                              								"attributes",
                              								lv_attributes_1_0,
                              								"com.gk_software.core.dsl.XMapping.Attribute");
                              							afterParserOrEnumRuleCall();
                              						
                            }

                            }


                            }


                            }
                            break;

                    }

                    // InternalXMapping.g:1478:4: (otherlv_2= ',' ( ( (lv_outputAttributes_3_0= ruleOutputAttribute ) ) | ( (lv_attributes_4_0= ruleAttribute ) ) ) )*
                    loop20:
                    do {
                        int alt20=2;
                        int LA20_0 = input.LA(1);

                        if ( (LA20_0==21) ) {
                            alt20=1;
                        }


                        switch (alt20) {
                    	case 1 :
                    	    // InternalXMapping.g:1479:5: otherlv_2= ',' ( ( (lv_outputAttributes_3_0= ruleOutputAttribute ) ) | ( (lv_attributes_4_0= ruleAttribute ) ) )
                    	    {
                    	    otherlv_2=(Token)match(input,21,FOLLOW_16); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      					newLeafNode(otherlv_2, grammarAccess.getOutputSectionAccess().getCommaKeyword_0_1_0());
                    	      				
                    	    }
                    	    // InternalXMapping.g:1483:5: ( ( (lv_outputAttributes_3_0= ruleOutputAttribute ) ) | ( (lv_attributes_4_0= ruleAttribute ) ) )
                    	    int alt19=2;
                    	    alt19 = dfa19.predict(input);
                    	    switch (alt19) {
                    	        case 1 :
                    	            // InternalXMapping.g:1484:6: ( (lv_outputAttributes_3_0= ruleOutputAttribute ) )
                    	            {
                    	            // InternalXMapping.g:1484:6: ( (lv_outputAttributes_3_0= ruleOutputAttribute ) )
                    	            // InternalXMapping.g:1485:7: (lv_outputAttributes_3_0= ruleOutputAttribute )
                    	            {
                    	            // InternalXMapping.g:1485:7: (lv_outputAttributes_3_0= ruleOutputAttribute )
                    	            // InternalXMapping.g:1486:8: lv_outputAttributes_3_0= ruleOutputAttribute
                    	            {
                    	            if ( state.backtracking==0 ) {

                    	              								newCompositeNode(grammarAccess.getOutputSectionAccess().getOutputAttributesOutputAttributeParserRuleCall_0_1_1_0_0());
                    	              							
                    	            }
                    	            pushFollow(FOLLOW_5);
                    	            lv_outputAttributes_3_0=ruleOutputAttribute();

                    	            state._fsp--;
                    	            if (state.failed) return current;
                    	            if ( state.backtracking==0 ) {

                    	              								if (current==null) {
                    	              									current = createModelElementForParent(grammarAccess.getOutputSectionRule());
                    	              								}
                    	              								add(
                    	              									current,
                    	              									"outputAttributes",
                    	              									lv_outputAttributes_3_0,
                    	              									"com.gk_software.core.dsl.XMapping.OutputAttribute");
                    	              								afterParserOrEnumRuleCall();
                    	              							
                    	            }

                    	            }


                    	            }


                    	            }
                    	            break;
                    	        case 2 :
                    	            // InternalXMapping.g:1504:6: ( (lv_attributes_4_0= ruleAttribute ) )
                    	            {
                    	            // InternalXMapping.g:1504:6: ( (lv_attributes_4_0= ruleAttribute ) )
                    	            // InternalXMapping.g:1505:7: (lv_attributes_4_0= ruleAttribute )
                    	            {
                    	            // InternalXMapping.g:1505:7: (lv_attributes_4_0= ruleAttribute )
                    	            // InternalXMapping.g:1506:8: lv_attributes_4_0= ruleAttribute
                    	            {
                    	            if ( state.backtracking==0 ) {

                    	              								newCompositeNode(grammarAccess.getOutputSectionAccess().getAttributesAttributeParserRuleCall_0_1_1_1_0());
                    	              							
                    	            }
                    	            pushFollow(FOLLOW_5);
                    	            lv_attributes_4_0=ruleAttribute();

                    	            state._fsp--;
                    	            if (state.failed) return current;
                    	            if ( state.backtracking==0 ) {

                    	              								if (current==null) {
                    	              									current = createModelElementForParent(grammarAccess.getOutputSectionRule());
                    	              								}
                    	              								add(
                    	              									current,
                    	              									"attributes",
                    	              									lv_attributes_4_0,
                    	              									"com.gk_software.core.dsl.XMapping.Attribute");
                    	              								afterParserOrEnumRuleCall();
                    	              							
                    	            }

                    	            }


                    	            }


                    	            }
                    	            break;

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop20;
                        }
                    } while (true);


                    }


                    }
                    break;
                case 2 :
                    // InternalXMapping.g:1527:3: ( () otherlv_6= 'Null' )
                    {
                    // InternalXMapping.g:1527:3: ( () otherlv_6= 'Null' )
                    // InternalXMapping.g:1528:4: () otherlv_6= 'Null'
                    {
                    // InternalXMapping.g:1528:4: ()
                    // InternalXMapping.g:1529:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					/* */
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElement(
                      						grammarAccess.getOutputSectionAccess().getOutputSectionAction_1_0(),
                      						current);
                      				
                    }

                    }

                    otherlv_6=(Token)match(input,30,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_6, grammarAccess.getOutputSectionAccess().getNullKeyword_1_1());
                      			
                    }

                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutputSection"


    // $ANTLR start "entryRuleRoutineName"
    // InternalXMapping.g:1547:1: entryRuleRoutineName returns [String current=null] : iv_ruleRoutineName= ruleRoutineName EOF ;
    public final String entryRuleRoutineName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleRoutineName = null;


        try {
            // InternalXMapping.g:1547:51: (iv_ruleRoutineName= ruleRoutineName EOF )
            // InternalXMapping.g:1548:2: iv_ruleRoutineName= ruleRoutineName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRoutineNameRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleRoutineName=ruleRoutineName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRoutineName.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRoutineName"


    // $ANTLR start "ruleRoutineName"
    // InternalXMapping.g:1554:1: ruleRoutineName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_RoutineType_0= ruleRoutineType kw= '.' this_ExtendedName_2= ruleExtendedName ) ;
    public final AntlrDatatypeRuleToken ruleRoutineName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_RoutineType_0 = null;

        AntlrDatatypeRuleToken this_ExtendedName_2 = null;



        	enterRule();

        try {
            // InternalXMapping.g:1560:2: ( (this_RoutineType_0= ruleRoutineType kw= '.' this_ExtendedName_2= ruleExtendedName ) )
            // InternalXMapping.g:1561:2: (this_RoutineType_0= ruleRoutineType kw= '.' this_ExtendedName_2= ruleExtendedName )
            {
            // InternalXMapping.g:1561:2: (this_RoutineType_0= ruleRoutineType kw= '.' this_ExtendedName_2= ruleExtendedName )
            // InternalXMapping.g:1562:3: this_RoutineType_0= ruleRoutineType kw= '.' this_ExtendedName_2= ruleExtendedName
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getRoutineNameAccess().getRoutineTypeParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_18);
            this_RoutineType_0=ruleRoutineType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_RoutineType_0);
              		
            }
            if ( state.backtracking==0 ) {

              			afterParserOrEnumRuleCall();
              		
            }
            kw=(Token)match(input,31,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(kw);
              			newLeafNode(kw, grammarAccess.getRoutineNameAccess().getFullStopKeyword_1());
              		
            }
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getRoutineNameAccess().getExtendedNameParserRuleCall_2());
              		
            }
            pushFollow(FOLLOW_2);
            this_ExtendedName_2=ruleExtendedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_ExtendedName_2);
              		
            }
            if ( state.backtracking==0 ) {

              			afterParserOrEnumRuleCall();
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRoutineName"


    // $ANTLR start "entryRuleRoutineSource"
    // InternalXMapping.g:1591:1: entryRuleRoutineSource returns [EObject current=null] : iv_ruleRoutineSource= ruleRoutineSource EOF ;
    public final EObject entryRuleRoutineSource() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRoutineSource = null;


        try {
            // InternalXMapping.g:1591:54: (iv_ruleRoutineSource= ruleRoutineSource EOF )
            // InternalXMapping.g:1592:2: iv_ruleRoutineSource= ruleRoutineSource EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRoutineSourceRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleRoutineSource=ruleRoutineSource();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRoutineSource; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRoutineSource"


    // $ANTLR start "ruleRoutineSource"
    // InternalXMapping.g:1598:1: ruleRoutineSource returns [EObject current=null] : ( ( ruleRoutineName ) ) ;
    public final EObject ruleRoutineSource() throws RecognitionException {
        EObject current = null;


        	enterRule();

        try {
            // InternalXMapping.g:1604:2: ( ( ( ruleRoutineName ) ) )
            // InternalXMapping.g:1605:2: ( ( ruleRoutineName ) )
            {
            // InternalXMapping.g:1605:2: ( ( ruleRoutineName ) )
            // InternalXMapping.g:1606:3: ( ruleRoutineName )
            {
            // InternalXMapping.g:1606:3: ( ruleRoutineName )
            // InternalXMapping.g:1607:4: ruleRoutineName
            {
            if ( state.backtracking==0 ) {

              				/* */
              			
            }
            if ( state.backtracking==0 ) {

              				if (current==null) {
              					current = createModelElement(grammarAccess.getRoutineSourceRule());
              				}
              			
            }
            if ( state.backtracking==0 ) {

              				newCompositeNode(grammarAccess.getRoutineSourceAccess().getSourceRoutineCrossReference_0());
              			
            }
            pushFollow(FOLLOW_2);
            ruleRoutineName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				afterParserOrEnumRuleCall();
              			
            }

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRoutineSource"


    // $ANTLR start "entryRuleRoutineType"
    // InternalXMapping.g:1627:1: entryRuleRoutineType returns [String current=null] : iv_ruleRoutineType= ruleRoutineType EOF ;
    public final String entryRuleRoutineType() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleRoutineType = null;


        try {
            // InternalXMapping.g:1627:51: (iv_ruleRoutineType= ruleRoutineType EOF )
            // InternalXMapping.g:1628:2: iv_ruleRoutineType= ruleRoutineType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRoutineTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleRoutineType=ruleRoutineType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRoutineType.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRoutineType"


    // $ANTLR start "ruleRoutineType"
    // InternalXMapping.g:1634:1: ruleRoutineType returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_MACRO_KEY_0= RULE_MACRO_KEY | this_FILTER_KEY_1= RULE_FILTER_KEY | this_FUNCTION_KEY_2= RULE_FUNCTION_KEY | this_MAPPER_KEY_3= RULE_MAPPER_KEY ) ;
    public final AntlrDatatypeRuleToken ruleRoutineType() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_MACRO_KEY_0=null;
        Token this_FILTER_KEY_1=null;
        Token this_FUNCTION_KEY_2=null;
        Token this_MAPPER_KEY_3=null;


        	enterRule();

        try {
            // InternalXMapping.g:1640:2: ( (this_MACRO_KEY_0= RULE_MACRO_KEY | this_FILTER_KEY_1= RULE_FILTER_KEY | this_FUNCTION_KEY_2= RULE_FUNCTION_KEY | this_MAPPER_KEY_3= RULE_MAPPER_KEY ) )
            // InternalXMapping.g:1641:2: (this_MACRO_KEY_0= RULE_MACRO_KEY | this_FILTER_KEY_1= RULE_FILTER_KEY | this_FUNCTION_KEY_2= RULE_FUNCTION_KEY | this_MAPPER_KEY_3= RULE_MAPPER_KEY )
            {
            // InternalXMapping.g:1641:2: (this_MACRO_KEY_0= RULE_MACRO_KEY | this_FILTER_KEY_1= RULE_FILTER_KEY | this_FUNCTION_KEY_2= RULE_FUNCTION_KEY | this_MAPPER_KEY_3= RULE_MAPPER_KEY )
            int alt22=4;
            switch ( input.LA(1) ) {
            case RULE_MACRO_KEY:
                {
                alt22=1;
                }
                break;
            case RULE_FILTER_KEY:
                {
                alt22=2;
                }
                break;
            case RULE_FUNCTION_KEY:
                {
                alt22=3;
                }
                break;
            case RULE_MAPPER_KEY:
                {
                alt22=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 22, 0, input);

                throw nvae;
            }

            switch (alt22) {
                case 1 :
                    // InternalXMapping.g:1642:3: this_MACRO_KEY_0= RULE_MACRO_KEY
                    {
                    this_MACRO_KEY_0=(Token)match(input,RULE_MACRO_KEY,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_MACRO_KEY_0);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newLeafNode(this_MACRO_KEY_0, grammarAccess.getRoutineTypeAccess().getMACRO_KEYTerminalRuleCall_0());
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalXMapping.g:1650:3: this_FILTER_KEY_1= RULE_FILTER_KEY
                    {
                    this_FILTER_KEY_1=(Token)match(input,RULE_FILTER_KEY,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_FILTER_KEY_1);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newLeafNode(this_FILTER_KEY_1, grammarAccess.getRoutineTypeAccess().getFILTER_KEYTerminalRuleCall_1());
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalXMapping.g:1658:3: this_FUNCTION_KEY_2= RULE_FUNCTION_KEY
                    {
                    this_FUNCTION_KEY_2=(Token)match(input,RULE_FUNCTION_KEY,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_FUNCTION_KEY_2);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newLeafNode(this_FUNCTION_KEY_2, grammarAccess.getRoutineTypeAccess().getFUNCTION_KEYTerminalRuleCall_2());
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalXMapping.g:1666:3: this_MAPPER_KEY_3= RULE_MAPPER_KEY
                    {
                    this_MAPPER_KEY_3=(Token)match(input,RULE_MAPPER_KEY,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_MAPPER_KEY_3);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newLeafNode(this_MAPPER_KEY_3, grammarAccess.getRoutineTypeAccess().getMAPPER_KEYTerminalRuleCall_3());
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRoutineType"


    // $ANTLR start "entryRuleInputAttribute"
    // InternalXMapping.g:1677:1: entryRuleInputAttribute returns [EObject current=null] : iv_ruleInputAttribute= ruleInputAttribute EOF ;
    public final EObject entryRuleInputAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInputAttribute = null;


        try {
            // InternalXMapping.g:1677:55: (iv_ruleInputAttribute= ruleInputAttribute EOF )
            // InternalXMapping.g:1678:2: iv_ruleInputAttribute= ruleInputAttribute EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInputAttributeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleInputAttribute=ruleInputAttribute();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInputAttribute; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInputAttribute"


    // $ANTLR start "ruleInputAttribute"
    // InternalXMapping.g:1684:1: ruleInputAttribute returns [EObject current=null] : ( ( (lv_inputType_0_0= ruleInputType ) ) (otherlv_1= ':' ( (lv_xPath_2_0= RULE_STRING ) ) )? ) ;
    public final EObject ruleInputAttribute() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_xPath_2_0=null;
        EObject lv_inputType_0_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:1690:2: ( ( ( (lv_inputType_0_0= ruleInputType ) ) (otherlv_1= ':' ( (lv_xPath_2_0= RULE_STRING ) ) )? ) )
            // InternalXMapping.g:1691:2: ( ( (lv_inputType_0_0= ruleInputType ) ) (otherlv_1= ':' ( (lv_xPath_2_0= RULE_STRING ) ) )? )
            {
            // InternalXMapping.g:1691:2: ( ( (lv_inputType_0_0= ruleInputType ) ) (otherlv_1= ':' ( (lv_xPath_2_0= RULE_STRING ) ) )? )
            // InternalXMapping.g:1692:3: ( (lv_inputType_0_0= ruleInputType ) ) (otherlv_1= ':' ( (lv_xPath_2_0= RULE_STRING ) ) )?
            {
            // InternalXMapping.g:1692:3: ( (lv_inputType_0_0= ruleInputType ) )
            // InternalXMapping.g:1693:4: (lv_inputType_0_0= ruleInputType )
            {
            // InternalXMapping.g:1693:4: (lv_inputType_0_0= ruleInputType )
            // InternalXMapping.g:1694:5: lv_inputType_0_0= ruleInputType
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getInputAttributeAccess().getInputTypeInputTypeParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_19);
            lv_inputType_0_0=ruleInputType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getInputAttributeRule());
              					}
              					set(
              						current,
              						"inputType",
              						lv_inputType_0_0,
              						"com.gk_software.core.dsl.XMapping.InputType");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalXMapping.g:1711:3: (otherlv_1= ':' ( (lv_xPath_2_0= RULE_STRING ) ) )?
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==32) ) {
                alt23=1;
            }
            switch (alt23) {
                case 1 :
                    // InternalXMapping.g:1712:4: otherlv_1= ':' ( (lv_xPath_2_0= RULE_STRING ) )
                    {
                    otherlv_1=(Token)match(input,32,FOLLOW_20); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_1, grammarAccess.getInputAttributeAccess().getColonKeyword_1_0());
                      			
                    }
                    // InternalXMapping.g:1716:4: ( (lv_xPath_2_0= RULE_STRING ) )
                    // InternalXMapping.g:1717:5: (lv_xPath_2_0= RULE_STRING )
                    {
                    // InternalXMapping.g:1717:5: (lv_xPath_2_0= RULE_STRING )
                    // InternalXMapping.g:1718:6: lv_xPath_2_0= RULE_STRING
                    {
                    lv_xPath_2_0=(Token)match(input,RULE_STRING,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_xPath_2_0, grammarAccess.getInputAttributeAccess().getXPathSTRINGTerminalRuleCall_1_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getInputAttributeRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"xPath",
                      							lv_xPath_2_0,
                      							"org.eclipse.xtext.common.Terminals.STRING");
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInputAttribute"


    // $ANTLR start "entryRuleInputType"
    // InternalXMapping.g:1739:1: entryRuleInputType returns [EObject current=null] : iv_ruleInputType= ruleInputType EOF ;
    public final EObject entryRuleInputType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInputType = null;


        try {
            // InternalXMapping.g:1739:50: (iv_ruleInputType= ruleInputType EOF )
            // InternalXMapping.g:1740:2: iv_ruleInputType= ruleInputType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInputTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleInputType=ruleInputType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInputType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInputType"


    // $ANTLR start "ruleInputType"
    // InternalXMapping.g:1746:1: ruleInputType returns [EObject current=null] : ( ( ( ruleInputName ) ) | ( ( ruleParamName ) ) ) ;
    public final EObject ruleInputType() throws RecognitionException {
        EObject current = null;


        	enterRule();

        try {
            // InternalXMapping.g:1752:2: ( ( ( ( ruleInputName ) ) | ( ( ruleParamName ) ) ) )
            // InternalXMapping.g:1753:2: ( ( ( ruleInputName ) ) | ( ( ruleParamName ) ) )
            {
            // InternalXMapping.g:1753:2: ( ( ( ruleInputName ) ) | ( ( ruleParamName ) ) )
            int alt24=2;
            int LA24_0 = input.LA(1);

            if ( (LA24_0==RULE_INPUT_KEY) ) {
                alt24=1;
            }
            else if ( (LA24_0==RULE_PARAM_KEY||LA24_0==RULE_ID||LA24_0==22||(LA24_0>=24 && LA24_0<=25)||LA24_0==28||LA24_0==35) ) {
                alt24=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 24, 0, input);

                throw nvae;
            }
            switch (alt24) {
                case 1 :
                    // InternalXMapping.g:1754:3: ( ( ruleInputName ) )
                    {
                    // InternalXMapping.g:1754:3: ( ( ruleInputName ) )
                    // InternalXMapping.g:1755:4: ( ruleInputName )
                    {
                    // InternalXMapping.g:1755:4: ( ruleInputName )
                    // InternalXMapping.g:1756:5: ruleInputName
                    {
                    if ( state.backtracking==0 ) {

                      					/* */
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getInputTypeRule());
                      					}
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getInputTypeAccess().getSourceInputCrossReference_0_0());
                      				
                    }
                    pushFollow(FOLLOW_2);
                    ruleInputName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalXMapping.g:1774:3: ( ( ruleParamName ) )
                    {
                    // InternalXMapping.g:1774:3: ( ( ruleParamName ) )
                    // InternalXMapping.g:1775:4: ( ruleParamName )
                    {
                    // InternalXMapping.g:1775:4: ( ruleParamName )
                    // InternalXMapping.g:1776:5: ruleParamName
                    {
                    if ( state.backtracking==0 ) {

                      					/* */
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getInputTypeRule());
                      					}
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getInputTypeAccess().getSourceParamCrossReference_1_0());
                      				
                    }
                    pushFollow(FOLLOW_2);
                    ruleParamName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInputType"


    // $ANTLR start "entryRuleOutputAttribute"
    // InternalXMapping.g:1797:1: entryRuleOutputAttribute returns [EObject current=null] : iv_ruleOutputAttribute= ruleOutputAttribute EOF ;
    public final EObject entryRuleOutputAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOutputAttribute = null;


        try {
            // InternalXMapping.g:1797:56: (iv_ruleOutputAttribute= ruleOutputAttribute EOF )
            // InternalXMapping.g:1798:2: iv_ruleOutputAttribute= ruleOutputAttribute EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOutputAttributeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleOutputAttribute=ruleOutputAttribute();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOutputAttribute; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutputAttribute"


    // $ANTLR start "ruleOutputAttribute"
    // InternalXMapping.g:1804:1: ruleOutputAttribute returns [EObject current=null] : ( ( (lv_outputType_0_0= ruleOutputType ) ) (otherlv_1= ':' ( (lv_xPath_2_0= RULE_STRING ) ) )? ) ;
    public final EObject ruleOutputAttribute() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_xPath_2_0=null;
        EObject lv_outputType_0_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:1810:2: ( ( ( (lv_outputType_0_0= ruleOutputType ) ) (otherlv_1= ':' ( (lv_xPath_2_0= RULE_STRING ) ) )? ) )
            // InternalXMapping.g:1811:2: ( ( (lv_outputType_0_0= ruleOutputType ) ) (otherlv_1= ':' ( (lv_xPath_2_0= RULE_STRING ) ) )? )
            {
            // InternalXMapping.g:1811:2: ( ( (lv_outputType_0_0= ruleOutputType ) ) (otherlv_1= ':' ( (lv_xPath_2_0= RULE_STRING ) ) )? )
            // InternalXMapping.g:1812:3: ( (lv_outputType_0_0= ruleOutputType ) ) (otherlv_1= ':' ( (lv_xPath_2_0= RULE_STRING ) ) )?
            {
            // InternalXMapping.g:1812:3: ( (lv_outputType_0_0= ruleOutputType ) )
            // InternalXMapping.g:1813:4: (lv_outputType_0_0= ruleOutputType )
            {
            // InternalXMapping.g:1813:4: (lv_outputType_0_0= ruleOutputType )
            // InternalXMapping.g:1814:5: lv_outputType_0_0= ruleOutputType
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getOutputAttributeAccess().getOutputTypeOutputTypeParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_19);
            lv_outputType_0_0=ruleOutputType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getOutputAttributeRule());
              					}
              					set(
              						current,
              						"outputType",
              						lv_outputType_0_0,
              						"com.gk_software.core.dsl.XMapping.OutputType");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalXMapping.g:1831:3: (otherlv_1= ':' ( (lv_xPath_2_0= RULE_STRING ) ) )?
            int alt25=2;
            int LA25_0 = input.LA(1);

            if ( (LA25_0==32) ) {
                alt25=1;
            }
            switch (alt25) {
                case 1 :
                    // InternalXMapping.g:1832:4: otherlv_1= ':' ( (lv_xPath_2_0= RULE_STRING ) )
                    {
                    otherlv_1=(Token)match(input,32,FOLLOW_20); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_1, grammarAccess.getOutputAttributeAccess().getColonKeyword_1_0());
                      			
                    }
                    // InternalXMapping.g:1836:4: ( (lv_xPath_2_0= RULE_STRING ) )
                    // InternalXMapping.g:1837:5: (lv_xPath_2_0= RULE_STRING )
                    {
                    // InternalXMapping.g:1837:5: (lv_xPath_2_0= RULE_STRING )
                    // InternalXMapping.g:1838:6: lv_xPath_2_0= RULE_STRING
                    {
                    lv_xPath_2_0=(Token)match(input,RULE_STRING,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_xPath_2_0, grammarAccess.getOutputAttributeAccess().getXPathSTRINGTerminalRuleCall_1_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getOutputAttributeRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"xPath",
                      							lv_xPath_2_0,
                      							"org.eclipse.xtext.common.Terminals.STRING");
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutputAttribute"


    // $ANTLR start "entryRuleOutputType"
    // InternalXMapping.g:1859:1: entryRuleOutputType returns [EObject current=null] : iv_ruleOutputType= ruleOutputType EOF ;
    public final EObject entryRuleOutputType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOutputType = null;


        try {
            // InternalXMapping.g:1859:51: (iv_ruleOutputType= ruleOutputType EOF )
            // InternalXMapping.g:1860:2: iv_ruleOutputType= ruleOutputType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOutputTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleOutputType=ruleOutputType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOutputType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutputType"


    // $ANTLR start "ruleOutputType"
    // InternalXMapping.g:1866:1: ruleOutputType returns [EObject current=null] : ( ( ( ruleOutputName ) ) | ( ( ruleReturnName ) ) ) ;
    public final EObject ruleOutputType() throws RecognitionException {
        EObject current = null;


        	enterRule();

        try {
            // InternalXMapping.g:1872:2: ( ( ( ( ruleOutputName ) ) | ( ( ruleReturnName ) ) ) )
            // InternalXMapping.g:1873:2: ( ( ( ruleOutputName ) ) | ( ( ruleReturnName ) ) )
            {
            // InternalXMapping.g:1873:2: ( ( ( ruleOutputName ) ) | ( ( ruleReturnName ) ) )
            int alt26=2;
            alt26 = dfa26.predict(input);
            switch (alt26) {
                case 1 :
                    // InternalXMapping.g:1874:3: ( ( ruleOutputName ) )
                    {
                    // InternalXMapping.g:1874:3: ( ( ruleOutputName ) )
                    // InternalXMapping.g:1875:4: ( ruleOutputName )
                    {
                    // InternalXMapping.g:1875:4: ( ruleOutputName )
                    // InternalXMapping.g:1876:5: ruleOutputName
                    {
                    if ( state.backtracking==0 ) {

                      					/* */
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getOutputTypeRule());
                      					}
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getOutputTypeAccess().getSourceOutputCrossReference_0_0());
                      				
                    }
                    pushFollow(FOLLOW_2);
                    ruleOutputName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalXMapping.g:1894:3: ( ( ruleReturnName ) )
                    {
                    // InternalXMapping.g:1894:3: ( ( ruleReturnName ) )
                    // InternalXMapping.g:1895:4: ( ruleReturnName )
                    {
                    // InternalXMapping.g:1895:4: ( ruleReturnName )
                    // InternalXMapping.g:1896:5: ruleReturnName
                    {
                    if ( state.backtracking==0 ) {

                      					/* */
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getOutputTypeRule());
                      					}
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getOutputTypeAccess().getSourceReturnCrossReference_1_0());
                      				
                    }
                    pushFollow(FOLLOW_2);
                    ruleReturnName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutputType"


    // $ANTLR start "entryRuleAttribute"
    // InternalXMapping.g:1917:1: entryRuleAttribute returns [EObject current=null] : iv_ruleAttribute= ruleAttribute EOF ;
    public final EObject entryRuleAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttribute = null;


        try {
            // InternalXMapping.g:1917:50: (iv_ruleAttribute= ruleAttribute EOF )
            // InternalXMapping.g:1918:2: iv_ruleAttribute= ruleAttribute EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAttributeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAttribute=ruleAttribute();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAttribute; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttribute"


    // $ANTLR start "ruleAttribute"
    // InternalXMapping.g:1924:1: ruleAttribute returns [EObject current=null] : ( ( ( ruleVariableName ) ) (otherlv_1= ':' ( (lv_xPath_2_0= RULE_STRING ) ) )? ) ;
    public final EObject ruleAttribute() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_xPath_2_0=null;


        	enterRule();

        try {
            // InternalXMapping.g:1930:2: ( ( ( ( ruleVariableName ) ) (otherlv_1= ':' ( (lv_xPath_2_0= RULE_STRING ) ) )? ) )
            // InternalXMapping.g:1931:2: ( ( ( ruleVariableName ) ) (otherlv_1= ':' ( (lv_xPath_2_0= RULE_STRING ) ) )? )
            {
            // InternalXMapping.g:1931:2: ( ( ( ruleVariableName ) ) (otherlv_1= ':' ( (lv_xPath_2_0= RULE_STRING ) ) )? )
            // InternalXMapping.g:1932:3: ( ( ruleVariableName ) ) (otherlv_1= ':' ( (lv_xPath_2_0= RULE_STRING ) ) )?
            {
            // InternalXMapping.g:1932:3: ( ( ruleVariableName ) )
            // InternalXMapping.g:1933:4: ( ruleVariableName )
            {
            // InternalXMapping.g:1933:4: ( ruleVariableName )
            // InternalXMapping.g:1934:5: ruleVariableName
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getAttributeRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getAttributeAccess().getAttrTypeVariableCrossReference_0_0());
              				
            }
            pushFollow(FOLLOW_19);
            ruleVariableName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalXMapping.g:1951:3: (otherlv_1= ':' ( (lv_xPath_2_0= RULE_STRING ) ) )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==32) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalXMapping.g:1952:4: otherlv_1= ':' ( (lv_xPath_2_0= RULE_STRING ) )
                    {
                    otherlv_1=(Token)match(input,32,FOLLOW_20); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_1, grammarAccess.getAttributeAccess().getColonKeyword_1_0());
                      			
                    }
                    // InternalXMapping.g:1956:4: ( (lv_xPath_2_0= RULE_STRING ) )
                    // InternalXMapping.g:1957:5: (lv_xPath_2_0= RULE_STRING )
                    {
                    // InternalXMapping.g:1957:5: (lv_xPath_2_0= RULE_STRING )
                    // InternalXMapping.g:1958:6: lv_xPath_2_0= RULE_STRING
                    {
                    lv_xPath_2_0=(Token)match(input,RULE_STRING,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_xPath_2_0, grammarAccess.getAttributeAccess().getXPathSTRINGTerminalRuleCall_1_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getAttributeRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"xPath",
                      							lv_xPath_2_0,
                      							"org.eclipse.xtext.common.Terminals.STRING");
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttribute"


    // $ANTLR start "entryRuleInputName"
    // InternalXMapping.g:1979:1: entryRuleInputName returns [String current=null] : iv_ruleInputName= ruleInputName EOF ;
    public final String entryRuleInputName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleInputName = null;


        try {
            // InternalXMapping.g:1979:49: (iv_ruleInputName= ruleInputName EOF )
            // InternalXMapping.g:1980:2: iv_ruleInputName= ruleInputName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInputNameRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleInputName=ruleInputName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInputName.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInputName"


    // $ANTLR start "ruleInputName"
    // InternalXMapping.g:1986:1: ruleInputName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_INPUT_KEY_0= RULE_INPUT_KEY (kw= '.' this_ExtendedName_2= ruleExtendedName )? ) ;
    public final AntlrDatatypeRuleToken ruleInputName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INPUT_KEY_0=null;
        Token kw=null;
        AntlrDatatypeRuleToken this_ExtendedName_2 = null;



        	enterRule();

        try {
            // InternalXMapping.g:1992:2: ( (this_INPUT_KEY_0= RULE_INPUT_KEY (kw= '.' this_ExtendedName_2= ruleExtendedName )? ) )
            // InternalXMapping.g:1993:2: (this_INPUT_KEY_0= RULE_INPUT_KEY (kw= '.' this_ExtendedName_2= ruleExtendedName )? )
            {
            // InternalXMapping.g:1993:2: (this_INPUT_KEY_0= RULE_INPUT_KEY (kw= '.' this_ExtendedName_2= ruleExtendedName )? )
            // InternalXMapping.g:1994:3: this_INPUT_KEY_0= RULE_INPUT_KEY (kw= '.' this_ExtendedName_2= ruleExtendedName )?
            {
            this_INPUT_KEY_0=(Token)match(input,RULE_INPUT_KEY,FOLLOW_21); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_INPUT_KEY_0);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_INPUT_KEY_0, grammarAccess.getInputNameAccess().getINPUT_KEYTerminalRuleCall_0());
              		
            }
            // InternalXMapping.g:2001:3: (kw= '.' this_ExtendedName_2= ruleExtendedName )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==31) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // InternalXMapping.g:2002:4: kw= '.' this_ExtendedName_2= ruleExtendedName
                    {
                    kw=(Token)match(input,31,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(kw);
                      				newLeafNode(kw, grammarAccess.getInputNameAccess().getFullStopKeyword_1_0());
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				newCompositeNode(grammarAccess.getInputNameAccess().getExtendedNameParserRuleCall_1_1());
                      			
                    }
                    pushFollow(FOLLOW_2);
                    this_ExtendedName_2=ruleExtendedName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(this_ExtendedName_2);
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				afterParserOrEnumRuleCall();
                      			
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInputName"


    // $ANTLR start "entryRuleInput"
    // InternalXMapping.g:2022:1: entryRuleInput returns [EObject current=null] : iv_ruleInput= ruleInput EOF ;
    public final EObject entryRuleInput() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInput = null;


        try {
            // InternalXMapping.g:2022:46: (iv_ruleInput= ruleInput EOF )
            // InternalXMapping.g:2023:2: iv_ruleInput= ruleInput EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInputRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleInput=ruleInput();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInput; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInput"


    // $ANTLR start "ruleInput"
    // InternalXMapping.g:2029:1: ruleInput returns [EObject current=null] : ( (lv_name_0_0= ruleInputName ) ) ;
    public final EObject ruleInput() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_0_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2035:2: ( ( (lv_name_0_0= ruleInputName ) ) )
            // InternalXMapping.g:2036:2: ( (lv_name_0_0= ruleInputName ) )
            {
            // InternalXMapping.g:2036:2: ( (lv_name_0_0= ruleInputName ) )
            // InternalXMapping.g:2037:3: (lv_name_0_0= ruleInputName )
            {
            // InternalXMapping.g:2037:3: (lv_name_0_0= ruleInputName )
            // InternalXMapping.g:2038:4: lv_name_0_0= ruleInputName
            {
            if ( state.backtracking==0 ) {

              				newCompositeNode(grammarAccess.getInputAccess().getNameInputNameParserRuleCall_0());
              			
            }
            pushFollow(FOLLOW_2);
            lv_name_0_0=ruleInputName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				if (current==null) {
              					current = createModelElementForParent(grammarAccess.getInputRule());
              				}
              				set(
              					current,
              					"name",
              					lv_name_0_0,
              					"com.gk_software.core.dsl.XMapping.InputName");
              				afterParserOrEnumRuleCall();
              			
            }

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInput"


    // $ANTLR start "entryRuleOutputName"
    // InternalXMapping.g:2058:1: entryRuleOutputName returns [String current=null] : iv_ruleOutputName= ruleOutputName EOF ;
    public final String entryRuleOutputName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleOutputName = null;


        try {
            // InternalXMapping.g:2058:50: (iv_ruleOutputName= ruleOutputName EOF )
            // InternalXMapping.g:2059:2: iv_ruleOutputName= ruleOutputName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOutputNameRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleOutputName=ruleOutputName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOutputName.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutputName"


    // $ANTLR start "ruleOutputName"
    // InternalXMapping.g:2065:1: ruleOutputName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (this_ExtendedID_0= ruleExtendedID kw= '.' )? this_OUTPUT_KEY_2= RULE_OUTPUT_KEY (kw= '.' this_ExtendedName_4= ruleExtendedName )? ) ;
    public final AntlrDatatypeRuleToken ruleOutputName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_OUTPUT_KEY_2=null;
        AntlrDatatypeRuleToken this_ExtendedID_0 = null;

        AntlrDatatypeRuleToken this_ExtendedName_4 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2071:2: ( ( (this_ExtendedID_0= ruleExtendedID kw= '.' )? this_OUTPUT_KEY_2= RULE_OUTPUT_KEY (kw= '.' this_ExtendedName_4= ruleExtendedName )? ) )
            // InternalXMapping.g:2072:2: ( (this_ExtendedID_0= ruleExtendedID kw= '.' )? this_OUTPUT_KEY_2= RULE_OUTPUT_KEY (kw= '.' this_ExtendedName_4= ruleExtendedName )? )
            {
            // InternalXMapping.g:2072:2: ( (this_ExtendedID_0= ruleExtendedID kw= '.' )? this_OUTPUT_KEY_2= RULE_OUTPUT_KEY (kw= '.' this_ExtendedName_4= ruleExtendedName )? )
            // InternalXMapping.g:2073:3: (this_ExtendedID_0= ruleExtendedID kw= '.' )? this_OUTPUT_KEY_2= RULE_OUTPUT_KEY (kw= '.' this_ExtendedName_4= ruleExtendedName )?
            {
            // InternalXMapping.g:2073:3: (this_ExtendedID_0= ruleExtendedID kw= '.' )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==RULE_ID||LA29_0==22||(LA29_0>=24 && LA29_0<=25)||LA29_0==28||LA29_0==35) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalXMapping.g:2074:4: this_ExtendedID_0= ruleExtendedID kw= '.'
                    {
                    if ( state.backtracking==0 ) {

                      				newCompositeNode(grammarAccess.getOutputNameAccess().getExtendedIDParserRuleCall_0_0());
                      			
                    }
                    pushFollow(FOLLOW_18);
                    this_ExtendedID_0=ruleExtendedID();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(this_ExtendedID_0);
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				afterParserOrEnumRuleCall();
                      			
                    }
                    kw=(Token)match(input,31,FOLLOW_22); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(kw);
                      				newLeafNode(kw, grammarAccess.getOutputNameAccess().getFullStopKeyword_0_1());
                      			
                    }

                    }
                    break;

            }

            this_OUTPUT_KEY_2=(Token)match(input,RULE_OUTPUT_KEY,FOLLOW_21); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_OUTPUT_KEY_2);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_OUTPUT_KEY_2, grammarAccess.getOutputNameAccess().getOUTPUT_KEYTerminalRuleCall_1());
              		
            }
            // InternalXMapping.g:2097:3: (kw= '.' this_ExtendedName_4= ruleExtendedName )?
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==31) ) {
                alt30=1;
            }
            switch (alt30) {
                case 1 :
                    // InternalXMapping.g:2098:4: kw= '.' this_ExtendedName_4= ruleExtendedName
                    {
                    kw=(Token)match(input,31,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(kw);
                      				newLeafNode(kw, grammarAccess.getOutputNameAccess().getFullStopKeyword_2_0());
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				newCompositeNode(grammarAccess.getOutputNameAccess().getExtendedNameParserRuleCall_2_1());
                      			
                    }
                    pushFollow(FOLLOW_2);
                    this_ExtendedName_4=ruleExtendedName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(this_ExtendedName_4);
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				afterParserOrEnumRuleCall();
                      			
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutputName"


    // $ANTLR start "entryRuleOutput"
    // InternalXMapping.g:2118:1: entryRuleOutput returns [EObject current=null] : iv_ruleOutput= ruleOutput EOF ;
    public final EObject entryRuleOutput() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOutput = null;


        try {
            // InternalXMapping.g:2118:47: (iv_ruleOutput= ruleOutput EOF )
            // InternalXMapping.g:2119:2: iv_ruleOutput= ruleOutput EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOutputRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleOutput=ruleOutput();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOutput; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutput"


    // $ANTLR start "ruleOutput"
    // InternalXMapping.g:2125:1: ruleOutput returns [EObject current=null] : ( (lv_name_0_0= ruleOutputName ) ) ;
    public final EObject ruleOutput() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_0_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2131:2: ( ( (lv_name_0_0= ruleOutputName ) ) )
            // InternalXMapping.g:2132:2: ( (lv_name_0_0= ruleOutputName ) )
            {
            // InternalXMapping.g:2132:2: ( (lv_name_0_0= ruleOutputName ) )
            // InternalXMapping.g:2133:3: (lv_name_0_0= ruleOutputName )
            {
            // InternalXMapping.g:2133:3: (lv_name_0_0= ruleOutputName )
            // InternalXMapping.g:2134:4: lv_name_0_0= ruleOutputName
            {
            if ( state.backtracking==0 ) {

              				newCompositeNode(grammarAccess.getOutputAccess().getNameOutputNameParserRuleCall_0());
              			
            }
            pushFollow(FOLLOW_2);
            lv_name_0_0=ruleOutputName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				if (current==null) {
              					current = createModelElementForParent(grammarAccess.getOutputRule());
              				}
              				set(
              					current,
              					"name",
              					lv_name_0_0,
              					"com.gk_software.core.dsl.XMapping.OutputName");
              				afterParserOrEnumRuleCall();
              			
            }

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutput"


    // $ANTLR start "entryRuleVariableName"
    // InternalXMapping.g:2154:1: entryRuleVariableName returns [String current=null] : iv_ruleVariableName= ruleVariableName EOF ;
    public final String entryRuleVariableName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleVariableName = null;


        try {
            // InternalXMapping.g:2154:52: (iv_ruleVariableName= ruleVariableName EOF )
            // InternalXMapping.g:2155:2: iv_ruleVariableName= ruleVariableName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableNameRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleVariableName=ruleVariableName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableName.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableName"


    // $ANTLR start "ruleVariableName"
    // InternalXMapping.g:2161:1: ruleVariableName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (this_ExtendedID_0= ruleExtendedID kw= '.' )? this_VARIABLE_KEY_2= RULE_VARIABLE_KEY kw= '.' this_ExtendedName_4= ruleExtendedName ) ;
    public final AntlrDatatypeRuleToken ruleVariableName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_VARIABLE_KEY_2=null;
        AntlrDatatypeRuleToken this_ExtendedID_0 = null;

        AntlrDatatypeRuleToken this_ExtendedName_4 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2167:2: ( ( (this_ExtendedID_0= ruleExtendedID kw= '.' )? this_VARIABLE_KEY_2= RULE_VARIABLE_KEY kw= '.' this_ExtendedName_4= ruleExtendedName ) )
            // InternalXMapping.g:2168:2: ( (this_ExtendedID_0= ruleExtendedID kw= '.' )? this_VARIABLE_KEY_2= RULE_VARIABLE_KEY kw= '.' this_ExtendedName_4= ruleExtendedName )
            {
            // InternalXMapping.g:2168:2: ( (this_ExtendedID_0= ruleExtendedID kw= '.' )? this_VARIABLE_KEY_2= RULE_VARIABLE_KEY kw= '.' this_ExtendedName_4= ruleExtendedName )
            // InternalXMapping.g:2169:3: (this_ExtendedID_0= ruleExtendedID kw= '.' )? this_VARIABLE_KEY_2= RULE_VARIABLE_KEY kw= '.' this_ExtendedName_4= ruleExtendedName
            {
            // InternalXMapping.g:2169:3: (this_ExtendedID_0= ruleExtendedID kw= '.' )?
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==RULE_ID||LA31_0==22||(LA31_0>=24 && LA31_0<=25)||LA31_0==28||LA31_0==35) ) {
                alt31=1;
            }
            switch (alt31) {
                case 1 :
                    // InternalXMapping.g:2170:4: this_ExtendedID_0= ruleExtendedID kw= '.'
                    {
                    if ( state.backtracking==0 ) {

                      				newCompositeNode(grammarAccess.getVariableNameAccess().getExtendedIDParserRuleCall_0_0());
                      			
                    }
                    pushFollow(FOLLOW_18);
                    this_ExtendedID_0=ruleExtendedID();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(this_ExtendedID_0);
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				afterParserOrEnumRuleCall();
                      			
                    }
                    kw=(Token)match(input,31,FOLLOW_23); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(kw);
                      				newLeafNode(kw, grammarAccess.getVariableNameAccess().getFullStopKeyword_0_1());
                      			
                    }

                    }
                    break;

            }

            this_VARIABLE_KEY_2=(Token)match(input,RULE_VARIABLE_KEY,FOLLOW_18); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_VARIABLE_KEY_2);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_VARIABLE_KEY_2, grammarAccess.getVariableNameAccess().getVARIABLE_KEYTerminalRuleCall_1());
              		
            }
            kw=(Token)match(input,31,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(kw);
              			newLeafNode(kw, grammarAccess.getVariableNameAccess().getFullStopKeyword_2());
              		
            }
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getVariableNameAccess().getExtendedNameParserRuleCall_3());
              		
            }
            pushFollow(FOLLOW_2);
            this_ExtendedName_4=ruleExtendedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_ExtendedName_4);
              		
            }
            if ( state.backtracking==0 ) {

              			afterParserOrEnumRuleCall();
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableName"


    // $ANTLR start "entryRuleVariable"
    // InternalXMapping.g:2212:1: entryRuleVariable returns [EObject current=null] : iv_ruleVariable= ruleVariable EOF ;
    public final EObject entryRuleVariable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariable = null;


        try {
            // InternalXMapping.g:2212:49: (iv_ruleVariable= ruleVariable EOF )
            // InternalXMapping.g:2213:2: iv_ruleVariable= ruleVariable EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleVariable=ruleVariable();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariable; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariable"


    // $ANTLR start "ruleVariable"
    // InternalXMapping.g:2219:1: ruleVariable returns [EObject current=null] : ( (lv_name_0_0= ruleVariableName ) ) ;
    public final EObject ruleVariable() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_0_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2225:2: ( ( (lv_name_0_0= ruleVariableName ) ) )
            // InternalXMapping.g:2226:2: ( (lv_name_0_0= ruleVariableName ) )
            {
            // InternalXMapping.g:2226:2: ( (lv_name_0_0= ruleVariableName ) )
            // InternalXMapping.g:2227:3: (lv_name_0_0= ruleVariableName )
            {
            // InternalXMapping.g:2227:3: (lv_name_0_0= ruleVariableName )
            // InternalXMapping.g:2228:4: lv_name_0_0= ruleVariableName
            {
            if ( state.backtracking==0 ) {

              				newCompositeNode(grammarAccess.getVariableAccess().getNameVariableNameParserRuleCall_0());
              			
            }
            pushFollow(FOLLOW_2);
            lv_name_0_0=ruleVariableName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				if (current==null) {
              					current = createModelElementForParent(grammarAccess.getVariableRule());
              				}
              				set(
              					current,
              					"name",
              					lv_name_0_0,
              					"com.gk_software.core.dsl.XMapping.VariableName");
              				afterParserOrEnumRuleCall();
              			
            }

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariable"


    // $ANTLR start "entryRuleParamName"
    // InternalXMapping.g:2248:1: entryRuleParamName returns [String current=null] : iv_ruleParamName= ruleParamName EOF ;
    public final String entryRuleParamName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleParamName = null;


        try {
            // InternalXMapping.g:2248:49: (iv_ruleParamName= ruleParamName EOF )
            // InternalXMapping.g:2249:2: iv_ruleParamName= ruleParamName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getParamNameRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleParamName=ruleParamName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleParamName.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParamName"


    // $ANTLR start "ruleParamName"
    // InternalXMapping.g:2255:1: ruleParamName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (this_ExtendedID_0= ruleExtendedID kw= '.' )? this_PARAM_KEY_2= RULE_PARAM_KEY kw= '.' this_ExtendedName_4= ruleExtendedName ) ;
    public final AntlrDatatypeRuleToken ruleParamName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_PARAM_KEY_2=null;
        AntlrDatatypeRuleToken this_ExtendedID_0 = null;

        AntlrDatatypeRuleToken this_ExtendedName_4 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2261:2: ( ( (this_ExtendedID_0= ruleExtendedID kw= '.' )? this_PARAM_KEY_2= RULE_PARAM_KEY kw= '.' this_ExtendedName_4= ruleExtendedName ) )
            // InternalXMapping.g:2262:2: ( (this_ExtendedID_0= ruleExtendedID kw= '.' )? this_PARAM_KEY_2= RULE_PARAM_KEY kw= '.' this_ExtendedName_4= ruleExtendedName )
            {
            // InternalXMapping.g:2262:2: ( (this_ExtendedID_0= ruleExtendedID kw= '.' )? this_PARAM_KEY_2= RULE_PARAM_KEY kw= '.' this_ExtendedName_4= ruleExtendedName )
            // InternalXMapping.g:2263:3: (this_ExtendedID_0= ruleExtendedID kw= '.' )? this_PARAM_KEY_2= RULE_PARAM_KEY kw= '.' this_ExtendedName_4= ruleExtendedName
            {
            // InternalXMapping.g:2263:3: (this_ExtendedID_0= ruleExtendedID kw= '.' )?
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==RULE_ID||LA32_0==22||(LA32_0>=24 && LA32_0<=25)||LA32_0==28||LA32_0==35) ) {
                alt32=1;
            }
            switch (alt32) {
                case 1 :
                    // InternalXMapping.g:2264:4: this_ExtendedID_0= ruleExtendedID kw= '.'
                    {
                    if ( state.backtracking==0 ) {

                      				newCompositeNode(grammarAccess.getParamNameAccess().getExtendedIDParserRuleCall_0_0());
                      			
                    }
                    pushFollow(FOLLOW_18);
                    this_ExtendedID_0=ruleExtendedID();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(this_ExtendedID_0);
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				afterParserOrEnumRuleCall();
                      			
                    }
                    kw=(Token)match(input,31,FOLLOW_24); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(kw);
                      				newLeafNode(kw, grammarAccess.getParamNameAccess().getFullStopKeyword_0_1());
                      			
                    }

                    }
                    break;

            }

            this_PARAM_KEY_2=(Token)match(input,RULE_PARAM_KEY,FOLLOW_18); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_PARAM_KEY_2);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_PARAM_KEY_2, grammarAccess.getParamNameAccess().getPARAM_KEYTerminalRuleCall_1());
              		
            }
            kw=(Token)match(input,31,FOLLOW_4); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(kw);
              			newLeafNode(kw, grammarAccess.getParamNameAccess().getFullStopKeyword_2());
              		
            }
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getParamNameAccess().getExtendedNameParserRuleCall_3());
              		
            }
            pushFollow(FOLLOW_2);
            this_ExtendedName_4=ruleExtendedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_ExtendedName_4);
              		
            }
            if ( state.backtracking==0 ) {

              			afterParserOrEnumRuleCall();
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParamName"


    // $ANTLR start "entryRuleParam"
    // InternalXMapping.g:2306:1: entryRuleParam returns [EObject current=null] : iv_ruleParam= ruleParam EOF ;
    public final EObject entryRuleParam() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParam = null;


        try {
            // InternalXMapping.g:2306:46: (iv_ruleParam= ruleParam EOF )
            // InternalXMapping.g:2307:2: iv_ruleParam= ruleParam EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getParamRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleParam=ruleParam();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleParam; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParam"


    // $ANTLR start "ruleParam"
    // InternalXMapping.g:2313:1: ruleParam returns [EObject current=null] : ( (lv_name_0_0= ruleParamName ) ) ;
    public final EObject ruleParam() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_0_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2319:2: ( ( (lv_name_0_0= ruleParamName ) ) )
            // InternalXMapping.g:2320:2: ( (lv_name_0_0= ruleParamName ) )
            {
            // InternalXMapping.g:2320:2: ( (lv_name_0_0= ruleParamName ) )
            // InternalXMapping.g:2321:3: (lv_name_0_0= ruleParamName )
            {
            // InternalXMapping.g:2321:3: (lv_name_0_0= ruleParamName )
            // InternalXMapping.g:2322:4: lv_name_0_0= ruleParamName
            {
            if ( state.backtracking==0 ) {

              				newCompositeNode(grammarAccess.getParamAccess().getNameParamNameParserRuleCall_0());
              			
            }
            pushFollow(FOLLOW_2);
            lv_name_0_0=ruleParamName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				if (current==null) {
              					current = createModelElementForParent(grammarAccess.getParamRule());
              				}
              				set(
              					current,
              					"name",
              					lv_name_0_0,
              					"com.gk_software.core.dsl.XMapping.ParamName");
              				afterParserOrEnumRuleCall();
              			
            }

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParam"


    // $ANTLR start "entryRuleReturnName"
    // InternalXMapping.g:2342:1: entryRuleReturnName returns [String current=null] : iv_ruleReturnName= ruleReturnName EOF ;
    public final String entryRuleReturnName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleReturnName = null;


        try {
            // InternalXMapping.g:2342:50: (iv_ruleReturnName= ruleReturnName EOF )
            // InternalXMapping.g:2343:2: iv_ruleReturnName= ruleReturnName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getReturnNameRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleReturnName=ruleReturnName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleReturnName.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReturnName"


    // $ANTLR start "ruleReturnName"
    // InternalXMapping.g:2349:1: ruleReturnName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (this_ExtendedID_0= ruleExtendedID kw= '.' )? this_RETURN_KEY_2= RULE_RETURN_KEY kw= '.' this_INT_4= RULE_INT ) ;
    public final AntlrDatatypeRuleToken ruleReturnName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_RETURN_KEY_2=null;
        Token this_INT_4=null;
        AntlrDatatypeRuleToken this_ExtendedID_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2355:2: ( ( (this_ExtendedID_0= ruleExtendedID kw= '.' )? this_RETURN_KEY_2= RULE_RETURN_KEY kw= '.' this_INT_4= RULE_INT ) )
            // InternalXMapping.g:2356:2: ( (this_ExtendedID_0= ruleExtendedID kw= '.' )? this_RETURN_KEY_2= RULE_RETURN_KEY kw= '.' this_INT_4= RULE_INT )
            {
            // InternalXMapping.g:2356:2: ( (this_ExtendedID_0= ruleExtendedID kw= '.' )? this_RETURN_KEY_2= RULE_RETURN_KEY kw= '.' this_INT_4= RULE_INT )
            // InternalXMapping.g:2357:3: (this_ExtendedID_0= ruleExtendedID kw= '.' )? this_RETURN_KEY_2= RULE_RETURN_KEY kw= '.' this_INT_4= RULE_INT
            {
            // InternalXMapping.g:2357:3: (this_ExtendedID_0= ruleExtendedID kw= '.' )?
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==RULE_ID||LA33_0==22||(LA33_0>=24 && LA33_0<=25)||LA33_0==28||LA33_0==35) ) {
                alt33=1;
            }
            switch (alt33) {
                case 1 :
                    // InternalXMapping.g:2358:4: this_ExtendedID_0= ruleExtendedID kw= '.'
                    {
                    if ( state.backtracking==0 ) {

                      				newCompositeNode(grammarAccess.getReturnNameAccess().getExtendedIDParserRuleCall_0_0());
                      			
                    }
                    pushFollow(FOLLOW_18);
                    this_ExtendedID_0=ruleExtendedID();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(this_ExtendedID_0);
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				afterParserOrEnumRuleCall();
                      			
                    }
                    kw=(Token)match(input,31,FOLLOW_25); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(kw);
                      				newLeafNode(kw, grammarAccess.getReturnNameAccess().getFullStopKeyword_0_1());
                      			
                    }

                    }
                    break;

            }

            this_RETURN_KEY_2=(Token)match(input,RULE_RETURN_KEY,FOLLOW_18); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_RETURN_KEY_2);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_RETURN_KEY_2, grammarAccess.getReturnNameAccess().getRETURN_KEYTerminalRuleCall_1());
              		
            }
            kw=(Token)match(input,31,FOLLOW_26); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(kw);
              			newLeafNode(kw, grammarAccess.getReturnNameAccess().getFullStopKeyword_2());
              		
            }
            this_INT_4=(Token)match(input,RULE_INT,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_INT_4);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_INT_4, grammarAccess.getReturnNameAccess().getINTTerminalRuleCall_3());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReturnName"


    // $ANTLR start "entryRuleReturn"
    // InternalXMapping.g:2397:1: entryRuleReturn returns [EObject current=null] : iv_ruleReturn= ruleReturn EOF ;
    public final EObject entryRuleReturn() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReturn = null;


        try {
            // InternalXMapping.g:2397:47: (iv_ruleReturn= ruleReturn EOF )
            // InternalXMapping.g:2398:2: iv_ruleReturn= ruleReturn EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getReturnRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleReturn=ruleReturn();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleReturn; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReturn"


    // $ANTLR start "ruleReturn"
    // InternalXMapping.g:2404:1: ruleReturn returns [EObject current=null] : ( (lv_name_0_0= ruleReturnName ) ) ;
    public final EObject ruleReturn() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_0_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2410:2: ( ( (lv_name_0_0= ruleReturnName ) ) )
            // InternalXMapping.g:2411:2: ( (lv_name_0_0= ruleReturnName ) )
            {
            // InternalXMapping.g:2411:2: ( (lv_name_0_0= ruleReturnName ) )
            // InternalXMapping.g:2412:3: (lv_name_0_0= ruleReturnName )
            {
            // InternalXMapping.g:2412:3: (lv_name_0_0= ruleReturnName )
            // InternalXMapping.g:2413:4: lv_name_0_0= ruleReturnName
            {
            if ( state.backtracking==0 ) {

              				newCompositeNode(grammarAccess.getReturnAccess().getNameReturnNameParserRuleCall_0());
              			
            }
            pushFollow(FOLLOW_2);
            lv_name_0_0=ruleReturnName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				if (current==null) {
              					current = createModelElementForParent(grammarAccess.getReturnRule());
              				}
              				set(
              					current,
              					"name",
              					lv_name_0_0,
              					"com.gk_software.core.dsl.XMapping.ReturnName");
              				afterParserOrEnumRuleCall();
              			
            }

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReturn"


    // $ANTLR start "entryRuleMacroName"
    // InternalXMapping.g:2433:1: entryRuleMacroName returns [String current=null] : iv_ruleMacroName= ruleMacroName EOF ;
    public final String entryRuleMacroName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleMacroName = null;


        try {
            // InternalXMapping.g:2433:49: (iv_ruleMacroName= ruleMacroName EOF )
            // InternalXMapping.g:2434:2: iv_ruleMacroName= ruleMacroName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMacroNameRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleMacroName=ruleMacroName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMacroName.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMacroName"


    // $ANTLR start "ruleMacroName"
    // InternalXMapping.g:2440:1: ruleMacroName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_MACRO_KEY_0= RULE_MACRO_KEY (kw= '.' this_ExtendedName_2= ruleExtendedName )? ) ;
    public final AntlrDatatypeRuleToken ruleMacroName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_MACRO_KEY_0=null;
        Token kw=null;
        AntlrDatatypeRuleToken this_ExtendedName_2 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2446:2: ( (this_MACRO_KEY_0= RULE_MACRO_KEY (kw= '.' this_ExtendedName_2= ruleExtendedName )? ) )
            // InternalXMapping.g:2447:2: (this_MACRO_KEY_0= RULE_MACRO_KEY (kw= '.' this_ExtendedName_2= ruleExtendedName )? )
            {
            // InternalXMapping.g:2447:2: (this_MACRO_KEY_0= RULE_MACRO_KEY (kw= '.' this_ExtendedName_2= ruleExtendedName )? )
            // InternalXMapping.g:2448:3: this_MACRO_KEY_0= RULE_MACRO_KEY (kw= '.' this_ExtendedName_2= ruleExtendedName )?
            {
            this_MACRO_KEY_0=(Token)match(input,RULE_MACRO_KEY,FOLLOW_21); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_MACRO_KEY_0);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_MACRO_KEY_0, grammarAccess.getMacroNameAccess().getMACRO_KEYTerminalRuleCall_0());
              		
            }
            // InternalXMapping.g:2455:3: (kw= '.' this_ExtendedName_2= ruleExtendedName )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==31) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // InternalXMapping.g:2456:4: kw= '.' this_ExtendedName_2= ruleExtendedName
                    {
                    kw=(Token)match(input,31,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(kw);
                      				newLeafNode(kw, grammarAccess.getMacroNameAccess().getFullStopKeyword_1_0());
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				newCompositeNode(grammarAccess.getMacroNameAccess().getExtendedNameParserRuleCall_1_1());
                      			
                    }
                    pushFollow(FOLLOW_2);
                    this_ExtendedName_2=ruleExtendedName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(this_ExtendedName_2);
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				afterParserOrEnumRuleCall();
                      			
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMacroName"


    // $ANTLR start "entryRuleMacro"
    // InternalXMapping.g:2476:1: entryRuleMacro returns [EObject current=null] : iv_ruleMacro= ruleMacro EOF ;
    public final EObject entryRuleMacro() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMacro = null;


        try {
            // InternalXMapping.g:2476:46: (iv_ruleMacro= ruleMacro EOF )
            // InternalXMapping.g:2477:2: iv_ruleMacro= ruleMacro EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMacroRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleMacro=ruleMacro();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMacro; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMacro"


    // $ANTLR start "ruleMacro"
    // InternalXMapping.g:2483:1: ruleMacro returns [EObject current=null] : ( () ( (lv_name_1_0= ruleMacroName ) ) ) ;
    public final EObject ruleMacro() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2489:2: ( ( () ( (lv_name_1_0= ruleMacroName ) ) ) )
            // InternalXMapping.g:2490:2: ( () ( (lv_name_1_0= ruleMacroName ) ) )
            {
            // InternalXMapping.g:2490:2: ( () ( (lv_name_1_0= ruleMacroName ) ) )
            // InternalXMapping.g:2491:3: () ( (lv_name_1_0= ruleMacroName ) )
            {
            // InternalXMapping.g:2491:3: ()
            // InternalXMapping.g:2492:4: 
            {
            if ( state.backtracking==0 ) {

              				/* */
              			
            }
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getMacroAccess().getMacroAction_0(),
              					current);
              			
            }

            }

            // InternalXMapping.g:2501:3: ( (lv_name_1_0= ruleMacroName ) )
            // InternalXMapping.g:2502:4: (lv_name_1_0= ruleMacroName )
            {
            // InternalXMapping.g:2502:4: (lv_name_1_0= ruleMacroName )
            // InternalXMapping.g:2503:5: lv_name_1_0= ruleMacroName
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getMacroAccess().getNameMacroNameParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_name_1_0=ruleMacroName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getMacroRule());
              					}
              					set(
              						current,
              						"name",
              						lv_name_1_0,
              						"com.gk_software.core.dsl.XMapping.MacroName");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMacro"


    // $ANTLR start "entryRuleFilterName"
    // InternalXMapping.g:2524:1: entryRuleFilterName returns [String current=null] : iv_ruleFilterName= ruleFilterName EOF ;
    public final String entryRuleFilterName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleFilterName = null;


        try {
            // InternalXMapping.g:2524:50: (iv_ruleFilterName= ruleFilterName EOF )
            // InternalXMapping.g:2525:2: iv_ruleFilterName= ruleFilterName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFilterNameRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleFilterName=ruleFilterName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFilterName.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFilterName"


    // $ANTLR start "ruleFilterName"
    // InternalXMapping.g:2531:1: ruleFilterName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_FILTER_KEY_0= RULE_FILTER_KEY (kw= '.' this_ExtendedName_2= ruleExtendedName )? ) ;
    public final AntlrDatatypeRuleToken ruleFilterName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_FILTER_KEY_0=null;
        Token kw=null;
        AntlrDatatypeRuleToken this_ExtendedName_2 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2537:2: ( (this_FILTER_KEY_0= RULE_FILTER_KEY (kw= '.' this_ExtendedName_2= ruleExtendedName )? ) )
            // InternalXMapping.g:2538:2: (this_FILTER_KEY_0= RULE_FILTER_KEY (kw= '.' this_ExtendedName_2= ruleExtendedName )? )
            {
            // InternalXMapping.g:2538:2: (this_FILTER_KEY_0= RULE_FILTER_KEY (kw= '.' this_ExtendedName_2= ruleExtendedName )? )
            // InternalXMapping.g:2539:3: this_FILTER_KEY_0= RULE_FILTER_KEY (kw= '.' this_ExtendedName_2= ruleExtendedName )?
            {
            this_FILTER_KEY_0=(Token)match(input,RULE_FILTER_KEY,FOLLOW_21); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_FILTER_KEY_0);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_FILTER_KEY_0, grammarAccess.getFilterNameAccess().getFILTER_KEYTerminalRuleCall_0());
              		
            }
            // InternalXMapping.g:2546:3: (kw= '.' this_ExtendedName_2= ruleExtendedName )?
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==31) ) {
                alt35=1;
            }
            switch (alt35) {
                case 1 :
                    // InternalXMapping.g:2547:4: kw= '.' this_ExtendedName_2= ruleExtendedName
                    {
                    kw=(Token)match(input,31,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(kw);
                      				newLeafNode(kw, grammarAccess.getFilterNameAccess().getFullStopKeyword_1_0());
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				newCompositeNode(grammarAccess.getFilterNameAccess().getExtendedNameParserRuleCall_1_1());
                      			
                    }
                    pushFollow(FOLLOW_2);
                    this_ExtendedName_2=ruleExtendedName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(this_ExtendedName_2);
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				afterParserOrEnumRuleCall();
                      			
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFilterName"


    // $ANTLR start "entryRuleFilter"
    // InternalXMapping.g:2567:1: entryRuleFilter returns [EObject current=null] : iv_ruleFilter= ruleFilter EOF ;
    public final EObject entryRuleFilter() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFilter = null;


        try {
            // InternalXMapping.g:2567:47: (iv_ruleFilter= ruleFilter EOF )
            // InternalXMapping.g:2568:2: iv_ruleFilter= ruleFilter EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFilterRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleFilter=ruleFilter();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFilter; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFilter"


    // $ANTLR start "ruleFilter"
    // InternalXMapping.g:2574:1: ruleFilter returns [EObject current=null] : ( () ( (lv_name_1_0= ruleFilterName ) ) ) ;
    public final EObject ruleFilter() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2580:2: ( ( () ( (lv_name_1_0= ruleFilterName ) ) ) )
            // InternalXMapping.g:2581:2: ( () ( (lv_name_1_0= ruleFilterName ) ) )
            {
            // InternalXMapping.g:2581:2: ( () ( (lv_name_1_0= ruleFilterName ) ) )
            // InternalXMapping.g:2582:3: () ( (lv_name_1_0= ruleFilterName ) )
            {
            // InternalXMapping.g:2582:3: ()
            // InternalXMapping.g:2583:4: 
            {
            if ( state.backtracking==0 ) {

              				/* */
              			
            }
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getFilterAccess().getFilterAction_0(),
              					current);
              			
            }

            }

            // InternalXMapping.g:2592:3: ( (lv_name_1_0= ruleFilterName ) )
            // InternalXMapping.g:2593:4: (lv_name_1_0= ruleFilterName )
            {
            // InternalXMapping.g:2593:4: (lv_name_1_0= ruleFilterName )
            // InternalXMapping.g:2594:5: lv_name_1_0= ruleFilterName
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getFilterAccess().getNameFilterNameParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_name_1_0=ruleFilterName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getFilterRule());
              					}
              					set(
              						current,
              						"name",
              						lv_name_1_0,
              						"com.gk_software.core.dsl.XMapping.FilterName");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFilter"


    // $ANTLR start "entryRuleFuctionName"
    // InternalXMapping.g:2615:1: entryRuleFuctionName returns [String current=null] : iv_ruleFuctionName= ruleFuctionName EOF ;
    public final String entryRuleFuctionName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleFuctionName = null;


        try {
            // InternalXMapping.g:2615:51: (iv_ruleFuctionName= ruleFuctionName EOF )
            // InternalXMapping.g:2616:2: iv_ruleFuctionName= ruleFuctionName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFuctionNameRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleFuctionName=ruleFuctionName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFuctionName.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFuctionName"


    // $ANTLR start "ruleFuctionName"
    // InternalXMapping.g:2622:1: ruleFuctionName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_FUNCTION_KEY_0= RULE_FUNCTION_KEY (kw= '.' this_ExtendedName_2= ruleExtendedName )? ) ;
    public final AntlrDatatypeRuleToken ruleFuctionName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_FUNCTION_KEY_0=null;
        Token kw=null;
        AntlrDatatypeRuleToken this_ExtendedName_2 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2628:2: ( (this_FUNCTION_KEY_0= RULE_FUNCTION_KEY (kw= '.' this_ExtendedName_2= ruleExtendedName )? ) )
            // InternalXMapping.g:2629:2: (this_FUNCTION_KEY_0= RULE_FUNCTION_KEY (kw= '.' this_ExtendedName_2= ruleExtendedName )? )
            {
            // InternalXMapping.g:2629:2: (this_FUNCTION_KEY_0= RULE_FUNCTION_KEY (kw= '.' this_ExtendedName_2= ruleExtendedName )? )
            // InternalXMapping.g:2630:3: this_FUNCTION_KEY_0= RULE_FUNCTION_KEY (kw= '.' this_ExtendedName_2= ruleExtendedName )?
            {
            this_FUNCTION_KEY_0=(Token)match(input,RULE_FUNCTION_KEY,FOLLOW_21); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_FUNCTION_KEY_0);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_FUNCTION_KEY_0, grammarAccess.getFuctionNameAccess().getFUNCTION_KEYTerminalRuleCall_0());
              		
            }
            // InternalXMapping.g:2637:3: (kw= '.' this_ExtendedName_2= ruleExtendedName )?
            int alt36=2;
            int LA36_0 = input.LA(1);

            if ( (LA36_0==31) ) {
                alt36=1;
            }
            switch (alt36) {
                case 1 :
                    // InternalXMapping.g:2638:4: kw= '.' this_ExtendedName_2= ruleExtendedName
                    {
                    kw=(Token)match(input,31,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(kw);
                      				newLeafNode(kw, grammarAccess.getFuctionNameAccess().getFullStopKeyword_1_0());
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				newCompositeNode(grammarAccess.getFuctionNameAccess().getExtendedNameParserRuleCall_1_1());
                      			
                    }
                    pushFollow(FOLLOW_2);
                    this_ExtendedName_2=ruleExtendedName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(this_ExtendedName_2);
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				afterParserOrEnumRuleCall();
                      			
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFuctionName"


    // $ANTLR start "entryRuleFunction"
    // InternalXMapping.g:2658:1: entryRuleFunction returns [EObject current=null] : iv_ruleFunction= ruleFunction EOF ;
    public final EObject entryRuleFunction() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFunction = null;


        try {
            // InternalXMapping.g:2658:49: (iv_ruleFunction= ruleFunction EOF )
            // InternalXMapping.g:2659:2: iv_ruleFunction= ruleFunction EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFunctionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleFunction=ruleFunction();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFunction; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFunction"


    // $ANTLR start "ruleFunction"
    // InternalXMapping.g:2665:1: ruleFunction returns [EObject current=null] : ( () ( (lv_name_1_0= ruleFuctionName ) ) ) ;
    public final EObject ruleFunction() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2671:2: ( ( () ( (lv_name_1_0= ruleFuctionName ) ) ) )
            // InternalXMapping.g:2672:2: ( () ( (lv_name_1_0= ruleFuctionName ) ) )
            {
            // InternalXMapping.g:2672:2: ( () ( (lv_name_1_0= ruleFuctionName ) ) )
            // InternalXMapping.g:2673:3: () ( (lv_name_1_0= ruleFuctionName ) )
            {
            // InternalXMapping.g:2673:3: ()
            // InternalXMapping.g:2674:4: 
            {
            if ( state.backtracking==0 ) {

              				/* */
              			
            }
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getFunctionAccess().getFunctionAction_0(),
              					current);
              			
            }

            }

            // InternalXMapping.g:2683:3: ( (lv_name_1_0= ruleFuctionName ) )
            // InternalXMapping.g:2684:4: (lv_name_1_0= ruleFuctionName )
            {
            // InternalXMapping.g:2684:4: (lv_name_1_0= ruleFuctionName )
            // InternalXMapping.g:2685:5: lv_name_1_0= ruleFuctionName
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getFunctionAccess().getNameFuctionNameParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_name_1_0=ruleFuctionName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getFunctionRule());
              					}
              					set(
              						current,
              						"name",
              						lv_name_1_0,
              						"com.gk_software.core.dsl.XMapping.FuctionName");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFunction"


    // $ANTLR start "entryRuleMapperName"
    // InternalXMapping.g:2706:1: entryRuleMapperName returns [String current=null] : iv_ruleMapperName= ruleMapperName EOF ;
    public final String entryRuleMapperName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleMapperName = null;


        try {
            // InternalXMapping.g:2706:50: (iv_ruleMapperName= ruleMapperName EOF )
            // InternalXMapping.g:2707:2: iv_ruleMapperName= ruleMapperName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMapperNameRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleMapperName=ruleMapperName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMapperName.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMapperName"


    // $ANTLR start "ruleMapperName"
    // InternalXMapping.g:2713:1: ruleMapperName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_MAPPER_KEY_0= RULE_MAPPER_KEY (kw= '.' this_ExtendedName_2= ruleExtendedName )? ) ;
    public final AntlrDatatypeRuleToken ruleMapperName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_MAPPER_KEY_0=null;
        Token kw=null;
        AntlrDatatypeRuleToken this_ExtendedName_2 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2719:2: ( (this_MAPPER_KEY_0= RULE_MAPPER_KEY (kw= '.' this_ExtendedName_2= ruleExtendedName )? ) )
            // InternalXMapping.g:2720:2: (this_MAPPER_KEY_0= RULE_MAPPER_KEY (kw= '.' this_ExtendedName_2= ruleExtendedName )? )
            {
            // InternalXMapping.g:2720:2: (this_MAPPER_KEY_0= RULE_MAPPER_KEY (kw= '.' this_ExtendedName_2= ruleExtendedName )? )
            // InternalXMapping.g:2721:3: this_MAPPER_KEY_0= RULE_MAPPER_KEY (kw= '.' this_ExtendedName_2= ruleExtendedName )?
            {
            this_MAPPER_KEY_0=(Token)match(input,RULE_MAPPER_KEY,FOLLOW_21); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_MAPPER_KEY_0);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_MAPPER_KEY_0, grammarAccess.getMapperNameAccess().getMAPPER_KEYTerminalRuleCall_0());
              		
            }
            // InternalXMapping.g:2728:3: (kw= '.' this_ExtendedName_2= ruleExtendedName )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==31) ) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // InternalXMapping.g:2729:4: kw= '.' this_ExtendedName_2= ruleExtendedName
                    {
                    kw=(Token)match(input,31,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(kw);
                      				newLeafNode(kw, grammarAccess.getMapperNameAccess().getFullStopKeyword_1_0());
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				newCompositeNode(grammarAccess.getMapperNameAccess().getExtendedNameParserRuleCall_1_1());
                      			
                    }
                    pushFollow(FOLLOW_2);
                    this_ExtendedName_2=ruleExtendedName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(this_ExtendedName_2);
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				afterParserOrEnumRuleCall();
                      			
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMapperName"


    // $ANTLR start "entryRuleMapper"
    // InternalXMapping.g:2749:1: entryRuleMapper returns [EObject current=null] : iv_ruleMapper= ruleMapper EOF ;
    public final EObject entryRuleMapper() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMapper = null;


        try {
            // InternalXMapping.g:2749:47: (iv_ruleMapper= ruleMapper EOF )
            // InternalXMapping.g:2750:2: iv_ruleMapper= ruleMapper EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMapperRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleMapper=ruleMapper();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMapper; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMapper"


    // $ANTLR start "ruleMapper"
    // InternalXMapping.g:2756:1: ruleMapper returns [EObject current=null] : ( () ( (lv_name_1_0= ruleMapperName ) ) ) ;
    public final EObject ruleMapper() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_1_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2762:2: ( ( () ( (lv_name_1_0= ruleMapperName ) ) ) )
            // InternalXMapping.g:2763:2: ( () ( (lv_name_1_0= ruleMapperName ) ) )
            {
            // InternalXMapping.g:2763:2: ( () ( (lv_name_1_0= ruleMapperName ) ) )
            // InternalXMapping.g:2764:3: () ( (lv_name_1_0= ruleMapperName ) )
            {
            // InternalXMapping.g:2764:3: ()
            // InternalXMapping.g:2765:4: 
            {
            if ( state.backtracking==0 ) {

              				/* */
              			
            }
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getMapperAccess().getMapperAction_0(),
              					current);
              			
            }

            }

            // InternalXMapping.g:2774:3: ( (lv_name_1_0= ruleMapperName ) )
            // InternalXMapping.g:2775:4: (lv_name_1_0= ruleMapperName )
            {
            // InternalXMapping.g:2775:4: (lv_name_1_0= ruleMapperName )
            // InternalXMapping.g:2776:5: lv_name_1_0= ruleMapperName
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getMapperAccess().getNameMapperNameParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_name_1_0=ruleMapperName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getMapperRule());
              					}
              					set(
              						current,
              						"name",
              						lv_name_1_0,
              						"com.gk_software.core.dsl.XMapping.MapperName");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMapper"


    // $ANTLR start "entryRuleStringPath"
    // InternalXMapping.g:2797:1: entryRuleStringPath returns [String current=null] : iv_ruleStringPath= ruleStringPath EOF ;
    public final String entryRuleStringPath() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleStringPath = null;


        try {
            // InternalXMapping.g:2797:50: (iv_ruleStringPath= ruleStringPath EOF )
            // InternalXMapping.g:2798:2: iv_ruleStringPath= ruleStringPath EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStringPathRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleStringPath=ruleStringPath();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStringPath.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStringPath"


    // $ANTLR start "ruleStringPath"
    // InternalXMapping.g:2804:1: ruleStringPath returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING ( (kw= '.' | kw= '-' )+ this_STRING_3= RULE_STRING )* ) ;
    public final AntlrDatatypeRuleToken ruleStringPath() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token kw=null;
        Token this_STRING_3=null;


        	enterRule();

        try {
            // InternalXMapping.g:2810:2: ( (this_STRING_0= RULE_STRING ( (kw= '.' | kw= '-' )+ this_STRING_3= RULE_STRING )* ) )
            // InternalXMapping.g:2811:2: (this_STRING_0= RULE_STRING ( (kw= '.' | kw= '-' )+ this_STRING_3= RULE_STRING )* )
            {
            // InternalXMapping.g:2811:2: (this_STRING_0= RULE_STRING ( (kw= '.' | kw= '-' )+ this_STRING_3= RULE_STRING )* )
            // InternalXMapping.g:2812:3: this_STRING_0= RULE_STRING ( (kw= '.' | kw= '-' )+ this_STRING_3= RULE_STRING )*
            {
            this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_27); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_STRING_0);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_STRING_0, grammarAccess.getStringPathAccess().getSTRINGTerminalRuleCall_0());
              		
            }
            // InternalXMapping.g:2819:3: ( (kw= '.' | kw= '-' )+ this_STRING_3= RULE_STRING )*
            loop39:
            do {
                int alt39=2;
                int LA39_0 = input.LA(1);

                if ( (LA39_0==31||LA39_0==33) ) {
                    alt39=1;
                }


                switch (alt39) {
            	case 1 :
            	    // InternalXMapping.g:2820:4: (kw= '.' | kw= '-' )+ this_STRING_3= RULE_STRING
            	    {
            	    // InternalXMapping.g:2820:4: (kw= '.' | kw= '-' )+
            	    int cnt38=0;
            	    loop38:
            	    do {
            	        int alt38=3;
            	        int LA38_0 = input.LA(1);

            	        if ( (LA38_0==31) ) {
            	            alt38=1;
            	        }
            	        else if ( (LA38_0==33) ) {
            	            alt38=2;
            	        }


            	        switch (alt38) {
            	    	case 1 :
            	    	    // InternalXMapping.g:2821:5: kw= '.'
            	    	    {
            	    	    kw=(Token)match(input,31,FOLLOW_28); if (state.failed) return current;
            	    	    if ( state.backtracking==0 ) {

            	    	      					current.merge(kw);
            	    	      					newLeafNode(kw, grammarAccess.getStringPathAccess().getFullStopKeyword_1_0_0());
            	    	      				
            	    	    }

            	    	    }
            	    	    break;
            	    	case 2 :
            	    	    // InternalXMapping.g:2827:5: kw= '-'
            	    	    {
            	    	    kw=(Token)match(input,33,FOLLOW_28); if (state.failed) return current;
            	    	    if ( state.backtracking==0 ) {

            	    	      					current.merge(kw);
            	    	      					newLeafNode(kw, grammarAccess.getStringPathAccess().getHyphenMinusKeyword_1_0_1());
            	    	      				
            	    	    }

            	    	    }
            	    	    break;

            	    	default :
            	    	    if ( cnt38 >= 1 ) break loop38;
            	    	    if (state.backtracking>0) {state.failed=true; return current;}
            	                EarlyExitException eee =
            	                    new EarlyExitException(38, input);
            	                throw eee;
            	        }
            	        cnt38++;
            	    } while (true);

            	    this_STRING_3=(Token)match(input,RULE_STRING,FOLLOW_27); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				current.merge(this_STRING_3);
            	      			
            	    }
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(this_STRING_3, grammarAccess.getStringPathAccess().getSTRINGTerminalRuleCall_1_1());
            	      			
            	    }

            	    }
            	    break;

            	default :
            	    break loop39;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStringPath"


    // $ANTLR start "entryRuleQualifiedNameWithWildcard"
    // InternalXMapping.g:2845:1: entryRuleQualifiedNameWithWildcard returns [String current=null] : iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF ;
    public final String entryRuleQualifiedNameWithWildcard() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedNameWithWildcard = null;


        try {
            // InternalXMapping.g:2845:65: (iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF )
            // InternalXMapping.g:2846:2: iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getQualifiedNameWithWildcardRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedNameWithWildcard=ruleQualifiedNameWithWildcard();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleQualifiedNameWithWildcard.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedNameWithWildcard"


    // $ANTLR start "ruleQualifiedNameWithWildcard"
    // InternalXMapping.g:2852:1: ruleQualifiedNameWithWildcard returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedNameWithWildcard() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_QualifiedName_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2858:2: ( (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? ) )
            // InternalXMapping.g:2859:2: (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? )
            {
            // InternalXMapping.g:2859:2: (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? )
            // InternalXMapping.g:2860:3: this_QualifiedName_0= ruleQualifiedName (kw= '.*' )?
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_29);
            this_QualifiedName_0=ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_QualifiedName_0);
              		
            }
            if ( state.backtracking==0 ) {

              			afterParserOrEnumRuleCall();
              		
            }
            // InternalXMapping.g:2870:3: (kw= '.*' )?
            int alt40=2;
            int LA40_0 = input.LA(1);

            if ( (LA40_0==34) ) {
                alt40=1;
            }
            switch (alt40) {
                case 1 :
                    // InternalXMapping.g:2871:4: kw= '.*'
                    {
                    kw=(Token)match(input,34,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(kw);
                      				newLeafNode(kw, grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1());
                      			
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedNameWithWildcard"


    // $ANTLR start "entryRuleExtendedID"
    // InternalXMapping.g:2881:1: entryRuleExtendedID returns [String current=null] : iv_ruleExtendedID= ruleExtendedID EOF ;
    public final String entryRuleExtendedID() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleExtendedID = null;


        try {
            // InternalXMapping.g:2881:50: (iv_ruleExtendedID= ruleExtendedID EOF )
            // InternalXMapping.g:2882:2: iv_ruleExtendedID= ruleExtendedID EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExtendedIDRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleExtendedID=ruleExtendedID();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExtendedID.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExtendedID"


    // $ANTLR start "ruleExtendedID"
    // InternalXMapping.g:2888:1: ruleExtendedID returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ValidID_0= ruleValidID (kw= '.' this_ValidID_2= ruleValidID )* ) ;
    public final AntlrDatatypeRuleToken ruleExtendedID() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_ValidID_0 = null;

        AntlrDatatypeRuleToken this_ValidID_2 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2894:2: ( (this_ValidID_0= ruleValidID (kw= '.' this_ValidID_2= ruleValidID )* ) )
            // InternalXMapping.g:2895:2: (this_ValidID_0= ruleValidID (kw= '.' this_ValidID_2= ruleValidID )* )
            {
            // InternalXMapping.g:2895:2: (this_ValidID_0= ruleValidID (kw= '.' this_ValidID_2= ruleValidID )* )
            // InternalXMapping.g:2896:3: this_ValidID_0= ruleValidID (kw= '.' this_ValidID_2= ruleValidID )*
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getExtendedIDAccess().getValidIDParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_21);
            this_ValidID_0=ruleValidID();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_ValidID_0);
              		
            }
            if ( state.backtracking==0 ) {

              			afterParserOrEnumRuleCall();
              		
            }
            // InternalXMapping.g:2906:3: (kw= '.' this_ValidID_2= ruleValidID )*
            loop41:
            do {
                int alt41=2;
                int LA41_0 = input.LA(1);

                if ( (LA41_0==31) ) {
                    int LA41_2 = input.LA(2);

                    if ( (LA41_2==RULE_ID||LA41_2==22||(LA41_2>=24 && LA41_2<=25)||LA41_2==28||LA41_2==35) ) {
                        alt41=1;
                    }


                }


                switch (alt41) {
            	case 1 :
            	    // InternalXMapping.g:2907:4: kw= '.' this_ValidID_2= ruleValidID
            	    {
            	    kw=(Token)match(input,31,FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				current.merge(kw);
            	      				newLeafNode(kw, grammarAccess.getExtendedIDAccess().getFullStopKeyword_1_0());
            	      			
            	    }
            	    if ( state.backtracking==0 ) {

            	      				newCompositeNode(grammarAccess.getExtendedIDAccess().getValidIDParserRuleCall_1_1());
            	      			
            	    }
            	    pushFollow(FOLLOW_21);
            	    this_ValidID_2=ruleValidID();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				current.merge(this_ValidID_2);
            	      			
            	    }
            	    if ( state.backtracking==0 ) {

            	      				afterParserOrEnumRuleCall();
            	      			
            	    }

            	    }
            	    break;

            	default :
            	    break loop41;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExtendedID"


    // $ANTLR start "entryRuleExtendedName"
    // InternalXMapping.g:2927:1: entryRuleExtendedName returns [String current=null] : iv_ruleExtendedName= ruleExtendedName EOF ;
    public final String entryRuleExtendedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleExtendedName = null;


        try {
            // InternalXMapping.g:2927:52: (iv_ruleExtendedName= ruleExtendedName EOF )
            // InternalXMapping.g:2928:2: iv_ruleExtendedName= ruleExtendedName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getExtendedNameRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleExtendedName=ruleExtendedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleExtendedName.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleExtendedName"


    // $ANTLR start "ruleExtendedName"
    // InternalXMapping.g:2934:1: ruleExtendedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ValidID_0= ruleValidID ( (kw= '-' )+ this_ValidID_2= ruleValidID )* ) ;
    public final AntlrDatatypeRuleToken ruleExtendedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_ValidID_0 = null;

        AntlrDatatypeRuleToken this_ValidID_2 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2940:2: ( (this_ValidID_0= ruleValidID ( (kw= '-' )+ this_ValidID_2= ruleValidID )* ) )
            // InternalXMapping.g:2941:2: (this_ValidID_0= ruleValidID ( (kw= '-' )+ this_ValidID_2= ruleValidID )* )
            {
            // InternalXMapping.g:2941:2: (this_ValidID_0= ruleValidID ( (kw= '-' )+ this_ValidID_2= ruleValidID )* )
            // InternalXMapping.g:2942:3: this_ValidID_0= ruleValidID ( (kw= '-' )+ this_ValidID_2= ruleValidID )*
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getExtendedNameAccess().getValidIDParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_30);
            this_ValidID_0=ruleValidID();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_ValidID_0);
              		
            }
            if ( state.backtracking==0 ) {

              			afterParserOrEnumRuleCall();
              		
            }
            // InternalXMapping.g:2952:3: ( (kw= '-' )+ this_ValidID_2= ruleValidID )*
            loop43:
            do {
                int alt43=2;
                int LA43_0 = input.LA(1);

                if ( (LA43_0==33) ) {
                    alt43=1;
                }


                switch (alt43) {
            	case 1 :
            	    // InternalXMapping.g:2953:4: (kw= '-' )+ this_ValidID_2= ruleValidID
            	    {
            	    // InternalXMapping.g:2953:4: (kw= '-' )+
            	    int cnt42=0;
            	    loop42:
            	    do {
            	        int alt42=2;
            	        int LA42_0 = input.LA(1);

            	        if ( (LA42_0==33) ) {
            	            alt42=1;
            	        }


            	        switch (alt42) {
            	    	case 1 :
            	    	    // InternalXMapping.g:2954:5: kw= '-'
            	    	    {
            	    	    kw=(Token)match(input,33,FOLLOW_31); if (state.failed) return current;
            	    	    if ( state.backtracking==0 ) {

            	    	      					current.merge(kw);
            	    	      					newLeafNode(kw, grammarAccess.getExtendedNameAccess().getHyphenMinusKeyword_1_0());
            	    	      				
            	    	    }

            	    	    }
            	    	    break;

            	    	default :
            	    	    if ( cnt42 >= 1 ) break loop42;
            	    	    if (state.backtracking>0) {state.failed=true; return current;}
            	                EarlyExitException eee =
            	                    new EarlyExitException(42, input);
            	                throw eee;
            	        }
            	        cnt42++;
            	    } while (true);

            	    if ( state.backtracking==0 ) {

            	      				newCompositeNode(grammarAccess.getExtendedNameAccess().getValidIDParserRuleCall_1_1());
            	      			
            	    }
            	    pushFollow(FOLLOW_30);
            	    this_ValidID_2=ruleValidID();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				current.merge(this_ValidID_2);
            	      			
            	    }
            	    if ( state.backtracking==0 ) {

            	      				afterParserOrEnumRuleCall();
            	      			
            	    }

            	    }
            	    break;

            	default :
            	    break loop43;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleExtendedName"


    // $ANTLR start "entryRuleValidID"
    // InternalXMapping.g:2975:1: entryRuleValidID returns [String current=null] : iv_ruleValidID= ruleValidID EOF ;
    public final String entryRuleValidID() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleValidID = null;


        try {
            // InternalXMapping.g:2975:47: (iv_ruleValidID= ruleValidID EOF )
            // InternalXMapping.g:2976:2: iv_ruleValidID= ruleValidID EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getValidIDRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleValidID=ruleValidID();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleValidID.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleValidID"


    // $ANTLR start "ruleValidID"
    // InternalXMapping.g:2982:1: ruleValidID returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID | this_KEYWORD_1= ruleKEYWORD ) ;
    public final AntlrDatatypeRuleToken ruleValidID() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        AntlrDatatypeRuleToken this_KEYWORD_1 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2988:2: ( (this_ID_0= RULE_ID | this_KEYWORD_1= ruleKEYWORD ) )
            // InternalXMapping.g:2989:2: (this_ID_0= RULE_ID | this_KEYWORD_1= ruleKEYWORD )
            {
            // InternalXMapping.g:2989:2: (this_ID_0= RULE_ID | this_KEYWORD_1= ruleKEYWORD )
            int alt44=2;
            int LA44_0 = input.LA(1);

            if ( (LA44_0==RULE_ID) ) {
                alt44=1;
            }
            else if ( (LA44_0==22||(LA44_0>=24 && LA44_0<=25)||LA44_0==28||LA44_0==35) ) {
                alt44=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 44, 0, input);

                throw nvae;
            }
            switch (alt44) {
                case 1 :
                    // InternalXMapping.g:2990:3: this_ID_0= RULE_ID
                    {
                    this_ID_0=(Token)match(input,RULE_ID,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_ID_0);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newLeafNode(this_ID_0, grammarAccess.getValidIDAccess().getIDTerminalRuleCall_0());
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalXMapping.g:2998:3: this_KEYWORD_1= ruleKEYWORD
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getValidIDAccess().getKEYWORDParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_KEYWORD_1=ruleKEYWORD();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_KEYWORD_1);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleValidID"


    // $ANTLR start "entryRuleKEYWORD"
    // InternalXMapping.g:3012:1: entryRuleKEYWORD returns [String current=null] : iv_ruleKEYWORD= ruleKEYWORD EOF ;
    public final String entryRuleKEYWORD() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleKEYWORD = null;


        try {
            // InternalXMapping.g:3012:47: (iv_ruleKEYWORD= ruleKEYWORD EOF )
            // InternalXMapping.g:3013:2: iv_ruleKEYWORD= ruleKEYWORD EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getKEYWORDRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleKEYWORD=ruleKEYWORD();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleKEYWORD.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleKEYWORD"


    // $ANTLR start "ruleKEYWORD"
    // InternalXMapping.g:3019:1: ruleKEYWORD returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'map' | kw= 'package' | kw= 'import' | kw= 'def' | kw= 'call' ) ;
    public final AntlrDatatypeRuleToken ruleKEYWORD() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalXMapping.g:3025:2: ( (kw= 'map' | kw= 'package' | kw= 'import' | kw= 'def' | kw= 'call' ) )
            // InternalXMapping.g:3026:2: (kw= 'map' | kw= 'package' | kw= 'import' | kw= 'def' | kw= 'call' )
            {
            // InternalXMapping.g:3026:2: (kw= 'map' | kw= 'package' | kw= 'import' | kw= 'def' | kw= 'call' )
            int alt45=5;
            switch ( input.LA(1) ) {
            case 25:
                {
                alt45=1;
                }
                break;
            case 22:
                {
                alt45=2;
                }
                break;
            case 24:
                {
                alt45=3;
                }
                break;
            case 35:
                {
                alt45=4;
                }
                break;
            case 28:
                {
                alt45=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 45, 0, input);

                throw nvae;
            }

            switch (alt45) {
                case 1 :
                    // InternalXMapping.g:3027:3: kw= 'map'
                    {
                    kw=(Token)match(input,25,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getKEYWORDAccess().getMapKeyword_0());
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalXMapping.g:3033:3: kw= 'package'
                    {
                    kw=(Token)match(input,22,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getKEYWORDAccess().getPackageKeyword_1());
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalXMapping.g:3039:3: kw= 'import'
                    {
                    kw=(Token)match(input,24,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getKEYWORDAccess().getImportKeyword_2());
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalXMapping.g:3045:3: kw= 'def'
                    {
                    kw=(Token)match(input,35,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getKEYWORDAccess().getDefKeyword_3());
                      		
                    }

                    }
                    break;
                case 5 :
                    // InternalXMapping.g:3051:3: kw= 'call'
                    {
                    kw=(Token)match(input,28,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getKEYWORDAccess().getCallKeyword_4());
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleKEYWORD"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalXMapping.g:3060:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // InternalXMapping.g:3060:53: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // InternalXMapping.g:3061:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleQualifiedName.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalXMapping.g:3067:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ExtendedName_0= ruleExtendedName (kw= '.' this_ExtendedName_2= ruleExtendedName )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_ExtendedName_0 = null;

        AntlrDatatypeRuleToken this_ExtendedName_2 = null;



        	enterRule();

        try {
            // InternalXMapping.g:3073:2: ( (this_ExtendedName_0= ruleExtendedName (kw= '.' this_ExtendedName_2= ruleExtendedName )* ) )
            // InternalXMapping.g:3074:2: (this_ExtendedName_0= ruleExtendedName (kw= '.' this_ExtendedName_2= ruleExtendedName )* )
            {
            // InternalXMapping.g:3074:2: (this_ExtendedName_0= ruleExtendedName (kw= '.' this_ExtendedName_2= ruleExtendedName )* )
            // InternalXMapping.g:3075:3: this_ExtendedName_0= ruleExtendedName (kw= '.' this_ExtendedName_2= ruleExtendedName )*
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getQualifiedNameAccess().getExtendedNameParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_21);
            this_ExtendedName_0=ruleExtendedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_ExtendedName_0);
              		
            }
            if ( state.backtracking==0 ) {

              			afterParserOrEnumRuleCall();
              		
            }
            // InternalXMapping.g:3085:3: (kw= '.' this_ExtendedName_2= ruleExtendedName )*
            loop46:
            do {
                int alt46=2;
                int LA46_0 = input.LA(1);

                if ( (LA46_0==31) ) {
                    alt46=1;
                }


                switch (alt46) {
            	case 1 :
            	    // InternalXMapping.g:3086:4: kw= '.' this_ExtendedName_2= ruleExtendedName
            	    {
            	    kw=(Token)match(input,31,FOLLOW_4); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				current.merge(kw);
            	      				newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0());
            	      			
            	    }
            	    if ( state.backtracking==0 ) {

            	      				newCompositeNode(grammarAccess.getQualifiedNameAccess().getExtendedNameParserRuleCall_1_1());
            	      			
            	    }
            	    pushFollow(FOLLOW_21);
            	    this_ExtendedName_2=ruleExtendedName();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				current.merge(this_ExtendedName_2);
            	      			
            	    }
            	    if ( state.backtracking==0 ) {

            	      				afterParserOrEnumRuleCall();
            	      			
            	    }

            	    }
            	    break;

            	default :
            	    break loop46;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRuleDOUBLE"
    // InternalXMapping.g:3106:1: entryRuleDOUBLE returns [String current=null] : iv_ruleDOUBLE= ruleDOUBLE EOF ;
    public final String entryRuleDOUBLE() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleDOUBLE = null;


        try {
            // InternalXMapping.g:3106:46: (iv_ruleDOUBLE= ruleDOUBLE EOF )
            // InternalXMapping.g:3107:2: iv_ruleDOUBLE= ruleDOUBLE EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDOUBLERule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleDOUBLE=ruleDOUBLE();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDOUBLE.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDOUBLE"


    // $ANTLR start "ruleDOUBLE"
    // InternalXMapping.g:3113:1: ruleDOUBLE returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_INT_0= RULE_INT kw= '.' this_INT_2= RULE_INT ) ;
    public final AntlrDatatypeRuleToken ruleDOUBLE() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INT_0=null;
        Token kw=null;
        Token this_INT_2=null;


        	enterRule();

        try {
            // InternalXMapping.g:3119:2: ( (this_INT_0= RULE_INT kw= '.' this_INT_2= RULE_INT ) )
            // InternalXMapping.g:3120:2: (this_INT_0= RULE_INT kw= '.' this_INT_2= RULE_INT )
            {
            // InternalXMapping.g:3120:2: (this_INT_0= RULE_INT kw= '.' this_INT_2= RULE_INT )
            // InternalXMapping.g:3121:3: this_INT_0= RULE_INT kw= '.' this_INT_2= RULE_INT
            {
            this_INT_0=(Token)match(input,RULE_INT,FOLLOW_18); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_INT_0);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_INT_0, grammarAccess.getDOUBLEAccess().getINTTerminalRuleCall_0());
              		
            }
            kw=(Token)match(input,31,FOLLOW_26); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(kw);
              			newLeafNode(kw, grammarAccess.getDOUBLEAccess().getFullStopKeyword_1());
              		
            }
            this_INT_2=(Token)match(input,RULE_INT,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_INT_2);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_INT_2, grammarAccess.getDOUBLEAccess().getINTTerminalRuleCall_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDOUBLE"

    // $ANTLR start synpred15_InternalXMapping
    public final void synpred15_InternalXMapping_fragment() throws RecognitionException {   
        EObject this_AssignmentNew_0 = null;


        // InternalXMapping.g:1073:3: (this_AssignmentNew_0= ruleAssignmentNew )
        // InternalXMapping.g:1073:3: this_AssignmentNew_0= ruleAssignmentNew
        {
        if ( state.backtracking==0 ) {

          			/* */
          		
        }
        pushFollow(FOLLOW_2);
        this_AssignmentNew_0=ruleAssignmentNew();

        state._fsp--;
        if (state.failed) return ;

        }
    }
    // $ANTLR end synpred15_InternalXMapping

    // Delegated rules

    public final boolean synpred15_InternalXMapping() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred15_InternalXMapping_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA4 dfa4 = new DFA4(this);
    protected DFA5 dfa5 = new DFA5(this);
    protected DFA6 dfa6 = new DFA6(this);
    protected DFA9 dfa9 = new DFA9(this);
    protected DFA11 dfa11 = new DFA11(this);
    protected DFA12 dfa12 = new DFA12(this);
    protected DFA13 dfa13 = new DFA13(this);
    protected DFA14 dfa14 = new DFA14(this);
    protected DFA15 dfa15 = new DFA15(this);
    protected DFA18 dfa18 = new DFA18(this);
    protected DFA19 dfa19 = new DFA19(this);
    protected DFA26 dfa26 = new DFA26(this);
    static final String dfa_1s = "\40\uffff";
    static final String dfa_2s = "\1\uffff\1\2\36\uffff";
    static final String dfa_3s = "\2\17\1\uffff\11\17\1\uffff\23\17";
    static final String dfa_4s = "\2\43\1\uffff\11\43\1\uffff\23\43";
    static final String dfa_5s = "\2\uffff\1\2\11\uffff\1\1\23\uffff";
    static final String dfa_6s = "\40\uffff}>";
    static final String[] dfa_7s = {
            "\1\2\6\uffff\1\2\1\uffff\1\1\1\2\2\uffff\1\2\6\uffff\1\2",
            "\1\4\4\uffff\1\2\1\uffff\1\6\1\uffff\1\3\1\5\2\uffff\1\10\2\uffff\1\2\1\uffff\2\2\1\7",
            "",
            "\1\2\4\uffff\1\2\1\uffff\1\2\1\14\2\2\2\uffff\1\2\2\uffff\1\12\1\uffff\1\11\1\13\1\2",
            "\1\2\4\uffff\1\2\1\uffff\1\2\1\14\2\2\2\uffff\1\2\2\uffff\1\12\1\uffff\1\11\1\13\1\2",
            "\1\2\4\uffff\1\2\1\uffff\1\2\1\14\2\2\2\uffff\1\2\2\uffff\1\12\1\uffff\1\11\1\13\1\2",
            "\1\2\4\uffff\1\2\1\uffff\1\2\1\14\2\2\2\uffff\1\2\2\uffff\1\12\1\uffff\1\11\1\13\1\2",
            "\1\2\4\uffff\1\2\1\uffff\1\2\1\14\2\2\2\uffff\1\2\2\uffff\1\12\1\uffff\1\11\1\13\1\2",
            "\1\2\4\uffff\1\2\1\uffff\1\2\1\14\2\2\2\uffff\1\2\2\uffff\1\12\1\uffff\1\11\1\13\1\2",
            "\1\15\6\uffff\1\17\1\uffff\1\20\1\16\2\uffff\1\22\4\uffff\1\11\1\uffff\1\21",
            "\1\23\6\uffff\1\25\1\uffff\1\26\1\24\2\uffff\1\30\6\uffff\1\27",
            "\1\2\4\uffff\1\2\1\uffff\1\2\1\14\2\2\2\uffff\1\2\6\uffff\1\2",
            "",
            "\1\2\4\uffff\1\2\1\uffff\1\2\1\14\2\2\2\uffff\1\2\2\uffff\1\12\1\uffff\1\11\1\13\1\2",
            "\1\2\4\uffff\1\2\1\uffff\1\2\1\14\2\2\2\uffff\1\2\2\uffff\1\12\1\uffff\1\11\1\13\1\2",
            "\1\2\4\uffff\1\2\1\uffff\1\2\1\14\2\2\2\uffff\1\2\2\uffff\1\12\1\uffff\1\11\1\13\1\2",
            "\1\2\4\uffff\1\2\1\uffff\1\2\1\14\2\2\2\uffff\1\2\2\uffff\1\12\1\uffff\1\11\1\13\1\2",
            "\1\2\4\uffff\1\2\1\uffff\1\2\1\14\2\2\2\uffff\1\2\2\uffff\1\12\1\uffff\1\11\1\13\1\2",
            "\1\2\4\uffff\1\2\1\uffff\1\2\1\14\2\2\2\uffff\1\2\2\uffff\1\12\1\uffff\1\11\1\13\1\2",
            "\1\2\4\uffff\1\2\1\uffff\1\2\1\14\2\2\2\uffff\1\2\2\uffff\1\12\1\uffff\1\31\1\13\1\2",
            "\1\2\4\uffff\1\2\1\uffff\1\2\1\14\2\2\2\uffff\1\2\2\uffff\1\12\1\uffff\1\31\1\13\1\2",
            "\1\2\4\uffff\1\2\1\uffff\1\2\1\14\2\2\2\uffff\1\2\2\uffff\1\12\1\uffff\1\31\1\13\1\2",
            "\1\2\4\uffff\1\2\1\uffff\1\2\1\14\2\2\2\uffff\1\2\2\uffff\1\12\1\uffff\1\31\1\13\1\2",
            "\1\2\4\uffff\1\2\1\uffff\1\2\1\14\2\2\2\uffff\1\2\2\uffff\1\12\1\uffff\1\31\1\13\1\2",
            "\1\2\4\uffff\1\2\1\uffff\1\2\1\14\2\2\2\uffff\1\2\2\uffff\1\12\1\uffff\1\31\1\13\1\2",
            "\1\32\6\uffff\1\34\1\uffff\1\35\1\33\2\uffff\1\37\4\uffff\1\31\1\uffff\1\36",
            "\1\2\4\uffff\1\2\1\uffff\1\2\1\14\2\2\2\uffff\1\2\2\uffff\1\12\1\uffff\1\31\1\13\1\2",
            "\1\2\4\uffff\1\2\1\uffff\1\2\1\14\2\2\2\uffff\1\2\2\uffff\1\12\1\uffff\1\31\1\13\1\2",
            "\1\2\4\uffff\1\2\1\uffff\1\2\1\14\2\2\2\uffff\1\2\2\uffff\1\12\1\uffff\1\31\1\13\1\2",
            "\1\2\4\uffff\1\2\1\uffff\1\2\1\14\2\2\2\uffff\1\2\2\uffff\1\12\1\uffff\1\31\1\13\1\2",
            "\1\2\4\uffff\1\2\1\uffff\1\2\1\14\2\2\2\uffff\1\2\2\uffff\1\12\1\uffff\1\31\1\13\1\2",
            "\1\2\4\uffff\1\2\1\uffff\1\2\1\14\2\2\2\uffff\1\2\2\uffff\1\12\1\uffff\1\31\1\13\1\2"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final short[] dfa_2 = DFA.unpackEncodedString(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final char[] dfa_4 = DFA.unpackEncodedStringToUnsignedChars(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[] dfa_6 = DFA.unpackEncodedString(dfa_6s);
    static final short[][] dfa_7 = unpackEncodedStringArray(dfa_7s);

    class DFA4 extends DFA {

        public DFA4(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 4;
            this.eot = dfa_1;
            this.eof = dfa_2;
            this.min = dfa_3;
            this.max = dfa_4;
            this.accept = dfa_5;
            this.special = dfa_6;
            this.transition = dfa_7;
        }
        public String getDescription() {
            return "359:2: ( (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) otherlv_2= ';' ) | ( ( ruleQualifiedNameWithWildcard ) ) )";
        }
    }
    static final String dfa_8s = "\137\uffff";
    static final String dfa_9s = "\2\12\1\uffff\4\37\1\12\1\37\1\uffff\1\13\2\uffff\1\17\6\37\6\25\1\17\1\12\1\5\6\25\7\37\1\25\1\13\1\17\1\12\6\37\6\25\7\37\1\17\1\5\1\13\1\17\7\25\6\37\6\25\1\17\6\25";
    static final String dfa_10s = "\2\43\1\uffff\4\37\1\43\1\37\1\uffff\1\43\2\uffff\1\43\6\37\6\41\2\43\1\5\6\41\7\37\1\33\3\43\6\37\6\41\7\37\1\43\1\5\2\43\6\41\1\33\6\37\6\41\1\43\6\41";
    static final String dfa_11s = "\2\uffff\1\2\6\uffff\1\3\1\uffff\1\1\1\4\122\uffff";
    static final String dfa_12s = "\137\uffff}>";
    static final String[] dfa_13s = {
            "\1\2\1\11\1\10\1\2\1\11\1\3\6\uffff\1\4\1\uffff\1\5\1\1\2\uffff\1\7\6\uffff\1\6",
            "\1\13\1\uffff\2\13\1\uffff\1\13\6\uffff\1\13\1\uffff\2\13\2\uffff\1\13\2\uffff\1\12\3\uffff\1\13",
            "",
            "\1\12",
            "\1\12",
            "\1\12",
            "\1\12",
            "\1\14\1\uffff\2\14\1\uffff\1\14\6\uffff\1\14\1\uffff\2\14\2\uffff\1\14\2\uffff\1\12\3\uffff\1\14",
            "\1\15",
            "",
            "\1\11\1\10\1\2\1\11\1\16\6\uffff\1\20\1\uffff\1\21\1\17\2\uffff\1\23\6\uffff\1\22",
            "",
            "",
            "\1\24\6\uffff\1\26\1\uffff\1\27\1\25\2\uffff\1\31\6\uffff\1\30",
            "\1\12",
            "\1\12",
            "\1\12",
            "\1\12",
            "\1\12",
            "\1\12",
            "\1\33\1\uffff\1\11\2\uffff\1\2\1\11\4\uffff\1\34\1\32",
            "\1\33\1\uffff\1\11\2\uffff\1\2\1\11\4\uffff\1\34\1\32",
            "\1\33\1\uffff\1\11\2\uffff\1\2\1\11\4\uffff\1\34\1\32",
            "\1\33\1\uffff\1\11\2\uffff\1\2\1\11\4\uffff\1\34\1\32",
            "\1\33\1\uffff\1\11\2\uffff\1\2\1\11\4\uffff\1\34\1\32",
            "\1\33\1\uffff\1\11\2\uffff\1\2\1\11\4\uffff\1\34\1\32",
            "\1\35\6\uffff\1\37\1\uffff\1\40\1\36\2\uffff\1\42\4\uffff\1\32\1\uffff\1\41",
            "\1\2\1\11\1\51\1\2\1\11\1\43\6\uffff\1\45\1\uffff\1\46\1\44\2\uffff\1\50\6\uffff\1\47",
            "\1\52",
            "\1\33\1\uffff\1\11\2\uffff\1\2\1\11\4\uffff\1\34\1\32",
            "\1\33\1\uffff\1\11\2\uffff\1\2\1\11\4\uffff\1\34\1\32",
            "\1\33\1\uffff\1\11\2\uffff\1\2\1\11\4\uffff\1\34\1\32",
            "\1\33\1\uffff\1\11\2\uffff\1\2\1\11\4\uffff\1\34\1\32",
            "\1\33\1\uffff\1\11\2\uffff\1\2\1\11\4\uffff\1\34\1\32",
            "\1\33\1\uffff\1\11\2\uffff\1\2\1\11\4\uffff\1\34\1\32",
            "\1\53",
            "\1\53",
            "\1\53",
            "\1\53",
            "\1\53",
            "\1\53",
            "\1\54",
            "\1\55\4\uffff\1\2\1\11",
            "\1\11\1\51\1\2\1\11\1\56\6\uffff\1\60\1\uffff\1\61\1\57\2\uffff\1\63\6\uffff\1\62",
            "\1\64\6\uffff\1\66\1\uffff\1\67\1\65\2\uffff\1\71\6\uffff\1\70",
            "\1\2\1\11\1\100\1\2\1\11\1\72\6\uffff\1\74\1\uffff\1\75\1\73\2\uffff\1\77\6\uffff\1\76",
            "\1\53",
            "\1\53",
            "\1\53",
            "\1\53",
            "\1\53",
            "\1\53",
            "\1\33\1\uffff\1\11\2\uffff\1\2\1\11\4\uffff\1\102\1\101",
            "\1\33\1\uffff\1\11\2\uffff\1\2\1\11\4\uffff\1\102\1\101",
            "\1\33\1\uffff\1\11\2\uffff\1\2\1\11\4\uffff\1\102\1\101",
            "\1\33\1\uffff\1\11\2\uffff\1\2\1\11\4\uffff\1\102\1\101",
            "\1\33\1\uffff\1\11\2\uffff\1\2\1\11\4\uffff\1\102\1\101",
            "\1\33\1\uffff\1\11\2\uffff\1\2\1\11\4\uffff\1\102\1\101",
            "\1\103",
            "\1\103",
            "\1\103",
            "\1\103",
            "\1\103",
            "\1\103",
            "\1\104",
            "\1\105\6\uffff\1\107\1\uffff\1\110\1\106\2\uffff\1\112\4\uffff\1\101\1\uffff\1\111",
            "\1\113",
            "\1\11\1\100\1\2\1\11\1\114\6\uffff\1\116\1\uffff\1\117\1\115\2\uffff\1\121\6\uffff\1\120",
            "\1\122\6\uffff\1\124\1\uffff\1\125\1\123\2\uffff\1\127\6\uffff\1\126",
            "\1\33\1\uffff\1\11\2\uffff\1\2\1\11\4\uffff\1\102\1\101",
            "\1\33\1\uffff\1\11\2\uffff\1\2\1\11\4\uffff\1\102\1\101",
            "\1\33\1\uffff\1\11\2\uffff\1\2\1\11\4\uffff\1\102\1\101",
            "\1\33\1\uffff\1\11\2\uffff\1\2\1\11\4\uffff\1\102\1\101",
            "\1\33\1\uffff\1\11\2\uffff\1\2\1\11\4\uffff\1\102\1\101",
            "\1\33\1\uffff\1\11\2\uffff\1\2\1\11\4\uffff\1\102\1\101",
            "\1\55\4\uffff\1\2\1\11",
            "\1\103",
            "\1\103",
            "\1\103",
            "\1\103",
            "\1\103",
            "\1\103",
            "\1\55\4\uffff\1\2\1\11\4\uffff\1\102\1\130",
            "\1\55\4\uffff\1\2\1\11\4\uffff\1\102\1\130",
            "\1\55\4\uffff\1\2\1\11\4\uffff\1\102\1\130",
            "\1\55\4\uffff\1\2\1\11\4\uffff\1\102\1\130",
            "\1\55\4\uffff\1\2\1\11\4\uffff\1\102\1\130",
            "\1\55\4\uffff\1\2\1\11\4\uffff\1\102\1\130",
            "\1\131\6\uffff\1\133\1\uffff\1\134\1\132\2\uffff\1\136\4\uffff\1\130\1\uffff\1\135",
            "\1\55\4\uffff\1\2\1\11\4\uffff\1\102\1\130",
            "\1\55\4\uffff\1\2\1\11\4\uffff\1\102\1\130",
            "\1\55\4\uffff\1\2\1\11\4\uffff\1\102\1\130",
            "\1\55\4\uffff\1\2\1\11\4\uffff\1\102\1\130",
            "\1\55\4\uffff\1\2\1\11\4\uffff\1\102\1\130",
            "\1\55\4\uffff\1\2\1\11\4\uffff\1\102\1\130"
    };

    static final short[] dfa_8 = DFA.unpackEncodedString(dfa_8s);
    static final char[] dfa_9 = DFA.unpackEncodedStringToUnsignedChars(dfa_9s);
    static final char[] dfa_10 = DFA.unpackEncodedStringToUnsignedChars(dfa_10s);
    static final short[] dfa_11 = DFA.unpackEncodedString(dfa_11s);
    static final short[] dfa_12 = DFA.unpackEncodedString(dfa_12s);
    static final short[][] dfa_13 = unpackEncodedStringArray(dfa_13s);

    class DFA5 extends DFA {

        public DFA5(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 5;
            this.eot = dfa_8;
            this.eof = dfa_8;
            this.min = dfa_9;
            this.max = dfa_10;
            this.accept = dfa_11;
            this.special = dfa_12;
            this.transition = dfa_13;
        }
        public String getDescription() {
            return "427:2: (this_MapStatement_0= ruleMapStatement | this_MapForwardStatement_1= ruleMapForwardStatement | this_AssignmentStatement_2= ruleAssignmentStatement | this_CallStatement_3= ruleCallStatement )";
        }
    }
    static final String dfa_14s = "\102\uffff";
    static final String dfa_15s = "\1\13\10\37\1\uffff\1\13\1\17\1\4\6\37\7\25\1\17\1\13\1\uffff\6\25\10\37\1\13\1\17\1\4\6\37\7\25\1\17\6\25";
    static final String dfa_16s = "\1\43\10\37\1\uffff\2\43\1\4\6\37\6\41\1\40\2\43\1\uffff\6\41\10\37\2\43\1\4\6\37\6\41\1\40\1\43\6\41";
    static final String dfa_17s = "\11\uffff\1\2\22\uffff\1\1\45\uffff";
    static final String dfa_18s = "\102\uffff}>";
    static final String[] dfa_19s = {
            "\1\11\1\7\1\uffff\1\10\1\1\6\uffff\1\3\1\uffff\1\4\1\2\2\uffff\1\6\6\uffff\1\5",
            "\1\12",
            "\1\12",
            "\1\12",
            "\1\12",
            "\1\12",
            "\1\12",
            "\1\13",
            "\1\14",
            "",
            "\1\11\1\7\1\uffff\1\10\1\15\6\uffff\1\17\1\uffff\1\20\1\16\2\uffff\1\22\6\uffff\1\21",
            "\1\23\6\uffff\1\25\1\uffff\1\26\1\24\2\uffff\1\30\6\uffff\1\27",
            "\1\31",
            "\1\12",
            "\1\12",
            "\1\12",
            "\1\12",
            "\1\12",
            "\1\12",
            "\1\33\1\uffff\1\34\3\uffff\1\11\4\uffff\1\11\1\32",
            "\1\33\1\uffff\1\34\3\uffff\1\11\4\uffff\1\11\1\32",
            "\1\33\1\uffff\1\34\3\uffff\1\11\4\uffff\1\11\1\32",
            "\1\33\1\uffff\1\34\3\uffff\1\11\4\uffff\1\11\1\32",
            "\1\33\1\uffff\1\34\3\uffff\1\11\4\uffff\1\11\1\32",
            "\1\33\1\uffff\1\34\3\uffff\1\11\4\uffff\1\11\1\32",
            "\1\33\1\uffff\1\34\3\uffff\1\11\4\uffff\1\11",
            "\1\35\6\uffff\1\37\1\uffff\1\40\1\36\2\uffff\1\42\4\uffff\1\32\1\uffff\1\41",
            "\1\11\1\51\1\uffff\1\52\1\43\6\uffff\1\45\1\uffff\1\46\1\44\2\uffff\1\50\6\uffff\1\47",
            "",
            "\1\33\1\uffff\1\34\3\uffff\1\11\4\uffff\1\11\1\32",
            "\1\33\1\uffff\1\34\3\uffff\1\11\4\uffff\1\11\1\32",
            "\1\33\1\uffff\1\34\3\uffff\1\11\4\uffff\1\11\1\32",
            "\1\33\1\uffff\1\34\3\uffff\1\11\4\uffff\1\11\1\32",
            "\1\33\1\uffff\1\34\3\uffff\1\11\4\uffff\1\11\1\32",
            "\1\33\1\uffff\1\34\3\uffff\1\11\4\uffff\1\11\1\32",
            "\1\53",
            "\1\53",
            "\1\53",
            "\1\53",
            "\1\53",
            "\1\53",
            "\1\54",
            "\1\55",
            "\1\11\1\51\1\uffff\1\52\1\56\6\uffff\1\60\1\uffff\1\61\1\57\2\uffff\1\63\6\uffff\1\62",
            "\1\64\6\uffff\1\66\1\uffff\1\67\1\65\2\uffff\1\71\6\uffff\1\70",
            "\1\72",
            "\1\53",
            "\1\53",
            "\1\53",
            "\1\53",
            "\1\53",
            "\1\53",
            "\1\33\1\uffff\1\34\3\uffff\1\11\4\uffff\1\11\1\73",
            "\1\33\1\uffff\1\34\3\uffff\1\11\4\uffff\1\11\1\73",
            "\1\33\1\uffff\1\34\3\uffff\1\11\4\uffff\1\11\1\73",
            "\1\33\1\uffff\1\34\3\uffff\1\11\4\uffff\1\11\1\73",
            "\1\33\1\uffff\1\34\3\uffff\1\11\4\uffff\1\11\1\73",
            "\1\33\1\uffff\1\34\3\uffff\1\11\4\uffff\1\11\1\73",
            "\1\33\1\uffff\1\34\3\uffff\1\11\4\uffff\1\11",
            "\1\74\6\uffff\1\76\1\uffff\1\77\1\75\2\uffff\1\101\4\uffff\1\73\1\uffff\1\100",
            "\1\33\1\uffff\1\34\3\uffff\1\11\4\uffff\1\11\1\73",
            "\1\33\1\uffff\1\34\3\uffff\1\11\4\uffff\1\11\1\73",
            "\1\33\1\uffff\1\34\3\uffff\1\11\4\uffff\1\11\1\73",
            "\1\33\1\uffff\1\34\3\uffff\1\11\4\uffff\1\11\1\73",
            "\1\33\1\uffff\1\34\3\uffff\1\11\4\uffff\1\11\1\73",
            "\1\33\1\uffff\1\34\3\uffff\1\11\4\uffff\1\11\1\73"
    };

    static final short[] dfa_14 = DFA.unpackEncodedString(dfa_14s);
    static final char[] dfa_15 = DFA.unpackEncodedStringToUnsignedChars(dfa_15s);
    static final char[] dfa_16 = DFA.unpackEncodedStringToUnsignedChars(dfa_16s);
    static final short[] dfa_17 = DFA.unpackEncodedString(dfa_17s);
    static final short[] dfa_18 = DFA.unpackEncodedString(dfa_18s);
    static final short[][] dfa_19 = unpackEncodedStringArray(dfa_19s);

    class DFA6 extends DFA {

        public DFA6(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 6;
            this.eot = dfa_14;
            this.eof = dfa_14;
            this.min = dfa_15;
            this.max = dfa_16;
            this.accept = dfa_17;
            this.special = dfa_18;
            this.transition = dfa_19;
        }
        public String getDescription() {
            return "650:2: (this_Declaration_0= ruleDeclaration | this_Initialization_1= ruleInitialization )";
        }
    }
    static final String dfa_20s = "\20\uffff";
    static final String dfa_21s = "\1\12\1\uffff\6\32\1\14\1\uffff\6\32";
    static final String dfa_22s = "\1\43\1\uffff\6\41\1\43\1\uffff\6\41";
    static final String dfa_23s = "\1\uffff\1\1\7\uffff\1\2\6\uffff";
    static final String dfa_24s = "\20\uffff}>";
    static final String[] dfa_25s = {
            "\1\1\1\uffff\2\1\1\uffff\1\2\6\uffff\1\4\1\uffff\1\5\1\3\2\uffff\1\7\6\uffff\1\6",
            "",
            "\1\11\4\uffff\1\10\1\uffff\1\11",
            "\1\11\4\uffff\1\10\1\uffff\1\11",
            "\1\11\4\uffff\1\10\1\uffff\1\11",
            "\1\11\4\uffff\1\10\1\uffff\1\11",
            "\1\11\4\uffff\1\10\1\uffff\1\11",
            "\1\11\4\uffff\1\10\1\uffff\1\11",
            "\2\1\1\uffff\1\12\6\uffff\1\14\1\uffff\1\15\1\13\2\uffff\1\17\6\uffff\1\16",
            "",
            "\1\11\4\uffff\1\10\1\uffff\1\11",
            "\1\11\4\uffff\1\10\1\uffff\1\11",
            "\1\11\4\uffff\1\10\1\uffff\1\11",
            "\1\11\4\uffff\1\10\1\uffff\1\11",
            "\1\11\4\uffff\1\10\1\uffff\1\11",
            "\1\11\4\uffff\1\10\1\uffff\1\11"
    };

    static final short[] dfa_20 = DFA.unpackEncodedString(dfa_20s);
    static final char[] dfa_21 = DFA.unpackEncodedStringToUnsignedChars(dfa_21s);
    static final char[] dfa_22 = DFA.unpackEncodedStringToUnsignedChars(dfa_22s);
    static final short[] dfa_23 = DFA.unpackEncodedString(dfa_23s);
    static final short[] dfa_24 = DFA.unpackEncodedString(dfa_24s);
    static final short[][] dfa_25 = unpackEncodedStringArray(dfa_25s);

    class DFA9 extends DFA {

        public DFA9(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 9;
            this.eot = dfa_20;
            this.eof = dfa_20;
            this.min = dfa_21;
            this.max = dfa_22;
            this.accept = dfa_23;
            this.special = dfa_24;
            this.transition = dfa_25;
        }
        public String getDescription() {
            return "854:3: ( ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->' )?";
        }
    }
    static final String dfa_26s = "\33\uffff";
    static final String dfa_27s = "\1\13\10\37\1\uffff\1\13\1\17\1\4\6\37\7\0\1\uffff";
    static final String dfa_28s = "\1\43\10\37\1\uffff\2\43\1\4\6\37\7\0\1\uffff";
    static final String dfa_29s = "\11\uffff\1\2\20\uffff\1\1";
    static final String dfa_30s = "\23\uffff\1\2\1\0\1\3\1\5\1\1\1\4\1\6\1\uffff}>";
    static final String[] dfa_31s = {
            "\1\11\1\7\1\uffff\1\10\1\1\6\uffff\1\3\1\uffff\1\4\1\2\2\uffff\1\6\6\uffff\1\5",
            "\1\12",
            "\1\12",
            "\1\12",
            "\1\12",
            "\1\12",
            "\1\12",
            "\1\13",
            "\1\14",
            "",
            "\1\11\1\7\1\uffff\1\10\1\15\6\uffff\1\17\1\uffff\1\20\1\16\2\uffff\1\22\6\uffff\1\21",
            "\1\23\6\uffff\1\25\1\uffff\1\26\1\24\2\uffff\1\30\6\uffff\1\27",
            "\1\31",
            "\1\12",
            "\1\12",
            "\1\12",
            "\1\12",
            "\1\12",
            "\1\12",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            ""
    };

    static final short[] dfa_26 = DFA.unpackEncodedString(dfa_26s);
    static final char[] dfa_27 = DFA.unpackEncodedStringToUnsignedChars(dfa_27s);
    static final char[] dfa_28 = DFA.unpackEncodedStringToUnsignedChars(dfa_28s);
    static final short[] dfa_29 = DFA.unpackEncodedString(dfa_29s);
    static final short[] dfa_30 = DFA.unpackEncodedString(dfa_30s);
    static final short[][] dfa_31 = unpackEncodedStringArray(dfa_31s);

    class DFA11 extends DFA {

        public DFA11(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 11;
            this.eot = dfa_26;
            this.eof = dfa_26;
            this.min = dfa_27;
            this.max = dfa_28;
            this.accept = dfa_29;
            this.special = dfa_30;
            this.transition = dfa_31;
        }
        public String getDescription() {
            return "1072:2: (this_AssignmentNew_0= ruleAssignmentNew | this_AssignmentReference_1= ruleAssignmentReference )";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA11_20 = input.LA(1);

                         
                        int index11_20 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred15_InternalXMapping()) ) {s = 26;}

                        else if ( (true) ) {s = 9;}

                         
                        input.seek(index11_20);
                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA11_23 = input.LA(1);

                         
                        int index11_23 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred15_InternalXMapping()) ) {s = 26;}

                        else if ( (true) ) {s = 9;}

                         
                        input.seek(index11_23);
                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA11_19 = input.LA(1);

                         
                        int index11_19 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred15_InternalXMapping()) ) {s = 26;}

                        else if ( (true) ) {s = 9;}

                         
                        input.seek(index11_19);
                        if ( s>=0 ) return s;
                        break;
                    case 3 : 
                        int LA11_21 = input.LA(1);

                         
                        int index11_21 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred15_InternalXMapping()) ) {s = 26;}

                        else if ( (true) ) {s = 9;}

                         
                        input.seek(index11_21);
                        if ( s>=0 ) return s;
                        break;
                    case 4 : 
                        int LA11_24 = input.LA(1);

                         
                        int index11_24 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred15_InternalXMapping()) ) {s = 26;}

                        else if ( (true) ) {s = 9;}

                         
                        input.seek(index11_24);
                        if ( s>=0 ) return s;
                        break;
                    case 5 : 
                        int LA11_22 = input.LA(1);

                         
                        int index11_22 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred15_InternalXMapping()) ) {s = 26;}

                        else if ( (true) ) {s = 9;}

                         
                        input.seek(index11_22);
                        if ( s>=0 ) return s;
                        break;
                    case 6 : 
                        int LA11_25 = input.LA(1);

                         
                        int index11_25 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred15_InternalXMapping()) ) {s = 26;}

                        else if ( (true) ) {s = 9;}

                         
                        input.seek(index11_25);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 11, _s, input);
            error(nvae);
            throw nvae;
        }
    }
    static final String dfa_32s = "\1\14\6\37\2\uffff\1\14\6\37";
    static final String dfa_33s = "\1\43\6\37\2\uffff\1\43\6\37";
    static final String dfa_34s = "\7\uffff\1\1\1\2\7\uffff";
    static final String[] dfa_35s = {
            "\1\7\1\uffff\1\10\1\1\6\uffff\1\3\1\uffff\1\4\1\2\2\uffff\1\6\6\uffff\1\5",
            "\1\11",
            "\1\11",
            "\1\11",
            "\1\11",
            "\1\11",
            "\1\11",
            "",
            "",
            "\1\7\1\uffff\1\10\1\12\6\uffff\1\14\1\uffff\1\15\1\13\2\uffff\1\17\6\uffff\1\16",
            "\1\11",
            "\1\11",
            "\1\11",
            "\1\11",
            "\1\11",
            "\1\11"
    };
    static final char[] dfa_32 = DFA.unpackEncodedStringToUnsignedChars(dfa_32s);
    static final char[] dfa_33 = DFA.unpackEncodedStringToUnsignedChars(dfa_33s);
    static final short[] dfa_34 = DFA.unpackEncodedString(dfa_34s);
    static final short[][] dfa_35 = unpackEncodedStringArray(dfa_35s);

    class DFA12 extends DFA {

        public DFA12(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 12;
            this.eot = dfa_20;
            this.eof = dfa_20;
            this.min = dfa_32;
            this.max = dfa_33;
            this.accept = dfa_34;
            this.special = dfa_24;
            this.transition = dfa_35;
        }
        public String getDescription() {
            return "1114:2: ( ( (lv_source_0_0= ruleVariable ) ) | ( (lv_source_1_0= ruleReturn ) ) )";
        }
    }
    static final String dfa_36s = "\1\13\6\37\2\uffff\1\13\6\37";
    static final String[] dfa_37s = {
            "\1\7\1\10\1\uffff\1\7\1\1\6\uffff\1\3\1\uffff\1\4\1\2\2\uffff\1\6\6\uffff\1\5",
            "\1\11",
            "\1\11",
            "\1\11",
            "\1\11",
            "\1\11",
            "\1\11",
            "",
            "",
            "\1\7\1\10\1\uffff\1\7\1\12\6\uffff\1\14\1\uffff\1\15\1\13\2\uffff\1\17\6\uffff\1\16",
            "\1\11",
            "\1\11",
            "\1\11",
            "\1\11",
            "\1\11",
            "\1\11"
    };
    static final char[] dfa_36 = DFA.unpackEncodedStringToUnsignedChars(dfa_36s);
    static final short[][] dfa_37 = unpackEncodedStringArray(dfa_37s);

    class DFA13 extends DFA {

        public DFA13(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 13;
            this.eot = dfa_20;
            this.eof = dfa_20;
            this.min = dfa_36;
            this.max = dfa_33;
            this.accept = dfa_34;
            this.special = dfa_24;
            this.transition = dfa_37;
        }
        public String getDescription() {
            return "1172:2: ( ( (lv_source_0_0= ruleOutputAttribute ) ) | ( (lv_source_1_0= ruleAttribute ) ) )";
        }
    }
    static final String dfa_38s = "\1\12\1\uffff\6\37\1\uffff\1\14\6\37";
    static final String dfa_39s = "\1\43\1\uffff\6\37\1\uffff\1\43\6\37";
    static final String dfa_40s = "\1\uffff\1\1\6\uffff\1\2\7\uffff";
    static final String[] dfa_41s = {
            "\1\1\1\uffff\1\10\1\1\1\uffff\1\2\6\uffff\1\4\1\uffff\1\5\1\3\2\uffff\1\7\6\uffff\1\6",
            "",
            "\1\11",
            "\1\11",
            "\1\11",
            "\1\11",
            "\1\11",
            "\1\11",
            "",
            "\1\10\1\1\1\uffff\1\12\6\uffff\1\14\1\uffff\1\15\1\13\2\uffff\1\17\6\uffff\1\16",
            "\1\11",
            "\1\11",
            "\1\11",
            "\1\11",
            "\1\11",
            "\1\11"
    };
    static final char[] dfa_38 = DFA.unpackEncodedStringToUnsignedChars(dfa_38s);
    static final char[] dfa_39 = DFA.unpackEncodedStringToUnsignedChars(dfa_39s);
    static final short[] dfa_40 = DFA.unpackEncodedString(dfa_40s);
    static final short[][] dfa_41 = unpackEncodedStringArray(dfa_41s);

    class DFA14 extends DFA {

        public DFA14(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 14;
            this.eot = dfa_20;
            this.eof = dfa_20;
            this.min = dfa_38;
            this.max = dfa_39;
            this.accept = dfa_40;
            this.special = dfa_24;
            this.transition = dfa_41;
        }
        public String getDescription() {
            return "1231:3: ( ( (lv_inputAttributes_0_0= ruleInputAttribute ) ) | ( (lv_attributes_1_0= ruleAttribute ) ) )";
        }
    }

    class DFA15 extends DFA {

        public DFA15(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 15;
            this.eot = dfa_20;
            this.eof = dfa_20;
            this.min = dfa_38;
            this.max = dfa_39;
            this.accept = dfa_40;
            this.special = dfa_24;
            this.transition = dfa_41;
        }
        public String getDescription() {
            return "1277:4: ( ( (lv_inputAttributes_3_0= ruleInputAttribute ) ) | ( (lv_attributes_4_0= ruleAttribute ) ) )";
        }
    }

    class DFA18 extends DFA {

        public DFA18(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 18;
            this.eot = dfa_20;
            this.eof = dfa_20;
            this.min = dfa_36;
            this.max = dfa_33;
            this.accept = dfa_34;
            this.special = dfa_24;
            this.transition = dfa_37;
        }
        public String getDescription() {
            return "1437:4: ( ( (lv_outputAttributes_0_0= ruleOutputAttribute ) ) | ( (lv_attributes_1_0= ruleAttribute ) ) )";
        }
    }

    class DFA19 extends DFA {

        public DFA19(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 19;
            this.eot = dfa_20;
            this.eof = dfa_20;
            this.min = dfa_36;
            this.max = dfa_33;
            this.accept = dfa_34;
            this.special = dfa_24;
            this.transition = dfa_37;
        }
        public String getDescription() {
            return "1483:5: ( ( (lv_outputAttributes_3_0= ruleOutputAttribute ) ) | ( (lv_attributes_4_0= ruleAttribute ) ) )";
        }
    }
    static final String[] dfa_42s = {
            "\1\7\2\uffff\1\10\1\1\6\uffff\1\3\1\uffff\1\4\1\2\2\uffff\1\6\6\uffff\1\5",
            "\1\11",
            "\1\11",
            "\1\11",
            "\1\11",
            "\1\11",
            "\1\11",
            "",
            "",
            "\1\7\2\uffff\1\10\1\12\6\uffff\1\14\1\uffff\1\15\1\13\2\uffff\1\17\6\uffff\1\16",
            "\1\11",
            "\1\11",
            "\1\11",
            "\1\11",
            "\1\11",
            "\1\11"
    };
    static final short[][] dfa_42 = unpackEncodedStringArray(dfa_42s);

    class DFA26 extends DFA {

        public DFA26(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 26;
            this.eot = dfa_20;
            this.eof = dfa_20;
            this.min = dfa_36;
            this.max = dfa_33;
            this.accept = dfa_34;
            this.special = dfa_24;
            this.transition = dfa_42;
        }
        public String getDescription() {
            return "1873:2: ( ( ( ruleOutputName ) ) | ( ( ruleReturnName ) ) )";
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000813508000L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000813408000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000200002L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x000000081340A000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x000000081340FC02L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x000000081340B400L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x00000000000003C0L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x000000085340FC00L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000A00000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x000000081340F400L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000008200000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x000000081340FC00L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000020000030L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000080000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x0000000100000002L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000000080000002L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000280000002L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000000280000020L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000000400000002L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000200000002L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000000A13408000L});

}