/**
 * generated by Xtext 2.24.0
 */
package com.gk_software.core.dsl.xMapping.impl;

import com.gk_software.core.dsl.xMapping.CallStatement;
import com.gk_software.core.dsl.xMapping.InputSection;
import com.gk_software.core.dsl.xMapping.OutputSection;
import com.gk_software.core.dsl.xMapping.XMappingPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Call Statement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link com.gk_software.core.dsl.xMapping.impl.CallStatementImpl#getInputSection <em>Input Section</em>}</li>
 *   <li>{@link com.gk_software.core.dsl.xMapping.impl.CallStatementImpl#getRoutineSection <em>Routine Section</em>}</li>
 *   <li>{@link com.gk_software.core.dsl.xMapping.impl.CallStatementImpl#getOutputSection <em>Output Section</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CallStatementImpl extends StatementImpl implements CallStatement
{
  /**
   * The cached value of the '{@link #getInputSection() <em>Input Section</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getInputSection()
   * @generated
   * @ordered
   */
  protected InputSection inputSection;

  /**
   * The default value of the '{@link #getRoutineSection() <em>Routine Section</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRoutineSection()
   * @generated
   * @ordered
   */
  protected static final String ROUTINE_SECTION_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getRoutineSection() <em>Routine Section</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getRoutineSection()
   * @generated
   * @ordered
   */
  protected String routineSection = ROUTINE_SECTION_EDEFAULT;

  /**
   * The cached value of the '{@link #getOutputSection() <em>Output Section</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOutputSection()
   * @generated
   * @ordered
   */
  protected OutputSection outputSection;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected CallStatementImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return XMappingPackage.Literals.CALL_STATEMENT;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public InputSection getInputSection()
  {
    return inputSection;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetInputSection(InputSection newInputSection, NotificationChain msgs)
  {
    InputSection oldInputSection = inputSection;
    inputSection = newInputSection;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, XMappingPackage.CALL_STATEMENT__INPUT_SECTION, oldInputSection, newInputSection);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setInputSection(InputSection newInputSection)
  {
    if (newInputSection != inputSection)
    {
      NotificationChain msgs = null;
      if (inputSection != null)
        msgs = ((InternalEObject)inputSection).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - XMappingPackage.CALL_STATEMENT__INPUT_SECTION, null, msgs);
      if (newInputSection != null)
        msgs = ((InternalEObject)newInputSection).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - XMappingPackage.CALL_STATEMENT__INPUT_SECTION, null, msgs);
      msgs = basicSetInputSection(newInputSection, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, XMappingPackage.CALL_STATEMENT__INPUT_SECTION, newInputSection, newInputSection));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getRoutineSection()
  {
    return routineSection;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setRoutineSection(String newRoutineSection)
  {
    String oldRoutineSection = routineSection;
    routineSection = newRoutineSection;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, XMappingPackage.CALL_STATEMENT__ROUTINE_SECTION, oldRoutineSection, routineSection));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public OutputSection getOutputSection()
  {
    return outputSection;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetOutputSection(OutputSection newOutputSection, NotificationChain msgs)
  {
    OutputSection oldOutputSection = outputSection;
    outputSection = newOutputSection;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, XMappingPackage.CALL_STATEMENT__OUTPUT_SECTION, oldOutputSection, newOutputSection);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setOutputSection(OutputSection newOutputSection)
  {
    if (newOutputSection != outputSection)
    {
      NotificationChain msgs = null;
      if (outputSection != null)
        msgs = ((InternalEObject)outputSection).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - XMappingPackage.CALL_STATEMENT__OUTPUT_SECTION, null, msgs);
      if (newOutputSection != null)
        msgs = ((InternalEObject)newOutputSection).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - XMappingPackage.CALL_STATEMENT__OUTPUT_SECTION, null, msgs);
      msgs = basicSetOutputSection(newOutputSection, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, XMappingPackage.CALL_STATEMENT__OUTPUT_SECTION, newOutputSection, newOutputSection));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case XMappingPackage.CALL_STATEMENT__INPUT_SECTION:
        return basicSetInputSection(null, msgs);
      case XMappingPackage.CALL_STATEMENT__OUTPUT_SECTION:
        return basicSetOutputSection(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case XMappingPackage.CALL_STATEMENT__INPUT_SECTION:
        return getInputSection();
      case XMappingPackage.CALL_STATEMENT__ROUTINE_SECTION:
        return getRoutineSection();
      case XMappingPackage.CALL_STATEMENT__OUTPUT_SECTION:
        return getOutputSection();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case XMappingPackage.CALL_STATEMENT__INPUT_SECTION:
        setInputSection((InputSection)newValue);
        return;
      case XMappingPackage.CALL_STATEMENT__ROUTINE_SECTION:
        setRoutineSection((String)newValue);
        return;
      case XMappingPackage.CALL_STATEMENT__OUTPUT_SECTION:
        setOutputSection((OutputSection)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case XMappingPackage.CALL_STATEMENT__INPUT_SECTION:
        setInputSection((InputSection)null);
        return;
      case XMappingPackage.CALL_STATEMENT__ROUTINE_SECTION:
        setRoutineSection(ROUTINE_SECTION_EDEFAULT);
        return;
      case XMappingPackage.CALL_STATEMENT__OUTPUT_SECTION:
        setOutputSection((OutputSection)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case XMappingPackage.CALL_STATEMENT__INPUT_SECTION:
        return inputSection != null;
      case XMappingPackage.CALL_STATEMENT__ROUTINE_SECTION:
        return ROUTINE_SECTION_EDEFAULT == null ? routineSection != null : !ROUTINE_SECTION_EDEFAULT.equals(routineSection);
      case XMappingPackage.CALL_STATEMENT__OUTPUT_SECTION:
        return outputSection != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (routineSection: ");
    result.append(routineSection);
    result.append(')');
    return result.toString();
  }

} //CallStatementImpl
