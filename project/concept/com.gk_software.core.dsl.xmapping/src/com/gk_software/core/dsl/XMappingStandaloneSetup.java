/*
 * generated by Xtext 2.24.0
 */
package com.gk_software.core.dsl;


/**
 * Initialization support for running Xtext languages without Equinox extension registry.
 */
public class XMappingStandaloneSetup extends XMappingStandaloneSetupGenerated {

	public static void doSetup() {
		new XMappingStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
}
