package org.xtext.example.mydsl.ui.messaging;

import java.io.OutputStream;
import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.IOConsoleOutputStream;
import org.xtext.example.mydsl.helpers.classes.IMessageListener;

public class MessageProvider implements IMessageListener {
	
	private IOConsoleOutputStream outputStream;
	private IOConsoleOutputStream errorStream;
	
	MessageProvider(){
		this.outputStream = (IOConsoleOutputStream) startStream("XMapping Console", "org.eclipse.ui.MessageConsole", false);
		this.errorStream = (IOConsoleOutputStream) startStream("XMapping Console", "org.eclipse.ui.MessageConsole", true);
	}
	
	//receiver.start("B3 log", "org.eclipse.ui.MessageConsole", true, errorStream);
	public OutputStream startStream(String title, String type, boolean errorStream) {
		IConsoleManager mgr = ConsolePlugin.getDefault().getConsoleManager();
		PluginConsole ourConsole = null;
		for(org.eclipse.ui.console.IConsole console : mgr.getConsoles()) {
			if(console instanceof PluginConsole && title.equals(console.getName()) &&
					type.equals(console.getType())) {
				ourConsole = (PluginConsole) console;
				break;
			}
		}
		if(ourConsole == null) {
			ourConsole = new PluginConsole(title, type);
			IConsole[] array = new IConsole[] {(IConsole) ourConsole };
			mgr.addConsoles(array);

			// Don't activate the console. It will be activated on
			// first write.
		}

		final IOConsoleOutputStream stream = ourConsole.newOutputStream(errorStream);
		return stream;
	}

	@Override
	public void writeOutput(String message, String where) {
		if(outputStream != null) {
			PrintStream printOutput = new PrintStream(outputStream);
			String formatedMessage = formatMessage(message,where);
			printOutput.println(formatedMessage);	
		}
	}

	@Override
	public void writeError(String message, String where) {
		if(errorStream != null) {
			PrintStream printOutput = new PrintStream(errorStream);
			String formatedMessage = formatMessage(message, where);
			printOutput.println(formatedMessage);	
		}
	}
	
	private String formatMessage(String message, String where) {
		String time = getDate();
		String extended = "[XMapping: "+time+"]: ("+where+") ";
		return extended + message;
	}
	
	private String getDate() {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss.SSS");
		return formatter.format(date);
	}
	
}
