package org.xtext.example.mydsl.ui.messaging;

import java.io.PrintStream;

import org.eclipse.debug.ui.console.IConsole;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.console.AbstractConsole;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

public class MessagesUtils {

	public static final String CONSOLE = "Plugin Console";

	public static MessageConsole getConsole() {
		IConsoleManager consoleManager = ConsolePlugin.getDefault().getConsoleManager();
		IConsole[] existing = (IConsole[]) consoleManager.getConsoles();

		for(int i = 0; i < existing.length; i++)
			if(CONSOLE.equals(((AbstractConsole) existing[i]).getName()))
				return (MessageConsole) existing[i];

		// no console found, so create a new one
		MessageConsole beeLangConsole = new MessageConsole(CONSOLE, null);
		// TODO: preference page defining the console colors
		// beeLangConsole.setBackground(???);
		consoleManager.addConsoles((org.eclipse.ui.console.IConsole[]) new IConsole[] { (IConsole) beeLangConsole });
		return beeLangConsole;
	}

	public static PrintStream getConsoleErrorStream(MessageConsole console) {
		MessageConsoleStream stream = console.newMessageStream();
		// TODO: preference page defining the console colors
		Display display = PlatformUI.getWorkbench().getDisplay();
		stream.setColor(display.getSystemColor(SWT.COLOR_RED));
		return new PrintStream(stream);
	}

	public static PrintStream getConsoleOutputStream(MessageConsole console) {
		MessageConsoleStream stream = console.newMessageStream();
		// TODO: preference page defining the console colors
		// stream.setColor(???);
		return new PrintStream(stream);
	}

}
