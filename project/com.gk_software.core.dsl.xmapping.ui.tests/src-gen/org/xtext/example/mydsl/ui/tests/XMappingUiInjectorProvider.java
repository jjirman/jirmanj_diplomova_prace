/*
 * generated by Xtext 2.24.0
 */
package org.xtext.example.mydsl.ui.tests;

import com.gk_software.core.dsl.xmapping.ui.internal.XmappingActivator;
import com.google.inject.Injector;
import org.eclipse.xtext.testing.IInjectorProvider;

public class XMappingUiInjectorProvider implements IInjectorProvider {

	@Override
	public Injector getInjector() {
		return XmappingActivator.getInstance().getInjector("org.xtext.example.mydsl.XMapping");
	}

}
