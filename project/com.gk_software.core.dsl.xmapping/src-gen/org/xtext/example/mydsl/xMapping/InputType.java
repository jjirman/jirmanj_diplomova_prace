/**
 * generated by Xtext 2.24.0
 */
package org.xtext.example.mydsl.xMapping;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Input Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.xMapping.InputType#getInputRef <em>Input Ref</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.xMapping.InputType#getParamRef <em>Param Ref</em>}</li>
 * </ul>
 *
 * @see org.xtext.example.mydsl.xMapping.XMappingPackage#getInputType()
 * @model
 * @generated
 */
public interface InputType extends EObject
{
  /**
   * Returns the value of the '<em><b>Input Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Input Ref</em>' reference.
   * @see #setInputRef(Input)
   * @see org.xtext.example.mydsl.xMapping.XMappingPackage#getInputType_InputRef()
   * @model
   * @generated
   */
  Input getInputRef();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.xMapping.InputType#getInputRef <em>Input Ref</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Input Ref</em>' reference.
   * @see #getInputRef()
   * @generated
   */
  void setInputRef(Input value);

  /**
   * Returns the value of the '<em><b>Param Ref</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Param Ref</em>' reference.
   * @see #setParamRef(Param)
   * @see org.xtext.example.mydsl.xMapping.XMappingPackage#getInputType_ParamRef()
   * @model
   * @generated
   */
  Param getParamRef();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.xMapping.InputType#getParamRef <em>Param Ref</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Param Ref</em>' reference.
   * @see #getParamRef()
   * @generated
   */
  void setParamRef(Param value);

} // InputType
