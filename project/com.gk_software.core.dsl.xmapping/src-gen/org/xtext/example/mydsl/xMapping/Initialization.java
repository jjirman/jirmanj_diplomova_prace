/**
 * generated by Xtext 2.24.0
 */
package org.xtext.example.mydsl.xMapping;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Initialization</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.xMapping.Initialization#getAssignment <em>Assignment</em>}</li>
 * </ul>
 *
 * @see org.xtext.example.mydsl.xMapping.XMappingPackage#getInitialization()
 * @model
 * @generated
 */
public interface Initialization extends EObject
{
  /**
   * Returns the value of the '<em><b>Assignment</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Assignment</em>' containment reference.
   * @see #setAssignment(Assignment)
   * @see org.xtext.example.mydsl.xMapping.XMappingPackage#getInitialization_Assignment()
   * @model containment="true"
   * @generated
   */
  Assignment getAssignment();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.xMapping.Initialization#getAssignment <em>Assignment</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Assignment</em>' containment reference.
   * @see #getAssignment()
   * @generated
   */
  void setAssignment(Assignment value);

} // Initialization
