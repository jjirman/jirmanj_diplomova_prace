/**
 * generated by Xtext 2.24.0
 */
package org.xtext.example.mydsl.xMapping;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Assignment</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xtext.example.mydsl.xMapping.XMappingPackage#getAssignment()
 * @model
 * @generated
 */
public interface Assignment extends EObject
{
} // Assignment
