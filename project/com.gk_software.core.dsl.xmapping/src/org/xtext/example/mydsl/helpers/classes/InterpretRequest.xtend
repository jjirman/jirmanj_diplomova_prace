package org.xtext.example.mydsl.helpers.classes

/**
 * Class used in interpret (to make commands and so on..)
 * @author Bc. Jan Jirman
 *
 */
class InterpretRequest {
	
	/** request type */
	RequestTypeEnum requestType
	/** code to write into template */
	String code
	
	//************************************** CONSTRUCTORS *****************************************//
	
	/**
	 * Constructor which creates InterpretRequest
	 */
	new(String code, RequestTypeEnum requestType){
		this.code = code
		this.requestType = requestType
	}
	
	//************************************** SETTER & GETTER **************************************//
	
	/**
	 * Gets request type
	 */
	def RequestTypeEnum getRequestType(){
		return this.requestType
	}
	
	/**
	 * Gets code
	 */
	def String getCode(){
		return this.code	
	}
}
