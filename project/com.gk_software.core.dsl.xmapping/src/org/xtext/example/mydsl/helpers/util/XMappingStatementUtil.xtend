package org.xtext.example.mydsl.helpers.util

import org.xtext.example.mydsl.xMapping.Block
import java.util.List
import org.eclipse.xtext.EcoreUtil2
import org.xtext.example.mydsl.xMapping.Variable
import org.eclipse.xtext.validation.Check
import org.xtext.example.mydsl.typing.Type
import org.xtext.example.mydsl.typing.XMappingTypeProvider
import org.xtext.example.mydsl.xMapping.AssignmentStatement
import com.google.inject.Inject
import org.xtext.example.mydsl.xMapping.XMappingPackage
import org.eclipse.emf.ecore.EReference
import org.xtext.example.mydsl.xMapping.Return
import org.xtext.example.mydsl.typing.NotType
import org.xtext.example.mydsl.xMapping.Statement
import org.eclipse.emf.ecore.EObject
import org.xtext.example.mydsl.xMapping.Initialization
import org.xtext.example.mydsl.xMapping.Declaration
import org.eclipse.xtext.naming.IQualifiedNameProvider
import org.xtext.example.mydsl.xMapping.MapForwardStatement
import org.eclipse.xtext.nodemodel.util.NodeModelUtils
import org.xtext.example.mydsl.xMapping.MapStatement
import org.xtext.example.mydsl.xMapping.CallStatement
import org.xtext.example.mydsl.xMapping.OutputSection
import org.xtext.example.mydsl.xMapping.OutputAttribute
import org.eclipse.emf.common.util.EList
import org.xtext.example.mydsl.xMapping.Attribute
import org.xtext.example.mydsl.xMapping.Param
import org.xtext.example.mydsl.xMapping.RoutineBlock
import org.xtext.example.mydsl.xMapping.ParamSection
import org.xtext.example.mydsl.xMapping.InputAttribute
import java.util.ArrayList
import org.xtext.example.mydsl.xMapping.Model
import org.xtext.example.mydsl.xMapping.Routine

/**
 * Utility which helps interpet with statements
 * @author Bc. Jan Jirman
 *
 */
class XMappingStatementUtil {
	
	/** Name provider for objects */
	@Inject IQualifiedNameProvider nameProvider;	
	/** object util */
	@Inject XMappingEObjectUtil objectUtil;
	
	@Inject XMappingStatementUtil statementUtil;
	

	/**
	 * Finds the first initialization statement of the specific object 
	 * @param object object
	 * @return statement
	 */
 	def Statement findObjectInitialization(EObject object){
 		//gets block where object is located
		val block = EcoreUtil2.getContainerOfType(object, typeof(Block))
		val assignmentStatement = statementUtil.getAssignmentStatement(block,nameProvider.getFullyQualifiedName(object).lastSegment)
		if(assignmentStatement !== null && assignmentStatement.initialization !== null){
			return assignmentStatement;
		}
		return block.findFirstInit(object)	
	}
	
	/**
	 * Finds declared params
	 * @param statement statement
	 * @param param param
	 * @return object
	 */
	def EObject findDeclaredParam(Statement statement, Param param){
		if(statement !== null){
			val paramSection = EcoreUtil2.getContainerOfType(param, typeof(ParamSection)).paramSection
			if(statement instanceof MapStatement){
				if(paramSection.size == statement.inputSection.inputAttributes.length){
					val index = paramSection.indexOf(param);
					return statement.inputSection.inputAttributes.get(index)
				}else{
					var i = -1
					for(EObject input : statement.inputSection.inputAttributes){
						i++
						if(objectUtil.getBasicEObject(input).hashCode == param.hashCode){
							return param
						}
					}
				}
			}
		}	
		//not declared
		null
	}
	
	/**
	 * Gets assigment statement
	 * @param block block
	 * @param name name
	 * @return AssignmentStatement
	 */
	def AssignmentStatement getAssignmentStatement(Block block, String name){
		var statements = EcoreUtil2.getAllContentsOfType(block, typeof(AssignmentStatement))
		statements.findFirst[
			it.declaration.^new !== null && it.declaration.^new.exists[
				var source = it.source
				if(source instanceof Return){
					source.name.equals(name)
				}else if(source instanceof Variable){
					source.name.equals(name)
				}
			]
		]
	}
	
	 	/**
 	 * Gets assignment list
 	 * @param declaration Declaration object
 	 * @return assigment list
 	 */
 	def <T extends EObject> EList<T> getAssigmnentList(Declaration declaration){
 		var refSource = declaration.target
 		var newSource = declaration.^new
 		if(refSource !== null){
 			refSource.reference as EList<T>
 		}else if(newSource !== null){
 			newSource as EList<T>
 		}
 	}
	
	/**
	 * Finds the first initialization a returns its statement
	 * @param block block
	 * @param object object
	 */
	def Statement findFirstInit(Block block, EObject object){
		//gets all statements
		var statements = EcoreUtil2.getAllContentsOfType(block, typeof(Statement))
		//finds first statement which meets requirements
		if(statements !== null){
			return statements.findFirst[
				if(it instanceof AssignmentStatement){
					it.initialization !== null && 	
					//finds in object references
					((it.declaration.target !== null && it.declaration.target.reference.exists[it.source !== null && nameProvider.getFullyQualifiedName(it.source).equals(nameProvider.getFullyQualifiedName(object))])
						|| 
					//finds in object declaration
					(it.declaration.^new !== null && it.declaration.^new.exists[it.source !== null && nameProvider.getFullyQualifiedName(it.source).equals(nameProvider.getFullyQualifiedName(object))]))
				}else if(it instanceof MapStatement){
					//value can be NULL or OutputSection
					val outputSection = it.outputSection
					if(outputSection !== null){
						it.outputSection.outputAttributes.existsObjectInOutputSection(object)
					}
				}else if(it instanceof MapForwardStatement){
					it.outputSection.outputAttributes.existsObjectInOutputSection(object)
				}else if(it instanceof CallStatement){
					it.outputSection.outputAttributes.existsObjectInOutputSection(object)
				}
			]
		}
		null		
	}
	
	/**
	 * Gets returns from routine
	 * @param statement
	 * @return list of statements
	 */
	def List<EObject> getReturnsFromRoutine(Statement statement){
		var list = new ArrayList<EObject>();
		var statements = null as List<Statement>
 		statements = getListStatementWithReturns(statement)
 		for(Statement assignment : statements){
			if(assignment instanceof AssignmentStatement){
				var objects = getAssigmnentList(assignment.declaration)
 				for(EObject attr : objects){
 					var obj = objectUtil.getBasicEObject(attr);
 					if(obj instanceof Return){
 						list.add(obj);
 					}
 				}
			}
		}
 		
 		return list
	}
	
	/**
	 * Gets list of statements where are returns
	 * @param statement map statement
	 */
	private def getListStatementWithReturns(Statement statement){
		if(statement instanceof MapStatement){
			val routineName = statement.routineSection.source.name
			val target = statement.routineSection.target
			var obj = statement as EObject
			if(target !== null){
				obj = statement.routineSection.target.class_	
			}
			var model = EcoreUtil2.getContainerOfType(obj,typeof(Model))
			var routineBlock = EcoreUtil2.getAllContentsOfType(model,typeof(RoutineBlock))
			var routine = routineBlock.findFirst[it.routineName.name.equals(routineName)]
			return getListOfReturnStatements(routine)	
		}
		
		var routineBlock = EcoreUtil2.getContainerOfType(statement,typeof(RoutineBlock))
		return getListOfReturnStatements(routineBlock)
		
	}
	
	/**
	 * Gets list of statements with returns
	 * @param routine block routine block
	 */
	def List<Statement> getListOfReturnStatements(RoutineBlock routine){
		if(routine !== null){
				return routine.routineBlock.statement.filter[it |
					if(it instanceof AssignmentStatement){
						if(it.declaration.^new !== null){
							it.declaration.^new.exists[it |
								return it.source instanceof Return	
							]
						}else
						if(it.declaration.reference !== null){
							it.declaration.reference.exists[it |
								return it.source instanceof Return
							]
						}
					}
				].toList
		}	
		return new ArrayList<Statement>();
	}
	
	/**
	 * Checks that object is in ouput section of statement (used for MapStatement / MapForwardStatement / Call statement)
	 * @param outputAttributes output attributes
	 * @param object object
	 */
	def boolean existsObjectInOutputSection(EList<EObject> outputAttributes, EObject object){
		return outputAttributes.exists[ 
			if(it instanceof Attribute){
				nameProvider.getFullyQualifiedName(it.type).equals(nameProvider.getFullyQualifiedName(object))
			}else if(it instanceof OutputAttribute){
				//2 refs (output and return)
				val returnRef = it.outputType.returnRef 
				val outputRed = it.outputType.outputRef
				if(returnRef !== null){
					nameProvider.getFullyQualifiedName(returnRef).equals(nameProvider.getFullyQualifiedName(object))
				}else{
					nameProvider.getFullyQualifiedName(outputRed).equals(nameProvider.getFullyQualifiedName(object))
				}
			}
		]
	}
}
