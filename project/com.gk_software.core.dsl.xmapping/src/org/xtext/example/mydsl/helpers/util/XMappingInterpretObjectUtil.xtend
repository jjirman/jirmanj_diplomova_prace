package org.xtext.example.mydsl.helpers.util

import org.eclipse.emf.ecore.EObject
import com.google.inject.Inject
import org.xtext.example.mydsl.typing.XMappingTypeProvider
import org.xtext.example.mydsl.xMapping.InputAttribute
import org.xtext.example.mydsl.xMapping.OutputAttribute
import org.xtext.example.mydsl.xMapping.Return
import org.xtext.example.mydsl.typing.IntType
import org.xtext.example.mydsl.typing.DoubleType
import org.xtext.example.mydsl.typing.StringType
import org.xtext.example.mydsl.typing.StructureType
import org.xtext.example.mydsl.xMapping.Attribute
import org.xtext.example.mydsl.helpers.util.XMappingEObjectUtil
import org.xtext.example.mydsl.xMapping.Input

/**
 * Utility which helps interpet with objects
 * @author Bc. Jan Jirman
 *
 */
class XMappingInterpretObjectUtil {

	/** type provider */
	@Inject XMappingTypeProvider typeProvider
	/** object util */
	@Inject XMappingEObjectUtil objectUtil
	
	/**
	 * Generates java name from object name
	 * @param objectName object name
	 * @return object java name
	 */
 	def String getJavaName(String objectName){
 		var betterName = objectName.replace(".","_")
 		betterName = betterName.toLowerCase
 	}
 	
 	/**
 	 * Gets Xpath string from object
 	 * @param object object
 	 * @return charSequence xpath
 	 */
 	def getObjectXPathCode(EObject object){
 		switch(object){
	 		InputAttribute: {
	 			if(object.XPath !== null){
	 				return '''"�object.XPath�"'''
	 			}
			}
			OutputAttribute:{
				if(object.XPath !== null){
					return '''"�object.XPath�"'''
				}
			}
			Attribute: {
				if(object.XPath !== null){
					return '''"�object.XPath�"'''	
				}	
			}
		}
 	}
 	
 	/**
 	 * Gets object type
 	 * @param object object
 	 * @return object type
 	 */
	def String getObjectType(EObject object){
		val basicObject = objectUtil.getBasicEObject(object)
		if(basicObject instanceof Return){
			return	'''ReturnValue'''
		}else{
			val type = typeProvider.typeForObject(object)
			switch(type){
				IntType:'''int'''
				DoubleType:'''double'''
				StringType:'''String'''
				StructureType:{
					if(basicObject instanceof Input){
						'''FileInput'''
					}else{
						'''DocumentOutput'''
					}
				}
			}
		}
	}
}
