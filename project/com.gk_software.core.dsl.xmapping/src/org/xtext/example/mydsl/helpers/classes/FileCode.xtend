package org.xtext.example.mydsl.helpers.classes

import java.util.List
import java.util.ArrayList
import org.xtext.example.mydsl.xMapping.OutputImport
import org.xtext.example.mydsl.xMapping.InputImport

/**
 * Class used in generator - contains all information about class / routine
 * @author Bc. Jan Jirman
 *
 */
class FileCode {
	/** File package */
	String packageCode
	/** Imports which class contains */
	List<InputImport> inputs = new ArrayList<InputImport>();
	/** Outputs which class contains */
	List<OutputImport> outputs = new ArrayList<OutputImport>();
	/** Class name */
	String className;
	/** Import to add (not defined in .mapping file) */
	List<String> importsCode
	/** chaSequence of class main block  */
	CharSequence blockCode;
	
	//************************************** CONSTRUCTORS *****************************************//
	
	/**
	 * Constructor which creates FileCode
	 * @param packageCode package code
	 * @param className class name
	 */
	new(String packageCode, String className){
		this.packageCode = packageCode
		this.className = className;
		this.importsCode = new ArrayList<String>()
		this.blockCode = new StringBuffer("")
	}
	
	//************************************** PUBLIC METHODS ***************************************//
	
	/**
	 * adds part of main code block
	 * @param blockCode code block
	 */
	def void addCodeBlock(CharSequence blockCode){
		var buffer = new StringBuffer(this.blockCode)
		buffer.append(blockCode)
		this.blockCode = buffer;
	}

	/**
	 * Adds new import into importsCode (which are added dynamically)
	 * @param importValue new import 
	 */
	def void addImport(String importValue){
		if(importValue === null || !importValue.equals("")){
			this.importsCode.add(importValue)
		}
	}
	
	/**
	 * Adds input
	 * @param input input
	 */
	def addInput(InputImport input){
		inputs.add(input);
	}
	
	/**
	 * Adds output
	 * @param output output
	 */
	def addOutput(OutputImport output){
		outputs.add(output);
	}
	
	/**
	 * Gets full routine name
	 * @return routine fullname
	 */
	def String getRoutineFullName(){
		var realPackage = getRoutinePackage()
		return realPackage + "." + this.className
	}
	
	/**
	 * Gets routine package name
	 * @return package name
	 */
	def String getRoutinePackage(){
		var toSplitArray = this.packageCode.split("\\.")
		val toSplit = toSplitArray.get(toSplitArray.size - 1);
		var fullName = this.packageCode.replace(toSplit,"statement")
		fullName = fullName + "." + toSplit.toLowerCase
	}
	
	//************************************** SETTER & GETTER **************************************//
	
	/**
	 * Gets all inputs
	 * @return all inputs
	 */
	def List<InputImport> getInputs(){
		return this.inputs
	}
	
	/**
	 * Gets all outputs
	 * @return all outputs
	 */
	def List<OutputImport> getOutputs(){
		return this.outputs
	}
	
	/**
	 * Sets block code
	 * @param block code
	 */
	def CharSequence setBlockCode(CharSequence blockCode){
		return this.blockCode = blockCode
	}
	
	/**
	 * Gets package code
	 * @return package code
	 */
	def String getPackageCode(){
		return this.packageCode
	}
	
	/**
	 * Gets full name
	 * @return full name
	 */
	def String getFullName(){
		return this.packageCode + "." + this.className
	}
	
	/**
	 * Gets class name
	 * @return class name
	 */
	def String getClassName(){
		return this.className
	}
	
	/**
	 * Gets all imports
	 * @return list of imports
	 */
	def List<String> getImportsCode(){
		return this.importsCode
	}
	
	/**
	 * Gets block code
	 * @return block code
	 */
	def CharSequence getBlockCode(){
		return this.blockCode
	}
	
	/**
	 * Sets imports code
	 * @param list of importCode
	 */
	def void setImportsCode(List<String> importsCode){
		this.importsCode = importsCode
	}
}
