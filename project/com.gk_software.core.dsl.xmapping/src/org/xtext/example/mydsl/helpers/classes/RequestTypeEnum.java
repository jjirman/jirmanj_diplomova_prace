package org.xtext.example.mydsl.helpers.classes;

/**
 * Enum for interpret request type
 * @author Bc. Jan Jirman
 *
 */
public enum RequestTypeEnum {
	REQUEST_MACRO, //request for creating macro
	REQUEST_IMPORT, // request to import file
	REQUEST_COMMAND //request to make command
}
