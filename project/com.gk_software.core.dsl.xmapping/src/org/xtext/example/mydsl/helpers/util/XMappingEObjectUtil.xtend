package org.xtext.example.mydsl.helpers.util

import org.eclipse.emf.ecore.EObject
import org.xtext.example.mydsl.xMapping.InputAttribute
import org.xtext.example.mydsl.xMapping.OutputAttribute
import org.xtext.example.mydsl.xMapping.Attribute
import org.xtext.example.mydsl.xMapping.Block
import org.eclipse.emf.common.util.EList
import org.eclipse.xtext.naming.IQualifiedNameProvider
import com.google.inject.Inject
import org.xtext.example.mydsl.xMapping.Return
import org.xtext.example.mydsl.xMapping.Variable
import org.xtext.example.mydsl.xMapping.Statement
import org.xtext.example.mydsl.xMapping.NewAttribute
import org.xtext.example.mydsl.xMapping.UsedAttribute
import org.xtext.example.mydsl.xMapping.Param
import java.util.List
import org.xtext.example.mydsl.xMapping.ParamSection
import org.xtext.example.mydsl.xMapping.Input
import org.xtext.example.mydsl.xMapping.Output
import java.util.ArrayList
import org.xtext.example.mydsl.xMapping.AssignmentStatement
import org.xtext.example.mydsl.xMapping.MapStatement
import org.eclipse.xtext.EcoreUtil2
import org.xtext.example.mydsl.xMapping.Model
import org.xtext.example.mydsl.xMapping.RoutineBlock
import org.xtext.example.mydsl.xMapping.InputImport
import org.xtext.example.mydsl.xMapping.OutputImport

/**
 * Utility which helps work with EObjects
 * @author Bc. Jan Jirman
 *
 */
class XMappingEObjectUtil {
	
	/** statement util */
	@Inject XMappingStatementUtil statementUtil
	/** name provider */
	@Inject IQualifiedNameProvider nameProvider	
	
	/**
	 * Gets object name
	 */
	def String getObjectName(EObject object){
		switch(object){
			InputAttribute: {
				val reference = object.inputType
 				if(reference.paramRef !== null){
 					var container = reference.paramRef.eContainer
					if(container instanceof ParamSection){
						val index = container.paramSection.indexOf(object.getBasicEObject);
						return "input"+index
					}
 					return reference.paramRef.name
 				}
 				return reference.inputRef.name
			}
			OutputAttribute:{
				val reference = object.outputType
 				if(reference.returnRef !== null){
 					return reference.returnRef.name
 				}
 				return reference.outputRef.name
			}
			Attribute: {
				return object.type.name
			}
			Param: {
				var container = object.eCrossReferences.get(0).eContainer
				if(container instanceof List){
					val index = container.indexOf(object);
					return "input"+index
				}
				return object.name
			}
			Variable: object.name
			Return: object.name	
			Input: object.name
			Output: object.name
		}
	}

	
	/**
	 * Gets Object's Xpath
	 * @param object object
	 */
	def String getObjectXPath(EObject object){
		switch(object){
	 		InputAttribute: {
	 			if(object.XPath !== null){
	 				return object.XPath	
	 			}
			}
			OutputAttribute:{
				if(object.XPath !== null){
					return object.XPath	
				}
			}
			Attribute: {
				if(object.XPath !== null){
					return object.XPath	
				}	
			}
		}
		return null
 	}
 	
 	/**
 	 * Gets basic object (like Variable, Input, Param) from object like InputAttribute, ...
 	 */
 	def EObject getBasicEObject(EObject object){
 		switch(object){
 			InputAttribute: {
				val reference = object.inputType
				if(reference !== null){
					if(reference.paramRef !== null){
	 					return reference.paramRef
	 				}
	 				return reference.inputRef
				}	
			}
			OutputAttribute:{
				val reference = object.outputType
				if(reference !== null){
					if(reference.returnRef !== null){
	 					return reference.returnRef
		 			}
	 				return reference.outputRef
				}
			}
			Attribute: {
				return object.type
			}
			NewAttribute: {
				return object.source
			}
			UsedAttribute:{
				return object.source
			}
			InputImport:{
				return object.name
			}
			OutputImport:{
				return object.name
			}
		}
		return object
 	}
 	
 	/**
	 * Finds type for object in CallStatement
	 * @param <T> extends EObject
	 * @param list list with attributes
	 * @param object object
	 * @return object index in list
	 */
 	def<T extends EObject> int getObjectIndexByObject(EList<T> list, EObject object){
 		var objectIndex = -1
 		//iteration
 		for(EObject obj : list){
 			objectIndex++
 			//OutputAttribute type
 			if(obj instanceof OutputAttribute){
 				var instance = obj.outputType
 				if (instance.returnRef !== null){
	 				if(nameProvider.getFullyQualifiedName(instance.returnRef).equals(nameProvider.getFullyQualifiedName(object))){
	 					return objectIndex
	 				}
	 			}else if(instance.outputRef !== null){
	 				if(nameProvider.getFullyQualifiedName(instance.returnRef).equals(nameProvider.getFullyQualifiedName(object))){
	 					return objectIndex
	 				}
	 			}	
	 		//Attribute type
 			}else if (obj instanceof Attribute){
 				if(nameProvider.getFullyQualifiedName(obj.type).equals(nameProvider.getFullyQualifiedName(object))){
 					return objectIndex
 				}
 			}	
 		}
 		objectIndex = -1
 		return objectIndex		
 	}
 	
 	/**
	 * Gets object's declaration
	 * @param <T> extends EObject
	 * @param statement statement which has declaration of object
	 * @return declaration list
	 */
	def <T extends EObject> List<T> getObjectFromDeclaration(Statement statement){
		//is assignment statement
		if(statement instanceof AssignmentStatement){
			//gets declaration list
			if(statement.declaration.^new !== null){
				return statement.declaration.^new as List<T>;
			}else if (statement.declaration.reference !== null){
				return statement.declaration.reference as List<T>;
			}
		}
		//nothing found
		return new ArrayList<T>();
	}
 	
 	
 	/**
	 * Gets object by its name
	 * @param block block where to find
	 * @param name object name
	 * @return EObject instance
	 */
 	def EObject getObjectByName(Block block, String name){
 		val index = 0;
 		val assignmentStatement = statementUtil.getAssignmentStatement(block,name)
 		if(assignmentStatement !== null){
 			//checks that assignmentStatement contains name
 			return assignmentStatement.declaration.^new.filter[ it |
 				var source = it.source
				if(source instanceof Return){
					source.name.equals(name)
				}else if(source instanceof Variable){
					source.name.equals(name)
				}
 			].get(index).source //.source returns specific object
 		}
 		null	
 	}
 	
 	/**
 	 * Checks that object is Initialized in current statement
 	 */
 	def boolean isEObjectInitializedInStatement(Statement statement, EObject object){
 		//find object init
 		val findStatement = statementUtil.findObjectInitialization(object.getBasicEObject)
 		//check
 		if(findStatement !== null){
 			if(findStatement.hashCode == statement.hashCode){
 				return true;
 			}
 		}
 		return false;
 	}	
}
