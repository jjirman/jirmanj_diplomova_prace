package org.xtext.example.mydsl.helpers.util

import org.xtext.example.mydsl.xMapping.Assignment
import org.xtext.example.mydsl.xMapping.AssignmentStructure
import org.xtext.example.mydsl.xMapping.Declaration
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EObject
import com.google.inject.Inject
import org.eclipse.xtext.EcoreUtil2
import org.xtext.example.mydsl.xMapping.Model
import org.xtext.example.mydsl.helpers.util.XMappingEObjectUtil
import org.xtext.example.mydsl.xMapping.MapStatement
import org.xtext.example.mydsl.xMapping.RoutineBlock
import org.xtext.example.mydsl.xMapping.AssignmentStatement
import org.xtext.example.mydsl.xMapping.Return
import org.xtext.example.mydsl.xMapping.Statement
import java.util.List
import org.eclipse.xtext.naming.IQualifiedNameProvider

/**
 * Utility which helps interpet in general
 * @author Bc. Jan Jirman
 *
 */
class XMappingInterpretUtil {
	
	/** interpret object */
	@Inject extension XMappingInterpretObjectUtil
	/** object util */
	@Inject XMappingEObjectUtil objectUtil
	/** statement util */
	@Inject XMappingStatementUtil statementUtil
	/** name provider */
	@Inject IQualifiedNameProvider nameProvider	
	
	/**
	 * Gets assignment value from "Assignment object"
	 * @param assignment Assignment object
	 * @return value
	 */
	def String getAssignmentValue(Assignment assignment){
 		switch(assignment){
 			org.xtext.example.mydsl.xMapping.IntType: '''�assignment.value�'''
 			org.xtext.example.mydsl.xMapping.DoubleType: '''�assignment.value�'''
 			org.xtext.example.mydsl.xMapping.StringType: '''"�assignment.value�"'''
 			AssignmentStructure: {
 				return specificAssignmentStructure(assignment)
 			}
 		}
 	}
 	
 	/**
 	 * Gets specific assigment structure
 	 * @param assignment assignment object
 	 * @return object java name
 	 */
 	def String specificAssignmentStructure(Assignment assignment){
 		if(assignment instanceof AssignmentStructure){
			var model = EcoreUtil2.getContainerOfType(assignment,typeof(Model))
			var mappingClass = model.class_.name
			var packageName = model.name
			return '''�packageName�.�mappingClass�.�getJavaName(objectUtil.getObjectName(assignment.source.outputType.outputRef))�.getPartOfDocument("�assignment.source.XPath�")'''
 		}
 	}
 	
 	/**
 	 * Gets return from routine by index
 	 * @param statement map statement
 	 * @param index index
 	 */
 	def EObject getReturnObjectByIndex(Statement statement, int index){
 		val returns = statementUtil.getReturnsFromRoutine(statement);
 		val returnsSize = returns.size;
 		if(index < returnsSize){
 			for(EObject returnObject : returns){
 				var name = nameProvider.getFullyQualifiedName(returnObject).lastSegment;
 				var returnToFind = name
 				val regex = "Return.\\d"
 				val nameToFind = "Return."+index
 				returnToFind = returnToFind.replaceAll(regex,nameToFind)
 				if(returnToFind.equals(name)){
 					return returnObject;
 				}
 			}
 			return null;
 		}
 		return null;
 	}
}
