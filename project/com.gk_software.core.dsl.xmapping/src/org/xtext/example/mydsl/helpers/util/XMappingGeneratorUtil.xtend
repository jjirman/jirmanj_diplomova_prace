package org.xtext.example.mydsl.helpers.util

import org.eclipse.emf.ecore.resource.Resource

/**
 * Utility which helps generator to generate right file paths, names
 * @author Bc. Jan Jirman
 *
 */
class XMappingGeneratorUtil {
	
	/**
	 * Gets resource fullname
	 * @param resource resource
	 */
	def String getResourceFullName(Resource resource){
    	val size = resource.URI.segments.length
		val name = resource.getURI.segment(size-1)
		return name
    }
	
	/**
	 * Gets resource name
	 * @param resource resource
	 */
	def String getResourceName(Resource resource){
    	val size = resource.URI.segments.length
		val name = resource.getURI.segment(size-1).split('\\.').iterator.next
		val finalName = name.substring(0,1).toUpperCase() + name.substring(1).toLowerCase()
		return finalName
    }
    
    /**
     * Gets class name
     * @param class name
     */
    def String getClassName(String className){
		val nameClass =  className.substring(0,1).toUpperCase() + className.substring(1)
		return nameClass
    }
    
    /**
     * Gets routine name
     * @param routineName routine name
     */
    def String getRoutineName(String routineName){
    	val index = 0;
    	val secondIndex = 1;
    	// Macro.<name><----
    	var splittedName = routineName.split("\\.")
    	var routinePartName = splittedName.get(secondIndex)
    	var routinePartMacro = splittedName.get(index)
    	routinePartName = routinePartName.toFirstUpper
    	
		return routinePartMacro + routinePartName
    }
    
    /**
     * Gets resource src - package is converted from "my.path" to "my/path/" 
     */
    def String getResourceSrcCommand(String packageName){
    	var packageInput = packageName
    	packageInput = packageInput.replaceAll("\\.","\\/")
    	packageInput = packageInput.replaceAll("\\-","\\_")
    	packageInput = packageInput + "/"
    	return packageInput
    }	
	
}
