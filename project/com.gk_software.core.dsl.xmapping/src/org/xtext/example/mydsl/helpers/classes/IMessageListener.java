package org.xtext.example.mydsl.helpers.classes;

import java.io.OutputStream;

import org.eclipse.core.runtime.CoreException;

public interface IMessageListener {
	
	void writeOutput(String message, String where);
	void writeError(String message, String where);
}
