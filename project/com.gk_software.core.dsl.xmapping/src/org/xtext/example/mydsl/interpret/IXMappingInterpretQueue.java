package org.xtext.example.mydsl.interpret;

import org.eclipse.emf.ecore.EObject;
import org.xtext.example.mydsl.helpers.classes.InterpretRequest;
import org.xtext.example.mydsl.typing.Type;
import org.xtext.example.mydsl.xMapping.Assignment;
import org.xtext.example.mydsl.xMapping.Statement;

public interface IXMappingInterpretQueue {

	void pushRoutineGenerator();
 	void pushNewImport(String importValue);
 	void pushObjectNonPrimitiveType(EObject object, String identifier, Type type);
 	String pushObjectType(EObject object);
 	void pushObjectName(EObject object);
 	void pushAssignmentValue(Assignment assignment);
 	void pushEndCommand();
 	void pushCustomCode(String code);
 	void pushNewLine();
 	void pushErrorCode(String text);
 	boolean pushXpath(EObject object);
 	boolean pushRequest(InterpretRequest request);
 	String popRequest();
 	int getStackSize();
 	
}
