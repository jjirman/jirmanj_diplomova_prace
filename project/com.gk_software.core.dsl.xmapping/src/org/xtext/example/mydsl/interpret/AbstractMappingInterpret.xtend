package org.xtext.example.mydsl.interpret

import org.xtext.example.mydsl.xMapping.Statement
import org.xtext.example.mydsl.xMapping.MapStatement
import java.util.List
import org.xtext.example.mydsl.xMapping.CallStatement
import org.xtext.example.mydsl.xMapping.AssignmentStatement
import org.xtext.example.mydsl.xMapping.InputImport
import org.xtext.example.mydsl.xMapping.MapForwardStatement
import org.xtext.example.mydsl.xMapping.OutputImport

abstract class AbstractMappingInterpret {
	
	/**
	 * Interprets an assignment statement
	 * @param statement assignment statement
	 */
	def dispatch boolean interpret(AssignmentStatement statement){
		return false;
	}
	/**
	 * Interprets a map forward statement
	 * @param statement map forward statement
	 */
	def dispatch boolean interpret(MapForwardStatement statement){
		return false;
	}
	/**
	 * Interprets a call statement
	 * @param statement call statement
	 */
	def dispatch boolean interpret(CallStatement statement){
		return false;
	}
	/**
	 * Interprets a map statement
	 * @param statement map statement
	 */
	def dispatch boolean interpret(MapStatement statement){
		return false;
	}
	
	/**
	 * Interprets an input import
	 * @param inputImport input import
	 */
	def dispatch boolean interpret(InputImport inputImport){
		return false;
	}
	
	/**
	 * Interprets an output import
	 * @param outputImport output import
	 */
	def dispatch boolean interpret(OutputImport outputImport){
		return false;
	}
	
	/**
	 * Interprets a method header (routine header
	 * @param statement map statement
	 * @param outer statements nested statements
	 */
	def abstract void interpretMethodHeader(MapStatement statement, List<MapStatement> outerStatements);
	/**
	 * Interprets routine
	 * @param statement statement
	 * @param outer statements nested statements
	 */
	def abstract boolean interpretRoutine(Statement statement, List<MapStatement> outerStatements);
	
	def abstract String getRequest();
	
	def abstract int getRequestsSize();
}