package org.xtext.example.mydsl.interpret

import org.xtext.example.mydsl.xMapping.Assignment
import org.eclipse.emf.ecore.EObject
import org.xtext.example.mydsl.xMapping.Statement
import org.xtext.example.mydsl.helpers.classes.InterpretRequest
import java.util.Queue
import java.util.LinkedList
import org.xtext.example.mydsl.helpers.classes.RequestTypeEnum
import org.xtext.example.mydsl.typing.XMappingTypeProvider
import javax.inject.Inject
import org.xtext.example.mydsl.helpers.util.XMappingInterpretObjectUtil
import org.xtext.example.mydsl.helpers.util.XMappingInterpretUtil
import org.xtext.example.mydsl.helpers.util.XMappingEObjectUtil
import org.xtext.example.mydsl.typing.Type
import org.xtext.example.mydsl.xMapping.Input
import org.xtext.example.mydsl.xMapping.Output
import org.eclipse.xtext.EcoreUtil2
import org.xtext.example.mydsl.xMapping.Model
import org.xtext.example.mydsl.helpers.classes.SourcesEnum

class XMappingInterpretQueue implements IXMappingInterpretQueue{
	/** type provider */
	@Inject XMappingTypeProvider typeProvider 
	/** interpret object */
	@Inject extension XMappingInterpretObjectUtil
	/** interpret util */
	@Inject extension XMappingInterpretUtil
	/** object util */
	@Inject XMappingEObjectUtil objectUtil
	
	/** constant for routine call */
	final static String GENERATOR_COMMAND = "//routine"
	/** queue for requests (to make command) */
	Queue<InterpretRequest> requests = new LinkedList<InterpretRequest>();
	
	override getStackSize() {
		this.requests.size
	}
		
	override pushRoutineGenerator() {
		val space = SourcesEnum.GENERAL_SPACE.value;
 		var request = new InterpretRequest(space + GENERATOR_COMMAND, RequestTypeEnum.REQUEST_MACRO)
 		request.pushRequest
	}
 	
 	override pushNewImport(String importValue){
 		val importCommand = SourcesEnum.CODE_IMPORT.value
 		val code = importCommand + importValue
 		
 		var request = new InterpretRequest(code,RequestTypeEnum.REQUEST_COMMAND)
 		request.pushRequest	
 	}
 	
 	override pushObjectNonPrimitiveType(EObject object, String identifier, Type type){
 		var request = null as InterpretRequest 
 		typeProvider.resetRoutineScope
 		var objectType = type		
 		if(identifier !== null){
 			objectType =typeProvider.methodType(identifier);
 		}else if(object !== null){
 			objectType = typeProvider.typeForObject(object);	
 		}
 		val code = typeProvider.getNonPrimitiveType(objectType);
 		request = new InterpretRequest(code,RequestTypeEnum.REQUEST_COMMAND)
 		request.pushRequest	
 	}
 	 	
 	override String pushObjectType(EObject object){
 		val document = SourcesEnum.CODE_DOCUMENT.value
 		val file = SourcesEnum.CODE_FILE_INPUT.value
 		var returnValue = SourcesEnum.CODE_RETURN_VALUE.value
 		val code = getObjectType(object)
 		if(code !== null){
 			if(code.contains(document)){			
	 			pushNewImport(SourcesEnum.IMPORT_DOCUMENT_OUTPUT.value)
	 		}else if(code.contains(returnValue)){
	 			pushNewImport(SourcesEnum.IMPORT_RETURN_VALUE.value)
	 		}else if(code.contains(file)){
	 			pushNewImport(SourcesEnum.IMPORT_FILE_INPUT.value)
	 		}
	 		var request = new InterpretRequest(code,RequestTypeEnum.REQUEST_COMMAND)
	 		request.pushRequest	
 		}
 		
 		return code
 	}
 	
 	override pushObjectName(EObject object){
 		var request = null as InterpretRequest 
 		var name = getJavaName(objectUtil.getObjectName(object))
 		val basicObject = objectUtil.getBasicEObject(object);
 		if(basicObject instanceof Input || basicObject instanceof Output){
 			var model = EcoreUtil2.getContainerOfType(object,typeof(Model))
			var mappingClass = model.class_.name
			var packageName = model.name
			val path = packageName + SourcesEnum.GENERAL_DOT.value + mappingClass+ SourcesEnum.GENERAL_DOT.value	
			name = path + name	
 		}
 		request = new InterpretRequest(name,RequestTypeEnum.REQUEST_COMMAND)
 		request.pushRequest	
 	}
 	
 	override pushAssignmentValue(Assignment assignment){
 		val code = getAssignmentValue(assignment)
 		var request = new InterpretRequest(code,RequestTypeEnum.REQUEST_COMMAND)
 		request.pushRequest
 	}
 	
 	override pushEndCommand(){
 		val character = SourcesEnum.GENERAL_DELIMITER.value
 		var request = new InterpretRequest(character,RequestTypeEnum.REQUEST_COMMAND)
 		request.pushRequest
 	}
 	
 	override pushCustomCode(String code){
 		var request = new InterpretRequest(code,RequestTypeEnum.REQUEST_COMMAND)
 		request.pushRequest
 	}
 	
 	override pushNewLine(){
 		val code = '''
 		
 		'''
 		var request = new InterpretRequest(code,RequestTypeEnum.REQUEST_COMMAND)
 		request.pushRequest
 	}
 	
 	override pushErrorCode(String text){
 		var request = new InterpretRequest(text,RequestTypeEnum.REQUEST_COMMAND)
 		request.pushRequest
 	}
 	
 	override boolean pushXpath(EObject object){
 		var code = getObjectXPathCode(object)
 		if(code === null){
 			return false
 		}
 		var request = new InterpretRequest(code,RequestTypeEnum.REQUEST_COMMAND)
 		request.pushRequest
 		return true
 	}
 	
 	override boolean pushRequest(InterpretRequest request){
 		if(request !== null){
 			requests.add(request)	
 			return true	
 		}	
 		return false
 	}
 	
 	override String popRequest(){
 		val request = requests.poll()
 		return request.code
 	}
 	 
}