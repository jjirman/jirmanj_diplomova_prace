package org.xtext.example.mydsl.interpret

import org.xtext.example.mydsl.xMapping.AssignmentStatement
import org.xtext.example.mydsl.typing.XMappingTypeProvider
import com.google.inject.Inject
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.common.util.EList
import org.xtext.example.mydsl.xMapping.MapForwardStatement
import org.xtext.example.mydsl.xMapping.Return
import java.util.List
import org.xtext.example.mydsl.xMapping.Attribute
import org.xtext.example.mydsl.typing.StructureType
import org.xtext.example.mydsl.xMapping.MapStatement
import org.xtext.example.mydsl.xMapping.Statement
import org.xtext.example.mydsl.xMapping.CallStatement
import org.eclipse.xtext.common.types.JvmOperation
import org.eclipse.xtext.common.types.JvmGenericType
import org.eclipse.xtext.common.types.JvmMember
import org.xtext.example.mydsl.xMapping.InputImport
import org.xtext.example.mydsl.xMapping.OutputImport
import org.xtext.example.mydsl.helpers.util.XMappingInterpretObjectUtil
import org.xtext.example.mydsl.helpers.util.XMappingInterpretUtil
import org.xtext.example.mydsl.helpers.util.XMappingEObjectUtil
import org.xtext.example.mydsl.helpers.util.XMappingGeneratorUtil
import org.xtext.example.mydsl.xMapping.Output
import org.xtext.example.mydsl.helpers.util.XMappingStatementUtil
import org.xtext.example.mydsl.helpers.classes.SourcesEnum
import org.xtext.example.mydsl.typing.Type
import org.xtext.example.mydsl.typing.NotType

class XMappingInterpret extends AbstractMappingInterpret {
		
	/** interpret object */
	@Inject extension XMappingInterpretObjectUtil
	/** interpret util */
	@Inject extension XMappingInterpretUtil
	/** object util */
	@Inject XMappingEObjectUtil objectUtil
	/** object util */
	@Inject XMappingStatementUtil statementUtil
	/** generator util */
	@Inject XMappingGeneratorUtil generatorUtil
	/** type provider */
	@Inject XMappingTypeProvider typeProvider
	/** interpret queue */
	@Inject extension IXMappingInterpretQueue 
	
	/*                                AssignmentCommand
 	 *  ------------------------------------------------------------------------------------
 	 *  ------------------------------------------------------------------------------------  
 	 */
 	 
 	 /**
 	  * Creates type T "<T>"
 	  * @param statement statement
 	  * @param object object
 	  */
 	 def createTType(EObject object, String identifier, Type type){
 	 	pushCustomCode(SourcesEnum.GENERAL_LESS_THAN.value)
 		pushObjectNonPrimitiveType(object, identifier, type)
 		pushCustomCode(SourcesEnum.GENERAL_GREATER_THAN.value)
 	 }
 	 
 	 /**
 	  * Creates casting "(type)"
 	  */
 	  def createCastType(EObject object, String identifier, Type type){
 	  	pushCustomCode(SourcesEnum.GENERAL_LEFT_BRACKET.value);  	
 		pushObjectNonPrimitiveType(object,identifier,type);
 		pushCustomCode(SourcesEnum.GENERAL_RIGHT_BRACKET.value)	
 	  }
 	  
 	  /**
 	   * Creates new ReturnValue "new ReturnValue<T>([value])"
 	   */
 	   def createNewReturnValue(Statement statement, EObject object){
 	   		pushCustomCode("new "+SourcesEnum.CODE_RETURN_VALUE.value)
			object.createTType(null,null);
			pushCustomCode(SourcesEnum.GENERAL_LEFT_BRACKET.value)
			//assignment command 
			if(statement instanceof AssignmentStatement){
				pushAssignmentValue(statement.initialization.assignment)
			}else{
				//TODO
				//pushCustomCode(value);
			}
			pushCustomCode(SourcesEnum.GENERAL_RIGHT_BRACKET.value)
			pushNewImport(SourcesEnum.IMPORT_RETURN_VALUE.value)
 	   }
 	 
 	 /**
 	  * Creates assignment return value "[name] = new ReturnValue<T>([value])"
 	  */
 	 def createAssignmentReturnValue(Statement statement, EObject object){
 	 	object.createTType(null,null);
 	 	pushCustomCode(SourcesEnum.GENERAL_SPACE.value)
		pushObjectName(object)
		pushCustomCode(SourcesEnum.GENERAL_EQUALS.value)
		statement.createNewReturnValue(object);		
 	 }
	
	override dispatch boolean interpret(AssignmentStatement statement) {	
		//is initialization	
		if(statement.initialization !== null){
			val list = statementUtil.getAssigmnentList(statement.declaration)
			for(EObject decl : list){
				var object = objectUtil.getBasicEObject(decl)
				// 'int <space> variable_name ='
				var initializedInStatement = objectUtil.isEObjectInitializedInStatement(statement,object)
				
				var type = "notType";
				if(initializedInStatement){
					type = pushObjectType(object)
				}else{
					type = getObjectType(object)
				}
				
				//if ReturnValue <space> return_<number>
 				if(type.equals(SourcesEnum.CODE_RETURN_VALUE.value)){
 					// 'new ReturnValue(5/"String", ..)'
 					statement.createAssignmentReturnValue(object);				
 				}else{
					pushCustomCode(SourcesEnum.GENERAL_SPACE.value)
	 				pushObjectName(object)	
					pushCustomCode(SourcesEnum.GENERAL_EQUALS.value)
 					// '5' / '"String' / getPartOfDocument("XPATH")
 					pushAssignmentValue(statement.initialization.assignment)
 				}
			}
			pushCustomCode(SourcesEnum.GENERAL_DELIMITER.value)
 		} 
 		true	
 	}
 	
 	//SEM - OK ----- ^^ --------------- ^^ --------- ^^ -------------- ^^ -------------
 	
 	override dispatch boolean interpret(MapForwardStatement statement) {		
 		var inputAttributes = statement.inputSection.inputAttributes
 		var outputAttributes = statement.outputSection.outputAttributes
		for(var i = 0; i < inputAttributes.size; i++){
			//input
			var inputObject = inputAttributes.get(i)
 			//output
 			var outputObject = outputAttributes.get(i)
 			val outputType = typeProvider.typeForObject(outputObject)

			//structure methods
 			if(outputType instanceof StructureType){
 				return createMapForwardStructure(inputObject,outputObject,statement)
 			}else{
 				createMapForwardType(inputObject,outputObject,statement)
 			}
		}
		return true
 	} 
 	
 	/**
 	 * Interprets other types than "structure type" (for Map forward statement)
 	 * @param inputObject input object
 	 * @param outputObject output object
 	 * @param statement statement
 	 */
 	def boolean createMapForwardType(EObject inputObject, EObject outputObject, MapForwardStatement statement){
 		val index = -1
 		return otherTypeCommand(inputObject,outputObject,statement,index)
 	}
 	
 	/**
 	 * Interprets other types than "structure type" (others statements)
 	 * @param index index
 	 * @param outputObject output object
 	 * @param statement statement
 	 */
 	def boolean createRoutineType(int index, EObject outputObject, Statement statement){
 		val object = null as EObject
 		return otherTypeCommand(object,outputObject,statement,index)
 	}
 	
 	/**
 	 * Basic method which interprets other types than Structure type
 	 * @param inputObject input object
 	 * @param index index
 	 * @param outputObject output object
 	 * @param statement statement
 	 * 
 	 */
 	def boolean otherTypeCommand(EObject inputObject, EObject outputObject, Statement statement, int index){
 		//checks initialization in this statement
 		var type = null as String
 		var object = objectUtil.getBasicEObject(outputObject)
 		
 		val isInitialized = isStructureInitialized(outputObject,statement,object);
 		//is initialized
 		if(!isInitialized){
 			type = pushObjectType(objectUtil.getBasicEObject(outputObject))
		 	pushCustomCode(SourcesEnum.GENERAL_SPACE.value)			
 		}
 		
		pushObjectName(outputObject)
		val line = '''routineReturns.get(�index�).getValue()'''
		if(type !== null && type.equals(SourcesEnum.CODE_RETURN_VALUE.value)){
			// = new ReturnValue<TYPE>([object_name]|[routineReturns.get(�index�).getValue()])
			pushCustomCode(SourcesEnum.GENERAL_EQUALS.value +"new "+SourcesEnum.CODE_RETURN_VALUE.value)
			pushNewImport(SourcesEnum.IMPORT_RETURN_VALUE.value)
			var obj = null as EObject
			// <TYPE>
			if(inputObject === null){
				obj = getReturnObjectByIndex(statement,index)
				obj.createTType(null,null)
			}else{
				inputObject.createTType(null,null)
			}
			// (.....)
			pushCustomCode(SourcesEnum.GENERAL_LEFT_BRACKET.value)
			if(inputObject !== null){
 				pushObjectName(inputObject)
 			}else{
 				obj.createCastType(null,null)
 				pushCustomCode(line)
 			}
			pushCustomCode(SourcesEnum.GENERAL_RIGHT_BRACKET.value)
		}else{
			pushCustomCode(SourcesEnum.GENERAL_EQUALS.value)
			if(inputObject !== null){
 				pushObjectName(inputObject)
 			}else{
 				outputObject.createCastType(null,null)
 				pushCustomCode(line)	
 			}		
		}	
		pushCustomCode(SourcesEnum.GENERAL_DELIMITER.value)
 		pushNewLine()
		return true
 	}
 	
 	/**
 	 * Interprets other types than "structure type" (others statements)
 	 * @param inputObject input object
 	 * @param outputObject output object
 	 * @param statement statement
 	 */
 	def boolean createMapForwardStructure(EObject inputObject, EObject outputObject, MapForwardStatement statement){
 		val index = -1
 		pushNewImport(SourcesEnum.IMPORT_XPATH_RESOLVER.value);
 		return structureCommand(inputObject,outputObject,statement,index)
 	}
 	
 	/**
 	 * Interprets other types than "structure type" (for map forward statement)
 	 * @param index index
 	 * @param outputObject output object
 	 * @param statement statement
 	 */
 	def boolean createRoutineStructure(int index, EObject outputObject, Statement statement){
 		val object = null as EObject
 		pushNewImport(SourcesEnum.IMPORT_XPATH_RESOLVER.value);
 		return structureCommand(object,outputObject,statement,index)
 	}
 	
 	/**
 	 * Checks that structure is initialized
 	 */
 	def boolean isStructureInitialized(EObject outputObject, Statement statement, EObject object){
 		if(!(object instanceof Output)){
 			var initializedInStatement = objectUtil.isEObjectInitializedInStatement(statement,outputObject)
			if(initializedInStatement){
				return false;
			}
 		}
 		return true
 	}
 	
 	def boolean createInput(EObject inputObject, Type inputType, int index){
 		//input is structure
 		if(inputType instanceof StructureType){	
			pushNewImport(SourcesEnum.IMPORT_XPATH_RESOLVER.value);			
			if(inputObject instanceof Attribute){
				//Variable.name:"Xpath" -> Output.name:"Xpath";
				pushCustomCode(SourcesEnum.CODE_QUERY_OUTPUT_OUTPUT.value) 
			}else{
				//Input.name:"Xpath" -> Output.name:"Xpath";
				pushCustomCode(SourcesEnum.CODE_QUERY_INPUT_OUTPUT.value)
			}
			pushObjectName(inputObject)
			pushCustomCode(SourcesEnum.GENERAL_COMMA.value)
			val isXpath = pushXpath(inputObject);
			if(!isXpath){
				return false;
			}
		
		}else{ //input is not structure
 			pushCustomCode(SourcesEnum.CODE_QUERY_VALUE_OUTPUT.value)
 			//name or index...
 			if(inputObject !== null){
 				pushObjectName(inputObject)
 			}else{
 				val line = ''' routineReturns.get(�index�).getValue()'''
 				//TODO nechapu? cast
 				createCastType(null,null,inputType)
 			//	pushCustomCode(SourcesEnum.GENERAL_LEFT_BRACKET.value + SourcesEnum.CODE_DOCUMENT_OUTPUT.value + SourcesEnum.GENERAL_RIGHT_BRACKET.value)
 				pushCustomCode(line)				
 			}		
		}
		return true;
 	}
 	
 	
 	/**
 	 * Basic method which interprets structure type
 	 * @param inputObject input object
 	 * @param index index
 	 * @param outputObject output object
 	 * @param statement statement
 	 * 
 	 */
 	def boolean structureCommand(EObject inputObject, EObject outputObject, Statement statement, int index){
 		var object = objectUtil.getBasicEObject(outputObject)
 		val isInitialized = isStructureInitialized(outputObject,statement,object);
 		//is structure initialized
 		if(isInitialized){
 			var inputType = typeProvider.typeForObject(inputObject)
			var basicObject = objectUtil.getBasicEObject(outputObject)
 			if(inputType instanceof NotType){
 				var returns = statementUtil.getReturnsFromRoutine(statement)
				inputType = typeProvider.typeForObject(returns.get(index));
 			}
 			val isOK = inputObject.createInput(inputType,index);
 			if(!isOK){
 				return false;
 			}
 			pushCustomCode(SourcesEnum.GENERAL_COMMA.value)
 			//to get Document / value - call ".getValue() for Return..
			if(basicObject instanceof Return){
				//TODO vyzkou�et
	 			inputObject.createCastType(null,inputType);
				pushObjectName(outputObject)
				val getValueText = ".getValue()" 
				pushCustomCode(getValueText)
			}else{
				pushObjectName(outputObject)
			}
			pushCustomCode(SourcesEnum.GENERAL_COMMA.value)
			val isXpath =pushXpath(outputObject)
			if(!isXpath){
				return false;
			}
			pushCustomCode(SourcesEnum.GENERAL_RIGHT_BRACKET.value + SourcesEnum.GENERAL_DELIMITER.value)		
			return true
 		}
 		return false;
 	}
 	
 	 	
 	override dispatch boolean interpret(CallStatement statement){
 		var outputSection = statement.outputSection
 		
 		if(outputSection !== null){
 			var outputAttributes = statement.outputSection.outputAttributes
 			val inputSection = statement.target.inputSection
 			var inputAttributes = null as List<EObject>
	 		if(inputSection !== null){
	 			inputAttributes = inputSection.inputAttributes
	 		}
	 		
 			pushNewLine()
 			val method = statement.java_method;
			if(method === null){
				return false;
			}
 			
	 		if(outputAttributes !== null){
	 			if(outputAttributes.length > 0){
	 				val firstLine = "routineReturns.clear();"
	 				val secondLine = "routineReturns.add("
	 				pushCustomCode(firstLine)
	 				pushNewLine()	
	 				pushNewImport(SourcesEnum.IMPORT_RETURN_VALUE.value)
	 				pushCustomCode(secondLine + "new "+SourcesEnum.CODE_RETURN_VALUE.value)
	 				createTType(null,method.returnType.identifier,null);
	 				pushCustomCode(SourcesEnum.GENERAL_LEFT_BRACKET.value)
	 				createCastType(null,method.returnType.identifier,null)
	 				pushCustomCode(SourcesEnum.GENERAL_SPACE.value);	 		
	 			}
	 		} 

			val methodName = method.simpleName
			var type = statement.target.javaClass;
			if (type instanceof JvmGenericType) {
				pushCustomCode(type.identifier + "." + methodName +"(")
		        var gt = type as JvmGenericType;
		        generateMethod(gt,methodName,inputAttributes)
		    pushCustomCode(SourcesEnum.GENERAL_RIGHT_BRACKET.value);
		    if(outputAttributes !== null){
		    	 pushCustomCode(SourcesEnum.GENERAL_RIGHT_BRACKET.value);
		    }
		    pushCustomCode(SourcesEnum.GENERAL_DELIMITER.value);
		    pushNewLine()
 		}
		if(outputAttributes !== null){
			for(var i = 0; i <outputAttributes.length; i++){
				var obj = outputAttributes.get(i)
				val outputType = typeProvider.typeForObject(obj)
				if(outputType instanceof StructureType){
					createRoutineStructure(i,obj,statement);
				}else{
					createRoutineType(i,obj,statement);
				}
			}
		}
		pushNewLine()
		return true;
	}
	return false;
 	}
 	
 	/**
 	 * generate method from call statement
 	 * @param gt generic type
 	 * @param methodName method name
 	 * @param inputAttributes input attribute
 	 */
 	def generateMethod(JvmGenericType gt, String methodName, List<EObject> inputAttributes){
 		for (JvmMember member : gt.getMembers()) {
			if (member instanceof JvmOperation) {
		    	if (methodName.equals(member.simpleName)) {
			  		var params = member.parameters
			  		val size = params.size
			  		val inputSize = (inputAttributes === null) ? 0 : inputAttributes.length
		  			val move = 1
			 		for(var i = 0; i < size; i++){
			 			//if is method(param,param) and inputAttributes(input) .. then call: method(input,null)
			 			if(i < inputSize){
			 				pushObjectName(inputAttributes.get(i));	
		 				}else{
		 					pushCustomCode(SourcesEnum.CODE_NULL.value)
		 				}
		 				if(i < (size - move)){
				 			pushCustomCode(SourcesEnum.GENERAL_COMMA.value)
				 		}
		  			}
		        }
		    }
		 }
 	}
 	
 	override dispatch boolean interpret(MapStatement statement) {
 		val inputSection = statement.inputSection
		//
 		var outputSection = statement.outputSection
 		var outputAttributes = null as List<EObject>
 		if(outputSection !== null){
 			outputAttributes = statement.outputSection.outputAttributes
 		}
 		//routine variables...
   		val name = statement.routineSection.source.name
   		if(name === null){
   			return false;
   		}
   		var routineName = generatorUtil.getRoutineName(name)
 		var javaRoutineName = routineName + "." + getJavaName(name)
 		
 		pushNewLine()
 		if(outputAttributes !== null){
 			if(outputAttributes.length > 0){
 				val line = "routineReturns = "
 				pushCustomCode(line)	
 			}
 		} 
 		pushCustomCode(javaRoutineName + SourcesEnum.GENERAL_LEFT_BRACKET.value)
 		if(inputSection !== null){	
	 		//method call
	 		var inputAttributes = statement.inputSection.inputAttributes	 		
	 		val size = inputAttributes.length
	 		val move = 1
	 		for(var i = 0; i < size; i++){
	 			pushObjectName(inputAttributes.get(i));
	 			if(i < (size - move)){
	 				pushCustomCode(SourcesEnum.GENERAL_COMMA.value)
	 			}
	 		}
	 	}	
	 	pushCustomCode(SourcesEnum.GENERAL_RIGHT_BRACKET.value + SourcesEnum.GENERAL_DELIMITER.value)
 		pushNewLine()
 		if(outputAttributes !== null){
 			for(var i = 0; i <outputAttributes.length; i++){
 				var obj = outputAttributes.get(i)
 				val outputType = typeProvider.typeForObject(obj)
 				if(outputType instanceof StructureType){
					createRoutineStructure(i,obj,statement);
				}else{
					createRoutineType(i,obj,statement);
				}
 			}	
 		}
 		
 		pushRoutineGenerator()	 	
 		return true;		
 	} 
 	
 	
 	override boolean interpretRoutine(Statement statement, List<MapStatement> outerStatements) {
 		typeProvider.activateRoutineScope(outerStatements)
 		var isWorking = statement.interpret
 		typeProvider.deactivateRoutineScope()
 		return isWorking
 	}
 	
 	override dispatch boolean interpret(InputImport inputImport){
 		val input = inputImport.name;
 		if(input !== null){
 			var name = objectUtil.getObjectName(input)
 			name = getJavaName(name);
 			pushCustomCode(name)
 			return true
 		}
 		return false
 	}
 	override dispatch boolean interpret(OutputImport outputImport){
 		val output = outputImport.name;
 		if(output !== null){
 			var name = objectUtil.getObjectName(output)
 			name = getJavaName(name);
 			pushCustomCode(name);
 			return true
 		}
 		return false
 	}
 	
 	override void interpretMethodHeader(MapStatement statement, List<MapStatement> outerStatements){
 		var outputSection = statement.outputSection
 		var outputAttributes = null as EList<EObject>
 		if(outputSection !== null){
 			outputAttributes = statement.outputSection.outputAttributes
 		}
 		val name = statement.routineSection.source.name
 		if(outputAttributes !== null){	
 			val line = "public static ArrayList<"+SourcesEnum.CODE_RETURN_VALUE.value+"> "
 			pushCustomCode(line)
 			pushNewImport(SourcesEnum.IMPORT_RETURN_VALUE.value)
 			pushNewImport(SourcesEnum.IMPORT_ARRAYLIST.value)
 		}else{
 			val line = "public static void " 
 			pushCustomCode(line)
 		} 
 		pushCustomCode(getJavaName(name) + SourcesEnum.GENERAL_LEFT_BRACKET.value)	
 			
 		
 		if(statement.inputSection !== null){
 			var inputAttributes = statement.inputSection.inputAttributes
 			val size = inputAttributes.length
	 		val move = 1	
	 		
	 		for(var i = 0; i < size; i++){
	 			typeProvider.activateRoutineScope(outerStatements)
	 			pushObjectType(inputAttributes.get(i))
	 			val inputPartName = " input"
	 			pushCustomCode(inputPartName+i)
	 			if(i < (size - move)){
	 				pushCustomCode(SourcesEnum.GENERAL_COMMA.value)
	 			}
	 			typeProvider.deactivateRoutineScope()
	 		}
 		}
 		
 		pushCustomCode(SourcesEnum.GENERAL_RIGHT_BRACKET.value + " {")	
 	}

	override String getRequest(){
		return popRequest
	}
	
	override int getRequestsSize(){
		return getStackSize
	}
}
	
	
	
	
