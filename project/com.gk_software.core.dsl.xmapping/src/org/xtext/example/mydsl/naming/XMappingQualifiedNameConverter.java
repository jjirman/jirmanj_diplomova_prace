package org.xtext.example.mydsl.naming;

import org.eclipse.xtext.naming.IQualifiedNameConverter;
import org.eclipse.xtext.naming.QualifiedName;

/**
 * Converter which converts QualifiedName into used version 
 * 	(edits bad QualifiedNames for attributes like Input, Ouptut, etc..).
 * @author Bc. Jan Jirman
 *
 */
public class XMappingQualifiedNameConverter extends IQualifiedNameConverter.DefaultImpl {
	
	// Attribute keywords	
	/** INPUT keyword */
	private static String INPUT = "Input";
	/** OUTPUT keyword */
	private static String OUTPUT = "Output";
	/** VARIABLE keyword */
	private static String VARIABLE = "Variable";
	/** RETURN keyword */
	private static String RETURN = "Return";
	/** PARAM keyword */
	private static String PARAM = "Param";
	/** MACRO keyword */
	private static String MACRO = "Macro";
	/** FILTER keyword */
	private static String FILTER = "Filter";
	/** FUNCTION keyword */
	private static String FUNCTION = "Function";
	/** MAPPER keyword */
	private static String MAPPER = "Mapper";
 
	// If the new attribute or structure is added, then add the attribute's keyword into this array
	/** String array with keywords */
	private static String[] attributeNames = {INPUT,OUTPUT, VARIABLE, MACRO, 
												FILTER, FUNCTION, MAPPER, RETURN, PARAM};
	
	
	/** index for first QualifiedName segment */
	private static int FIRST_INDEX = 0;
	/** length of segments */
	private static int MAX_LENGHT = 2;
	
	
	/**
     * Splits the given string into segments and returns them as a {@link QualifiedName}.
     * @param qualifiedNameAsString QualifiedName as String
     * @return QualifiendName
     */
    @Override
    public QualifiedName toQualifiedName(String qualifiedNameAsString) {
        QualifiedName qualifiedName = super.toQualifiedName(qualifiedNameAsString);
        //check that name contains '.' (dot)
        if (checkQualifiedName(qualifiedName)) {
            return QualifiedName.create(qualifiedNameAsString);
        }
        return qualifiedName;
    }
    
    /**
     * Checks that {@link QualifiedName} contains attribute name
     * @param qualifiedName QualifiedName
     * @return true - if QualifiedName has unused version.
     */
    private boolean checkQualifiedName(QualifiedName qualifiedName) {
    	if (qualifiedName.getSegmentCount() >= MAX_LENGHT) {
    		if(isQualifiedAttributeName(qualifiedName.getSegment(FIRST_INDEX))) {
    			return true;
    		}   
        }
    	return false;
    }
    
    /**
     * Checks that {@link QualifiedName} contains some attribute keyword.
     * @param firstSegment first segment of QualifiendName
     * @return true - if QualifiedName contains some attribute keyword.
     */
    private boolean isQualifiedAttributeName(String firstSegment) {
    	for(String attributeName : attributeNames) {
    		if(firstSegment.startsWith(attributeName)) {
    			return true;
    		}
    	}
    	return false;
    }
}