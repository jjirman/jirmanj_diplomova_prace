package org.xtext.example.mydsl.naming;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.naming.DefaultDeclarativeQualifiedNameProvider;
import org.eclipse.xtext.naming.QualifiedName;

/**
 * Class provides object qualified names
 * @author Bc. Jan Jirman
 *
 */
public class XMappingQualifiedNameProvider  extends DefaultDeclarativeQualifiedNameProvider {
		
	
	@Override
	public QualifiedName getFullyQualifiedName(EObject obj) {
		//same as original
		return super.getFullyQualifiedName(obj); 
	}
}
