package org.xtext.example.mydsl.typing;

import org.eclipse.emf.common.util.EList;

/**
 * Abstract class Type
 *
 */
public abstract class Type {
	/** name attribute */
	String name;
	
	/** name attribute */
	String codeType;

	/**
	 * Gets code type
	 * @return code type
	 */
	public String getCodeType() {
		return codeType;
	}

	/**
	 * Gets name
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * Constructor with name and code type params
	 * @param name name
	 * @param codeType code type
	 */
	public Type(String name, String codeType) {
		this.name = name;
		this.codeType = codeType;
	}

	/**
	 * Constructor with name parameter
	 * @param name name
	 */
	public Type(String name) {
        this(name,null);
    }
	
	@Override
	public String toString() {
		return this.name;
	}
	
	@Override
	public int hashCode() {
		return name.hashCode();
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this.hashCode() == obj.hashCode())
			return true;
		return false;
	}
	
}
