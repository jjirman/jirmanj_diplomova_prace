package org.xtext.example.mydsl.typing;

/**
 * Singleton Not Specified
 */
public final class NotSpecified extends Type{
	
	/** instance */
	public static final NotSpecified INSTANCE = new NotSpecified();  
	
	/** name */
	private static final String NAME = "notSpecified.";
	
	/**
	 * Constructor
	 */
	public NotSpecified() {
		super(NAME);
	}
	
}
