package org.xtext.example.mydsl.typing;

/**
 * Singleton Int type
 */
public final class IntType extends Type{
	
	/** instance */
	public static final IntType INSTANCE = new IntType();  
	
	/** name */
	private static final String NAME = "intType";
	
	/** code name */
	private static final String CODE_TYPE = "int";
	
	/**
	 * Constructor
	 */
	public IntType() {
		super(NAME, CODE_TYPE);
	}
	
}
