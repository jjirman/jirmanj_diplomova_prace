package org.xtext.example.mydsl.typing;

/**
 * Singleton Not type
 */
public final class NotType extends Type{
	
	/** instance */
	public static final NotType INSTANCE = new NotType();  
	
	/** name */
	private static final String NAME = "notType";
	
	/**
	 * Constructor
	 */
	public NotType() {
		super(NAME);
	}
	
}

