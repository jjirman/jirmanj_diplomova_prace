package org.xtext.example.mydsl.typing;

/**
 * Singleton Double type
 */
public final class DoubleType extends Type{
	
	/** instance */
	public static final IntType INSTANCE = new IntType();  
	
	/** name */
	private static final String NAME = "doubleType";
	
	/** code name */
	private static final String CODE_TYPE = "double";
	
	/**
	 * Constructor
	 */
	public DoubleType() {
		super(NAME, CODE_TYPE);
	}	
}

