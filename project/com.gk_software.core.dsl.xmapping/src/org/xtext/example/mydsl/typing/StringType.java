package org.xtext.example.mydsl.typing;

/**
 * Singleton String type
 */
public final class StringType extends Type{
	
	/** instance */
	public static final StringType INSTANCE = new StringType(); 
	
	/** name */
	private static final String NAME = "stringType";
	
	/** code name */
	private static final String CODE_TYPE = "String";
	
	/**
	 * Constructor
	 */
	public StringType() {
		super(NAME, CODE_TYPE);
	}
	
}
