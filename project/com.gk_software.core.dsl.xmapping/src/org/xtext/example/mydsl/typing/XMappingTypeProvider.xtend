package org.xtext.example.mydsl.typing

import org.xtext.example.mydsl.xMapping.AssignmentStatement
import org.xtext.example.mydsl.xMapping.AssignmentStructure
import org.xtext.example.mydsl.xMapping.Variable
import org.xtext.example.mydsl.xMapping.Return
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.emf.ecore.EObject
import org.xtext.example.mydsl.xMapping.RoutineBlock
import org.xtext.example.mydsl.xMapping.Param
import com.google.inject.Inject
import org.xtext.example.mydsl.xMapping.MapStatement
import org.xtext.example.mydsl.xMapping.MapForwardStatement
import org.xtext.example.mydsl.xMapping.CallStatement
import org.xtext.example.mydsl.xMapping.Statement
import org.xtext.example.mydsl.xMapping.Block
import org.xtext.example.mydsl.xMapping.Output
import org.xtext.example.mydsl.xMapping.Input
import org.eclipse.xtext.naming.IQualifiedNameProvider
import org.xtext.example.mydsl.xMapping.Attribute
import org.xtext.example.mydsl.xMapping.OutputAttribute
import org.eclipse.emf.common.util.EList
import org.xtext.example.mydsl.xMapping.InputAttribute
import java.util.Stack
import java.util.List
import org.xtext.example.mydsl.xMapping.ParamSection
import org.eclipse.xtext.common.types.JvmTypeReference
import org.eclipse.xtext.common.types.JvmGenericType
import org.xtext.example.mydsl.helpers.util.XMappingStatementUtil
import org.xtext.example.mydsl.helpers.util.XMappingEObjectUtil

/**
 * This class provide object type
 * @author Bc. Jan Jirman
 */
class XMappingTypeProvider {
	
	/** statement util */
	@Inject XMappingStatementUtil statementUtil
	/** object util */
	@Inject XMappingEObjectUtil objectUtil
	
	private static val STACK_MAX_SIZE = 50; 
	
	/** singleton string type */
	public static val stringType = new StringType
	/** singleton int type */
	public static val intType = new IntType
	/** singleton structure type */
	public static val structureType = new StructureType
	/** singleton not type */
	public static val notType = new NotType
	/** singleton double type */
 	public static val doubleType = new DoubleType
 	/** singleton not specified type */
 	public static val notSpecified = new NotSpecified
 	
 	/** statements stack */
 	static Stack<Statement> statements = new Stack<Statement>();
 	/** routine scope */
 	static boolean routineScope = false;
 	/** outer statement  */
 	static List<MapStatement> outerStatements;
 	
 	/**
 	 * Activates routine scope
 	 * @param list of map statements
 	 */
 	def void activateRoutineScope(List<MapStatement> statements){
 		routineScope = true;
 		outerStatements = statements;		
 	}
 	
 	/**
 	 * Deactivates routine scope
 	 */
 	def void deactivateRoutineScope(){
 		routineScope = false;
 		outerStatements = null;	
 	}
 	
 	/**
 	 * Resets stack with outer statements
 	 */
 	def void resetRoutineScope(){
 		if(routineScope){
 			for(Statement statement : outerStatements){
 				statements.add(statement)
 			}
 			
 		}
 	}

	/**
	 * Gets type of object
	 */
 	def Type typeForObject(EObject object){
 		resetRoutineScope()		
 		var finalObject = objectUtil.getBasicEObject(object)
 		//calls main object type method
 		val type = finalObject.objectType()
 		statements.clear
 		return type
 	}
 	
 	/**
	 * Gets type for specific object - in case of error, then object is notType
	 * @param object object
	 * @return object's type
	 */
 	def Type objectType(EObject object){
 		var statement = null as Statement
 		//types of objects
 		if(object instanceof Variable || object instanceof Return){
 			statement = statementUtil.findObjectInitialization(object)
 		} else if(object instanceof Param){
 			if(statements.size > 0){
 				statement = statements.pop
 			}else{
 				//if statements is 0, then "notSpecified" (used for validation)
 				return notSpecified
 			}
 			
 			//find declared param
 			var declaredObject = statementUtil.findDeclaredParam(statement,object)
 			if(declaredObject !== null){
 				if(declaredObject instanceof Attribute){
 					return declaredObject.type.objectType()	
	 			}else if(declaredObject instanceof InputAttribute){
	 				val obj = declaredObject.inputType
	 				if(obj.inputRef !== null){
	 					return declaredObject.inputType.inputRef.objectType();
	 				}else if(obj.paramRef !== null){
	 					return declaredObject.inputType.paramRef.objectType();
	 				}
	 			}else if(declaredObject instanceof Param){
	 				return declaredObject.objectType();
	 			}
 			}
 			
 		} else if(object instanceof Input || object instanceof Output){
 			return structureType
 		}
 		
 		//find type for object (Variable / Return)
 		if(statement !== null){
 			 return statement.typeFor(object)
 		}
		notType
 	}
 	
 	/**
	 * Finds type for object in MapStatement
	 * @param statement map statement
	 * @param object object
	 * @return object's type
	 */
 	def dispatch Type typeFor(MapStatement statement, EObject object) {
 		statements.push(statement);
 		
 		//recursion check
 		if(statements.size > STACK_MAX_SIZE){
 			return notType
 		}
 		//if statement contains outputSection
 		if(statement.outputSection !== null){
 			var outputAttributes = statement.outputSection.outputAttributes
	 		val outputIndex = objectUtil.getObjectIndexByObject(outputAttributes,object)
	 		//if -1: can not find any object
	 		if(outputIndex != -1){
	 			//gets routine and its block
	 			val routine = statement.routineSection.source
		 		val routineSection = EcoreUtil2.getContainerOfType(routine, typeof(RoutineBlock))
		 		val block = routineSection.routineBlock
		 		val name = "Return." + outputIndex
		 		//searching for return
		 		if(block !== null){
		 			val returnObject = objectUtil.getObjectByName(block, name)
		 			if(returnObject !== null){
		 				//and gets its type
		 				return objectType(returnObject)
		 			}
		 		}
 			}
 		}
 		notType
 	}
 	
 	/**
	 * Finds type for object in MapForwardStatement
	 * @param statement map forward statement
	 * @param object object
	 * @return object's type
	 */
 	def dispatch Type typeFor(MapForwardStatement statement, EObject object) {
 		var outputAttributes = statement.outputSection.outputAttributes
 		//gets object index in map forward statement
 		val outputIndex = objectUtil.getObjectIndexByObject(outputAttributes,object)
 		//if -1: can not find any object
 		if(outputIndex != -1){
 			var inputAttribute = statement.inputSection.inputAttributes.get(outputIndex)
 			//gets input attribute and finds its type
 			if(inputAttribute instanceof InputAttribute){
 				var instance = inputAttribute.inputType
 				if (instance.inputRef !== null){
					return instance.inputRef.objectType
	 			}else if(instance.paramRef !== null){
					return instance.paramRef.objectType
	 			}	
 			}else if (inputAttribute instanceof Attribute){
 				return inputAttribute.type.objectType
 			}	
 		}
 		notType
 	}
 	
 	/**
	 * Finds type for object in CallStatement
	 * @param statement call statement
	 * @param object object
	 * @return object's type
	 */
 	def dispatch Type typeFor(CallStatement statement, EObject object) {
 		var javaMethod = statement.java_method;
 		if(javaMethod !== null){
 			val methodReturnType = javaMethod.returnType
 			return methodType(methodReturnType.type.identifier)
 		}
 		
 		return notType	
 	}
 	
 	def Type methodType(String identifier){
 		//var type = returnReference.type.identifier as String;		
 		switch(identifier){
 			case "io.OutputDocument": return structureType
 			case "java.lang.String": return stringType
 			case "boolean": return stringType
 			case "int": return intType
 			case "double": return doubleType
 			case "void": return notType
 		} 
 		notSpecified
 	}
 
 	/**
	 * Finds type for object in AssignmentStatement
	 * @param statement assignment statement
	 * @param object object
	 * @return object's type
	 */
  	def dispatch Type typeFor(AssignmentStatement statement, EObject object) {
		//gets initialization
		val value = statement.initialization.assignment
 		switch value {
 			AssignmentStructure: structureType
 			org.xtext.example.mydsl.xMapping.IntType: intType
 			org.xtext.example.mydsl.xMapping.StringType: stringType
 			org.xtext.example.mydsl.xMapping.DoubleType: doubleType
	 	}	
 	}
 	
 	/**
 	 * Gets non-primitive type
 	 * @param type
 	 */
 	def String getNonPrimitiveType(Type type){
 		switch(type) {
			case structureType: return "DocumentOutput"
			case intType: return "Integer"
			case stringType: return "String"
			case doubleType: return "Double"
 		}
 		return "Object";
 	}
}