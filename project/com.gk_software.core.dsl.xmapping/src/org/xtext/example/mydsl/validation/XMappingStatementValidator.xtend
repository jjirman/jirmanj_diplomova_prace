package org.xtext.example.mydsl.validation

import org.xtext.example.mydsl.xMapping.AssignmentStatement
import org.eclipse.xtext.validation.Check
import com.google.inject.Inject
import org.xtext.example.mydsl.typing.XMappingTypeProvider
import org.xtext.example.mydsl.typing.NotType
import org.xtext.example.mydsl.typing.Type
import org.xtext.example.mydsl.xMapping.XMappingPackage
import org.xtext.example.mydsl.xMapping.UsedAttribute
import org.eclipse.emf.common.util.EList
import org.xtext.example.mydsl.xMapping.MapForwardStatement
import org.xtext.example.mydsl.xMapping.InputSection
import org.xtext.example.mydsl.xMapping.OutputSection
import org.xtext.example.mydsl.typing.StructureType
import org.xtext.example.mydsl.xMapping.InputAttribute
import org.eclipse.emf.ecore.EObject
import org.xtext.example.mydsl.xMapping.OutputAttribute
import org.eclipse.xtext.naming.IQualifiedNameProvider
import org.xtext.example.mydsl.xMapping.Return
import org.xtext.example.mydsl.typing.NotSpecified;
import org.xtext.example.mydsl.xMapping.Output
import org.eclipse.xtext.EcoreUtil2
import org.xtext.example.mydsl.xMapping.Block
import org.xtext.example.mydsl.xMapping.MapStatement
import org.xtext.example.mydsl.xMapping.RoutineBlock
import org.xtext.example.mydsl.xMapping.Model
import org.xtext.example.mydsl.xMapping.Statement
import org.xtext.example.mydsl.xMapping.CallStatement
import org.xtext.example.mydsl.xMapping.MappingClass
import java.util.ArrayList
import org.xtext.example.mydsl.xMapping.Input
import org.xtext.example.mydsl.xMapping.InputImport
import java.util.List
import org.xtext.example.mydsl.xMapping.OutputImport
import org.xtext.example.mydsl.helpers.util.XMappingStatementUtil
import org.xtext.example.mydsl.helpers.util.XMappingEObjectUtil
import org.eclipse.xtext.naming.QualifiedName
import org.xtext.example.mydsl.xMapping.Routine
import java.util.Comparator

/**
 * Class which checks statements
 * @author Bc. Jan Jirman
 */
class XMappingStatementValidator extends AbstractXMappingValidator {
	
	/** statement util */
	@Inject XMappingStatementUtil statementUtil
	/** type provider */
	@Inject XMappingTypeProvider typeProvider
	/** name provider */
	@Inject IQualifiedNameProvider nameProvider	
	/** object util */
	@Inject XMappingEObjectUtil objectUtil
			
	/* --------------------------- Assignment statement --------------------------- */
	
	/**
	 * Checks assignment statement
	 * @param statement assignment statement
	 */
	@Check
	def dispatch checkStatement(AssignmentStatement statement){
		statement.checkOrderedReturns
		if(statement.declaration.target !== null){
			var referenceDeclaration = statement.declaration.target.reference //declaration
			var initialization = statement.initialization
			if(initialization !== null){
				referenceDeclaration.checkReferenceType(statement)
			}
			
		}
	}
	
	/* --------------------------- MapForward statement --------------------------- */
	
	/**
	 * Checks map forward statement
	 * @param statement map forward statement
	 */
	@Check
	def dispatch checkStatement(MapForwardStatement statement){
		statement.inputSection.checkInputXpath(XMappingPackage.MAP_FORWARD_STATEMENT__INPUT_SECTION)
		statement.outputSection.checkOutputXpath(XMappingPackage.MAP_FORWARD_STATEMENT__OUTPUT_SECTION)
		statement.inputSection.checkInputSectionTypes
		statement.outputSection.checkOutputSectionTypes
		checkInputOutputNumber(statement.inputSection,statement.outputSection)
		statement.inputSection.compareInputOutputTypes(statement.outputSection)		
	}
	
	/* --------------------------- Map statement ---------------------------------- */
	
	/**
	 * Checks map statement
	 * @param statement map statement
	 */
	@Check
	def dispatch checkStatement(MapStatement statement){	
		if(statement.checkRecursion){
			statement.inputSection.checkInputXpath(XMappingPackage.MAP_STATEMENT__INPUT_SECTION)
			statement.outputSection.checkOutputXpath(XMappingPackage.MAP_STATEMENT__OUTPUT_SECTION)
			checkParamsWithInput(statement)		
			checkReturnWithOutput(statement)	
			statement.checkRoutineReturns
			checkRoutineCall(statement)	
			statement.inputSection.checkInputSectionTypes
			statement.outputSection.checkOutputSectionTypes
		}
		
	}
	
	/* --------------------------- Call statement --------------------------------- */
	
	/**
	 * Checks call statement
	 * @param statement call statement
	 */
	@Check
	def dispatch checkStatement(CallStatement statement){
		if(statement.target !== null){
			statement.target.inputSection.checkInputSectionTypes
			statement.target.inputSection.checkInputXpath(XMappingPackage.CALL_STATEMENT__INPUT_SECTION)
		}
		statement.outputSection.checkOutputSectionTypes
		checkCallReturnOutput(statement)
		statement.outputSection.checkOutputXpath(XMappingPackage.CALL_STATEMENT__OUTPUT_SECTION)
		checkReturnTypeWithOutputType(statement)
	}
	
	/* --------------------------- Other methods  --------------------------------- */
	
	/**
	 * Checks input section type
	 * @param input section
	 */
	private def checkInputSectionTypes(InputSection inputSection){
		if(inputSection !== null){
			//input attributes
			inputSection.inputAttributes.forEach[it |
				var object = objectUtil.getBasicEObject(it)				
				if(object !== null){
					var type = typeProvider.typeForObject(object)
					var name = nameProvider.getFullyQualifiedName(object)
					var objectName = name.lastSegment
					/*
					 * let Variable.output; Let Variable.input;
					 * Variable.input -> Variable.output
					 */
					if(type instanceof NotType){
						error(objectName + " is not initialized.", null, XMappingPackage.MAP_FORWARD_STATEMENT__INPUT_SECTION)
					/*
					 * Param.name -> Return.0;
					 */
					} else if (type instanceof NotSpecified){
						info(objectName + " is not exactly specified - can be any type.", null, XMappingPackage.MAP_FORWARD_STATEMENT__INPUT_SECTION)
					}
				}			
			]
		}	
	}	
	
	/**
	 * Checks output section type
	 * @param output section
	 */
	private def checkOutputSectionTypes(OutputSection outputSection){
		if(outputSection !== null){
			outputSection.outputAttributes.forEach[it |
				var object = objectUtil.getBasicEObject(it)				
				if(object !== null){
					var type = typeProvider.typeForObject(object)
					var name = nameProvider.getFullyQualifiedName(object)
					var objectName = name.lastSegment
					/*
					 * let Variable.output; Let Variable.input = new Output:"XPATH";
					 * Variable.input:"XPATH" -> Variable.output:"XPATH"
					 */
					if(type instanceof StructureType && !(object instanceof Output)){
						val block = EcoreUtil2.getContainerOfType(object,typeof(Block))
						val statement = statementUtil.findFirstInit(block,object);
						//same statement - gets type from actual one
						if(statement === outputSection.eContainer){
							error(objectName + " is not initialized.", null, XMappingPackage.MAP_FORWARD_STATEMENT__OUTPUT_SECTION)		
						}
					/*
					 * Param.name -> Return.0:"XPATH";
					 */
					} else if(type instanceof NotSpecified && objectUtil.getObjectXPath(it) !== null){
						val block = EcoreUtil2.getContainerOfType(object,typeof(Block))
						val statement = statementUtil.findFirstInit(block,object);
						//same statement - gets type from actual one
						if(statement === outputSection.eContainer){
							error(objectName + " is not initialized.", null, XMappingPackage.MAP_FORWARD_STATEMENT__OUTPUT_SECTION)		
						}
					/*
					 * Param.name -> Return.0;
					 */			
					}else if (type instanceof NotSpecified){
						info(objectName + " is not exactly specified - can be any type.", null, XMappingPackage.MAP_FORWARD_STATEMENT__INPUT_SECTION)
					}
				}			
			]
		}	
	}
	
	/**
	 * Checks input xPath
	 * @param inputSection input section
	 */
	private def checkInputXpath(InputSection inputSection, int reference){
		if(inputSection !== null){
			var indexCounter = 0
			for(EObject inputAttr : inputSection.inputAttributes){
				var inputType = typeProvider.typeForObject(inputAttr);
				// --> xpath rules
				xpathRules(inputType, inputAttr, reference)
				indexCounter++			
			}	
		}
	}	

	/**
	 * Checks output xPath
	 * @param outputSection output section
	 */	
	private def checkOutputXpath(OutputSection outputSection, int reference){
		if(outputSection !== null){
			var indexCounter = 0
			for(EObject outputAttr : outputSection.outputAttributes){
				var outputType = typeProvider.typeForObject(outputAttr);
				// --> xpath rules
				xpathRules(outputType, outputAttr, reference)
				indexCounter++			
			}	
		}
	}
	
	/**
	 * Map Forward rules
	 * @param inputAttribute input attribute
	 * @param outputAttribute output attribute
	 * @param isMapForward is map forward statement
	 */
	private def mapForwardRules(EObject inputAttribute, EObject outputAttribute, boolean isMapForward){
		var input = objectUtil.getBasicEObject(inputAttribute)
		var inputType = typeProvider.typeForObject(input);
		var output = objectUtil.getBasicEObject(outputAttribute);
		var outputType = typeProvider.typeForObject(output);
		
		//DIFFERENT TYPES BLOCK    (intType -> stringType)
		if(inputType != outputType){
			if(!(inputType instanceof NotSpecified)){
			   /*
			 	* let Variable.input = 5; let Variable.output = "String"
			 	* Variable.input -> Variable.output;
				*/
				if(!(outputType instanceof NotSpecified)){
					if(!(outputType instanceof StructureType)){
						var basicObject = objectUtil.getBasicEObject(inputAttribute)
						var inputName = nameProvider.getFullyQualifiedName(basicObject).lastSegment
						basicObject = objectUtil.getBasicEObject(outputAttribute)
						var outputName = nameProvider.getFullyQualifiedName(basicObject).lastSegment
						var featureId =  XMappingPackage.MAP_STATEMENT
						if(isMapForward){
							featureId = XMappingPackage.MAP_FORWARD_STATEMENT	
						}
						error(inputName + " ("+ inputType+") and "+outputName + " ("+outputType+") have different types", null, featureId);		
					}
				}
			}
		}
	}
	
	/**
	 * Xpath rules of object
	 * @param type type
	 * @param object object
	 * @param reference reference
	 */
	private def xpathRules(Type type, EObject object, int reference){
		var xpath = objectUtil.getObjectXPath(object)
		var basicObject = objectUtil.getBasicEObject(object)
		var qn = nameProvider.getFullyQualifiedName(basicObject)
		var name = qn.lastSegment
		
		if(!(type instanceof StructureType)){
		   /*
		 	* let Variable.input = 5;
		 	* Variable.input:"XPATH" -> Variable.output;
			*/
			if(xpath !== null && !(type instanceof NotSpecified)){
				warning(name+" has " + type+" - XPath will be ignored.", null, reference)		
			}	
		}else{
		   /*
		 	* let Variable.input = new Output:"XPATH";
		 	* Variable.input -> Variable.output;
			*/
			if(xpath === null && !(type instanceof NotSpecified)){				
				error(name+" has " + type+" - required XPath.", null, reference)			
			}
		}
	}	
	
	private def void checkOrderedReturns(AssignmentStatement statement){
		val routineBlock = EcoreUtil2.getContainerOfType(statement,typeof(RoutineBlock))
		var returns = statementUtil.getReturnsFromRoutine(statement)
		var comparator = new Comparator<EObject>() {
						
			override compare(EObject o1, EObject o2) {
				val return1 = objectUtil.getBasicEObject(o1) as Return;
				val return2 = objectUtil.getBasicEObject(o2) as Return;
				return1.name.compareTo(return2.name);
			}
			
		}
		
		if(routineBlock !== null){
			returns = returns.sortWith(comparator)
			val returnName = "Return."
			for(var i = 0; i < returns.size; i++){
				val returnFromList = nameProvider.getFullyQualifiedName(returns.get(i)).lastSegment;
				val expectedReturn = returnName + i
				if(!expectedReturn.equals(returnFromList)){
					val message = "Can not be initialized as "+returnFromList+" - required return name: "+expectedReturn
					error(message,null,XMappingPackage.ASSIGNMENT_STATEMENT__DECLARATION);
					return;		
				}
			}	
		}	
	}
	
	/**
	 * Checks routine returns
	 */
	private def void checkRoutineReturns(MapStatement statement) {
		var outputSection = statement.outputSection
		var outputAttributes = null as EList<EObject>
		if(outputSection !== null){
			outputAttributes = outputSection.outputAttributes
		}
		
		val routineName = statement.routineSection.source.name
		val target = statement.routineSection.target
		var obj = statement as EObject
		if(target !== null){
			obj = statement.routineSection.target.class_	
		}
		var model = EcoreUtil2.getContainerOfType(obj,typeof(Model))
		var routineBlock = EcoreUtil2.getAllContentsOfType(model,typeof(RoutineBlock))
		var routine = routineBlock.findFirst[it.routineName.name.equals(routineName)]
		if(routine !== null){
			//finds returns
			val returns = statementUtil.getReturnsFromRoutine(statement)
			var counter = -1;
			val isMapForward = false
			//all returns
			for(EObject object : returns){
				var basicObject = objectUtil.getBasicEObject(object)
				if(basicObject instanceof Return){
					counter ++;
					var name = nameProvider.getFullyQualifiedName(basicObject).lastSegment
					
					val initStatement = statementUtil.findFirstInit(routine.routineBlock,basicObject);							
					if(initStatement=== null){
						error(name + " from "+ routineName +" is not initialized.", null, XMappingPackage.ASSIGNMENT_STATEMENT__DECLARATION)			
						return;
					}
					if(outputAttributes !== null){	
						mapForwardRules(basicObject,outputAttributes.get(counter),isMapForward);
					}			
				}
			}
		}		
	}
	
	/**
	 * Compares output and input types - FOR MAP FORWARD STATEMENT
	 */
	private def compareInputOutputTypes(InputSection inputSection, OutputSection outputSection){
		if(inputSection !== null && outputSection !== null){
			val isMapForward = true
			var indexCounter = 0
			for(EObject inputAttr : inputSection.inputAttributes){
				var outputAttr = outputSection.outputAttributes.get(indexCounter) as EObject
				mapForwardRules(inputAttr,outputAttr,isMapForward)
				indexCounter++			
			}	
		}
	} 
	
	/**
	 * checks return type and output type - are they same?
	 * @statement call statement
	 */
	def checkReturnTypeWithOutputType(CallStatement statement){
		var type = typeProvider.typeFor(statement, null)
		// java method has void return type
		if(type instanceof NotType && statement.javaClass === null){
			error("Method can not have \"void\" return type.", null, XMappingPackage.CALL_STATEMENT);
			
		}else
		// java method has unknown return type
		if(type instanceof NotSpecified && statement.javaClass === null){
			error("Method does not have correct return type - unknown type.",null, XMappingPackage.CALL_STATEMENT);
			
		}else
		// java method has correct return type - checking output attribute
		if(statement.outputSection !== null){
			for(EObject outputAttr : statement.outputSection.outputAttributes){
				var outputType = typeProvider.typeForObject(outputAttr);
				var basicObject = objectUtil.getBasicEObject(outputAttr)
				var outputName = nameProvider.getFullyQualifiedName(basicObject).lastSegment
				if(outputType != type && !(outputType instanceof NotSpecified)){
					error("Bad types - "+outputName + " has to be "+type+".", null, XMappingPackage.CALL_STATEMENT);
				}		
			}	
		}
	}
	
	/**
	 * Checks number of Outputs for 1 Return from Java method
	 * @param statement call statement
	 */
	def void checkCallReturnOutput(CallStatement statement){
		val outputSection = statement.outputSection
		if(outputSection !== null){
			val outputAttributes = outputSection.outputAttributes
			if(outputAttributes !== null){
				val size = outputAttributes.size
				if(size == 1){
					return;
				}
			}
		}
		if(statement.javaClass === null){
			error("Method requires only 1 output.",null, XMappingPackage.CALL_STATEMENT__JAVA_METHOD)			
		}
	}
	
	/**
	 * Checks number of returns with number of outputs
	 * @param statement map statement
	 */
	def checkReturnWithOutput(MapStatement statement){
		if(statement.outputSection !== null){
			val outputSize = statement.outputSection.outputAttributes.size
			val target = statement.routineSection.target
			var object = statement as EObject
			//target - is from different class?
			if(target !== null){
				 object = statement.routineSection.target.class_	
			}
			val routineName = statement.routineSection.source.name
			var returnSize = -1
			var model = EcoreUtil2.getContainerOfType(object,typeof(Model))
			var routineBlock = EcoreUtil2.getAllContentsOfType(model,typeof(RoutineBlock))
			var routine = routineBlock.findFirst[it.routineName.name.equals(routineName)]
			if(routine !== null){
				//gets only return
				val returns = statementUtil.getReturnsFromRoutine(statement)
				returnSize = returns.size
			}
			if(outputSize != returnSize){
				error("Number of output objects does not match with Return count.", null, XMappingPackage.MAP_STATEMENT__INPUT_SECTION)		
			}
		}		
	}
	
	/**
	 * Checks number of params with number of inputs
	 * @param statement map statement
	 */
	def checkParamsWithInput(MapStatement statement){
		if(statement.inputSection !== null){
			val inputSize = statement.inputSection.inputAttributes.size
			val target = statement.routineSection.target
			var object = statement as EObject
			if(target !== null){
				 object = statement.routineSection.target.class_	
			}
			val routineName = statement.routineSection.source.name
			var paramsSize = 0
			var model = EcoreUtil2.getContainerOfType(object,typeof(Model))
			var routineBlocks = EcoreUtil2.getAllContentsOfType(model,typeof(RoutineBlock))
			var specificRoutine = routineBlocks.findFirst[it.routineName.name.equals(routineName)]
			if(specificRoutine !== null){
				val params = specificRoutine.params
				if(params !== null){
					paramsSize = params.paramSection.size
				}
			}
			if(inputSize != paramsSize){
				error("Number of input objects does not match with params size.", null, XMappingPackage.MAP_STATEMENT__INPUT_SECTION)		
			}
		}		
	}

	/**
	 * Checks number of input attributes with output attributes
	 * @param inputSection input section
	 * @param outputSection output section
	 */
	def checkInputOutputNumber(InputSection inputSection, OutputSection outputSection){
		if(inputSection !== null && outputSection !== null){
			var inputCount = inputSection.inputAttributes.size
			var outputCount = outputSection.outputAttributes.size
			/*
			 * Variable.input, Variable.input2 -> Variable.output; 
			*/
			if(inputCount !== outputCount){
				error("Number of input attributes must be the same as output attributes", null, XMappingPackage.MAP_FORWARD_STATEMENT)		
			}
		}	
	}
	
	/**
	 * Checks routine call
	 * @param statement map statement
	 */
	def checkRoutineCall(MapStatement statement){
		var routine = statement.routineSection.source
		var target = statement.routineSection.target
		if(target !== null){
			var inputMacroAttributes = EcoreUtil2.getAllContentsOfType((routine.eContainer as RoutineBlock).routineBlock,typeof(InputAttribute));
			var outputMacroAttributes = EcoreUtil2.getAllContentsOfType((routine.eContainer as RoutineBlock).routineBlock,typeof(OutputAttribute));
			
			for(InputAttribute attribute : inputMacroAttributes){
				val object = objectUtil.getBasicEObject(attribute)
				if(object instanceof Input){
					error("Can not call Macro with different Input objects.",null,XMappingPackage.MAP_STATEMENT__ROUTINE_SECTION);
					return;
				} 
			}
			
			for(OutputAttribute attribute : outputMacroAttributes){
				val object = objectUtil.getBasicEObject(attribute)
				if(object instanceof Output){
					error("Can not call Macro with different Output objects.",null,XMappingPackage.MAP_STATEMENT__ROUTINE_SECTION);
					return;
				} 
			}	
		}
	}
	
	/**
	 * Checks reference type
	 * @param referenceDeclaration reference declaration (List od UsedAttribute)
	 * @param statement assignment statement
	 */
	def checkReferenceType(EList<UsedAttribute> referenceDeclaration, AssignmentStatement statement){
		referenceDeclaration.forEach[it |
		{
			val statementType = typeProvider.typeFor(statement,null)
			var variableType = new NotType as Type
			var reference = XMappingPackage.VARIABLE__NAME
			var name = "Unknown"
			//object - source is not null
			if(it.source !== null){
				var object = it.source
				var qn = nameProvider.getFullyQualifiedName(object)
				name = qn.lastSegment
				variableType = typeProvider.typeForObject(object)
				if(object instanceof Return){
					reference = XMappingPackage.RETURN__NAME
				}
			}			

			/*
			* let Variable.first = 10;
			* Variable.first = "String";
			*/
			if(variableType != statementType){
				error(name + " is "+ variableType.name +" - can not be a new type.", null, reference)
			}	
		}]
	} 
	
	def boolean checkRecursion(MapStatement statement) {	
		var routineBlock = EcoreUtil2.getContainerOfType(statement, typeof(RoutineBlock))
		if(routineBlock !== null ) {
			var routine = statement.getRoutineSection().getSource();
			if(routine !== null) {
				val statementRoutineName = nameProvider.getFullyQualifiedName(routine);
				val routineName = nameProvider.getFullyQualifiedName(routineBlock.getRoutineName());
				if(routineName.hashCode() == statementRoutineName.hashCode()) {
					error("Recursion is not allowed.", null, XMappingPackage.MAP_STATEMENT);
					return false;
				}
			}
		}
		return true;
	}

}