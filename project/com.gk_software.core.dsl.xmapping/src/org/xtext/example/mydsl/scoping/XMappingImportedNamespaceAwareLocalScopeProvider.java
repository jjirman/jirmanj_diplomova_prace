package org.xtext.example.mydsl.scoping;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.resource.ISelectable;
import org.eclipse.xtext.scoping.IScope;
import org.eclipse.xtext.scoping.impl.ImportNormalizer;
import org.eclipse.xtext.scoping.impl.ImportScope;
import org.eclipse.xtext.scoping.impl.ImportedNamespaceAwareLocalScopeProvider;
import org.eclipse.xtext.util.Strings;
import org.xtext.example.mydsl.xMapping.Model;

/**
 *  Class with modified method of ImportedNamespaceAwareLocalScopeProvider
 *  @author Bc. Jan Jirman
 */
public class XMappingImportedNamespaceAwareLocalScopeProvider extends ImportedNamespaceAwareLocalScopeProvider
{
	/**
	 * Internal imported namespace Resolvers
	 * @param context context
	 * @param ignoreCase ignorecase
	 * @return list of ImportNormalizers
	 */
	protected List<ImportNormalizer> internalGetImportedNamespaceResolvers(EObject context, boolean ignoreCase) {
		List<ImportNormalizer> resolvers = this.internalGetExplicitImportedNamespaceResolvers(context, ignoreCase);
		if (context instanceof Model) {
			String qualifiedName = ((Model) context).getName();
			if (qualifiedName != null) {
				resolvers.add(createImportedNamespaceResolver(qualifiedName + ".*", ignoreCase));
			}
		} 
		return resolvers;
	}
	
	/**
	 * Gets explicit imported namespace Resolvers
	 * @param context context
	 * @param ignoreCase ignoreCase
	 * @return List of ImportNormalizers
	 */
	protected List<ImportNormalizer> internalGetExplicitImportedNamespaceResolvers(final EObject context, final boolean ignoreCase) {
        final List<ImportNormalizer> importedNamespaceResolvers = new ArrayList<ImportNormalizer>();
        final EList<EObject> eContents = (EList<EObject>)context.eContents();
        for (final EObject child : eContents) {
            final String value = this.getImportedNamespace(child);
            final ImportNormalizer resolver = this.createImportedNamespaceResolver(value, ignoreCase);
            if (resolver != null) {
                importedNamespaceResolvers.add(resolver);
            }
        }
		return importedNamespaceResolvers;
    }
	
	
	
	
}
