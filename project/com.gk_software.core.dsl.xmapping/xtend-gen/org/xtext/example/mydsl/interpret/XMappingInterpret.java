package org.xtext.example.mydsl.interpret;

import com.google.inject.Inject;
import java.util.Arrays;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.common.types.JvmFormalParameter;
import org.eclipse.xtext.common.types.JvmGenericType;
import org.eclipse.xtext.common.types.JvmMember;
import org.eclipse.xtext.common.types.JvmOperation;
import org.eclipse.xtext.common.types.JvmType;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.xtext.example.mydsl.helpers.classes.SourcesEnum;
import org.xtext.example.mydsl.helpers.util.XMappingEObjectUtil;
import org.xtext.example.mydsl.helpers.util.XMappingGeneratorUtil;
import org.xtext.example.mydsl.helpers.util.XMappingInterpretObjectUtil;
import org.xtext.example.mydsl.helpers.util.XMappingInterpretUtil;
import org.xtext.example.mydsl.helpers.util.XMappingStatementUtil;
import org.xtext.example.mydsl.interpret.AbstractMappingInterpret;
import org.xtext.example.mydsl.interpret.IXMappingInterpretQueue;
import org.xtext.example.mydsl.typing.NotType;
import org.xtext.example.mydsl.typing.StructureType;
import org.xtext.example.mydsl.typing.Type;
import org.xtext.example.mydsl.typing.XMappingTypeProvider;
import org.xtext.example.mydsl.xMapping.AssignmentStatement;
import org.xtext.example.mydsl.xMapping.Attribute;
import org.xtext.example.mydsl.xMapping.CallStatement;
import org.xtext.example.mydsl.xMapping.Initialization;
import org.xtext.example.mydsl.xMapping.Input;
import org.xtext.example.mydsl.xMapping.InputImport;
import org.xtext.example.mydsl.xMapping.InputSection;
import org.xtext.example.mydsl.xMapping.MapForwardStatement;
import org.xtext.example.mydsl.xMapping.MapStatement;
import org.xtext.example.mydsl.xMapping.Output;
import org.xtext.example.mydsl.xMapping.OutputImport;
import org.xtext.example.mydsl.xMapping.OutputSection;
import org.xtext.example.mydsl.xMapping.Return;
import org.xtext.example.mydsl.xMapping.Statement;

@SuppressWarnings("all")
public class XMappingInterpret extends AbstractMappingInterpret {
  /**
   * interpret object
   */
  @Inject
  @Extension
  private XMappingInterpretObjectUtil _xMappingInterpretObjectUtil;
  
  /**
   * interpret util
   */
  @Inject
  @Extension
  private XMappingInterpretUtil _xMappingInterpretUtil;
  
  /**
   * object util
   */
  @Inject
  private XMappingEObjectUtil objectUtil;
  
  /**
   * object util
   */
  @Inject
  private XMappingStatementUtil statementUtil;
  
  /**
   * generator util
   */
  @Inject
  private XMappingGeneratorUtil generatorUtil;
  
  /**
   * type provider
   */
  @Inject
  private XMappingTypeProvider typeProvider;
  
  /**
   * interpret queue
   */
  @Inject
  @Extension
  private IXMappingInterpretQueue _iXMappingInterpretQueue;
  
  /**
   * Creates type T "<T>"
   * @param statement statement
   * @param object object
   */
  public void createTType(final EObject object, final String identifier, final Type type) {
    this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.GENERAL_LESS_THAN.getValue());
    this._iXMappingInterpretQueue.pushObjectNonPrimitiveType(object, identifier, type);
    this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.GENERAL_GREATER_THAN.getValue());
  }
  
  /**
   * Creates casting "(type)"
   */
  public void createCastType(final EObject object, final String identifier, final Type type) {
    this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.GENERAL_LEFT_BRACKET.getValue());
    this._iXMappingInterpretQueue.pushObjectNonPrimitiveType(object, identifier, type);
    this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.GENERAL_RIGHT_BRACKET.getValue());
  }
  
  /**
   * Creates new ReturnValue "new ReturnValue<T>([value])"
   */
  public void createNewReturnValue(final Statement statement, final EObject object) {
    String _value = SourcesEnum.CODE_RETURN_VALUE.getValue();
    String _plus = ("new " + _value);
    this._iXMappingInterpretQueue.pushCustomCode(_plus);
    this.createTType(object, null, null);
    this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.GENERAL_LEFT_BRACKET.getValue());
    if ((statement instanceof AssignmentStatement)) {
      this._iXMappingInterpretQueue.pushAssignmentValue(((AssignmentStatement)statement).getInitialization().getAssignment());
    } else {
    }
    this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.GENERAL_RIGHT_BRACKET.getValue());
    this._iXMappingInterpretQueue.pushNewImport(SourcesEnum.IMPORT_RETURN_VALUE.getValue());
  }
  
  /**
   * Creates assignment return value "[name] = new ReturnValue<T>([value])"
   */
  public void createAssignmentReturnValue(final Statement statement, final EObject object) {
    this.createTType(object, null, null);
    this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.GENERAL_SPACE.getValue());
    this._iXMappingInterpretQueue.pushObjectName(object);
    this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.GENERAL_EQUALS.getValue());
    this.createNewReturnValue(statement, object);
  }
  
  @Override
  protected boolean _interpret(final AssignmentStatement statement) {
    boolean _xblockexpression = false;
    {
      Initialization _initialization = statement.getInitialization();
      boolean _tripleNotEquals = (_initialization != null);
      if (_tripleNotEquals) {
        final EList<EObject> list = this.statementUtil.<EObject>getAssigmnentList(statement.getDeclaration());
        for (final EObject decl : list) {
          {
            EObject object = this.objectUtil.getBasicEObject(decl);
            boolean initializedInStatement = this.objectUtil.isEObjectInitializedInStatement(statement, object);
            String type = "notType";
            if (initializedInStatement) {
              type = this._iXMappingInterpretQueue.pushObjectType(object);
            } else {
              type = this._xMappingInterpretObjectUtil.getObjectType(object);
            }
            boolean _equals = type.equals(SourcesEnum.CODE_RETURN_VALUE.getValue());
            if (_equals) {
              this.createAssignmentReturnValue(statement, object);
            } else {
              this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.GENERAL_SPACE.getValue());
              this._iXMappingInterpretQueue.pushObjectName(object);
              this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.GENERAL_EQUALS.getValue());
              this._iXMappingInterpretQueue.pushAssignmentValue(statement.getInitialization().getAssignment());
            }
          }
        }
        this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.GENERAL_DELIMITER.getValue());
      }
      _xblockexpression = true;
    }
    return _xblockexpression;
  }
  
  @Override
  protected boolean _interpret(final MapForwardStatement statement) {
    EList<EObject> inputAttributes = statement.getInputSection().getInputAttributes();
    EList<EObject> outputAttributes = statement.getOutputSection().getOutputAttributes();
    for (int i = 0; (i < inputAttributes.size()); i++) {
      {
        EObject inputObject = inputAttributes.get(i);
        EObject outputObject = outputAttributes.get(i);
        final Type outputType = this.typeProvider.typeForObject(outputObject);
        if ((outputType instanceof StructureType)) {
          return this.createMapForwardStructure(inputObject, outputObject, statement);
        } else {
          this.createMapForwardType(inputObject, outputObject, statement);
        }
      }
    }
    return true;
  }
  
  /**
   * Interprets other types than "structure type" (for Map forward statement)
   * @param inputObject input object
   * @param outputObject output object
   * @param statement statement
   */
  public boolean createMapForwardType(final EObject inputObject, final EObject outputObject, final MapForwardStatement statement) {
    final int index = (-1);
    return this.otherTypeCommand(inputObject, outputObject, statement, index);
  }
  
  /**
   * Interprets other types than "structure type" (others statements)
   * @param index index
   * @param outputObject output object
   * @param statement statement
   */
  public boolean createRoutineType(final int index, final EObject outputObject, final Statement statement) {
    final EObject object = ((EObject) null);
    return this.otherTypeCommand(object, outputObject, statement, index);
  }
  
  /**
   * Basic method which interprets other types than Structure type
   * @param inputObject input object
   * @param index index
   * @param outputObject output object
   * @param statement statement
   */
  public boolean otherTypeCommand(final EObject inputObject, final EObject outputObject, final Statement statement, final int index) {
    String type = ((String) null);
    EObject object = this.objectUtil.getBasicEObject(outputObject);
    final boolean isInitialized = this.isStructureInitialized(outputObject, statement, object);
    if ((!isInitialized)) {
      type = this._iXMappingInterpretQueue.pushObjectType(this.objectUtil.getBasicEObject(outputObject));
      this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.GENERAL_SPACE.getValue());
    }
    this._iXMappingInterpretQueue.pushObjectName(outputObject);
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("routineReturns.get(");
    _builder.append(index);
    _builder.append(").getValue()");
    final String line = _builder.toString();
    if (((type != null) && type.equals(SourcesEnum.CODE_RETURN_VALUE.getValue()))) {
      String _value = SourcesEnum.GENERAL_EQUALS.getValue();
      String _plus = (_value + "new ");
      String _value_1 = SourcesEnum.CODE_RETURN_VALUE.getValue();
      String _plus_1 = (_plus + _value_1);
      this._iXMappingInterpretQueue.pushCustomCode(_plus_1);
      this._iXMappingInterpretQueue.pushNewImport(SourcesEnum.IMPORT_RETURN_VALUE.getValue());
      EObject obj = ((EObject) null);
      if ((inputObject == null)) {
        obj = this._xMappingInterpretUtil.getReturnObjectByIndex(statement, index);
        this.createTType(obj, null, null);
      } else {
        this.createTType(inputObject, null, null);
      }
      this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.GENERAL_LEFT_BRACKET.getValue());
      if ((inputObject != null)) {
        this._iXMappingInterpretQueue.pushObjectName(inputObject);
      } else {
        this.createCastType(obj, null, null);
        this._iXMappingInterpretQueue.pushCustomCode(line);
      }
      this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.GENERAL_RIGHT_BRACKET.getValue());
    } else {
      this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.GENERAL_EQUALS.getValue());
      if ((inputObject != null)) {
        this._iXMappingInterpretQueue.pushObjectName(inputObject);
      } else {
        this.createCastType(outputObject, null, null);
        this._iXMappingInterpretQueue.pushCustomCode(line);
      }
    }
    this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.GENERAL_DELIMITER.getValue());
    this._iXMappingInterpretQueue.pushNewLine();
    return true;
  }
  
  /**
   * Interprets other types than "structure type" (others statements)
   * @param inputObject input object
   * @param outputObject output object
   * @param statement statement
   */
  public boolean createMapForwardStructure(final EObject inputObject, final EObject outputObject, final MapForwardStatement statement) {
    final int index = (-1);
    this._iXMappingInterpretQueue.pushNewImport(SourcesEnum.IMPORT_XPATH_RESOLVER.getValue());
    return this.structureCommand(inputObject, outputObject, statement, index);
  }
  
  /**
   * Interprets other types than "structure type" (for map forward statement)
   * @param index index
   * @param outputObject output object
   * @param statement statement
   */
  public boolean createRoutineStructure(final int index, final EObject outputObject, final Statement statement) {
    final EObject object = ((EObject) null);
    this._iXMappingInterpretQueue.pushNewImport(SourcesEnum.IMPORT_XPATH_RESOLVER.getValue());
    return this.structureCommand(object, outputObject, statement, index);
  }
  
  /**
   * Checks that structure is initialized
   */
  public boolean isStructureInitialized(final EObject outputObject, final Statement statement, final EObject object) {
    if ((!(object instanceof Output))) {
      boolean initializedInStatement = this.objectUtil.isEObjectInitializedInStatement(statement, outputObject);
      if (initializedInStatement) {
        return false;
      }
    }
    return true;
  }
  
  public boolean createInput(final EObject inputObject, final Type inputType, final int index) {
    if ((inputType instanceof StructureType)) {
      this._iXMappingInterpretQueue.pushNewImport(SourcesEnum.IMPORT_XPATH_RESOLVER.getValue());
      if ((inputObject instanceof Attribute)) {
        this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.CODE_QUERY_OUTPUT_OUTPUT.getValue());
      } else {
        this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.CODE_QUERY_INPUT_OUTPUT.getValue());
      }
      this._iXMappingInterpretQueue.pushObjectName(inputObject);
      this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.GENERAL_COMMA.getValue());
      final boolean isXpath = this._iXMappingInterpretQueue.pushXpath(inputObject);
      if ((!isXpath)) {
        return false;
      }
    } else {
      this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.CODE_QUERY_VALUE_OUTPUT.getValue());
      if ((inputObject != null)) {
        this._iXMappingInterpretQueue.pushObjectName(inputObject);
      } else {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append(" ");
        _builder.append("routineReturns.get(");
        _builder.append(index, " ");
        _builder.append(").getValue()");
        final String line = _builder.toString();
        this.createCastType(null, null, inputType);
        this._iXMappingInterpretQueue.pushCustomCode(line);
      }
    }
    return true;
  }
  
  /**
   * Basic method which interprets structure type
   * @param inputObject input object
   * @param index index
   * @param outputObject output object
   * @param statement statement
   */
  public boolean structureCommand(final EObject inputObject, final EObject outputObject, final Statement statement, final int index) {
    EObject object = this.objectUtil.getBasicEObject(outputObject);
    final boolean isInitialized = this.isStructureInitialized(outputObject, statement, object);
    if (isInitialized) {
      Type inputType = this.typeProvider.typeForObject(inputObject);
      EObject basicObject = this.objectUtil.getBasicEObject(outputObject);
      if ((inputType instanceof NotType)) {
        List<EObject> returns = this.statementUtil.getReturnsFromRoutine(statement);
        inputType = this.typeProvider.typeForObject(returns.get(index));
      }
      final boolean isOK = this.createInput(inputObject, inputType, index);
      if ((!isOK)) {
        return false;
      }
      this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.GENERAL_COMMA.getValue());
      if ((basicObject instanceof Return)) {
        this.createCastType(inputObject, null, inputType);
        this._iXMappingInterpretQueue.pushObjectName(outputObject);
        final String getValueText = ".getValue()";
        this._iXMappingInterpretQueue.pushCustomCode(getValueText);
      } else {
        this._iXMappingInterpretQueue.pushObjectName(outputObject);
      }
      this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.GENERAL_COMMA.getValue());
      final boolean isXpath = this._iXMappingInterpretQueue.pushXpath(outputObject);
      if ((!isXpath)) {
        return false;
      }
      String _value = SourcesEnum.GENERAL_RIGHT_BRACKET.getValue();
      String _value_1 = SourcesEnum.GENERAL_DELIMITER.getValue();
      String _plus = (_value + _value_1);
      this._iXMappingInterpretQueue.pushCustomCode(_plus);
      return true;
    }
    return false;
  }
  
  @Override
  protected boolean _interpret(final CallStatement statement) {
    OutputSection outputSection = statement.getOutputSection();
    if ((outputSection != null)) {
      EList<EObject> outputAttributes = statement.getOutputSection().getOutputAttributes();
      final InputSection inputSection = statement.getTarget().getInputSection();
      List<EObject> inputAttributes = ((List<EObject>) null);
      if ((inputSection != null)) {
        inputAttributes = inputSection.getInputAttributes();
      }
      this._iXMappingInterpretQueue.pushNewLine();
      final JvmOperation method = statement.getJava_method();
      if ((method == null)) {
        return false;
      }
      if ((outputAttributes != null)) {
        final EList<EObject> _converted_outputAttributes = (EList<EObject>)outputAttributes;
        int _length = ((Object[])Conversions.unwrapArray(_converted_outputAttributes, Object.class)).length;
        boolean _greaterThan = (_length > 0);
        if (_greaterThan) {
          final String firstLine = "routineReturns.clear();";
          final String secondLine = "routineReturns.add(";
          this._iXMappingInterpretQueue.pushCustomCode(firstLine);
          this._iXMappingInterpretQueue.pushNewLine();
          this._iXMappingInterpretQueue.pushNewImport(SourcesEnum.IMPORT_RETURN_VALUE.getValue());
          String _value = SourcesEnum.CODE_RETURN_VALUE.getValue();
          String _plus = ((secondLine + "new ") + _value);
          this._iXMappingInterpretQueue.pushCustomCode(_plus);
          this.createTType(null, method.getReturnType().getIdentifier(), null);
          this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.GENERAL_LEFT_BRACKET.getValue());
          this.createCastType(null, method.getReturnType().getIdentifier(), null);
          this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.GENERAL_SPACE.getValue());
        }
      }
      final String methodName = method.getSimpleName();
      JvmType type = statement.getTarget().getJavaClass();
      if ((type instanceof JvmGenericType)) {
        String _identifier = ((JvmGenericType)type).getIdentifier();
        String _plus_1 = (_identifier + ".");
        String _plus_2 = (_plus_1 + methodName);
        String _plus_3 = (_plus_2 + "(");
        this._iXMappingInterpretQueue.pushCustomCode(_plus_3);
        JvmGenericType gt = ((JvmGenericType) type);
        this.generateMethod(gt, methodName, inputAttributes);
        this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.GENERAL_RIGHT_BRACKET.getValue());
        if ((outputAttributes != null)) {
          this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.GENERAL_RIGHT_BRACKET.getValue());
        }
        this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.GENERAL_DELIMITER.getValue());
        this._iXMappingInterpretQueue.pushNewLine();
      }
      if ((outputAttributes != null)) {
        for (int i = 0; (i < ((Object[])Conversions.unwrapArray(outputAttributes, Object.class)).length); i++) {
          {
            EObject obj = outputAttributes.get(i);
            final Type outputType = this.typeProvider.typeForObject(obj);
            if ((outputType instanceof StructureType)) {
              this.createRoutineStructure(i, obj, statement);
            } else {
              this.createRoutineType(i, obj, statement);
            }
          }
        }
      }
      this._iXMappingInterpretQueue.pushNewLine();
      return true;
    }
    return false;
  }
  
  /**
   * generate method from call statement
   * @param gt generic type
   * @param methodName method name
   * @param inputAttributes input attribute
   */
  public void generateMethod(final JvmGenericType gt, final String methodName, final List<EObject> inputAttributes) {
    EList<JvmMember> _members = gt.getMembers();
    for (final JvmMember member : _members) {
      if ((member instanceof JvmOperation)) {
        boolean _equals = methodName.equals(((JvmOperation)member).getSimpleName());
        if (_equals) {
          EList<JvmFormalParameter> params = ((JvmOperation)member).getParameters();
          final int size = params.size();
          int _xifexpression = (int) 0;
          if ((inputAttributes == null)) {
            _xifexpression = 0;
          } else {
            _xifexpression = ((Object[])Conversions.unwrapArray(inputAttributes, Object.class)).length;
          }
          final int inputSize = _xifexpression;
          final int move = 1;
          for (int i = 0; (i < size); i++) {
            {
              if ((i < inputSize)) {
                this._iXMappingInterpretQueue.pushObjectName(inputAttributes.get(i));
              } else {
                this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.CODE_NULL.getValue());
              }
              if ((i < (size - move))) {
                this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.GENERAL_COMMA.getValue());
              }
            }
          }
        }
      }
    }
  }
  
  @Override
  protected boolean _interpret(final MapStatement statement) {
    final InputSection inputSection = statement.getInputSection();
    OutputSection outputSection = statement.getOutputSection();
    List<EObject> outputAttributes = ((List<EObject>) null);
    if ((outputSection != null)) {
      outputAttributes = statement.getOutputSection().getOutputAttributes();
    }
    final String name = statement.getRoutineSection().getSource().getName();
    if ((name == null)) {
      return false;
    }
    String routineName = this.generatorUtil.getRoutineName(name);
    String _javaName = this._xMappingInterpretObjectUtil.getJavaName(name);
    String javaRoutineName = ((routineName + ".") + _javaName);
    this._iXMappingInterpretQueue.pushNewLine();
    if ((outputAttributes != null)) {
      final List<EObject> _converted_outputAttributes = (List<EObject>)outputAttributes;
      int _length = ((Object[])Conversions.unwrapArray(_converted_outputAttributes, Object.class)).length;
      boolean _greaterThan = (_length > 0);
      if (_greaterThan) {
        final String line = "routineReturns = ";
        this._iXMappingInterpretQueue.pushCustomCode(line);
      }
    }
    String _value = SourcesEnum.GENERAL_LEFT_BRACKET.getValue();
    String _plus = (javaRoutineName + _value);
    this._iXMappingInterpretQueue.pushCustomCode(_plus);
    if ((inputSection != null)) {
      EList<EObject> inputAttributes = statement.getInputSection().getInputAttributes();
      final EList<EObject> _converted_inputAttributes = (EList<EObject>)inputAttributes;
      final int size = ((Object[])Conversions.unwrapArray(_converted_inputAttributes, Object.class)).length;
      final int move = 1;
      for (int i = 0; (i < size); i++) {
        {
          this._iXMappingInterpretQueue.pushObjectName(inputAttributes.get(i));
          if ((i < (size - move))) {
            this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.GENERAL_COMMA.getValue());
          }
        }
      }
    }
    String _value_1 = SourcesEnum.GENERAL_RIGHT_BRACKET.getValue();
    String _value_2 = SourcesEnum.GENERAL_DELIMITER.getValue();
    String _plus_1 = (_value_1 + _value_2);
    this._iXMappingInterpretQueue.pushCustomCode(_plus_1);
    this._iXMappingInterpretQueue.pushNewLine();
    if ((outputAttributes != null)) {
      for (int i = 0; (i < ((Object[])Conversions.unwrapArray(outputAttributes, Object.class)).length); i++) {
        {
          EObject obj = outputAttributes.get(i);
          final Type outputType = this.typeProvider.typeForObject(obj);
          if ((outputType instanceof StructureType)) {
            this.createRoutineStructure(i, obj, statement);
          } else {
            this.createRoutineType(i, obj, statement);
          }
        }
      }
    }
    this._iXMappingInterpretQueue.pushRoutineGenerator();
    return true;
  }
  
  @Override
  public boolean interpretRoutine(final Statement statement, final List<MapStatement> outerStatements) {
    this.typeProvider.activateRoutineScope(outerStatements);
    boolean isWorking = this.interpret(statement);
    this.typeProvider.deactivateRoutineScope();
    return isWorking;
  }
  
  @Override
  protected boolean _interpret(final InputImport inputImport) {
    final Input input = inputImport.getName();
    if ((input != null)) {
      String name = this.objectUtil.getObjectName(input);
      name = this._xMappingInterpretObjectUtil.getJavaName(name);
      this._iXMappingInterpretQueue.pushCustomCode(name);
      return true;
    }
    return false;
  }
  
  @Override
  protected boolean _interpret(final OutputImport outputImport) {
    final Output output = outputImport.getName();
    if ((output != null)) {
      String name = this.objectUtil.getObjectName(output);
      name = this._xMappingInterpretObjectUtil.getJavaName(name);
      this._iXMappingInterpretQueue.pushCustomCode(name);
      return true;
    }
    return false;
  }
  
  @Override
  public void interpretMethodHeader(final MapStatement statement, final List<MapStatement> outerStatements) {
    OutputSection outputSection = statement.getOutputSection();
    EList<EObject> outputAttributes = ((EList<EObject>) null);
    if ((outputSection != null)) {
      outputAttributes = statement.getOutputSection().getOutputAttributes();
    }
    final String name = statement.getRoutineSection().getSource().getName();
    if ((outputAttributes != null)) {
      String _value = SourcesEnum.CODE_RETURN_VALUE.getValue();
      String _plus = ("public static ArrayList<" + _value);
      final String line = (_plus + "> ");
      this._iXMappingInterpretQueue.pushCustomCode(line);
      this._iXMappingInterpretQueue.pushNewImport(SourcesEnum.IMPORT_RETURN_VALUE.getValue());
      this._iXMappingInterpretQueue.pushNewImport(SourcesEnum.IMPORT_ARRAYLIST.getValue());
    } else {
      final String line_1 = "public static void ";
      this._iXMappingInterpretQueue.pushCustomCode(line_1);
    }
    String _javaName = this._xMappingInterpretObjectUtil.getJavaName(name);
    String _value_1 = SourcesEnum.GENERAL_LEFT_BRACKET.getValue();
    String _plus_1 = (_javaName + _value_1);
    this._iXMappingInterpretQueue.pushCustomCode(_plus_1);
    InputSection _inputSection = statement.getInputSection();
    boolean _tripleNotEquals = (_inputSection != null);
    if (_tripleNotEquals) {
      EList<EObject> inputAttributes = statement.getInputSection().getInputAttributes();
      final EList<EObject> _converted_inputAttributes = (EList<EObject>)inputAttributes;
      final int size = ((Object[])Conversions.unwrapArray(_converted_inputAttributes, Object.class)).length;
      final int move = 1;
      for (int i = 0; (i < size); i++) {
        {
          this.typeProvider.activateRoutineScope(outerStatements);
          this._iXMappingInterpretQueue.pushObjectType(inputAttributes.get(i));
          final String inputPartName = " input";
          this._iXMappingInterpretQueue.pushCustomCode((inputPartName + Integer.valueOf(i)));
          if ((i < (size - move))) {
            this._iXMappingInterpretQueue.pushCustomCode(SourcesEnum.GENERAL_COMMA.getValue());
          }
          this.typeProvider.deactivateRoutineScope();
        }
      }
    }
    String _value_2 = SourcesEnum.GENERAL_RIGHT_BRACKET.getValue();
    String _plus_2 = (_value_2 + " {");
    this._iXMappingInterpretQueue.pushCustomCode(_plus_2);
  }
  
  @Override
  public String getRequest() {
    return this._iXMappingInterpretQueue.popRequest();
  }
  
  @Override
  public int getRequestsSize() {
    return this._iXMappingInterpretQueue.getStackSize();
  }
  
  @Override
  public boolean interpret(final EObject statement) {
    if (statement instanceof AssignmentStatement) {
      return _interpret((AssignmentStatement)statement);
    } else if (statement instanceof CallStatement) {
      return _interpret((CallStatement)statement);
    } else if (statement instanceof MapForwardStatement) {
      return _interpret((MapForwardStatement)statement);
    } else if (statement instanceof MapStatement) {
      return _interpret((MapStatement)statement);
    } else if (statement instanceof InputImport) {
      return _interpret((InputImport)statement);
    } else if (statement instanceof OutputImport) {
      return _interpret((OutputImport)statement);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(statement).toString());
    }
  }
}
