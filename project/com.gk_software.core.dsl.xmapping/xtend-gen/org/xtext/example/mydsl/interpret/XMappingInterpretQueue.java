package org.xtext.example.mydsl.interpret;

import java.util.LinkedList;
import java.util.Queue;
import javax.inject.Inject;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.xbase.lib.Extension;
import org.xtext.example.mydsl.helpers.classes.InterpretRequest;
import org.xtext.example.mydsl.helpers.classes.RequestTypeEnum;
import org.xtext.example.mydsl.helpers.classes.SourcesEnum;
import org.xtext.example.mydsl.helpers.util.XMappingEObjectUtil;
import org.xtext.example.mydsl.helpers.util.XMappingInterpretObjectUtil;
import org.xtext.example.mydsl.helpers.util.XMappingInterpretUtil;
import org.xtext.example.mydsl.interpret.IXMappingInterpretQueue;
import org.xtext.example.mydsl.typing.Type;
import org.xtext.example.mydsl.typing.XMappingTypeProvider;
import org.xtext.example.mydsl.xMapping.Assignment;
import org.xtext.example.mydsl.xMapping.Input;
import org.xtext.example.mydsl.xMapping.Model;
import org.xtext.example.mydsl.xMapping.Output;

@SuppressWarnings("all")
public class XMappingInterpretQueue implements IXMappingInterpretQueue {
  /**
   * type provider
   */
  @Inject
  private XMappingTypeProvider typeProvider;
  
  /**
   * interpret object
   */
  @Inject
  @Extension
  private XMappingInterpretObjectUtil _xMappingInterpretObjectUtil;
  
  /**
   * interpret util
   */
  @Inject
  @Extension
  private XMappingInterpretUtil _xMappingInterpretUtil;
  
  /**
   * object util
   */
  @Inject
  private XMappingEObjectUtil objectUtil;
  
  /**
   * constant for routine call
   */
  private static final String GENERATOR_COMMAND = "//routine";
  
  /**
   * queue for requests (to make command)
   */
  private Queue<InterpretRequest> requests = new LinkedList<InterpretRequest>();
  
  @Override
  public int getStackSize() {
    return this.requests.size();
  }
  
  @Override
  public void pushRoutineGenerator() {
    final String space = SourcesEnum.GENERAL_SPACE.getValue();
    InterpretRequest request = new InterpretRequest((space + XMappingInterpretQueue.GENERATOR_COMMAND), RequestTypeEnum.REQUEST_MACRO);
    this.pushRequest(request);
  }
  
  @Override
  public void pushNewImport(final String importValue) {
    final String importCommand = SourcesEnum.CODE_IMPORT.getValue();
    final String code = (importCommand + importValue);
    InterpretRequest request = new InterpretRequest(code, RequestTypeEnum.REQUEST_COMMAND);
    this.pushRequest(request);
  }
  
  @Override
  public void pushObjectNonPrimitiveType(final EObject object, final String identifier, final Type type) {
    InterpretRequest request = ((InterpretRequest) null);
    this.typeProvider.resetRoutineScope();
    Type objectType = type;
    if ((identifier != null)) {
      objectType = this.typeProvider.methodType(identifier);
    } else {
      if ((object != null)) {
        objectType = this.typeProvider.typeForObject(object);
      }
    }
    final String code = this.typeProvider.getNonPrimitiveType(objectType);
    InterpretRequest _interpretRequest = new InterpretRequest(code, RequestTypeEnum.REQUEST_COMMAND);
    request = _interpretRequest;
    this.pushRequest(request);
  }
  
  @Override
  public String pushObjectType(final EObject object) {
    final String document = SourcesEnum.CODE_DOCUMENT.getValue();
    final String file = SourcesEnum.CODE_FILE_INPUT.getValue();
    String returnValue = SourcesEnum.CODE_RETURN_VALUE.getValue();
    final String code = this._xMappingInterpretObjectUtil.getObjectType(object);
    if ((code != null)) {
      boolean _contains = code.contains(document);
      if (_contains) {
        this.pushNewImport(SourcesEnum.IMPORT_DOCUMENT_OUTPUT.getValue());
      } else {
        boolean _contains_1 = code.contains(returnValue);
        if (_contains_1) {
          this.pushNewImport(SourcesEnum.IMPORT_RETURN_VALUE.getValue());
        } else {
          boolean _contains_2 = code.contains(file);
          if (_contains_2) {
            this.pushNewImport(SourcesEnum.IMPORT_FILE_INPUT.getValue());
          }
        }
      }
      InterpretRequest request = new InterpretRequest(code, RequestTypeEnum.REQUEST_COMMAND);
      this.pushRequest(request);
    }
    return code;
  }
  
  @Override
  public void pushObjectName(final EObject object) {
    InterpretRequest request = ((InterpretRequest) null);
    String name = this._xMappingInterpretObjectUtil.getJavaName(this.objectUtil.getObjectName(object));
    final EObject basicObject = this.objectUtil.getBasicEObject(object);
    if (((basicObject instanceof Input) || (basicObject instanceof Output))) {
      Model model = EcoreUtil2.<Model>getContainerOfType(object, Model.class);
      String mappingClass = model.getClass_().getName();
      String packageName = model.getName();
      String _value = SourcesEnum.GENERAL_DOT.getValue();
      String _plus = (packageName + _value);
      String _plus_1 = (_plus + mappingClass);
      String _value_1 = SourcesEnum.GENERAL_DOT.getValue();
      final String path = (_plus_1 + _value_1);
      name = (path + name);
    }
    InterpretRequest _interpretRequest = new InterpretRequest(name, RequestTypeEnum.REQUEST_COMMAND);
    request = _interpretRequest;
    this.pushRequest(request);
  }
  
  @Override
  public void pushAssignmentValue(final Assignment assignment) {
    final String code = this._xMappingInterpretUtil.getAssignmentValue(assignment);
    InterpretRequest request = new InterpretRequest(code, RequestTypeEnum.REQUEST_COMMAND);
    this.pushRequest(request);
  }
  
  @Override
  public void pushEndCommand() {
    final String character = SourcesEnum.GENERAL_DELIMITER.getValue();
    InterpretRequest request = new InterpretRequest(character, RequestTypeEnum.REQUEST_COMMAND);
    this.pushRequest(request);
  }
  
  @Override
  public void pushCustomCode(final String code) {
    InterpretRequest request = new InterpretRequest(code, RequestTypeEnum.REQUEST_COMMAND);
    this.pushRequest(request);
  }
  
  @Override
  public void pushNewLine() {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append(" \t\t");
    _builder.newLine();
    final String code = _builder.toString();
    InterpretRequest request = new InterpretRequest(code, RequestTypeEnum.REQUEST_COMMAND);
    this.pushRequest(request);
  }
  
  @Override
  public void pushErrorCode(final String text) {
    InterpretRequest request = new InterpretRequest(text, RequestTypeEnum.REQUEST_COMMAND);
    this.pushRequest(request);
  }
  
  @Override
  public boolean pushXpath(final EObject object) {
    String code = this._xMappingInterpretObjectUtil.getObjectXPathCode(object);
    if ((code == null)) {
      return false;
    }
    InterpretRequest request = new InterpretRequest(code, RequestTypeEnum.REQUEST_COMMAND);
    this.pushRequest(request);
    return true;
  }
  
  @Override
  public boolean pushRequest(final InterpretRequest request) {
    if ((request != null)) {
      this.requests.add(request);
      return true;
    }
    return false;
  }
  
  @Override
  public String popRequest() {
    final InterpretRequest request = this.requests.poll();
    return request.getCode();
  }
}
