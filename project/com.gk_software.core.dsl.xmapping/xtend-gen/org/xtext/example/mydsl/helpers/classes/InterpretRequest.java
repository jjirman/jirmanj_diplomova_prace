package org.xtext.example.mydsl.helpers.classes;

import org.xtext.example.mydsl.helpers.classes.RequestTypeEnum;

/**
 * Class used in interpret (to make commands and so on..)
 * @author Bc. Jan Jirman
 */
@SuppressWarnings("all")
public class InterpretRequest {
  /**
   * request type
   */
  private RequestTypeEnum requestType;
  
  /**
   * code to write into template
   */
  private String code;
  
  /**
   * Constructor which creates InterpretRequest
   */
  public InterpretRequest(final String code, final RequestTypeEnum requestType) {
    this.code = code;
    this.requestType = requestType;
  }
  
  /**
   * Gets request type
   */
  public RequestTypeEnum getRequestType() {
    return this.requestType;
  }
  
  /**
   * Gets code
   */
  public String getCode() {
    return this.code;
  }
}
