package org.xtext.example.mydsl.helpers.util;

import com.google.inject.Inject;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.xtext.example.mydsl.helpers.util.XMappingEObjectUtil;
import org.xtext.example.mydsl.typing.DoubleType;
import org.xtext.example.mydsl.typing.IntType;
import org.xtext.example.mydsl.typing.StringType;
import org.xtext.example.mydsl.typing.StructureType;
import org.xtext.example.mydsl.typing.Type;
import org.xtext.example.mydsl.typing.XMappingTypeProvider;
import org.xtext.example.mydsl.xMapping.Attribute;
import org.xtext.example.mydsl.xMapping.Input;
import org.xtext.example.mydsl.xMapping.InputAttribute;
import org.xtext.example.mydsl.xMapping.OutputAttribute;
import org.xtext.example.mydsl.xMapping.Return;

/**
 * Utility which helps interpet with objects
 * @author Bc. Jan Jirman
 */
@SuppressWarnings("all")
public class XMappingInterpretObjectUtil {
  /**
   * type provider
   */
  @Inject
  private XMappingTypeProvider typeProvider;
  
  /**
   * object util
   */
  @Inject
  private XMappingEObjectUtil objectUtil;
  
  /**
   * Generates java name from object name
   * @param objectName object name
   * @return object java name
   */
  public String getJavaName(final String objectName) {
    String _xblockexpression = null;
    {
      String betterName = objectName.replace(".", "_");
      _xblockexpression = betterName = betterName.toLowerCase();
    }
    return _xblockexpression;
  }
  
  /**
   * Gets Xpath string from object
   * @param object object
   * @return charSequence xpath
   */
  public String getObjectXPathCode(final EObject object) {
    boolean _matched = false;
    if (object instanceof InputAttribute) {
      _matched=true;
      String _xPath = ((InputAttribute)object).getXPath();
      boolean _tripleNotEquals = (_xPath != null);
      if (_tripleNotEquals) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("\"");
        String _xPath_1 = ((InputAttribute)object).getXPath();
        _builder.append(_xPath_1);
        _builder.append("\"");
        return _builder.toString();
      }
    }
    if (!_matched) {
      if (object instanceof OutputAttribute) {
        _matched=true;
        String _xPath = ((OutputAttribute)object).getXPath();
        boolean _tripleNotEquals = (_xPath != null);
        if (_tripleNotEquals) {
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("\"");
          String _xPath_1 = ((OutputAttribute)object).getXPath();
          _builder.append(_xPath_1);
          _builder.append("\"");
          return _builder.toString();
        }
      }
    }
    if (!_matched) {
      if (object instanceof Attribute) {
        _matched=true;
        String _xPath = ((Attribute)object).getXPath();
        boolean _tripleNotEquals = (_xPath != null);
        if (_tripleNotEquals) {
          StringConcatenation _builder = new StringConcatenation();
          _builder.append("\"");
          String _xPath_1 = ((Attribute)object).getXPath();
          _builder.append(_xPath_1);
          _builder.append("\"");
          return _builder.toString();
        }
      }
    }
    return null;
  }
  
  /**
   * Gets object type
   * @param object object
   * @return object type
   */
  public String getObjectType(final EObject object) {
    String _xblockexpression = null;
    {
      final EObject basicObject = this.objectUtil.getBasicEObject(object);
      String _xifexpression = null;
      if ((basicObject instanceof Return)) {
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("ReturnValue");
        return _builder.toString();
      } else {
        String _xblockexpression_1 = null;
        {
          final Type type = this.typeProvider.typeForObject(object);
          String _switchResult = null;
          boolean _matched = false;
          if (type instanceof IntType) {
            _matched=true;
            StringConcatenation _builder_1 = new StringConcatenation();
            _builder_1.append("int");
            _switchResult = _builder_1.toString();
          }
          if (!_matched) {
            if (type instanceof DoubleType) {
              _matched=true;
              StringConcatenation _builder_1 = new StringConcatenation();
              _builder_1.append("double");
              _switchResult = _builder_1.toString();
            }
          }
          if (!_matched) {
            if (type instanceof StringType) {
              _matched=true;
              StringConcatenation _builder_1 = new StringConcatenation();
              _builder_1.append("String");
              _switchResult = _builder_1.toString();
            }
          }
          if (!_matched) {
            if (type instanceof StructureType) {
              _matched=true;
              String _xifexpression_1 = null;
              if ((basicObject instanceof Input)) {
                StringConcatenation _builder_1 = new StringConcatenation();
                _builder_1.append("FileInput");
                _xifexpression_1 = _builder_1.toString();
              } else {
                StringConcatenation _builder_2 = new StringConcatenation();
                _builder_2.append("DocumentOutput");
                _xifexpression_1 = _builder_2.toString();
              }
              _switchResult = _xifexpression_1;
            }
          }
          _xblockexpression_1 = _switchResult;
        }
        _xifexpression = _xblockexpression_1;
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
}
