package org.xtext.example.mydsl.helpers.util;

import java.util.List;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.StringExtensions;

/**
 * Utility which helps generator to generate right file paths, names
 * @author Bc. Jan Jirman
 */
@SuppressWarnings("all")
public class XMappingGeneratorUtil {
  /**
   * Gets resource fullname
   * @param resource resource
   */
  public String getResourceFullName(final Resource resource) {
    final int size = resource.getURI().segments().length;
    final String name = resource.getURI().segment((size - 1));
    return name;
  }
  
  /**
   * Gets resource name
   * @param resource resource
   */
  public String getResourceName(final Resource resource) {
    final int size = resource.getURI().segments().length;
    final String name = ((List<String>)Conversions.doWrapArray(resource.getURI().segment((size - 1)).split("\\."))).iterator().next();
    String _upperCase = name.substring(0, 1).toUpperCase();
    String _lowerCase = name.substring(1).toLowerCase();
    final String finalName = (_upperCase + _lowerCase);
    return finalName;
  }
  
  /**
   * Gets class name
   * @param class name
   */
  public String getClassName(final String className) {
    String _upperCase = className.substring(0, 1).toUpperCase();
    String _substring = className.substring(1);
    final String nameClass = (_upperCase + _substring);
    return nameClass;
  }
  
  /**
   * Gets routine name
   * @param routineName routine name
   */
  public String getRoutineName(final String routineName) {
    final int index = 0;
    final int secondIndex = 1;
    String[] splittedName = routineName.split("\\.");
    String routinePartName = splittedName[secondIndex];
    String routinePartMacro = splittedName[index];
    routinePartName = StringExtensions.toFirstUpper(routinePartName);
    return (routinePartMacro + routinePartName);
  }
  
  /**
   * Gets resource src - package is converted from "my.path" to "my/path/"
   */
  public String getResourceSrcCommand(final String packageName) {
    String packageInput = packageName;
    packageInput = packageInput.replaceAll("\\.", "\\/");
    packageInput = packageInput.replaceAll("\\-", "\\_");
    packageInput = (packageInput + "/");
    return packageInput;
  }
}
