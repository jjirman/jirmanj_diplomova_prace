package org.xtext.example.mydsl.helpers.classes;

import java.util.ArrayList;
import java.util.List;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.xtext.example.mydsl.xMapping.InputImport;
import org.xtext.example.mydsl.xMapping.OutputImport;

/**
 * Class used in generator - contains all information about class / routine
 * @author Bc. Jan Jirman
 */
@SuppressWarnings("all")
public class FileCode {
  /**
   * File package
   */
  private String packageCode;
  
  /**
   * Imports which class contains
   */
  private List<InputImport> inputs = new ArrayList<InputImport>();
  
  /**
   * Outputs which class contains
   */
  private List<OutputImport> outputs = new ArrayList<OutputImport>();
  
  /**
   * Class name
   */
  private String className;
  
  /**
   * Import to add (not defined in .mapping file)
   */
  private List<String> importsCode;
  
  /**
   * chaSequence of class main block
   */
  private CharSequence blockCode;
  
  /**
   * Constructor which creates FileCode
   * @param packageCode package code
   * @param className class name
   */
  public FileCode(final String packageCode, final String className) {
    this.packageCode = packageCode;
    this.className = className;
    ArrayList<String> _arrayList = new ArrayList<String>();
    this.importsCode = _arrayList;
    StringBuffer _stringBuffer = new StringBuffer("");
    this.blockCode = _stringBuffer;
  }
  
  /**
   * adds part of main code block
   * @param blockCode code block
   */
  public void addCodeBlock(final CharSequence blockCode) {
    StringBuffer buffer = new StringBuffer(this.blockCode);
    buffer.append(blockCode);
    this.blockCode = buffer;
  }
  
  /**
   * Adds new import into importsCode (which are added dynamically)
   * @param importValue new import
   */
  public void addImport(final String importValue) {
    if (((importValue == null) || (!importValue.equals("")))) {
      this.importsCode.add(importValue);
    }
  }
  
  /**
   * Adds input
   * @param input input
   */
  public boolean addInput(final InputImport input) {
    return this.inputs.add(input);
  }
  
  /**
   * Adds output
   * @param output output
   */
  public boolean addOutput(final OutputImport output) {
    return this.outputs.add(output);
  }
  
  /**
   * Gets full routine name
   * @return routine fullname
   */
  public String getRoutineFullName() {
    String realPackage = this.getRoutinePackage();
    return ((realPackage + ".") + this.className);
  }
  
  /**
   * Gets routine package name
   * @return package name
   */
  public String getRoutinePackage() {
    String _xblockexpression = null;
    {
      String[] toSplitArray = this.packageCode.split("\\.");
      final String[] _converted_toSplitArray = (String[])toSplitArray;
      int _size = ((List<String>)Conversions.doWrapArray(_converted_toSplitArray)).size();
      int _minus = (_size - 1);
      final String toSplit = toSplitArray[_minus];
      String fullName = this.packageCode.replace(toSplit, "statement");
      String _lowerCase = toSplit.toLowerCase();
      String _plus = ((fullName + ".") + _lowerCase);
      _xblockexpression = fullName = _plus;
    }
    return _xblockexpression;
  }
  
  /**
   * Gets all inputs
   * @return all inputs
   */
  public List<InputImport> getInputs() {
    return this.inputs;
  }
  
  /**
   * Gets all outputs
   * @return all outputs
   */
  public List<OutputImport> getOutputs() {
    return this.outputs;
  }
  
  /**
   * Sets block code
   * @param block code
   */
  public CharSequence setBlockCode(final CharSequence blockCode) {
    return this.blockCode = blockCode;
  }
  
  /**
   * Gets package code
   * @return package code
   */
  public String getPackageCode() {
    return this.packageCode;
  }
  
  /**
   * Gets full name
   * @return full name
   */
  public String getFullName() {
    return ((this.packageCode + ".") + this.className);
  }
  
  /**
   * Gets class name
   * @return class name
   */
  public String getClassName() {
    return this.className;
  }
  
  /**
   * Gets all imports
   * @return list of imports
   */
  public List<String> getImportsCode() {
    return this.importsCode;
  }
  
  /**
   * Gets block code
   * @return block code
   */
  public CharSequence getBlockCode() {
    return this.blockCode;
  }
  
  /**
   * Sets imports code
   * @param list of importCode
   */
  public void setImportsCode(final List<String> importsCode) {
    this.importsCode = importsCode;
  }
}
