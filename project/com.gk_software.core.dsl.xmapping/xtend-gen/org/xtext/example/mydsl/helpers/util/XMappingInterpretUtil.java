package org.xtext.example.mydsl.helpers.util;

import com.google.inject.Inject;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.xbase.lib.Extension;
import org.xtext.example.mydsl.helpers.util.XMappingEObjectUtil;
import org.xtext.example.mydsl.helpers.util.XMappingInterpretObjectUtil;
import org.xtext.example.mydsl.helpers.util.XMappingStatementUtil;
import org.xtext.example.mydsl.xMapping.Assignment;
import org.xtext.example.mydsl.xMapping.AssignmentStructure;
import org.xtext.example.mydsl.xMapping.DoubleType;
import org.xtext.example.mydsl.xMapping.IntType;
import org.xtext.example.mydsl.xMapping.Model;
import org.xtext.example.mydsl.xMapping.Statement;
import org.xtext.example.mydsl.xMapping.StringType;

/**
 * Utility which helps interpet in general
 * @author Bc. Jan Jirman
 */
@SuppressWarnings("all")
public class XMappingInterpretUtil {
  /**
   * interpret object
   */
  @Inject
  @Extension
  private XMappingInterpretObjectUtil _xMappingInterpretObjectUtil;
  
  /**
   * object util
   */
  @Inject
  private XMappingEObjectUtil objectUtil;
  
  /**
   * statement util
   */
  @Inject
  private XMappingStatementUtil statementUtil;
  
  /**
   * name provider
   */
  @Inject
  private IQualifiedNameProvider nameProvider;
  
  /**
   * Gets assignment value from "Assignment object"
   * @param assignment Assignment object
   * @return value
   */
  public String getAssignmentValue(final Assignment assignment) {
    String _switchResult = null;
    boolean _matched = false;
    if (assignment instanceof IntType) {
      _matched=true;
      StringConcatenation _builder = new StringConcatenation();
      String _value = ((IntType)assignment).getValue();
      _builder.append(_value);
      _switchResult = _builder.toString();
    }
    if (!_matched) {
      if (assignment instanceof DoubleType) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        String _value = ((DoubleType)assignment).getValue();
        _builder.append(_value);
        _switchResult = _builder.toString();
      }
    }
    if (!_matched) {
      if (assignment instanceof StringType) {
        _matched=true;
        StringConcatenation _builder = new StringConcatenation();
        _builder.append("\"");
        String _value = ((StringType)assignment).getValue();
        _builder.append(_value);
        _builder.append("\"");
        _switchResult = _builder.toString();
      }
    }
    if (!_matched) {
      if (assignment instanceof AssignmentStructure) {
        _matched=true;
        return this.specificAssignmentStructure(assignment);
      }
    }
    return _switchResult;
  }
  
  /**
   * Gets specific assigment structure
   * @param assignment assignment object
   * @return object java name
   */
  public String specificAssignmentStructure(final Assignment assignment) {
    if ((assignment instanceof AssignmentStructure)) {
      Model model = EcoreUtil2.<Model>getContainerOfType(assignment, Model.class);
      String mappingClass = model.getClass_().getName();
      String packageName = model.getName();
      StringConcatenation _builder = new StringConcatenation();
      _builder.append(packageName);
      _builder.append(".");
      _builder.append(mappingClass);
      _builder.append(".");
      String _javaName = this._xMappingInterpretObjectUtil.getJavaName(this.objectUtil.getObjectName(((AssignmentStructure)assignment).getSource().getOutputType().getOutputRef()));
      _builder.append(_javaName);
      _builder.append(".getPartOfDocument(\"");
      String _xPath = ((AssignmentStructure)assignment).getSource().getXPath();
      _builder.append(_xPath);
      _builder.append("\")");
      return _builder.toString();
    }
    return null;
  }
  
  /**
   * Gets return from routine by index
   * @param statement map statement
   * @param index index
   */
  public EObject getReturnObjectByIndex(final Statement statement, final int index) {
    final List<EObject> returns = this.statementUtil.getReturnsFromRoutine(statement);
    final int returnsSize = returns.size();
    if ((index < returnsSize)) {
      for (final EObject returnObject : returns) {
        {
          String name = this.nameProvider.getFullyQualifiedName(returnObject).getLastSegment();
          String returnToFind = name;
          final String regex = "Return.\\d";
          final String nameToFind = ("Return." + Integer.valueOf(index));
          returnToFind = returnToFind.replaceAll(regex, nameToFind);
          boolean _equals = returnToFind.equals(name);
          if (_equals) {
            return returnObject;
          }
        }
      }
      return null;
    }
    return null;
  }
}
