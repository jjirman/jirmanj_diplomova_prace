package org.xtext.example.mydsl.helpers.util;

import com.google.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.xtext.example.mydsl.helpers.util.XMappingStatementUtil;
import org.xtext.example.mydsl.xMapping.AssignmentStatement;
import org.xtext.example.mydsl.xMapping.Attribute;
import org.xtext.example.mydsl.xMapping.Block;
import org.xtext.example.mydsl.xMapping.Input;
import org.xtext.example.mydsl.xMapping.InputAttribute;
import org.xtext.example.mydsl.xMapping.InputImport;
import org.xtext.example.mydsl.xMapping.InputType;
import org.xtext.example.mydsl.xMapping.NewAttribute;
import org.xtext.example.mydsl.xMapping.Output;
import org.xtext.example.mydsl.xMapping.OutputAttribute;
import org.xtext.example.mydsl.xMapping.OutputImport;
import org.xtext.example.mydsl.xMapping.OutputType;
import org.xtext.example.mydsl.xMapping.Param;
import org.xtext.example.mydsl.xMapping.ParamSection;
import org.xtext.example.mydsl.xMapping.Return;
import org.xtext.example.mydsl.xMapping.Statement;
import org.xtext.example.mydsl.xMapping.UsedAttribute;
import org.xtext.example.mydsl.xMapping.Variable;

/**
 * Utility which helps work with EObjects
 * @author Bc. Jan Jirman
 */
@SuppressWarnings("all")
public class XMappingEObjectUtil {
  /**
   * statement util
   */
  @Inject
  private XMappingStatementUtil statementUtil;
  
  /**
   * name provider
   */
  @Inject
  private IQualifiedNameProvider nameProvider;
  
  /**
   * Gets object name
   */
  public String getObjectName(final EObject object) {
    String _switchResult = null;
    boolean _matched = false;
    if (object instanceof InputAttribute) {
      _matched=true;
      final InputType reference = ((InputAttribute)object).getInputType();
      Param _paramRef = reference.getParamRef();
      boolean _tripleNotEquals = (_paramRef != null);
      if (_tripleNotEquals) {
        EObject container = reference.getParamRef().eContainer();
        if ((container instanceof ParamSection)) {
          final int index = ((ParamSection)container).getParamSection().indexOf(this.getBasicEObject(object));
          return ("input" + Integer.valueOf(index));
        }
        return reference.getParamRef().getName();
      }
      return reference.getInputRef().getName();
    }
    if (!_matched) {
      if (object instanceof OutputAttribute) {
        _matched=true;
        final OutputType reference = ((OutputAttribute)object).getOutputType();
        Return _returnRef = reference.getReturnRef();
        boolean _tripleNotEquals = (_returnRef != null);
        if (_tripleNotEquals) {
          return reference.getReturnRef().getName();
        }
        return reference.getOutputRef().getName();
      }
    }
    if (!_matched) {
      if (object instanceof Attribute) {
        _matched=true;
        return ((Attribute)object).getType().getName();
      }
    }
    if (!_matched) {
      if (object instanceof Param) {
        _matched=true;
        EObject container = ((Param)object).eCrossReferences().get(0).eContainer();
        if ((container instanceof List)) {
          final int index = ((List)container).indexOf(object);
          return ("input" + Integer.valueOf(index));
        }
        return ((Param)object).getName();
      }
    }
    if (!_matched) {
      if (object instanceof Variable) {
        _matched=true;
        _switchResult = ((Variable)object).getName();
      }
    }
    if (!_matched) {
      if (object instanceof Return) {
        _matched=true;
        _switchResult = ((Return)object).getName();
      }
    }
    if (!_matched) {
      if (object instanceof Input) {
        _matched=true;
        _switchResult = ((Input)object).getName();
      }
    }
    if (!_matched) {
      if (object instanceof Output) {
        _matched=true;
        _switchResult = ((Output)object).getName();
      }
    }
    return _switchResult;
  }
  
  /**
   * Gets Object's Xpath
   * @param object object
   */
  public String getObjectXPath(final EObject object) {
    boolean _matched = false;
    if (object instanceof InputAttribute) {
      _matched=true;
      String _xPath = ((InputAttribute)object).getXPath();
      boolean _tripleNotEquals = (_xPath != null);
      if (_tripleNotEquals) {
        return ((InputAttribute)object).getXPath();
      }
    }
    if (!_matched) {
      if (object instanceof OutputAttribute) {
        _matched=true;
        String _xPath = ((OutputAttribute)object).getXPath();
        boolean _tripleNotEquals = (_xPath != null);
        if (_tripleNotEquals) {
          return ((OutputAttribute)object).getXPath();
        }
      }
    }
    if (!_matched) {
      if (object instanceof Attribute) {
        _matched=true;
        String _xPath = ((Attribute)object).getXPath();
        boolean _tripleNotEquals = (_xPath != null);
        if (_tripleNotEquals) {
          return ((Attribute)object).getXPath();
        }
      }
    }
    return null;
  }
  
  /**
   * Gets basic object (like Variable, Input, Param) from object like InputAttribute, ...
   */
  public EObject getBasicEObject(final EObject object) {
    boolean _matched = false;
    if (object instanceof InputAttribute) {
      _matched=true;
      final InputType reference = ((InputAttribute)object).getInputType();
      if ((reference != null)) {
        Param _paramRef = reference.getParamRef();
        boolean _tripleNotEquals = (_paramRef != null);
        if (_tripleNotEquals) {
          return reference.getParamRef();
        }
        return reference.getInputRef();
      }
    }
    if (!_matched) {
      if (object instanceof OutputAttribute) {
        _matched=true;
        final OutputType reference = ((OutputAttribute)object).getOutputType();
        if ((reference != null)) {
          Return _returnRef = reference.getReturnRef();
          boolean _tripleNotEquals = (_returnRef != null);
          if (_tripleNotEquals) {
            return reference.getReturnRef();
          }
          return reference.getOutputRef();
        }
      }
    }
    if (!_matched) {
      if (object instanceof Attribute) {
        _matched=true;
        return ((Attribute)object).getType();
      }
    }
    if (!_matched) {
      if (object instanceof NewAttribute) {
        _matched=true;
        return ((NewAttribute)object).getSource();
      }
    }
    if (!_matched) {
      if (object instanceof UsedAttribute) {
        _matched=true;
        return ((UsedAttribute)object).getSource();
      }
    }
    if (!_matched) {
      if (object instanceof InputImport) {
        _matched=true;
        return ((InputImport)object).getName();
      }
    }
    if (!_matched) {
      if (object instanceof OutputImport) {
        _matched=true;
        return ((OutputImport)object).getName();
      }
    }
    return object;
  }
  
  /**
   * Finds type for object in CallStatement
   * @param <T> extends EObject
   * @param list list with attributes
   * @param object object
   * @return object index in list
   */
  public <T extends EObject> int getObjectIndexByObject(final EList<T> list, final EObject object) {
    int objectIndex = (-1);
    for (final EObject obj : list) {
      {
        objectIndex++;
        if ((obj instanceof OutputAttribute)) {
          OutputType instance = ((OutputAttribute)obj).getOutputType();
          Return _returnRef = instance.getReturnRef();
          boolean _tripleNotEquals = (_returnRef != null);
          if (_tripleNotEquals) {
            boolean _equals = this.nameProvider.getFullyQualifiedName(instance.getReturnRef()).equals(this.nameProvider.getFullyQualifiedName(object));
            if (_equals) {
              return objectIndex;
            }
          } else {
            Output _outputRef = instance.getOutputRef();
            boolean _tripleNotEquals_1 = (_outputRef != null);
            if (_tripleNotEquals_1) {
              boolean _equals_1 = this.nameProvider.getFullyQualifiedName(instance.getReturnRef()).equals(this.nameProvider.getFullyQualifiedName(object));
              if (_equals_1) {
                return objectIndex;
              }
            }
          }
        } else {
          if ((obj instanceof Attribute)) {
            boolean _equals_2 = this.nameProvider.getFullyQualifiedName(((Attribute)obj).getType()).equals(this.nameProvider.getFullyQualifiedName(object));
            if (_equals_2) {
              return objectIndex;
            }
          }
        }
      }
    }
    objectIndex = (-1);
    return objectIndex;
  }
  
  /**
   * Gets object's declaration
   * @param <T> extends EObject
   * @param statement statement which has declaration of object
   * @return declaration list
   */
  public <T extends EObject> List<T> getObjectFromDeclaration(final Statement statement) {
    if ((statement instanceof AssignmentStatement)) {
      EList<NewAttribute> _new = ((AssignmentStatement)statement).getDeclaration().getNew();
      boolean _tripleNotEquals = (_new != null);
      if (_tripleNotEquals) {
        EList<NewAttribute> _new_1 = ((AssignmentStatement)statement).getDeclaration().getNew();
        return ((List<T>) _new_1);
      } else {
        EList<UsedAttribute> _reference = ((AssignmentStatement)statement).getDeclaration().getReference();
        boolean _tripleNotEquals_1 = (_reference != null);
        if (_tripleNotEquals_1) {
          EList<UsedAttribute> _reference_1 = ((AssignmentStatement)statement).getDeclaration().getReference();
          return ((List<T>) _reference_1);
        }
      }
    }
    return new ArrayList<T>();
  }
  
  /**
   * Gets object by its name
   * @param block block where to find
   * @param name object name
   * @return EObject instance
   */
  public EObject getObjectByName(final Block block, final String name) {
    Object _xblockexpression = null;
    {
      final int index = 0;
      final AssignmentStatement assignmentStatement = this.statementUtil.getAssignmentStatement(block, name);
      if ((assignmentStatement != null)) {
        final Function1<NewAttribute, Boolean> _function = (NewAttribute it) -> {
          boolean _xblockexpression_1 = false;
          {
            EObject source = it.getSource();
            boolean _xifexpression = false;
            if ((source instanceof Return)) {
              _xifexpression = ((Return)source).getName().equals(name);
            } else {
              boolean _xifexpression_1 = false;
              if ((source instanceof Variable)) {
                _xifexpression_1 = ((Variable)source).getName().equals(name);
              }
              _xifexpression = _xifexpression_1;
            }
            _xblockexpression_1 = _xifexpression;
          }
          return Boolean.valueOf(_xblockexpression_1);
        };
        return (((NewAttribute[])Conversions.unwrapArray(IterableExtensions.<NewAttribute>filter(assignmentStatement.getDeclaration().getNew(), _function), NewAttribute.class))[index]).getSource();
      }
      _xblockexpression = null;
    }
    return ((EObject)_xblockexpression);
  }
  
  /**
   * Checks that object is Initialized in current statement
   */
  public boolean isEObjectInitializedInStatement(final Statement statement, final EObject object) {
    final Statement findStatement = this.statementUtil.findObjectInitialization(this.getBasicEObject(object));
    if ((findStatement != null)) {
      int _hashCode = findStatement.hashCode();
      int _hashCode_1 = statement.hashCode();
      boolean _equals = (_hashCode == _hashCode_1);
      if (_equals) {
        return true;
      }
    }
    return false;
  }
}
