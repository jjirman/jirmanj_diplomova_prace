package org.xtext.example.mydsl.helpers.util;

import com.google.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.xtext.example.mydsl.helpers.util.XMappingEObjectUtil;
import org.xtext.example.mydsl.xMapping.AssignmentStatement;
import org.xtext.example.mydsl.xMapping.Attribute;
import org.xtext.example.mydsl.xMapping.Block;
import org.xtext.example.mydsl.xMapping.CallStatement;
import org.xtext.example.mydsl.xMapping.Declaration;
import org.xtext.example.mydsl.xMapping.MapForwardStatement;
import org.xtext.example.mydsl.xMapping.MapStatement;
import org.xtext.example.mydsl.xMapping.Model;
import org.xtext.example.mydsl.xMapping.NewAttribute;
import org.xtext.example.mydsl.xMapping.Output;
import org.xtext.example.mydsl.xMapping.OutputAttribute;
import org.xtext.example.mydsl.xMapping.OutputSection;
import org.xtext.example.mydsl.xMapping.Param;
import org.xtext.example.mydsl.xMapping.ParamSection;
import org.xtext.example.mydsl.xMapping.Return;
import org.xtext.example.mydsl.xMapping.RoutineBlock;
import org.xtext.example.mydsl.xMapping.RoutineSource;
import org.xtext.example.mydsl.xMapping.Statement;
import org.xtext.example.mydsl.xMapping.UsedAttribute;
import org.xtext.example.mydsl.xMapping.Variable;

/**
 * Utility which helps interpet with statements
 * @author Bc. Jan Jirman
 */
@SuppressWarnings("all")
public class XMappingStatementUtil {
  /**
   * Name provider for objects
   */
  @Inject
  private IQualifiedNameProvider nameProvider;
  
  /**
   * object util
   */
  @Inject
  private XMappingEObjectUtil objectUtil;
  
  @Inject
  private XMappingStatementUtil statementUtil;
  
  /**
   * Finds the first initialization statement of the specific object
   * @param object object
   * @return statement
   */
  public Statement findObjectInitialization(final EObject object) {
    final Block block = EcoreUtil2.<Block>getContainerOfType(object, Block.class);
    final AssignmentStatement assignmentStatement = this.statementUtil.getAssignmentStatement(block, this.nameProvider.getFullyQualifiedName(object).getLastSegment());
    if (((assignmentStatement != null) && (assignmentStatement.getInitialization() != null))) {
      return assignmentStatement;
    }
    return this.findFirstInit(block, object);
  }
  
  /**
   * Finds declared params
   * @param statement statement
   * @param param param
   * @return object
   */
  public EObject findDeclaredParam(final Statement statement, final Param param) {
    Object _xblockexpression = null;
    {
      if ((statement != null)) {
        final EList<Param> paramSection = EcoreUtil2.<ParamSection>getContainerOfType(param, ParamSection.class).getParamSection();
        if ((statement instanceof MapStatement)) {
          int _size = paramSection.size();
          int _length = ((Object[])Conversions.unwrapArray(((MapStatement)statement).getInputSection().getInputAttributes(), Object.class)).length;
          boolean _equals = (_size == _length);
          if (_equals) {
            final int index = paramSection.indexOf(param);
            return ((MapStatement)statement).getInputSection().getInputAttributes().get(index);
          } else {
            int i = (-1);
            EList<EObject> _inputAttributes = ((MapStatement)statement).getInputSection().getInputAttributes();
            for (final EObject input : _inputAttributes) {
              {
                i++;
                int _hashCode = this.objectUtil.getBasicEObject(input).hashCode();
                int _hashCode_1 = param.hashCode();
                boolean _equals_1 = (_hashCode == _hashCode_1);
                if (_equals_1) {
                  return param;
                }
              }
            }
          }
        }
      }
      _xblockexpression = null;
    }
    return ((EObject)_xblockexpression);
  }
  
  /**
   * Gets assigment statement
   * @param block block
   * @param name name
   * @return AssignmentStatement
   */
  public AssignmentStatement getAssignmentStatement(final Block block, final String name) {
    AssignmentStatement _xblockexpression = null;
    {
      List<AssignmentStatement> statements = EcoreUtil2.<AssignmentStatement>getAllContentsOfType(block, AssignmentStatement.class);
      final Function1<AssignmentStatement, Boolean> _function = (AssignmentStatement it) -> {
        return Boolean.valueOf(((it.getDeclaration().getNew() != null) && IterableExtensions.<NewAttribute>exists(it.getDeclaration().getNew(), 
          ((Function1<NewAttribute, Boolean>) (NewAttribute it_1) -> {
            boolean _xblockexpression_1 = false;
            {
              EObject source = it_1.getSource();
              boolean _xifexpression = false;
              if ((source instanceof Return)) {
                _xifexpression = ((Return)source).getName().equals(name);
              } else {
                boolean _xifexpression_1 = false;
                if ((source instanceof Variable)) {
                  _xifexpression_1 = ((Variable)source).getName().equals(name);
                }
                _xifexpression = _xifexpression_1;
              }
              _xblockexpression_1 = _xifexpression;
            }
            return Boolean.valueOf(_xblockexpression_1);
          }))));
      };
      _xblockexpression = IterableExtensions.<AssignmentStatement>findFirst(statements, _function);
    }
    return _xblockexpression;
  }
  
  /**
   * Gets assignment list
   * @param declaration Declaration object
   * @return assigment list
   */
  public <T extends EObject> EList<T> getAssigmnentList(final Declaration declaration) {
    EList<T> _xblockexpression = null;
    {
      Declaration refSource = declaration.getTarget();
      EList<NewAttribute> newSource = declaration.getNew();
      EList<T> _xifexpression = null;
      if ((refSource != null)) {
        EList<UsedAttribute> _reference = refSource.getReference();
        _xifexpression = ((EList<T>) _reference);
      } else {
        EList<T> _xifexpression_1 = null;
        if ((newSource != null)) {
          _xifexpression_1 = ((EList<T>) newSource);
        }
        _xifexpression = _xifexpression_1;
      }
      _xblockexpression = _xifexpression;
    }
    return _xblockexpression;
  }
  
  /**
   * Finds the first initialization a returns its statement
   * @param block block
   * @param object object
   */
  public Statement findFirstInit(final Block block, final EObject object) {
    Object _xblockexpression = null;
    {
      List<Statement> statements = EcoreUtil2.<Statement>getAllContentsOfType(block, Statement.class);
      if ((statements != null)) {
        final Function1<Statement, Boolean> _function = (Statement it) -> {
          boolean _xifexpression = false;
          if ((it instanceof AssignmentStatement)) {
            _xifexpression = ((((AssignmentStatement)it).getInitialization() != null) && (((((AssignmentStatement)it).getDeclaration().getTarget() != null) && IterableExtensions.<UsedAttribute>exists(((AssignmentStatement)it).getDeclaration().getTarget().getReference(), ((Function1<UsedAttribute, Boolean>) (UsedAttribute it_1) -> {
              return Boolean.valueOf(((it_1.getSource() != null) && this.nameProvider.getFullyQualifiedName(it_1.getSource()).equals(this.nameProvider.getFullyQualifiedName(object))));
            }))) || ((((AssignmentStatement)it).getDeclaration().getNew() != null) && IterableExtensions.<NewAttribute>exists(((AssignmentStatement)it).getDeclaration().getNew(), ((Function1<NewAttribute, Boolean>) (NewAttribute it_1) -> {
              return Boolean.valueOf(((it_1.getSource() != null) && this.nameProvider.getFullyQualifiedName(it_1.getSource()).equals(this.nameProvider.getFullyQualifiedName(object))));
            })))));
          } else {
            boolean _xifexpression_1 = false;
            if ((it instanceof MapStatement)) {
              boolean _xblockexpression_1 = false;
              {
                final OutputSection outputSection = ((MapStatement)it).getOutputSection();
                boolean _xifexpression_2 = false;
                if ((outputSection != null)) {
                  _xifexpression_2 = this.existsObjectInOutputSection(((MapStatement)it).getOutputSection().getOutputAttributes(), object);
                }
                _xblockexpression_1 = _xifexpression_2;
              }
              _xifexpression_1 = _xblockexpression_1;
            } else {
              boolean _xifexpression_2 = false;
              if ((it instanceof MapForwardStatement)) {
                _xifexpression_2 = this.existsObjectInOutputSection(((MapForwardStatement)it).getOutputSection().getOutputAttributes(), object);
              } else {
                boolean _xifexpression_3 = false;
                if ((it instanceof CallStatement)) {
                  _xifexpression_3 = this.existsObjectInOutputSection(((CallStatement)it).getOutputSection().getOutputAttributes(), object);
                }
                _xifexpression_2 = _xifexpression_3;
              }
              _xifexpression_1 = _xifexpression_2;
            }
            _xifexpression = _xifexpression_1;
          }
          return Boolean.valueOf(_xifexpression);
        };
        return IterableExtensions.<Statement>findFirst(statements, _function);
      }
      _xblockexpression = null;
    }
    return ((Statement)_xblockexpression);
  }
  
  /**
   * Gets returns from routine
   * @param statement
   * @return list of statements
   */
  public List<EObject> getReturnsFromRoutine(final Statement statement) {
    ArrayList<EObject> list = new ArrayList<EObject>();
    List<Statement> statements = ((List<Statement>) null);
    statements = this.getListStatementWithReturns(statement);
    for (final Statement assignment : statements) {
      if ((assignment instanceof AssignmentStatement)) {
        EList<EObject> objects = this.<EObject>getAssigmnentList(((AssignmentStatement)assignment).getDeclaration());
        for (final EObject attr : objects) {
          {
            EObject obj = this.objectUtil.getBasicEObject(attr);
            if ((obj instanceof Return)) {
              list.add(obj);
            }
          }
        }
      }
    }
    return list;
  }
  
  /**
   * Gets list of statements where are returns
   * @param statement map statement
   */
  private List<Statement> getListStatementWithReturns(final Statement statement) {
    if ((statement instanceof MapStatement)) {
      final String routineName = ((MapStatement)statement).getRoutineSection().getSource().getName();
      final RoutineSource target = ((MapStatement)statement).getRoutineSection().getTarget();
      EObject obj = ((EObject) statement);
      if ((target != null)) {
        obj = ((MapStatement)statement).getRoutineSection().getTarget().getClass_();
      }
      Model model = EcoreUtil2.<Model>getContainerOfType(obj, Model.class);
      List<RoutineBlock> routineBlock = EcoreUtil2.<RoutineBlock>getAllContentsOfType(model, RoutineBlock.class);
      final Function1<RoutineBlock, Boolean> _function = (RoutineBlock it) -> {
        return Boolean.valueOf(it.getRoutineName().getName().equals(routineName));
      };
      RoutineBlock routine = IterableExtensions.<RoutineBlock>findFirst(routineBlock, _function);
      return this.getListOfReturnStatements(routine);
    }
    RoutineBlock routineBlock_1 = EcoreUtil2.<RoutineBlock>getContainerOfType(statement, RoutineBlock.class);
    return this.getListOfReturnStatements(routineBlock_1);
  }
  
  /**
   * Gets list of statements with returns
   * @param routine block routine block
   */
  public List<Statement> getListOfReturnStatements(final RoutineBlock routine) {
    if ((routine != null)) {
      final Function1<Statement, Boolean> _function = (Statement it) -> {
        boolean _xifexpression = false;
        if ((it instanceof AssignmentStatement)) {
          boolean _xifexpression_1 = false;
          EList<NewAttribute> _new = ((AssignmentStatement)it).getDeclaration().getNew();
          boolean _tripleNotEquals = (_new != null);
          if (_tripleNotEquals) {
            final Function1<NewAttribute, Boolean> _function_1 = (NewAttribute it_1) -> {
              EObject _source = it_1.getSource();
              return Boolean.valueOf((_source instanceof Return));
            };
            _xifexpression_1 = IterableExtensions.<NewAttribute>exists(((AssignmentStatement)it).getDeclaration().getNew(), _function_1);
          } else {
            boolean _xifexpression_2 = false;
            EList<UsedAttribute> _reference = ((AssignmentStatement)it).getDeclaration().getReference();
            boolean _tripleNotEquals_1 = (_reference != null);
            if (_tripleNotEquals_1) {
              final Function1<UsedAttribute, Boolean> _function_2 = (UsedAttribute it_1) -> {
                EObject _source = it_1.getSource();
                return Boolean.valueOf((_source instanceof Return));
              };
              _xifexpression_2 = IterableExtensions.<UsedAttribute>exists(((AssignmentStatement)it).getDeclaration().getReference(), _function_2);
            }
            _xifexpression_1 = _xifexpression_2;
          }
          _xifexpression = _xifexpression_1;
        }
        return Boolean.valueOf(_xifexpression);
      };
      return IterableExtensions.<Statement>toList(IterableExtensions.<Statement>filter(routine.getRoutineBlock().getStatement(), _function));
    }
    return new ArrayList<Statement>();
  }
  
  /**
   * Checks that object is in ouput section of statement (used for MapStatement / MapForwardStatement / Call statement)
   * @param outputAttributes output attributes
   * @param object object
   */
  public boolean existsObjectInOutputSection(final EList<EObject> outputAttributes, final EObject object) {
    final Function1<EObject, Boolean> _function = (EObject it) -> {
      boolean _xifexpression = false;
      if ((it instanceof Attribute)) {
        _xifexpression = this.nameProvider.getFullyQualifiedName(((Attribute)it).getType()).equals(this.nameProvider.getFullyQualifiedName(object));
      } else {
        boolean _xifexpression_1 = false;
        if ((it instanceof OutputAttribute)) {
          boolean _xblockexpression = false;
          {
            final Return returnRef = ((OutputAttribute)it).getOutputType().getReturnRef();
            final Output outputRed = ((OutputAttribute)it).getOutputType().getOutputRef();
            boolean _xifexpression_2 = false;
            if ((returnRef != null)) {
              _xifexpression_2 = this.nameProvider.getFullyQualifiedName(returnRef).equals(this.nameProvider.getFullyQualifiedName(object));
            } else {
              _xifexpression_2 = this.nameProvider.getFullyQualifiedName(outputRed).equals(this.nameProvider.getFullyQualifiedName(object));
            }
            _xblockexpression = _xifexpression_2;
          }
          _xifexpression_1 = _xblockexpression;
        }
        _xifexpression = _xifexpression_1;
      }
      return Boolean.valueOf(_xifexpression);
    };
    return IterableExtensions.<EObject>exists(outputAttributes, _function);
  }
}
