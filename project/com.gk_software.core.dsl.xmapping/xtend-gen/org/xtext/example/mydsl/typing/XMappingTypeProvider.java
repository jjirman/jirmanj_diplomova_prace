package org.xtext.example.mydsl.typing;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import java.util.Arrays;
import java.util.List;
import java.util.Stack;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.common.types.JvmOperation;
import org.eclipse.xtext.common.types.JvmTypeReference;
import org.xtext.example.mydsl.helpers.util.XMappingEObjectUtil;
import org.xtext.example.mydsl.helpers.util.XMappingStatementUtil;
import org.xtext.example.mydsl.typing.DoubleType;
import org.xtext.example.mydsl.typing.IntType;
import org.xtext.example.mydsl.typing.NotSpecified;
import org.xtext.example.mydsl.typing.NotType;
import org.xtext.example.mydsl.typing.StringType;
import org.xtext.example.mydsl.typing.StructureType;
import org.xtext.example.mydsl.typing.Type;
import org.xtext.example.mydsl.xMapping.Assignment;
import org.xtext.example.mydsl.xMapping.AssignmentStatement;
import org.xtext.example.mydsl.xMapping.AssignmentStructure;
import org.xtext.example.mydsl.xMapping.Attribute;
import org.xtext.example.mydsl.xMapping.Block;
import org.xtext.example.mydsl.xMapping.CallStatement;
import org.xtext.example.mydsl.xMapping.Input;
import org.xtext.example.mydsl.xMapping.InputAttribute;
import org.xtext.example.mydsl.xMapping.InputType;
import org.xtext.example.mydsl.xMapping.MapForwardStatement;
import org.xtext.example.mydsl.xMapping.MapStatement;
import org.xtext.example.mydsl.xMapping.Output;
import org.xtext.example.mydsl.xMapping.OutputSection;
import org.xtext.example.mydsl.xMapping.Param;
import org.xtext.example.mydsl.xMapping.Return;
import org.xtext.example.mydsl.xMapping.Routine;
import org.xtext.example.mydsl.xMapping.RoutineBlock;
import org.xtext.example.mydsl.xMapping.Statement;
import org.xtext.example.mydsl.xMapping.Variable;

/**
 * This class provide object type
 * @author Bc. Jan Jirman
 */
@SuppressWarnings("all")
public class XMappingTypeProvider {
  /**
   * statement util
   */
  @Inject
  private XMappingStatementUtil statementUtil;
  
  /**
   * object util
   */
  @Inject
  private XMappingEObjectUtil objectUtil;
  
  private static final int STACK_MAX_SIZE = 50;
  
  /**
   * singleton string type
   */
  public static final StringType stringType = new StringType();
  
  /**
   * singleton int type
   */
  public static final IntType intType = new IntType();
  
  /**
   * singleton structure type
   */
  public static final StructureType structureType = new StructureType();
  
  /**
   * singleton not type
   */
  public static final NotType notType = new NotType();
  
  /**
   * singleton double type
   */
  public static final DoubleType doubleType = new DoubleType();
  
  /**
   * singleton not specified type
   */
  public static final NotSpecified notSpecified = new NotSpecified();
  
  /**
   * statements stack
   */
  private static Stack<Statement> statements = new Stack<Statement>();
  
  /**
   * routine scope
   */
  private static boolean routineScope = false;
  
  /**
   * outer statement
   */
  private static List<MapStatement> outerStatements;
  
  /**
   * Activates routine scope
   * @param list of map statements
   */
  public void activateRoutineScope(final List<MapStatement> statements) {
    XMappingTypeProvider.routineScope = true;
    XMappingTypeProvider.outerStatements = statements;
  }
  
  /**
   * Deactivates routine scope
   */
  public void deactivateRoutineScope() {
    XMappingTypeProvider.routineScope = false;
    XMappingTypeProvider.outerStatements = null;
  }
  
  /**
   * Resets stack with outer statements
   */
  public void resetRoutineScope() {
    if (XMappingTypeProvider.routineScope) {
      for (final Statement statement : XMappingTypeProvider.outerStatements) {
        XMappingTypeProvider.statements.add(statement);
      }
    }
  }
  
  /**
   * Gets type of object
   */
  public Type typeForObject(final EObject object) {
    this.resetRoutineScope();
    EObject finalObject = this.objectUtil.getBasicEObject(object);
    final Type type = this.objectType(finalObject);
    XMappingTypeProvider.statements.clear();
    return type;
  }
  
  /**
   * Gets type for specific object - in case of error, then object is notType
   * @param object object
   * @return object's type
   */
  public Type objectType(final EObject object) {
    NotType _xblockexpression = null;
    {
      Statement statement = ((Statement) null);
      if (((object instanceof Variable) || (object instanceof Return))) {
        statement = this.statementUtil.findObjectInitialization(object);
      } else {
        if ((object instanceof Param)) {
          int _size = XMappingTypeProvider.statements.size();
          boolean _greaterThan = (_size > 0);
          if (_greaterThan) {
            statement = XMappingTypeProvider.statements.pop();
          } else {
            return XMappingTypeProvider.notSpecified;
          }
          EObject declaredObject = this.statementUtil.findDeclaredParam(statement, ((Param)object));
          if ((declaredObject != null)) {
            if ((declaredObject instanceof Attribute)) {
              return this.objectType(((Attribute)declaredObject).getType());
            } else {
              if ((declaredObject instanceof InputAttribute)) {
                final InputType obj = ((InputAttribute)declaredObject).getInputType();
                Input _inputRef = obj.getInputRef();
                boolean _tripleNotEquals = (_inputRef != null);
                if (_tripleNotEquals) {
                  return this.objectType(((InputAttribute)declaredObject).getInputType().getInputRef());
                } else {
                  Param _paramRef = obj.getParamRef();
                  boolean _tripleNotEquals_1 = (_paramRef != null);
                  if (_tripleNotEquals_1) {
                    return this.objectType(((InputAttribute)declaredObject).getInputType().getParamRef());
                  }
                }
              } else {
                if ((declaredObject instanceof Param)) {
                  return this.objectType(declaredObject);
                }
              }
            }
          }
        } else {
          if (((object instanceof Input) || (object instanceof Output))) {
            return XMappingTypeProvider.structureType;
          }
        }
      }
      if ((statement != null)) {
        return this.typeFor(statement, object);
      }
      _xblockexpression = XMappingTypeProvider.notType;
    }
    return _xblockexpression;
  }
  
  /**
   * Finds type for object in MapStatement
   * @param statement map statement
   * @param object object
   * @return object's type
   */
  protected Type _typeFor(final MapStatement statement, final EObject object) {
    NotType _xblockexpression = null;
    {
      XMappingTypeProvider.statements.push(statement);
      int _size = XMappingTypeProvider.statements.size();
      boolean _greaterThan = (_size > XMappingTypeProvider.STACK_MAX_SIZE);
      if (_greaterThan) {
        return XMappingTypeProvider.notType;
      }
      OutputSection _outputSection = statement.getOutputSection();
      boolean _tripleNotEquals = (_outputSection != null);
      if (_tripleNotEquals) {
        EList<EObject> outputAttributes = statement.getOutputSection().getOutputAttributes();
        final int outputIndex = this.objectUtil.<EObject>getObjectIndexByObject(outputAttributes, object);
        if ((outputIndex != (-1))) {
          final Routine routine = statement.getRoutineSection().getSource();
          final RoutineBlock routineSection = EcoreUtil2.<RoutineBlock>getContainerOfType(routine, RoutineBlock.class);
          final Block block = routineSection.getRoutineBlock();
          final String name = ("Return." + Integer.valueOf(outputIndex));
          if ((block != null)) {
            final EObject returnObject = this.objectUtil.getObjectByName(block, name);
            if ((returnObject != null)) {
              return this.objectType(returnObject);
            }
          }
        }
      }
      _xblockexpression = XMappingTypeProvider.notType;
    }
    return _xblockexpression;
  }
  
  /**
   * Finds type for object in MapForwardStatement
   * @param statement map forward statement
   * @param object object
   * @return object's type
   */
  protected Type _typeFor(final MapForwardStatement statement, final EObject object) {
    NotType _xblockexpression = null;
    {
      EList<EObject> outputAttributes = statement.getOutputSection().getOutputAttributes();
      final int outputIndex = this.objectUtil.<EObject>getObjectIndexByObject(outputAttributes, object);
      if ((outputIndex != (-1))) {
        EObject inputAttribute = statement.getInputSection().getInputAttributes().get(outputIndex);
        if ((inputAttribute instanceof InputAttribute)) {
          InputType instance = ((InputAttribute)inputAttribute).getInputType();
          Input _inputRef = instance.getInputRef();
          boolean _tripleNotEquals = (_inputRef != null);
          if (_tripleNotEquals) {
            return this.objectType(instance.getInputRef());
          } else {
            Param _paramRef = instance.getParamRef();
            boolean _tripleNotEquals_1 = (_paramRef != null);
            if (_tripleNotEquals_1) {
              return this.objectType(instance.getParamRef());
            }
          }
        } else {
          if ((inputAttribute instanceof Attribute)) {
            return this.objectType(((Attribute)inputAttribute).getType());
          }
        }
      }
      _xblockexpression = XMappingTypeProvider.notType;
    }
    return _xblockexpression;
  }
  
  /**
   * Finds type for object in CallStatement
   * @param statement call statement
   * @param object object
   * @return object's type
   */
  protected Type _typeFor(final CallStatement statement, final EObject object) {
    JvmOperation javaMethod = statement.getJava_method();
    if ((javaMethod != null)) {
      final JvmTypeReference methodReturnType = javaMethod.getReturnType();
      return this.methodType(methodReturnType.getType().getIdentifier());
    }
    return XMappingTypeProvider.notType;
  }
  
  public Type methodType(final String identifier) {
    NotSpecified _xblockexpression = null;
    {
      if (identifier != null) {
        switch (identifier) {
          case "io.OutputDocument":
            return XMappingTypeProvider.structureType;
          case "java.lang.String":
            return XMappingTypeProvider.stringType;
          case "boolean":
            return XMappingTypeProvider.stringType;
          case "int":
            return XMappingTypeProvider.intType;
          case "double":
            return XMappingTypeProvider.doubleType;
          case "void":
            return XMappingTypeProvider.notType;
        }
      }
      _xblockexpression = XMappingTypeProvider.notSpecified;
    }
    return _xblockexpression;
  }
  
  /**
   * Finds type for object in AssignmentStatement
   * @param statement assignment statement
   * @param object object
   * @return object's type
   */
  protected Type _typeFor(final AssignmentStatement statement, final EObject object) {
    Type _xblockexpression = null;
    {
      final Assignment value = statement.getInitialization().getAssignment();
      Type _switchResult = null;
      boolean _matched = false;
      if (value instanceof AssignmentStructure) {
        _matched=true;
        _switchResult = XMappingTypeProvider.structureType;
      }
      if (!_matched) {
        if (value instanceof org.xtext.example.mydsl.xMapping.IntType) {
          _matched=true;
          _switchResult = XMappingTypeProvider.intType;
        }
      }
      if (!_matched) {
        if (value instanceof org.xtext.example.mydsl.xMapping.StringType) {
          _matched=true;
          _switchResult = XMappingTypeProvider.stringType;
        }
      }
      if (!_matched) {
        if (value instanceof org.xtext.example.mydsl.xMapping.DoubleType) {
          _matched=true;
          _switchResult = XMappingTypeProvider.doubleType;
        }
      }
      _xblockexpression = _switchResult;
    }
    return _xblockexpression;
  }
  
  /**
   * Gets non-primitive type
   * @param type
   */
  public String getNonPrimitiveType(final Type type) {
    boolean _matched = false;
    if (Objects.equal(type, XMappingTypeProvider.structureType)) {
      _matched=true;
      return "DocumentOutput";
    }
    if (!_matched) {
      if (Objects.equal(type, XMappingTypeProvider.intType)) {
        _matched=true;
        return "Integer";
      }
    }
    if (!_matched) {
      if (Objects.equal(type, XMappingTypeProvider.stringType)) {
        _matched=true;
        return "String";
      }
    }
    if (!_matched) {
      if (Objects.equal(type, XMappingTypeProvider.doubleType)) {
        _matched=true;
        return "Double";
      }
    }
    return "Object";
  }
  
  public Type typeFor(final Statement statement, final EObject object) {
    if (statement instanceof AssignmentStatement) {
      return _typeFor((AssignmentStatement)statement, object);
    } else if (statement instanceof CallStatement) {
      return _typeFor((CallStatement)statement, object);
    } else if (statement instanceof MapForwardStatement) {
      return _typeFor((MapForwardStatement)statement, object);
    } else if (statement instanceof MapStatement) {
      return _typeFor((MapStatement)statement, object);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(statement, object).toString());
    }
  }
}
