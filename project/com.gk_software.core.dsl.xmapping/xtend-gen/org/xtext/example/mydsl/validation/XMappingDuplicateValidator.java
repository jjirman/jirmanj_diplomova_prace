package org.xtext.example.mydsl.validation;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import java.util.Arrays;
import java.util.List;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.validation.Check;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.xtext.example.mydsl.helpers.util.XMappingEObjectUtil;
import org.xtext.example.mydsl.validation.AbstractXMappingValidator;
import org.xtext.example.mydsl.xMapping.AssignmentStatement;
import org.xtext.example.mydsl.xMapping.Block;
import org.xtext.example.mydsl.xMapping.Input;
import org.xtext.example.mydsl.xMapping.MappingClass;
import org.xtext.example.mydsl.xMapping.Model;
import org.xtext.example.mydsl.xMapping.NewAttribute;
import org.xtext.example.mydsl.xMapping.Output;
import org.xtext.example.mydsl.xMapping.Return;
import org.xtext.example.mydsl.xMapping.Routine;
import org.xtext.example.mydsl.xMapping.Variable;
import org.xtext.example.mydsl.xMapping.XMappingPackage;

/**
 * Class which checks duplicates
 * @author Bc. Jan Jirman
 */
@SuppressWarnings("all")
public class XMappingDuplicateValidator extends AbstractXMappingValidator {
  /**
   * name provider
   */
  @Inject
  private IQualifiedNameProvider nameProvider;
  
  /**
   * object util
   */
  @Inject
  private XMappingEObjectUtil objectUtil;
  
  /**
   * constant of number of same objects
   */
  private static final int numberOfSameObjects = 1;
  
  /**
   * Check mapping class and package duplicate
   * @param mappingClass mapping class
   */
  @Check
  protected void _checkDuplicate(final MappingClass mappingClass) {
    EObject _eContainer = mappingClass.eContainer();
    boolean _equals = mappingClass.getName().equals(((Model) _eContainer).getName());
    if (_equals) {
      final String message = "Mapping class has same name as package name.";
      this.error(message, null, XMappingPackage.MODEL__NAME);
    }
  }
  
  /**
   * Check input duplicates
   * @param input input
   */
  @Check
  protected void _checkDuplicate(final Input input) {
    EObject xsdImport = input.eContainer().eContainer();
    List<Input> _allContentsOfType = EcoreUtil2.<Input>getAllContentsOfType(xsdImport, Input.class);
    Iterable<Input> inputs = ((Iterable<Input>) _allContentsOfType);
    final Function1<Input, Boolean> _function = (Input it) -> {
      boolean _xblockexpression = false;
      {
        EObject basicObject = this.objectUtil.getBasicEObject(it);
        _xblockexpression = ((Input) basicObject).getName().equals(input.getName());
      }
      return Boolean.valueOf(_xblockexpression);
    };
    inputs = IterableExtensions.<Input>filter(inputs, _function);
    int size = this.<Input>getSizeOfIterable(inputs);
    this.<Input>showError(size, input);
  }
  
  /**
   * Check output duplicates
   * @param output output
   */
  @Check
  protected void _checkDuplicate(final Output output) {
    EObject xsdImport = output.eContainer().eContainer();
    List<Output> _allContentsOfType = EcoreUtil2.<Output>getAllContentsOfType(xsdImport, Output.class);
    Iterable<Output> outputs = ((Iterable<Output>) _allContentsOfType);
    final Function1<Output, Boolean> _function = (Output it) -> {
      boolean _xblockexpression = false;
      {
        EObject basicObject = this.objectUtil.getBasicEObject(it);
        _xblockexpression = ((Output) basicObject).getName().equals(output.getName());
      }
      return Boolean.valueOf(_xblockexpression);
    };
    outputs = IterableExtensions.<Output>filter(outputs, _function);
    int size = this.<Output>getSizeOfIterable(outputs);
    this.<Output>showError(size, output);
  }
  
  /**
   * Checks variable duplicates
   * @param variable variable
   */
  @Check
  protected void _checkDuplicate(final Variable variable) {
    this.checkduplicateObject(variable);
  }
  
  /**
   * Checks return duplicates
   * @param ^return return
   */
  @Check
  protected void _checkDuplicate(final Return return_) {
    this.checkduplicateObject(return_);
  }
  
  /**
   * Check object duplicates
   * @param object
   */
  public void checkduplicateObject(final EObject object) {
    Class<? extends EObject> clazz = object.getClass();
    Iterable<AssignmentStatement> declaration = this.<EObject>checkDeclaredObject(object, clazz);
    int size = this.<AssignmentStatement>getSizeOfIterable(declaration);
    this.<EObject>showError(size, object);
  }
  
  /**
   * Checks routine duplicates
   */
  @Check
  protected void _checkDuplicate(final Routine routine) {
    final MappingClass mappingClass = EcoreUtil2.<MappingClass>getContainerOfType(routine, MappingClass.class);
    List<Routine> routines = EcoreUtil2.<Routine>getAllContentsOfType(mappingClass, Routine.class);
    final Function1<Routine, Boolean> _function = (Routine it) -> {
      String _name = it.getName();
      String _name_1 = routine.getName();
      return Boolean.valueOf(Objects.equal(_name, _name_1));
    };
    final Iterable<Routine> sameRoutines = IterableExtensions.<Routine>filter(routines, _function);
    int _size = IterableExtensions.size(sameRoutines);
    boolean _greaterThan = (_size > XMappingDuplicateValidator.numberOfSameObjects);
    if (_greaterThan) {
      String _name = routine.getName();
      final String message = (_name + " already exists.");
      this.error(message, null, XMappingPackage.ROUTINE__NAME);
    }
  }
  
  /**
   * Shows error
   * @param <T> extends EObject
   * @param size size
   * @param object object
   */
  private <T extends EObject> void showError(final int size, final T object) {
    final int minSize = 0;
    QualifiedName qn = this.nameProvider.getFullyQualifiedName(object);
    String name = qn.getLastSegment();
    if ((size == minSize)) {
      final String message = (name + " is not declared.");
      if ((object instanceof Return)) {
        this.error(message, null, XMappingPackage.RETURN__NAME);
      } else {
        if ((object instanceof Variable)) {
          this.error(message, null, XMappingPackage.VARIABLE__NAME);
        }
      }
    } else {
      if ((size > XMappingDuplicateValidator.numberOfSameObjects)) {
        final String message_1 = (name + " already exists.");
        this.error(message_1, null, XMappingPackage.DECLARATION__NEW);
      }
    }
  }
  
  /**
   * Size of "Iterable object"
   * @param iterable iterable object
   * @return number
   */
  private <T extends EObject> int getSizeOfIterable(final Iterable<T> iterable) {
    int _xblockexpression = (int) 0;
    {
      if ((iterable == null)) {
        return 0;
      }
      _xblockexpression = ((Object[])Conversions.unwrapArray(iterable, Object.class)).length;
    }
    return _xblockexpression;
  }
  
  /**
   * Checks declared objects
   * @param object
   * @param clazz class
   */
  private <T extends EObject> Iterable<AssignmentStatement> checkDeclaredObject(final T object, final Class clazz) {
    final Block block = EcoreUtil2.<Block>getContainerOfType(object, Block.class);
    final Function1<AssignmentStatement, Boolean> _function = (AssignmentStatement it) -> {
      return Boolean.valueOf(((it.getDeclaration().getNew() != null) && IterableExtensions.<NewAttribute>exists(it.getDeclaration().getNew(), ((Function1<NewAttribute, Boolean>) (NewAttribute it_1) -> {
        return Boolean.valueOf(((it_1.getSource() != null) && this.nameProvider.getFullyQualifiedName(it_1.getSource()).equals(this.nameProvider.getFullyQualifiedName(object))));
      }))));
    };
    return IterableExtensions.<AssignmentStatement>filter(EcoreUtil2.<AssignmentStatement>getAllContentsOfType(block, AssignmentStatement.class), _function);
  }
  
  public void checkDuplicate(final EObject input) {
    if (input instanceof Input) {
      _checkDuplicate((Input)input);
      return;
    } else if (input instanceof MappingClass) {
      _checkDuplicate((MappingClass)input);
      return;
    } else if (input instanceof Output) {
      _checkDuplicate((Output)input);
      return;
    } else if (input instanceof Return) {
      _checkDuplicate((Return)input);
      return;
    } else if (input instanceof Routine) {
      _checkDuplicate((Routine)input);
      return;
    } else if (input instanceof Variable) {
      _checkDuplicate((Variable)input);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(input).toString());
    }
  }
}
