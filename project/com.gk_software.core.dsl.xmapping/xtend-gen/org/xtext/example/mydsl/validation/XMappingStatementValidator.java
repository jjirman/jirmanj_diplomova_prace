package org.xtext.example.mydsl.validation;

import com.google.common.base.Objects;
import com.google.inject.Inject;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Consumer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.EcoreUtil2;
import org.eclipse.xtext.common.types.JvmType;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.validation.Check;
import org.eclipse.xtext.xbase.lib.Functions.Function1;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.xtext.example.mydsl.helpers.util.XMappingEObjectUtil;
import org.xtext.example.mydsl.helpers.util.XMappingStatementUtil;
import org.xtext.example.mydsl.typing.NotSpecified;
import org.xtext.example.mydsl.typing.NotType;
import org.xtext.example.mydsl.typing.StructureType;
import org.xtext.example.mydsl.typing.Type;
import org.xtext.example.mydsl.typing.XMappingTypeProvider;
import org.xtext.example.mydsl.validation.AbstractXMappingValidator;
import org.xtext.example.mydsl.xMapping.AssignmentStatement;
import org.xtext.example.mydsl.xMapping.Block;
import org.xtext.example.mydsl.xMapping.CallStatement;
import org.xtext.example.mydsl.xMapping.Declaration;
import org.xtext.example.mydsl.xMapping.Initialization;
import org.xtext.example.mydsl.xMapping.Input;
import org.xtext.example.mydsl.xMapping.InputAttribute;
import org.xtext.example.mydsl.xMapping.InputSection;
import org.xtext.example.mydsl.xMapping.MapForwardStatement;
import org.xtext.example.mydsl.xMapping.MapStatement;
import org.xtext.example.mydsl.xMapping.Model;
import org.xtext.example.mydsl.xMapping.Output;
import org.xtext.example.mydsl.xMapping.OutputAttribute;
import org.xtext.example.mydsl.xMapping.OutputSection;
import org.xtext.example.mydsl.xMapping.ParamSection;
import org.xtext.example.mydsl.xMapping.Return;
import org.xtext.example.mydsl.xMapping.Routine;
import org.xtext.example.mydsl.xMapping.RoutineBlock;
import org.xtext.example.mydsl.xMapping.RoutineSource;
import org.xtext.example.mydsl.xMapping.Statement;
import org.xtext.example.mydsl.xMapping.UsedAttribute;
import org.xtext.example.mydsl.xMapping.XMappingPackage;

/**
 * Class which checks statements
 * @author Bc. Jan Jirman
 */
@SuppressWarnings("all")
public class XMappingStatementValidator extends AbstractXMappingValidator {
  /**
   * statement util
   */
  @Inject
  private XMappingStatementUtil statementUtil;
  
  /**
   * type provider
   */
  @Inject
  private XMappingTypeProvider typeProvider;
  
  /**
   * name provider
   */
  @Inject
  private IQualifiedNameProvider nameProvider;
  
  /**
   * object util
   */
  @Inject
  private XMappingEObjectUtil objectUtil;
  
  /**
   * Checks assignment statement
   * @param statement assignment statement
   */
  @Check
  protected void _checkStatement(final AssignmentStatement statement) {
    this.checkOrderedReturns(statement);
    Declaration _target = statement.getDeclaration().getTarget();
    boolean _tripleNotEquals = (_target != null);
    if (_tripleNotEquals) {
      EList<UsedAttribute> referenceDeclaration = statement.getDeclaration().getTarget().getReference();
      Initialization initialization = statement.getInitialization();
      if ((initialization != null)) {
        this.checkReferenceType(referenceDeclaration, statement);
      }
    }
  }
  
  /**
   * Checks map forward statement
   * @param statement map forward statement
   */
  @Check
  protected void _checkStatement(final MapForwardStatement statement) {
    this.checkInputXpath(statement.getInputSection(), XMappingPackage.MAP_FORWARD_STATEMENT__INPUT_SECTION);
    this.checkOutputXpath(statement.getOutputSection(), XMappingPackage.MAP_FORWARD_STATEMENT__OUTPUT_SECTION);
    this.checkInputSectionTypes(statement.getInputSection());
    this.checkOutputSectionTypes(statement.getOutputSection());
    this.checkInputOutputNumber(statement.getInputSection(), statement.getOutputSection());
    this.compareInputOutputTypes(statement.getInputSection(), statement.getOutputSection());
  }
  
  /**
   * Checks map statement
   * @param statement map statement
   */
  @Check
  protected void _checkStatement(final MapStatement statement) {
    boolean _checkRecursion = this.checkRecursion(statement);
    if (_checkRecursion) {
      this.checkInputXpath(statement.getInputSection(), XMappingPackage.MAP_STATEMENT__INPUT_SECTION);
      this.checkOutputXpath(statement.getOutputSection(), XMappingPackage.MAP_STATEMENT__OUTPUT_SECTION);
      this.checkParamsWithInput(statement);
      this.checkReturnWithOutput(statement);
      this.checkRoutineReturns(statement);
      this.checkRoutineCall(statement);
      this.checkInputSectionTypes(statement.getInputSection());
      this.checkOutputSectionTypes(statement.getOutputSection());
    }
  }
  
  /**
   * Checks call statement
   * @param statement call statement
   */
  @Check
  protected void _checkStatement(final CallStatement statement) {
    CallStatement _target = statement.getTarget();
    boolean _tripleNotEquals = (_target != null);
    if (_tripleNotEquals) {
      this.checkInputSectionTypes(statement.getTarget().getInputSection());
      this.checkInputXpath(statement.getTarget().getInputSection(), XMappingPackage.CALL_STATEMENT__INPUT_SECTION);
    }
    this.checkOutputSectionTypes(statement.getOutputSection());
    this.checkCallReturnOutput(statement);
    this.checkOutputXpath(statement.getOutputSection(), XMappingPackage.CALL_STATEMENT__OUTPUT_SECTION);
    this.checkReturnTypeWithOutputType(statement);
  }
  
  /**
   * Checks input section type
   * @param input section
   */
  private void checkInputSectionTypes(final InputSection inputSection) {
    if ((inputSection != null)) {
      final Consumer<EObject> _function = (EObject it) -> {
        EObject object = this.objectUtil.getBasicEObject(it);
        if ((object != null)) {
          Type type = this.typeProvider.typeForObject(object);
          QualifiedName name = this.nameProvider.getFullyQualifiedName(object);
          String objectName = name.getLastSegment();
          if ((type instanceof NotType)) {
            this.error((objectName + " is not initialized."), null, XMappingPackage.MAP_FORWARD_STATEMENT__INPUT_SECTION);
          } else {
            if ((type instanceof NotSpecified)) {
              this.info((objectName + " is not exactly specified - can be any type."), null, XMappingPackage.MAP_FORWARD_STATEMENT__INPUT_SECTION);
            }
          }
        }
      };
      inputSection.getInputAttributes().forEach(_function);
    }
  }
  
  /**
   * Checks output section type
   * @param output section
   */
  private void checkOutputSectionTypes(final OutputSection outputSection) {
    if ((outputSection != null)) {
      final Consumer<EObject> _function = (EObject it) -> {
        EObject object = this.objectUtil.getBasicEObject(it);
        if ((object != null)) {
          Type type = this.typeProvider.typeForObject(object);
          QualifiedName name = this.nameProvider.getFullyQualifiedName(object);
          String objectName = name.getLastSegment();
          if (((type instanceof StructureType) && (!(object instanceof Output)))) {
            final Block block = EcoreUtil2.<Block>getContainerOfType(object, Block.class);
            final Statement statement = this.statementUtil.findFirstInit(block, object);
            EObject _eContainer = outputSection.eContainer();
            boolean _tripleEquals = (statement == _eContainer);
            if (_tripleEquals) {
              this.error((objectName + " is not initialized."), null, XMappingPackage.MAP_FORWARD_STATEMENT__OUTPUT_SECTION);
            }
          } else {
            if (((type instanceof NotSpecified) && (this.objectUtil.getObjectXPath(it) != null))) {
              final Block block_1 = EcoreUtil2.<Block>getContainerOfType(object, Block.class);
              final Statement statement_1 = this.statementUtil.findFirstInit(block_1, object);
              EObject _eContainer_1 = outputSection.eContainer();
              boolean _tripleEquals_1 = (statement_1 == _eContainer_1);
              if (_tripleEquals_1) {
                this.error((objectName + " is not initialized."), null, XMappingPackage.MAP_FORWARD_STATEMENT__OUTPUT_SECTION);
              }
            } else {
              if ((type instanceof NotSpecified)) {
                this.info((objectName + " is not exactly specified - can be any type."), null, XMappingPackage.MAP_FORWARD_STATEMENT__INPUT_SECTION);
              }
            }
          }
        }
      };
      outputSection.getOutputAttributes().forEach(_function);
    }
  }
  
  /**
   * Checks input xPath
   * @param inputSection input section
   */
  private void checkInputXpath(final InputSection inputSection, final int reference) {
    if ((inputSection != null)) {
      int indexCounter = 0;
      EList<EObject> _inputAttributes = inputSection.getInputAttributes();
      for (final EObject inputAttr : _inputAttributes) {
        {
          Type inputType = this.typeProvider.typeForObject(inputAttr);
          this.xpathRules(inputType, inputAttr, reference);
          indexCounter++;
        }
      }
    }
  }
  
  /**
   * Checks output xPath
   * @param outputSection output section
   */
  private void checkOutputXpath(final OutputSection outputSection, final int reference) {
    if ((outputSection != null)) {
      int indexCounter = 0;
      EList<EObject> _outputAttributes = outputSection.getOutputAttributes();
      for (final EObject outputAttr : _outputAttributes) {
        {
          Type outputType = this.typeProvider.typeForObject(outputAttr);
          this.xpathRules(outputType, outputAttr, reference);
          indexCounter++;
        }
      }
    }
  }
  
  /**
   * Map Forward rules
   * @param inputAttribute input attribute
   * @param outputAttribute output attribute
   * @param isMapForward is map forward statement
   */
  private void mapForwardRules(final EObject inputAttribute, final EObject outputAttribute, final boolean isMapForward) {
    EObject input = this.objectUtil.getBasicEObject(inputAttribute);
    Type inputType = this.typeProvider.typeForObject(input);
    EObject output = this.objectUtil.getBasicEObject(outputAttribute);
    Type outputType = this.typeProvider.typeForObject(output);
    boolean _notEquals = (!Objects.equal(inputType, outputType));
    if (_notEquals) {
      if ((!(inputType instanceof NotSpecified))) {
        if ((!(outputType instanceof NotSpecified))) {
          if ((!(outputType instanceof StructureType))) {
            EObject basicObject = this.objectUtil.getBasicEObject(inputAttribute);
            String inputName = this.nameProvider.getFullyQualifiedName(basicObject).getLastSegment();
            basicObject = this.objectUtil.getBasicEObject(outputAttribute);
            String outputName = this.nameProvider.getFullyQualifiedName(basicObject).getLastSegment();
            int featureId = XMappingPackage.MAP_STATEMENT;
            if (isMapForward) {
              featureId = XMappingPackage.MAP_FORWARD_STATEMENT;
            }
            this.error((((((((inputName + " (") + inputType) + ") and ") + outputName) + " (") + outputType) + ") have different types"), null, featureId);
          }
        }
      }
    }
  }
  
  /**
   * Xpath rules of object
   * @param type type
   * @param object object
   * @param reference reference
   */
  private void xpathRules(final Type type, final EObject object, final int reference) {
    String xpath = this.objectUtil.getObjectXPath(object);
    EObject basicObject = this.objectUtil.getBasicEObject(object);
    QualifiedName qn = this.nameProvider.getFullyQualifiedName(basicObject);
    String name = qn.getLastSegment();
    if ((!(type instanceof StructureType))) {
      if (((xpath != null) && (!(type instanceof NotSpecified)))) {
        this.warning((((name + " has ") + type) + " - XPath will be ignored."), null, reference);
      }
    } else {
      if (((xpath == null) && (!(type instanceof NotSpecified)))) {
        this.error((((name + " has ") + type) + " - required XPath."), null, reference);
      }
    }
  }
  
  private void checkOrderedReturns(final AssignmentStatement statement) {
    final RoutineBlock routineBlock = EcoreUtil2.<RoutineBlock>getContainerOfType(statement, RoutineBlock.class);
    List<EObject> returns = this.statementUtil.getReturnsFromRoutine(statement);
    Comparator<EObject> comparator = new Comparator<EObject>() {
      @Override
      public int compare(final EObject o1, final EObject o2) {
        int _xblockexpression = (int) 0;
        {
          EObject _basicEObject = XMappingStatementValidator.this.objectUtil.getBasicEObject(o1);
          final Return return1 = ((Return) _basicEObject);
          EObject _basicEObject_1 = XMappingStatementValidator.this.objectUtil.getBasicEObject(o2);
          final Return return2 = ((Return) _basicEObject_1);
          _xblockexpression = return1.getName().compareTo(return2.getName());
        }
        return _xblockexpression;
      }
    };
    if ((routineBlock != null)) {
      returns = IterableExtensions.<EObject>sortWith(returns, comparator);
      final String returnName = "Return.";
      for (int i = 0; (i < returns.size()); i++) {
        {
          final String returnFromList = this.nameProvider.getFullyQualifiedName(returns.get(i)).getLastSegment();
          final String expectedReturn = (returnName + Integer.valueOf(i));
          boolean _equals = expectedReturn.equals(returnFromList);
          boolean _not = (!_equals);
          if (_not) {
            final String message = ((("Can not be initialized as " + returnFromList) + " - required return name: ") + expectedReturn);
            this.error(message, null, XMappingPackage.ASSIGNMENT_STATEMENT__DECLARATION);
            return;
          }
        }
      }
    }
  }
  
  /**
   * Checks routine returns
   */
  private void checkRoutineReturns(final MapStatement statement) {
    OutputSection outputSection = statement.getOutputSection();
    EList<EObject> outputAttributes = ((EList<EObject>) null);
    if ((outputSection != null)) {
      outputAttributes = outputSection.getOutputAttributes();
    }
    final String routineName = statement.getRoutineSection().getSource().getName();
    final RoutineSource target = statement.getRoutineSection().getTarget();
    EObject obj = ((EObject) statement);
    if ((target != null)) {
      obj = statement.getRoutineSection().getTarget().getClass_();
    }
    Model model = EcoreUtil2.<Model>getContainerOfType(obj, Model.class);
    List<RoutineBlock> routineBlock = EcoreUtil2.<RoutineBlock>getAllContentsOfType(model, RoutineBlock.class);
    final Function1<RoutineBlock, Boolean> _function = (RoutineBlock it) -> {
      return Boolean.valueOf(it.getRoutineName().getName().equals(routineName));
    };
    RoutineBlock routine = IterableExtensions.<RoutineBlock>findFirst(routineBlock, _function);
    if ((routine != null)) {
      final List<EObject> returns = this.statementUtil.getReturnsFromRoutine(statement);
      int counter = (-1);
      final boolean isMapForward = false;
      for (final EObject object : returns) {
        {
          EObject basicObject = this.objectUtil.getBasicEObject(object);
          if ((basicObject instanceof Return)) {
            counter++;
            String name = this.nameProvider.getFullyQualifiedName(basicObject).getLastSegment();
            final Statement initStatement = this.statementUtil.findFirstInit(routine.getRoutineBlock(), basicObject);
            if ((initStatement == null)) {
              this.error((((name + " from ") + routineName) + " is not initialized."), null, XMappingPackage.ASSIGNMENT_STATEMENT__DECLARATION);
              return;
            }
            if ((outputAttributes != null)) {
              this.mapForwardRules(basicObject, outputAttributes.get(counter), isMapForward);
            }
          }
        }
      }
    }
  }
  
  /**
   * Compares output and input types - FOR MAP FORWARD STATEMENT
   */
  private void compareInputOutputTypes(final InputSection inputSection, final OutputSection outputSection) {
    if (((inputSection != null) && (outputSection != null))) {
      final boolean isMapForward = true;
      int indexCounter = 0;
      EList<EObject> _inputAttributes = inputSection.getInputAttributes();
      for (final EObject inputAttr : _inputAttributes) {
        {
          EObject _get = outputSection.getOutputAttributes().get(indexCounter);
          EObject outputAttr = ((EObject) _get);
          this.mapForwardRules(inputAttr, outputAttr, isMapForward);
          indexCounter++;
        }
      }
    }
  }
  
  /**
   * checks return type and output type - are they same?
   * @statement call statement
   */
  public void checkReturnTypeWithOutputType(final CallStatement statement) {
    Type type = this.typeProvider.typeFor(statement, null);
    if (((type instanceof NotType) && (statement.getJavaClass() == null))) {
      this.error("Method can not have \"void\" return type.", null, XMappingPackage.CALL_STATEMENT);
    } else {
      if (((type instanceof NotSpecified) && (statement.getJavaClass() == null))) {
        this.error("Method does not have correct return type - unknown type.", null, XMappingPackage.CALL_STATEMENT);
      } else {
        OutputSection _outputSection = statement.getOutputSection();
        boolean _tripleNotEquals = (_outputSection != null);
        if (_tripleNotEquals) {
          EList<EObject> _outputAttributes = statement.getOutputSection().getOutputAttributes();
          for (final EObject outputAttr : _outputAttributes) {
            {
              Type outputType = this.typeProvider.typeForObject(outputAttr);
              EObject basicObject = this.objectUtil.getBasicEObject(outputAttr);
              String outputName = this.nameProvider.getFullyQualifiedName(basicObject).getLastSegment();
              if (((!Objects.equal(outputType, type)) && (!(outputType instanceof NotSpecified)))) {
                this.error((((("Bad types - " + outputName) + " has to be ") + type) + "."), null, XMappingPackage.CALL_STATEMENT);
              }
            }
          }
        }
      }
    }
  }
  
  /**
   * Checks number of Outputs for 1 Return from Java method
   * @param statement call statement
   */
  public void checkCallReturnOutput(final CallStatement statement) {
    final OutputSection outputSection = statement.getOutputSection();
    if ((outputSection != null)) {
      final EList<EObject> outputAttributes = outputSection.getOutputAttributes();
      if ((outputAttributes != null)) {
        final int size = outputAttributes.size();
        if ((size == 1)) {
          return;
        }
      }
    }
    JvmType _javaClass = statement.getJavaClass();
    boolean _tripleEquals = (_javaClass == null);
    if (_tripleEquals) {
      this.error("Method requires only 1 output.", null, XMappingPackage.CALL_STATEMENT__JAVA_METHOD);
    }
  }
  
  /**
   * Checks number of returns with number of outputs
   * @param statement map statement
   */
  public void checkReturnWithOutput(final MapStatement statement) {
    OutputSection _outputSection = statement.getOutputSection();
    boolean _tripleNotEquals = (_outputSection != null);
    if (_tripleNotEquals) {
      final int outputSize = statement.getOutputSection().getOutputAttributes().size();
      final RoutineSource target = statement.getRoutineSection().getTarget();
      EObject object = ((EObject) statement);
      if ((target != null)) {
        object = statement.getRoutineSection().getTarget().getClass_();
      }
      final String routineName = statement.getRoutineSection().getSource().getName();
      int returnSize = (-1);
      Model model = EcoreUtil2.<Model>getContainerOfType(object, Model.class);
      List<RoutineBlock> routineBlock = EcoreUtil2.<RoutineBlock>getAllContentsOfType(model, RoutineBlock.class);
      final Function1<RoutineBlock, Boolean> _function = (RoutineBlock it) -> {
        return Boolean.valueOf(it.getRoutineName().getName().equals(routineName));
      };
      RoutineBlock routine = IterableExtensions.<RoutineBlock>findFirst(routineBlock, _function);
      if ((routine != null)) {
        final List<EObject> returns = this.statementUtil.getReturnsFromRoutine(statement);
        returnSize = returns.size();
      }
      if ((outputSize != returnSize)) {
        this.error("Number of output objects does not match with Return count.", null, XMappingPackage.MAP_STATEMENT__INPUT_SECTION);
      }
    }
  }
  
  /**
   * Checks number of params with number of inputs
   * @param statement map statement
   */
  public void checkParamsWithInput(final MapStatement statement) {
    InputSection _inputSection = statement.getInputSection();
    boolean _tripleNotEquals = (_inputSection != null);
    if (_tripleNotEquals) {
      final int inputSize = statement.getInputSection().getInputAttributes().size();
      final RoutineSource target = statement.getRoutineSection().getTarget();
      EObject object = ((EObject) statement);
      if ((target != null)) {
        object = statement.getRoutineSection().getTarget().getClass_();
      }
      final String routineName = statement.getRoutineSection().getSource().getName();
      int paramsSize = 0;
      Model model = EcoreUtil2.<Model>getContainerOfType(object, Model.class);
      List<RoutineBlock> routineBlocks = EcoreUtil2.<RoutineBlock>getAllContentsOfType(model, RoutineBlock.class);
      final Function1<RoutineBlock, Boolean> _function = (RoutineBlock it) -> {
        return Boolean.valueOf(it.getRoutineName().getName().equals(routineName));
      };
      RoutineBlock specificRoutine = IterableExtensions.<RoutineBlock>findFirst(routineBlocks, _function);
      if ((specificRoutine != null)) {
        final ParamSection params = specificRoutine.getParams();
        if ((params != null)) {
          paramsSize = params.getParamSection().size();
        }
      }
      if ((inputSize != paramsSize)) {
        this.error("Number of input objects does not match with params size.", null, XMappingPackage.MAP_STATEMENT__INPUT_SECTION);
      }
    }
  }
  
  /**
   * Checks number of input attributes with output attributes
   * @param inputSection input section
   * @param outputSection output section
   */
  public void checkInputOutputNumber(final InputSection inputSection, final OutputSection outputSection) {
    if (((inputSection != null) && (outputSection != null))) {
      int inputCount = inputSection.getInputAttributes().size();
      int outputCount = outputSection.getOutputAttributes().size();
      if ((inputCount != outputCount)) {
        this.error("Number of input attributes must be the same as output attributes", null, XMappingPackage.MAP_FORWARD_STATEMENT);
      }
    }
  }
  
  /**
   * Checks routine call
   * @param statement map statement
   */
  public void checkRoutineCall(final MapStatement statement) {
    Routine routine = statement.getRoutineSection().getSource();
    RoutineSource target = statement.getRoutineSection().getTarget();
    if ((target != null)) {
      EObject _eContainer = routine.eContainer();
      List<InputAttribute> inputMacroAttributes = EcoreUtil2.<InputAttribute>getAllContentsOfType(((RoutineBlock) _eContainer).getRoutineBlock(), InputAttribute.class);
      EObject _eContainer_1 = routine.eContainer();
      List<OutputAttribute> outputMacroAttributes = EcoreUtil2.<OutputAttribute>getAllContentsOfType(((RoutineBlock) _eContainer_1).getRoutineBlock(), OutputAttribute.class);
      for (final InputAttribute attribute : inputMacroAttributes) {
        {
          final EObject object = this.objectUtil.getBasicEObject(attribute);
          if ((object instanceof Input)) {
            this.error("Can not call Macro with different Input objects.", null, XMappingPackage.MAP_STATEMENT__ROUTINE_SECTION);
            return;
          }
        }
      }
      for (final OutputAttribute attribute_1 : outputMacroAttributes) {
        {
          final EObject object = this.objectUtil.getBasicEObject(attribute_1);
          if ((object instanceof Output)) {
            this.error("Can not call Macro with different Output objects.", null, XMappingPackage.MAP_STATEMENT__ROUTINE_SECTION);
            return;
          }
        }
      }
    }
  }
  
  /**
   * Checks reference type
   * @param referenceDeclaration reference declaration (List od UsedAttribute)
   * @param statement assignment statement
   */
  public void checkReferenceType(final EList<UsedAttribute> referenceDeclaration, final AssignmentStatement statement) {
    final Consumer<UsedAttribute> _function = (UsedAttribute it) -> {
      final Type statementType = this.typeProvider.typeFor(statement, null);
      NotType _notType = new NotType();
      Type variableType = ((Type) _notType);
      int reference = XMappingPackage.VARIABLE__NAME;
      String name = "Unknown";
      EObject _source = it.getSource();
      boolean _tripleNotEquals = (_source != null);
      if (_tripleNotEquals) {
        EObject object = it.getSource();
        QualifiedName qn = this.nameProvider.getFullyQualifiedName(object);
        name = qn.getLastSegment();
        variableType = this.typeProvider.typeForObject(object);
        if ((object instanceof Return)) {
          reference = XMappingPackage.RETURN__NAME;
        }
      }
      boolean _notEquals = (!Objects.equal(variableType, statementType));
      if (_notEquals) {
        String _name = variableType.getName();
        String _plus = ((name + " is ") + _name);
        String _plus_1 = (_plus + " - can not be a new type.");
        this.error(_plus_1, null, reference);
      }
    };
    referenceDeclaration.forEach(_function);
  }
  
  public boolean checkRecursion(final MapStatement statement) {
    RoutineBlock routineBlock = EcoreUtil2.<RoutineBlock>getContainerOfType(statement, RoutineBlock.class);
    if ((routineBlock != null)) {
      Routine routine = statement.getRoutineSection().getSource();
      if ((routine != null)) {
        final QualifiedName statementRoutineName = this.nameProvider.getFullyQualifiedName(routine);
        final QualifiedName routineName = this.nameProvider.getFullyQualifiedName(routineBlock.getRoutineName());
        int _hashCode = routineName.hashCode();
        int _hashCode_1 = statementRoutineName.hashCode();
        boolean _equals = (_hashCode == _hashCode_1);
        if (_equals) {
          this.error("Recursion is not allowed.", null, XMappingPackage.MAP_STATEMENT);
          return false;
        }
      }
    }
    return true;
  }
  
  public void checkStatement(final Statement statement) {
    if (statement instanceof AssignmentStatement) {
      _checkStatement((AssignmentStatement)statement);
      return;
    } else if (statement instanceof CallStatement) {
      _checkStatement((CallStatement)statement);
      return;
    } else if (statement instanceof MapForwardStatement) {
      _checkStatement((MapForwardStatement)statement);
      return;
    } else if (statement instanceof MapStatement) {
      _checkStatement((MapStatement)statement);
      return;
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(statement).toString());
    }
  }
}
