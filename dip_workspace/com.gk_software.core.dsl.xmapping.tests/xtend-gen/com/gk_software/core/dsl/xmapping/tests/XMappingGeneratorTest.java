package com.gk_software.core.dsl.xmapping.tests;

import com.gk_software.core.dsl.xmapping.tests.XMappingInjectorProvider;
import com.google.inject.Inject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.testing.InjectWith;
import org.eclipse.xtext.testing.extensions.InjectionExtension;
import org.eclipse.xtext.util.IAcceptor;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.testing.CompilationTestHelper;
import org.eclipse.xtext.xbase.testing.TemporaryFolder;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(InjectionExtension.class)
@InjectWith(XMappingInjectorProvider.class)
@SuppressWarnings("all")
public class XMappingGeneratorTest {
  @Rule
  @Inject
  public TemporaryFolder temporaryFolder;
  
  @Inject
  @Extension
  private CompilationTestHelper _compilationTestHelper;
  
  @Test
  public void testEmptyProgram() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      final IAcceptor<CompilationTestHelper.Result> _function = (CompilationTestHelper.Result it) -> {
      };
      this._compilationTestHelper.compile(_builder, _function);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void testOnlyTemplateProgram() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("package jj.d;");
      _builder.newLine();
      _builder.append("MappingClass Task1");
      _builder.newLine();
      _builder.append("Input:\"ZSORTLST_WBBDLD05.xsd\";");
      _builder.newLine();
      _builder.append("Output.first:\"masterData_Item.xsd\";");
      _builder.newLine();
      _builder.newLine();
      _builder.append("StartMapping");
      _builder.newLine();
      _builder.append("EndMapping\t\t");
      _builder.newLine();
      final IAcceptor<CompilationTestHelper.Result> _function = (CompilationTestHelper.Result it) -> {
      };
      this._compilationTestHelper.compile(_builder, _function);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void testOnlyCommandProgram() {
    try {
      StringConcatenation _builder = new StringConcatenation();
      _builder.append("package jj.d;");
      _builder.newLine();
      _builder.append("MappingClass Task1");
      _builder.newLine();
      _builder.append("Input:\"ZSORTLST_WBBDLD05.xsd\";");
      _builder.newLine();
      _builder.append("Output.first:\"masterData_Item.xsd\";");
      _builder.newLine();
      _builder.newLine();
      _builder.append("StartMapping");
      _builder.newLine();
      _builder.append("\t");
      _builder.append("let Variable.name = 5;");
      _builder.newLine();
      _builder.append("EndMapping\t\t");
      _builder.newLine();
      StringConcatenation _builder_1 = new StringConcatenation();
      _builder_1.append("dasdsadsa");
      this._compilationTestHelper.assertCompilesTo(_builder, _builder_1);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
