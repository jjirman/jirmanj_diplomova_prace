package com.gk_software.core.dsl.xmapping.tests;

import com.gk_software.core.dsl.xmapping.tests.XMappingInjectorProvider;
import com.gk_software.core.dsl.xmapping.xMapping.Model;
import com.google.inject.Inject;
import org.eclipse.xtext.testing.InjectWith;
import org.eclipse.xtext.testing.extensions.InjectionExtension;
import org.eclipse.xtext.testing.util.ParseHelper;
import org.eclipse.xtext.testing.validation.ValidationTestHelper;
import org.eclipse.xtext.xbase.lib.Extension;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(InjectionExtension.class)
@InjectWith(XMappingInjectorProvider.class)
@SuppressWarnings("all")
public class XMappingValidatorTest {
  @Inject
  private ParseHelper<Model> parseHelper;
  
  @Inject
  @Extension
  private ValidationTestHelper _validationTestHelper;
}
