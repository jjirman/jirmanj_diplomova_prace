package com.gk_software.core.dsl.xmapping.tests

import org.junit.runner.RunWith
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.XtextRunner
import org.eclipse.xtext.testing.util.ParseHelper
import com.google.inject.Inject
import org.eclipse.xtext.testing.validation.ValidationTestHelper
import com.gk_software.core.dsl.xmapping.xMapping.Model
import org.eclipse.xtext.testing.extensions.InjectionExtension
import org.junit.jupiter.api.^extension.ExtendWith
import org.junit.jupiter.api.Test
import com.gk_software.core.dsl.xmapping.xMapping.XMappingPackage

@ExtendWith(InjectionExtension)
@InjectWith(XMappingInjectorProvider)
class XMappingValidatorTest {
		
	 @Inject
	 ParseHelper<Model> parseHelper
	 
	 @Inject extension ValidationTestHelper	
	 
	/* @Test
	  def void testReturnIsInRoutine() {
		val result = parseHelper.parse('''
			package com.gk_software;
			MappingClass MyClass
			Input:"ZSORTLST_WBBDLD05.xsd";
			Output.first:"masterData_Item.xsd";
			
			StartMapping
				let Return.0;
			EndMapping
		''')
		result.assertError(XMappingPackage.Literals.RETURN, null, "Return must be in routine block.")
	//	result.assertNoErrors()
	}*/
	
}
