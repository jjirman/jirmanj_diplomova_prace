package com.gk_software.core.dsl.xmapping.ui.messaging;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.eclipse.jface.resource.StringConverter;
import org.eclipse.jface.util.IPropertyChangeListener;
import org.eclipse.jface.util.PropertyChangeEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.console.IOConsole;
import org.eclipse.ui.console.IOConsoleOutputStream;

public class PluginConsole extends IOConsole {
	private static final RGB DEFAULT_COLOR = new RGB(0, 0, 0);

	private Set<IOConsoleOutputStream> m_messageStreamSet = new HashSet<IOConsoleOutputStream>();

	private Set<IOConsoleOutputStream> m_errorStreamSet = new HashSet<IOConsoleOutputStream>();

	private boolean m_showOnMessage;

	private boolean m_showOnError;

	private Color m_messageColor;

	private Color m_errorColor;

	public PluginConsole(String name, String type) {
		super(name, type, null);
		initLimitOutput();
		m_showOnMessage = true;
		m_showOnError = true; 
		m_messageColor = createColor("0,0,0"); // createColor
		m_errorColor = createColor("255,0,0");
	}

	public IOConsoleOutputStream newOutputStream(boolean errorStream) {
		removeClosedStreams();

		IOConsoleOutputStream newStream = super.newOutputStream();

		if(errorStream) {
			newStream.setActivateOnWrite(m_showOnError);
			newStream.setColor(m_errorColor);
			m_errorStreamSet.add(newStream);
		}
		else {
			newStream.setActivateOnWrite(m_showOnMessage);
			newStream.setColor(m_messageColor);
			m_messageStreamSet.add(newStream);
		}

		return newStream;
	}

	/**
	 * Returns a color instance based on RGB color string (e.g. "0,0,256" - blue)
	 */
	private Color createColor(String colorString) {
		RGB rgb = StringConverter.asRGB(colorString, null);
		if(rgb == null)
			rgb = DEFAULT_COLOR;

		return new Color(Display.getCurrent(), rgb);
	}

	private void initLimitOutput() {
		setWaterMarks(-1, 0);
	}

	private void removeClosedStreams() {
		removeClosedStreams(m_messageStreamSet.iterator());
		removeClosedStreams(m_errorStreamSet.iterator());
	}

	private void removeClosedStreams(Iterator<IOConsoleOutputStream> iterator) {
		while(iterator.hasNext())
			if(iterator.next().isClosed())
				iterator.remove();
	}

}
