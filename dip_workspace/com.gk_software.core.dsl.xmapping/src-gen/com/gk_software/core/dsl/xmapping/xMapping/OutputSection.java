/**
 * generated by Xtext 2.24.0
 */
package com.gk_software.core.dsl.xmapping.xMapping;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Output Section</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.gk_software.core.dsl.xmapping.xMapping.OutputSection#getOutputAttributes <em>Output Attributes</em>}</li>
 * </ul>
 *
 * @see com.gk_software.core.dsl.xmapping.xMapping.XMappingPackage#getOutputSection()
 * @model
 * @generated
 */
public interface OutputSection extends EObject
{
  /**
   * Returns the value of the '<em><b>Output Attributes</b></em>' containment reference list.
   * The list contents are of type {@link org.eclipse.emf.ecore.EObject}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Output Attributes</em>' containment reference list.
   * @see com.gk_software.core.dsl.xmapping.xMapping.XMappingPackage#getOutputSection_OutputAttributes()
   * @model containment="true"
   * @generated
   */
  EList<EObject> getOutputAttributes();

} // OutputSection
