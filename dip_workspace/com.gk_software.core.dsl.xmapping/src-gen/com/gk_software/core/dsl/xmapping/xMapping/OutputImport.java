/**
 * generated by Xtext 2.24.0
 */
package com.gk_software.core.dsl.xmapping.xMapping;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Output Import</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link com.gk_software.core.dsl.xmapping.xMapping.OutputImport#getName <em>Name</em>}</li>
 *   <li>{@link com.gk_software.core.dsl.xmapping.xMapping.OutputImport#getPath <em>Path</em>}</li>
 * </ul>
 *
 * @see com.gk_software.core.dsl.xmapping.xMapping.XMappingPackage#getOutputImport()
 * @model
 * @generated
 */
public interface OutputImport extends EObject
{
  /**
   * Returns the value of the '<em><b>Name</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Name</em>' containment reference.
   * @see #setName(Output)
   * @see com.gk_software.core.dsl.xmapping.xMapping.XMappingPackage#getOutputImport_Name()
   * @model containment="true"
   * @generated
   */
  Output getName();

  /**
   * Sets the value of the '{@link com.gk_software.core.dsl.xmapping.xMapping.OutputImport#getName <em>Name</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Name</em>' containment reference.
   * @see #getName()
   * @generated
   */
  void setName(Output value);

  /**
   * Returns the value of the '<em><b>Path</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Path</em>' attribute.
   * @see #setPath(String)
   * @see com.gk_software.core.dsl.xmapping.xMapping.XMappingPackage#getOutputImport_Path()
   * @model
   * @generated
   */
  String getPath();

  /**
   * Sets the value of the '{@link com.gk_software.core.dsl.xmapping.xMapping.OutputImport#getPath <em>Path</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Path</em>' attribute.
   * @see #getPath()
   * @generated
   */
  void setPath(String value);

} // OutputImport
