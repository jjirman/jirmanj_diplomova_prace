package com.gk_software.core.dsl.xmapping.parser.antlr.internal;

// Hack: Use our own Lexer superclass by means of import. 
// Currently there is no other way to specify the superclass for the lexer.
import org.eclipse.xtext.parser.antlr.Lexer;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalXMappingLexer extends Lexer {
    public static final int T__50=50;
    public static final int RULE_VARIABLE_KEY=9;
    public static final int RULE_OUTPUT_KEY=8;
    public static final int RULE_MACRO_KEY=11;
    public static final int RULE_RETURN_KEY=10;
    public static final int T__55=55;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int RULE_MAPPER_KEY=14;
    public static final int RULE_ID=4;
    public static final int RULE_INPUT_KEY=7;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=16;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int RULE_PARAM_KEY=15;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_FUNCTION_KEY=13;
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=17;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=18;
    public static final int RULE_FILTER_KEY=12;
    public static final int RULE_ANY_OTHER=19;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators

    public InternalXMappingLexer() {;} 
    public InternalXMappingLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public InternalXMappingLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "InternalXMapping.g"; }

    // $ANTLR start "T__20"
    public final void mT__20() throws RecognitionException {
        try {
            int _type = T__20;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:11:7: ( 'package' )
            // InternalXMapping.g:11:9: 'package'
            {
            match("package"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__20"

    // $ANTLR start "T__21"
    public final void mT__21() throws RecognitionException {
        try {
            int _type = T__21;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:12:7: ( ';' )
            // InternalXMapping.g:12:9: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__21"

    // $ANTLR start "T__22"
    public final void mT__22() throws RecognitionException {
        try {
            int _type = T__22;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:13:7: ( 'import' )
            // InternalXMapping.g:13:9: 'import'
            {
            match("import"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__22"

    // $ANTLR start "T__23"
    public final void mT__23() throws RecognitionException {
        try {
            int _type = T__23;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:14:7: ( 'MappingClass' )
            // InternalXMapping.g:14:9: 'MappingClass'
            {
            match("MappingClass"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__23"

    // $ANTLR start "T__24"
    public final void mT__24() throws RecognitionException {
        try {
            int _type = T__24;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:15:7: ( ':' )
            // InternalXMapping.g:15:9: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__24"

    // $ANTLR start "T__25"
    public final void mT__25() throws RecognitionException {
        try {
            int _type = T__25;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:16:7: ( 'def' )
            // InternalXMapping.g:16:9: 'def'
            {
            match("def"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__25"

    // $ANTLR start "T__26"
    public final void mT__26() throws RecognitionException {
        try {
            int _type = T__26;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:17:7: ( '->' )
            // InternalXMapping.g:17:9: '->'
            {
            match("->"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__26"

    // $ANTLR start "T__27"
    public final void mT__27() throws RecognitionException {
        try {
            int _type = T__27;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:18:7: ( '{' )
            // InternalXMapping.g:18:9: '{'
            {
            match('{'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__27"

    // $ANTLR start "T__28"
    public final void mT__28() throws RecognitionException {
        try {
            int _type = T__28;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:19:7: ( '}' )
            // InternalXMapping.g:19:9: '}'
            {
            match('}'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__28"

    // $ANTLR start "T__29"
    public final void mT__29() throws RecognitionException {
        try {
            int _type = T__29;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:20:7: ( 'StartMapping' )
            // InternalXMapping.g:20:9: 'StartMapping'
            {
            match("StartMapping"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__29"

    // $ANTLR start "T__30"
    public final void mT__30() throws RecognitionException {
        try {
            int _type = T__30;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:21:7: ( 'EndMapping' )
            // InternalXMapping.g:21:9: 'EndMapping'
            {
            match("EndMapping"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__30"

    // $ANTLR start "T__31"
    public final void mT__31() throws RecognitionException {
        try {
            int _type = T__31;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:22:7: ( 'call' )
            // InternalXMapping.g:22:9: 'call'
            {
            match("call"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__31"

    // $ANTLR start "T__32"
    public final void mT__32() throws RecognitionException {
        try {
            int _type = T__32;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:23:7: ( '::' )
            // InternalXMapping.g:23:9: '::'
            {
            match("::"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__32"

    // $ANTLR start "T__33"
    public final void mT__33() throws RecognitionException {
        try {
            int _type = T__33;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:24:7: ( 'map' )
            // InternalXMapping.g:24:9: 'map'
            {
            match("map"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__33"

    // $ANTLR start "T__34"
    public final void mT__34() throws RecognitionException {
        try {
            int _type = T__34;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:25:7: ( 'NULL' )
            // InternalXMapping.g:25:9: 'NULL'
            {
            match("NULL"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__34"

    // $ANTLR start "T__35"
    public final void mT__35() throws RecognitionException {
        try {
            int _type = T__35;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:26:7: ( '.' )
            // InternalXMapping.g:26:9: '.'
            {
            match('.'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__35"

    // $ANTLR start "T__36"
    public final void mT__36() throws RecognitionException {
        try {
            int _type = T__36;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:27:7: ( '=' )
            // InternalXMapping.g:27:9: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__36"

    // $ANTLR start "T__37"
    public final void mT__37() throws RecognitionException {
        try {
            int _type = T__37;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:28:7: ( 'new' )
            // InternalXMapping.g:28:9: 'new'
            {
            match("new"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__37"

    // $ANTLR start "T__38"
    public final void mT__38() throws RecognitionException {
        try {
            int _type = T__38;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:29:7: ( '-' )
            // InternalXMapping.g:29:9: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__38"

    // $ANTLR start "T__39"
    public final void mT__39() throws RecognitionException {
        try {
            int _type = T__39;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:30:7: ( 'let' )
            // InternalXMapping.g:30:9: 'let'
            {
            match("let"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__39"

    // $ANTLR start "T__40"
    public final void mT__40() throws RecognitionException {
        try {
            int _type = T__40;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:31:7: ( ',' )
            // InternalXMapping.g:31:9: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__40"

    // $ANTLR start "T__41"
    public final void mT__41() throws RecognitionException {
        try {
            int _type = T__41;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:32:7: ( '.*' )
            // InternalXMapping.g:32:9: '.*'
            {
            match(".*"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__41"

    // $ANTLR start "T__42"
    public final void mT__42() throws RecognitionException {
        try {
            int _type = T__42;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:33:7: ( '[' )
            // InternalXMapping.g:33:9: '['
            {
            match('['); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__42"

    // $ANTLR start "T__43"
    public final void mT__43() throws RecognitionException {
        try {
            int _type = T__43;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:34:7: ( ']' )
            // InternalXMapping.g:34:9: ']'
            {
            match(']'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__43"

    // $ANTLR start "T__44"
    public final void mT__44() throws RecognitionException {
        try {
            int _type = T__44;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:35:7: ( '(' )
            // InternalXMapping.g:35:9: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__44"

    // $ANTLR start "T__45"
    public final void mT__45() throws RecognitionException {
        try {
            int _type = T__45;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:36:7: ( ')' )
            // InternalXMapping.g:36:9: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__45"

    // $ANTLR start "T__46"
    public final void mT__46() throws RecognitionException {
        try {
            int _type = T__46;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:37:7: ( '=>' )
            // InternalXMapping.g:37:9: '=>'
            {
            match("=>"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__46"

    // $ANTLR start "T__47"
    public final void mT__47() throws RecognitionException {
        try {
            int _type = T__47;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:38:7: ( '<' )
            // InternalXMapping.g:38:9: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__47"

    // $ANTLR start "T__48"
    public final void mT__48() throws RecognitionException {
        try {
            int _type = T__48;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:39:7: ( '>' )
            // InternalXMapping.g:39:9: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__48"

    // $ANTLR start "T__49"
    public final void mT__49() throws RecognitionException {
        try {
            int _type = T__49;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:40:7: ( '?' )
            // InternalXMapping.g:40:9: '?'
            {
            match('?'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__49"

    // $ANTLR start "T__50"
    public final void mT__50() throws RecognitionException {
        try {
            int _type = T__50;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:41:7: ( 'extends' )
            // InternalXMapping.g:41:9: 'extends'
            {
            match("extends"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__50"

    // $ANTLR start "T__51"
    public final void mT__51() throws RecognitionException {
        try {
            int _type = T__51;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:42:7: ( '&' )
            // InternalXMapping.g:42:9: '&'
            {
            match('&'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__51"

    // $ANTLR start "T__52"
    public final void mT__52() throws RecognitionException {
        try {
            int _type = T__52;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:43:7: ( 'super' )
            // InternalXMapping.g:43:9: 'super'
            {
            match("super"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__52"

    // $ANTLR start "T__53"
    public final void mT__53() throws RecognitionException {
        try {
            int _type = T__53;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:44:7: ( 'static' )
            // InternalXMapping.g:44:9: 'static'
            {
            match("static"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__53"

    // $ANTLR start "T__54"
    public final void mT__54() throws RecognitionException {
        try {
            int _type = T__54;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:45:7: ( 'extension' )
            // InternalXMapping.g:45:9: 'extension'
            {
            match("extension"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__54"

    // $ANTLR start "T__55"
    public final void mT__55() throws RecognitionException {
        try {
            int _type = T__55;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:46:7: ( '*' )
            // InternalXMapping.g:46:9: '*'
            {
            match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "T__55"

    // $ANTLR start "RULE_PARAM_KEY"
    public final void mRULE_PARAM_KEY() throws RecognitionException {
        try {
            int _type = RULE_PARAM_KEY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:4284:16: ( 'Param' )
            // InternalXMapping.g:4284:18: 'Param'
            {
            match("Param"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_PARAM_KEY"

    // $ANTLR start "RULE_MACRO_KEY"
    public final void mRULE_MACRO_KEY() throws RecognitionException {
        try {
            int _type = RULE_MACRO_KEY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:4286:16: ( 'Macro' )
            // InternalXMapping.g:4286:18: 'Macro'
            {
            match("Macro"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_MACRO_KEY"

    // $ANTLR start "RULE_FILTER_KEY"
    public final void mRULE_FILTER_KEY() throws RecognitionException {
        try {
            int _type = RULE_FILTER_KEY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:4288:17: ( 'Filter' )
            // InternalXMapping.g:4288:19: 'Filter'
            {
            match("Filter"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_FILTER_KEY"

    // $ANTLR start "RULE_FUNCTION_KEY"
    public final void mRULE_FUNCTION_KEY() throws RecognitionException {
        try {
            int _type = RULE_FUNCTION_KEY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:4290:19: ( 'Function' )
            // InternalXMapping.g:4290:21: 'Function'
            {
            match("Function"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_FUNCTION_KEY"

    // $ANTLR start "RULE_MAPPER_KEY"
    public final void mRULE_MAPPER_KEY() throws RecognitionException {
        try {
            int _type = RULE_MAPPER_KEY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:4292:17: ( 'Mapper' )
            // InternalXMapping.g:4292:19: 'Mapper'
            {
            match("Mapper"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_MAPPER_KEY"

    // $ANTLR start "RULE_VARIABLE_KEY"
    public final void mRULE_VARIABLE_KEY() throws RecognitionException {
        try {
            int _type = RULE_VARIABLE_KEY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:4294:19: ( 'Variable' )
            // InternalXMapping.g:4294:21: 'Variable'
            {
            match("Variable"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_VARIABLE_KEY"

    // $ANTLR start "RULE_INPUT_KEY"
    public final void mRULE_INPUT_KEY() throws RecognitionException {
        try {
            int _type = RULE_INPUT_KEY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:4296:16: ( 'Input' )
            // InternalXMapping.g:4296:18: 'Input'
            {
            match("Input"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INPUT_KEY"

    // $ANTLR start "RULE_OUTPUT_KEY"
    public final void mRULE_OUTPUT_KEY() throws RecognitionException {
        try {
            int _type = RULE_OUTPUT_KEY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:4298:17: ( 'Output' )
            // InternalXMapping.g:4298:19: 'Output'
            {
            match("Output"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_OUTPUT_KEY"

    // $ANTLR start "RULE_RETURN_KEY"
    public final void mRULE_RETURN_KEY() throws RecognitionException {
        try {
            int _type = RULE_RETURN_KEY;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:4300:17: ( 'Return' )
            // InternalXMapping.g:4300:19: 'Return'
            {
            match("Return"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_RETURN_KEY"

    // $ANTLR start "RULE_INT"
    public final void mRULE_INT() throws RecognitionException {
        try {
            int _type = RULE_INT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:4302:10: ( ( '0' .. '9' )+ )
            // InternalXMapping.g:4302:12: ( '0' .. '9' )+
            {
            // InternalXMapping.g:4302:12: ( '0' .. '9' )+
            int cnt1=0;
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>='0' && LA1_0<='9')) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalXMapping.g:4302:13: '0' .. '9'
            	    {
            	    matchRange('0','9'); 

            	    }
            	    break;

            	default :
            	    if ( cnt1 >= 1 ) break loop1;
                        EarlyExitException eee =
                            new EarlyExitException(1, input);
                        throw eee;
                }
                cnt1++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_INT"

    // $ANTLR start "RULE_ID"
    public final void mRULE_ID() throws RecognitionException {
        try {
            int _type = RULE_ID;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:4304:9: ( ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '$' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '$' | '_' | '0' .. '9' )* )
            // InternalXMapping.g:4304:11: ( '^' )? ( 'a' .. 'z' | 'A' .. 'Z' | '$' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '$' | '_' | '0' .. '9' )*
            {
            // InternalXMapping.g:4304:11: ( '^' )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0=='^') ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // InternalXMapping.g:4304:11: '^'
                    {
                    match('^'); 

                    }
                    break;

            }

            if ( input.LA(1)=='$'||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            // InternalXMapping.g:4304:44: ( 'a' .. 'z' | 'A' .. 'Z' | '$' | '_' | '0' .. '9' )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0=='$'||(LA3_0>='0' && LA3_0<='9')||(LA3_0>='A' && LA3_0<='Z')||LA3_0=='_'||(LA3_0>='a' && LA3_0<='z')) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalXMapping.g:
            	    {
            	    if ( input.LA(1)=='$'||(input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ID"

    // $ANTLR start "RULE_STRING"
    public final void mRULE_STRING() throws RecognitionException {
        try {
            int _type = RULE_STRING;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:4306:13: ( ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* ( '\"' )? | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* ( '\\'' )? ) )
            // InternalXMapping.g:4306:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* ( '\"' )? | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* ( '\\'' )? )
            {
            // InternalXMapping.g:4306:15: ( '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* ( '\"' )? | '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* ( '\\'' )? )
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0=='\"') ) {
                alt8=1;
            }
            else if ( (LA8_0=='\'') ) {
                alt8=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 8, 0, input);

                throw nvae;
            }
            switch (alt8) {
                case 1 :
                    // InternalXMapping.g:4306:16: '\"' ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )* ( '\"' )?
                    {
                    match('\"'); 
                    // InternalXMapping.g:4306:20: ( '\\\\' . | ~ ( ( '\\\\' | '\"' ) ) )*
                    loop4:
                    do {
                        int alt4=3;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0=='\\') ) {
                            alt4=1;
                        }
                        else if ( ((LA4_0>='\u0000' && LA4_0<='!')||(LA4_0>='#' && LA4_0<='[')||(LA4_0>=']' && LA4_0<='\uFFFF')) ) {
                            alt4=2;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // InternalXMapping.g:4306:21: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalXMapping.g:4306:28: ~ ( ( '\\\\' | '\"' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='!')||(input.LA(1)>='#' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop4;
                        }
                    } while (true);

                    // InternalXMapping.g:4306:44: ( '\"' )?
                    int alt5=2;
                    int LA5_0 = input.LA(1);

                    if ( (LA5_0=='\"') ) {
                        alt5=1;
                    }
                    switch (alt5) {
                        case 1 :
                            // InternalXMapping.g:4306:44: '\"'
                            {
                            match('\"'); 

                            }
                            break;

                    }


                    }
                    break;
                case 2 :
                    // InternalXMapping.g:4306:49: '\\'' ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )* ( '\\'' )?
                    {
                    match('\''); 
                    // InternalXMapping.g:4306:54: ( '\\\\' . | ~ ( ( '\\\\' | '\\'' ) ) )*
                    loop6:
                    do {
                        int alt6=3;
                        int LA6_0 = input.LA(1);

                        if ( (LA6_0=='\\') ) {
                            alt6=1;
                        }
                        else if ( ((LA6_0>='\u0000' && LA6_0<='&')||(LA6_0>='(' && LA6_0<='[')||(LA6_0>=']' && LA6_0<='\uFFFF')) ) {
                            alt6=2;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // InternalXMapping.g:4306:55: '\\\\' .
                    	    {
                    	    match('\\'); 
                    	    matchAny(); 

                    	    }
                    	    break;
                    	case 2 :
                    	    // InternalXMapping.g:4306:62: ~ ( ( '\\\\' | '\\'' ) )
                    	    {
                    	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='&')||(input.LA(1)>='(' && input.LA(1)<='[')||(input.LA(1)>=']' && input.LA(1)<='\uFFFF') ) {
                    	        input.consume();

                    	    }
                    	    else {
                    	        MismatchedSetException mse = new MismatchedSetException(null,input);
                    	        recover(mse);
                    	        throw mse;}


                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);

                    // InternalXMapping.g:4306:79: ( '\\'' )?
                    int alt7=2;
                    int LA7_0 = input.LA(1);

                    if ( (LA7_0=='\'') ) {
                        alt7=1;
                    }
                    switch (alt7) {
                        case 1 :
                            // InternalXMapping.g:4306:79: '\\''
                            {
                            match('\''); 

                            }
                            break;

                    }


                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_STRING"

    // $ANTLR start "RULE_ML_COMMENT"
    public final void mRULE_ML_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_ML_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:4308:17: ( '/*' ( options {greedy=false; } : . )* '*/' )
            // InternalXMapping.g:4308:19: '/*' ( options {greedy=false; } : . )* '*/'
            {
            match("/*"); 

            // InternalXMapping.g:4308:24: ( options {greedy=false; } : . )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0=='*') ) {
                    int LA9_1 = input.LA(2);

                    if ( (LA9_1=='/') ) {
                        alt9=2;
                    }
                    else if ( ((LA9_1>='\u0000' && LA9_1<='.')||(LA9_1>='0' && LA9_1<='\uFFFF')) ) {
                        alt9=1;
                    }


                }
                else if ( ((LA9_0>='\u0000' && LA9_0<=')')||(LA9_0>='+' && LA9_0<='\uFFFF')) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalXMapping.g:4308:52: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

            match("*/"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ML_COMMENT"

    // $ANTLR start "RULE_SL_COMMENT"
    public final void mRULE_SL_COMMENT() throws RecognitionException {
        try {
            int _type = RULE_SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:4310:17: ( '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )? )
            // InternalXMapping.g:4310:19: '//' (~ ( ( '\\n' | '\\r' ) ) )* ( ( '\\r' )? '\\n' )?
            {
            match("//"); 

            // InternalXMapping.g:4310:24: (~ ( ( '\\n' | '\\r' ) ) )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( ((LA10_0>='\u0000' && LA10_0<='\t')||(LA10_0>='\u000B' && LA10_0<='\f')||(LA10_0>='\u000E' && LA10_0<='\uFFFF')) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalXMapping.g:4310:24: ~ ( ( '\\n' | '\\r' ) )
            	    {
            	    if ( (input.LA(1)>='\u0000' && input.LA(1)<='\t')||(input.LA(1)>='\u000B' && input.LA(1)<='\f')||(input.LA(1)>='\u000E' && input.LA(1)<='\uFFFF') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

            // InternalXMapping.g:4310:40: ( ( '\\r' )? '\\n' )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0=='\n'||LA12_0=='\r') ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalXMapping.g:4310:41: ( '\\r' )? '\\n'
                    {
                    // InternalXMapping.g:4310:41: ( '\\r' )?
                    int alt11=2;
                    int LA11_0 = input.LA(1);

                    if ( (LA11_0=='\r') ) {
                        alt11=1;
                    }
                    switch (alt11) {
                        case 1 :
                            // InternalXMapping.g:4310:41: '\\r'
                            {
                            match('\r'); 

                            }
                            break;

                    }

                    match('\n'); 

                    }
                    break;

            }


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_SL_COMMENT"

    // $ANTLR start "RULE_WS"
    public final void mRULE_WS() throws RecognitionException {
        try {
            int _type = RULE_WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:4312:9: ( ( ' ' | '\\t' | '\\r' | '\\n' )+ )
            // InternalXMapping.g:4312:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            {
            // InternalXMapping.g:4312:11: ( ' ' | '\\t' | '\\r' | '\\n' )+
            int cnt13=0;
            loop13:
            do {
                int alt13=2;
                int LA13_0 = input.LA(1);

                if ( ((LA13_0>='\t' && LA13_0<='\n')||LA13_0=='\r'||LA13_0==' ') ) {
                    alt13=1;
                }


                switch (alt13) {
            	case 1 :
            	    // InternalXMapping.g:
            	    {
            	    if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    if ( cnt13 >= 1 ) break loop13;
                        EarlyExitException eee =
                            new EarlyExitException(13, input);
                        throw eee;
                }
                cnt13++;
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_WS"

    // $ANTLR start "RULE_ANY_OTHER"
    public final void mRULE_ANY_OTHER() throws RecognitionException {
        try {
            int _type = RULE_ANY_OTHER;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // InternalXMapping.g:4314:16: ( . )
            // InternalXMapping.g:4314:18: .
            {
            matchAny(); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "RULE_ANY_OTHER"

    public void mTokens() throws RecognitionException {
        // InternalXMapping.g:1:8: ( T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | RULE_PARAM_KEY | RULE_MACRO_KEY | RULE_FILTER_KEY | RULE_FUNCTION_KEY | RULE_MAPPER_KEY | RULE_VARIABLE_KEY | RULE_INPUT_KEY | RULE_OUTPUT_KEY | RULE_RETURN_KEY | RULE_INT | RULE_ID | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER )
        int alt14=52;
        alt14 = dfa14.predict(input);
        switch (alt14) {
            case 1 :
                // InternalXMapping.g:1:10: T__20
                {
                mT__20(); 

                }
                break;
            case 2 :
                // InternalXMapping.g:1:16: T__21
                {
                mT__21(); 

                }
                break;
            case 3 :
                // InternalXMapping.g:1:22: T__22
                {
                mT__22(); 

                }
                break;
            case 4 :
                // InternalXMapping.g:1:28: T__23
                {
                mT__23(); 

                }
                break;
            case 5 :
                // InternalXMapping.g:1:34: T__24
                {
                mT__24(); 

                }
                break;
            case 6 :
                // InternalXMapping.g:1:40: T__25
                {
                mT__25(); 

                }
                break;
            case 7 :
                // InternalXMapping.g:1:46: T__26
                {
                mT__26(); 

                }
                break;
            case 8 :
                // InternalXMapping.g:1:52: T__27
                {
                mT__27(); 

                }
                break;
            case 9 :
                // InternalXMapping.g:1:58: T__28
                {
                mT__28(); 

                }
                break;
            case 10 :
                // InternalXMapping.g:1:64: T__29
                {
                mT__29(); 

                }
                break;
            case 11 :
                // InternalXMapping.g:1:70: T__30
                {
                mT__30(); 

                }
                break;
            case 12 :
                // InternalXMapping.g:1:76: T__31
                {
                mT__31(); 

                }
                break;
            case 13 :
                // InternalXMapping.g:1:82: T__32
                {
                mT__32(); 

                }
                break;
            case 14 :
                // InternalXMapping.g:1:88: T__33
                {
                mT__33(); 

                }
                break;
            case 15 :
                // InternalXMapping.g:1:94: T__34
                {
                mT__34(); 

                }
                break;
            case 16 :
                // InternalXMapping.g:1:100: T__35
                {
                mT__35(); 

                }
                break;
            case 17 :
                // InternalXMapping.g:1:106: T__36
                {
                mT__36(); 

                }
                break;
            case 18 :
                // InternalXMapping.g:1:112: T__37
                {
                mT__37(); 

                }
                break;
            case 19 :
                // InternalXMapping.g:1:118: T__38
                {
                mT__38(); 

                }
                break;
            case 20 :
                // InternalXMapping.g:1:124: T__39
                {
                mT__39(); 

                }
                break;
            case 21 :
                // InternalXMapping.g:1:130: T__40
                {
                mT__40(); 

                }
                break;
            case 22 :
                // InternalXMapping.g:1:136: T__41
                {
                mT__41(); 

                }
                break;
            case 23 :
                // InternalXMapping.g:1:142: T__42
                {
                mT__42(); 

                }
                break;
            case 24 :
                // InternalXMapping.g:1:148: T__43
                {
                mT__43(); 

                }
                break;
            case 25 :
                // InternalXMapping.g:1:154: T__44
                {
                mT__44(); 

                }
                break;
            case 26 :
                // InternalXMapping.g:1:160: T__45
                {
                mT__45(); 

                }
                break;
            case 27 :
                // InternalXMapping.g:1:166: T__46
                {
                mT__46(); 

                }
                break;
            case 28 :
                // InternalXMapping.g:1:172: T__47
                {
                mT__47(); 

                }
                break;
            case 29 :
                // InternalXMapping.g:1:178: T__48
                {
                mT__48(); 

                }
                break;
            case 30 :
                // InternalXMapping.g:1:184: T__49
                {
                mT__49(); 

                }
                break;
            case 31 :
                // InternalXMapping.g:1:190: T__50
                {
                mT__50(); 

                }
                break;
            case 32 :
                // InternalXMapping.g:1:196: T__51
                {
                mT__51(); 

                }
                break;
            case 33 :
                // InternalXMapping.g:1:202: T__52
                {
                mT__52(); 

                }
                break;
            case 34 :
                // InternalXMapping.g:1:208: T__53
                {
                mT__53(); 

                }
                break;
            case 35 :
                // InternalXMapping.g:1:214: T__54
                {
                mT__54(); 

                }
                break;
            case 36 :
                // InternalXMapping.g:1:220: T__55
                {
                mT__55(); 

                }
                break;
            case 37 :
                // InternalXMapping.g:1:226: RULE_PARAM_KEY
                {
                mRULE_PARAM_KEY(); 

                }
                break;
            case 38 :
                // InternalXMapping.g:1:241: RULE_MACRO_KEY
                {
                mRULE_MACRO_KEY(); 

                }
                break;
            case 39 :
                // InternalXMapping.g:1:256: RULE_FILTER_KEY
                {
                mRULE_FILTER_KEY(); 

                }
                break;
            case 40 :
                // InternalXMapping.g:1:272: RULE_FUNCTION_KEY
                {
                mRULE_FUNCTION_KEY(); 

                }
                break;
            case 41 :
                // InternalXMapping.g:1:290: RULE_MAPPER_KEY
                {
                mRULE_MAPPER_KEY(); 

                }
                break;
            case 42 :
                // InternalXMapping.g:1:306: RULE_VARIABLE_KEY
                {
                mRULE_VARIABLE_KEY(); 

                }
                break;
            case 43 :
                // InternalXMapping.g:1:324: RULE_INPUT_KEY
                {
                mRULE_INPUT_KEY(); 

                }
                break;
            case 44 :
                // InternalXMapping.g:1:339: RULE_OUTPUT_KEY
                {
                mRULE_OUTPUT_KEY(); 

                }
                break;
            case 45 :
                // InternalXMapping.g:1:355: RULE_RETURN_KEY
                {
                mRULE_RETURN_KEY(); 

                }
                break;
            case 46 :
                // InternalXMapping.g:1:371: RULE_INT
                {
                mRULE_INT(); 

                }
                break;
            case 47 :
                // InternalXMapping.g:1:380: RULE_ID
                {
                mRULE_ID(); 

                }
                break;
            case 48 :
                // InternalXMapping.g:1:388: RULE_STRING
                {
                mRULE_STRING(); 

                }
                break;
            case 49 :
                // InternalXMapping.g:1:400: RULE_ML_COMMENT
                {
                mRULE_ML_COMMENT(); 

                }
                break;
            case 50 :
                // InternalXMapping.g:1:416: RULE_SL_COMMENT
                {
                mRULE_SL_COMMENT(); 

                }
                break;
            case 51 :
                // InternalXMapping.g:1:432: RULE_WS
                {
                mRULE_WS(); 

                }
                break;
            case 52 :
                // InternalXMapping.g:1:440: RULE_ANY_OTHER
                {
                mRULE_ANY_OTHER(); 

                }
                break;

        }

    }


    protected DFA14 dfa14 = new DFA14(this);
    static final String DFA14_eotS =
        "\1\uffff\1\56\1\uffff\2\56\1\63\1\56\1\66\2\uffff\5\56\1\77\1\101\2\56\10\uffff\1\56\1\uffff\1\56\1\uffff\6\56\1\uffff\1\54\3\uffff\1\54\2\uffff\1\56\2\uffff\2\56\2\uffff\1\56\4\uffff\5\56\4\uffff\2\56\10\uffff\1\56\1\uffff\2\56\1\uffff\7\56\5\uffff\4\56\1\167\3\56\1\173\1\56\1\175\1\176\16\56\1\uffff\2\56\1\u0090\1\uffff\1\u0091\2\uffff\16\56\1\u00a0\2\56\2\uffff\1\56\1\u00a5\1\56\1\u00a7\3\56\1\u00ab\3\56\1\u00af\1\56\1\u00b1\1\uffff\4\56\1\uffff\1\u00b6\1\uffff\1\u00b7\2\56\1\uffff\1\u00ba\1\u00bb\1\u00bc\1\uffff\1\56\1\uffff\2\56\1\u00c0\1\56\2\uffff\2\56\3\uffff\3\56\1\uffff\1\56\1\u00c8\1\u00c9\3\56\1\u00cd\2\uffff\2\56\1\u00d0\1\uffff\2\56\1\uffff\1\u00d3\1\u00d4\2\uffff";
    static final String DFA14_eofS =
        "\u00d5\uffff";
    static final String DFA14_minS =
        "\1\0\1\141\1\uffff\1\155\1\141\1\72\1\145\1\76\2\uffff\1\164\1\156\2\141\1\125\1\52\1\76\2\145\10\uffff\1\170\1\uffff\1\164\1\uffff\1\141\1\151\1\141\1\156\1\165\1\145\1\uffff\1\44\3\uffff\1\52\2\uffff\1\143\2\uffff\1\160\1\143\2\uffff\1\146\4\uffff\1\141\1\144\1\154\1\160\1\114\4\uffff\1\167\1\164\10\uffff\1\164\1\uffff\1\160\1\141\1\uffff\1\162\1\154\1\156\1\162\1\160\2\164\5\uffff\1\153\1\157\1\160\1\162\1\44\1\162\1\115\1\154\1\44\1\114\2\44\2\145\1\164\1\141\1\164\1\143\1\151\1\165\1\160\1\165\1\141\1\162\1\145\1\157\1\uffff\1\164\1\141\1\44\1\uffff\1\44\2\uffff\1\156\1\162\1\151\1\155\1\145\1\164\1\141\1\164\1\165\1\162\1\147\1\164\1\156\1\162\1\44\1\115\1\160\2\uffff\1\144\1\44\1\143\1\44\1\162\1\151\1\142\1\44\1\164\1\156\1\145\1\44\1\147\1\44\1\uffff\1\141\1\160\1\163\1\151\1\uffff\1\44\1\uffff\1\44\1\157\1\154\1\uffff\3\44\1\uffff\1\103\1\uffff\1\160\1\151\1\44\1\157\2\uffff\1\156\1\145\3\uffff\1\154\1\160\1\156\1\uffff\1\156\2\44\1\141\1\151\1\147\1\44\2\uffff\1\163\1\156\1\44\1\uffff\1\163\1\147\1\uffff\2\44\2\uffff";
    static final String DFA14_maxS =
        "\1\uffff\1\141\1\uffff\1\155\1\141\1\72\1\145\1\76\2\uffff\1\164\1\156\2\141\1\125\1\52\1\76\2\145\10\uffff\1\170\1\uffff\1\165\1\uffff\1\141\1\165\1\141\1\156\1\165\1\145\1\uffff\1\172\3\uffff\1\57\2\uffff\1\143\2\uffff\2\160\2\uffff\1\146\4\uffff\1\141\1\144\1\154\1\160\1\114\4\uffff\1\167\1\164\10\uffff\1\164\1\uffff\1\160\1\141\1\uffff\1\162\1\154\1\156\1\162\1\160\2\164\5\uffff\1\153\1\157\1\160\1\162\1\172\1\162\1\115\1\154\1\172\1\114\2\172\2\145\1\164\1\141\1\164\1\143\1\151\1\165\1\160\1\165\1\141\1\162\1\151\1\157\1\uffff\1\164\1\141\1\172\1\uffff\1\172\2\uffff\1\156\1\162\1\151\1\155\1\145\1\164\1\141\1\164\1\165\1\162\1\147\1\164\1\156\1\162\1\172\1\115\1\160\2\uffff\1\163\1\172\1\143\1\172\1\162\1\151\1\142\1\172\1\164\1\156\1\145\1\172\1\147\1\172\1\uffff\1\141\1\160\1\163\1\151\1\uffff\1\172\1\uffff\1\172\1\157\1\154\1\uffff\3\172\1\uffff\1\103\1\uffff\1\160\1\151\1\172\1\157\2\uffff\1\156\1\145\3\uffff\1\154\1\160\1\156\1\uffff\1\156\2\172\1\141\1\151\1\147\1\172\2\uffff\1\163\1\156\1\172\1\uffff\1\163\1\147\1\uffff\2\172\2\uffff";
    static final String DFA14_acceptS =
        "\2\uffff\1\2\5\uffff\1\10\1\11\11\uffff\1\25\1\27\1\30\1\31\1\32\1\34\1\35\1\36\1\uffff\1\40\1\uffff\1\44\6\uffff\1\56\1\uffff\1\57\2\60\1\uffff\1\63\1\64\1\uffff\1\57\1\2\2\uffff\1\15\1\5\1\uffff\1\7\1\23\1\10\1\11\5\uffff\1\26\1\20\1\33\1\21\2\uffff\1\25\1\27\1\30\1\31\1\32\1\34\1\35\1\36\1\uffff\1\40\2\uffff\1\44\7\uffff\1\56\1\60\1\61\1\62\1\63\32\uffff\1\6\3\uffff\1\16\1\uffff\1\22\1\24\21\uffff\1\14\1\17\16\uffff\1\46\4\uffff\1\41\1\uffff\1\45\3\uffff\1\53\3\uffff\1\3\1\uffff\1\51\4\uffff\1\42\1\47\2\uffff\1\54\1\55\1\1\3\uffff\1\37\7\uffff\1\50\1\52\3\uffff\1\43\2\uffff\1\13\2\uffff\1\4\1\12";
    static final String DFA14_specialS =
        "\1\0\u00d4\uffff}>";
    static final String[] DFA14_transitionS = {
            "\11\54\2\53\2\54\1\53\22\54\1\53\1\54\1\50\1\54\1\47\1\54\1\34\1\51\1\26\1\27\1\36\1\54\1\23\1\7\1\17\1\52\12\45\1\5\1\2\1\30\1\20\1\31\1\32\1\54\4\47\1\13\1\40\2\47\1\42\3\47\1\4\1\16\1\43\1\37\1\47\1\44\1\12\2\47\1\41\4\47\1\24\1\54\1\25\1\46\1\47\1\54\2\47\1\14\1\6\1\33\3\47\1\3\2\47\1\22\1\15\1\21\1\47\1\1\2\47\1\35\7\47\1\10\1\54\1\11\uff82\54",
            "\1\55",
            "",
            "\1\60",
            "\1\61",
            "\1\62",
            "\1\64",
            "\1\65",
            "",
            "",
            "\1\71",
            "\1\72",
            "\1\73",
            "\1\74",
            "\1\75",
            "\1\76",
            "\1\100",
            "\1\102",
            "\1\103",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\114",
            "",
            "\1\117\1\116",
            "",
            "\1\121",
            "\1\122\13\uffff\1\123",
            "\1\124",
            "\1\125",
            "\1\126",
            "\1\127",
            "",
            "\1\56\34\uffff\32\56\4\uffff\1\56\1\uffff\32\56",
            "",
            "",
            "",
            "\1\132\4\uffff\1\133",
            "",
            "",
            "\1\135",
            "",
            "",
            "\1\136",
            "\1\140\14\uffff\1\137",
            "",
            "",
            "\1\141",
            "",
            "",
            "",
            "",
            "\1\142",
            "\1\143",
            "\1\144",
            "\1\145",
            "\1\146",
            "",
            "",
            "",
            "",
            "\1\147",
            "\1\150",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\151",
            "",
            "\1\152",
            "\1\153",
            "",
            "\1\154",
            "\1\155",
            "\1\156",
            "\1\157",
            "\1\160",
            "\1\161",
            "\1\162",
            "",
            "",
            "",
            "",
            "",
            "\1\163",
            "\1\164",
            "\1\165",
            "\1\166",
            "\1\56\13\uffff\12\56\7\uffff\32\56\4\uffff\1\56\1\uffff\32\56",
            "\1\170",
            "\1\171",
            "\1\172",
            "\1\56\13\uffff\12\56\7\uffff\32\56\4\uffff\1\56\1\uffff\32\56",
            "\1\174",
            "\1\56\13\uffff\12\56\7\uffff\32\56\4\uffff\1\56\1\uffff\32\56",
            "\1\56\13\uffff\12\56\7\uffff\32\56\4\uffff\1\56\1\uffff\32\56",
            "\1\177",
            "\1\u0080",
            "\1\u0081",
            "\1\u0082",
            "\1\u0083",
            "\1\u0084",
            "\1\u0085",
            "\1\u0086",
            "\1\u0087",
            "\1\u0088",
            "\1\u0089",
            "\1\u008a",
            "\1\u008c\3\uffff\1\u008b",
            "\1\u008d",
            "",
            "\1\u008e",
            "\1\u008f",
            "\1\56\13\uffff\12\56\7\uffff\32\56\4\uffff\1\56\1\uffff\32\56",
            "",
            "\1\56\13\uffff\12\56\7\uffff\32\56\4\uffff\1\56\1\uffff\32\56",
            "",
            "",
            "\1\u0092",
            "\1\u0093",
            "\1\u0094",
            "\1\u0095",
            "\1\u0096",
            "\1\u0097",
            "\1\u0098",
            "\1\u0099",
            "\1\u009a",
            "\1\u009b",
            "\1\u009c",
            "\1\u009d",
            "\1\u009e",
            "\1\u009f",
            "\1\56\13\uffff\12\56\7\uffff\32\56\4\uffff\1\56\1\uffff\32\56",
            "\1\u00a1",
            "\1\u00a2",
            "",
            "",
            "\1\u00a3\16\uffff\1\u00a4",
            "\1\56\13\uffff\12\56\7\uffff\32\56\4\uffff\1\56\1\uffff\32\56",
            "\1\u00a6",
            "\1\56\13\uffff\12\56\7\uffff\32\56\4\uffff\1\56\1\uffff\32\56",
            "\1\u00a8",
            "\1\u00a9",
            "\1\u00aa",
            "\1\56\13\uffff\12\56\7\uffff\32\56\4\uffff\1\56\1\uffff\32\56",
            "\1\u00ac",
            "\1\u00ad",
            "\1\u00ae",
            "\1\56\13\uffff\12\56\7\uffff\32\56\4\uffff\1\56\1\uffff\32\56",
            "\1\u00b0",
            "\1\56\13\uffff\12\56\7\uffff\32\56\4\uffff\1\56\1\uffff\32\56",
            "",
            "\1\u00b2",
            "\1\u00b3",
            "\1\u00b4",
            "\1\u00b5",
            "",
            "\1\56\13\uffff\12\56\7\uffff\32\56\4\uffff\1\56\1\uffff\32\56",
            "",
            "\1\56\13\uffff\12\56\7\uffff\32\56\4\uffff\1\56\1\uffff\32\56",
            "\1\u00b8",
            "\1\u00b9",
            "",
            "\1\56\13\uffff\12\56\7\uffff\32\56\4\uffff\1\56\1\uffff\32\56",
            "\1\56\13\uffff\12\56\7\uffff\32\56\4\uffff\1\56\1\uffff\32\56",
            "\1\56\13\uffff\12\56\7\uffff\32\56\4\uffff\1\56\1\uffff\32\56",
            "",
            "\1\u00bd",
            "",
            "\1\u00be",
            "\1\u00bf",
            "\1\56\13\uffff\12\56\7\uffff\32\56\4\uffff\1\56\1\uffff\32\56",
            "\1\u00c1",
            "",
            "",
            "\1\u00c2",
            "\1\u00c3",
            "",
            "",
            "",
            "\1\u00c4",
            "\1\u00c5",
            "\1\u00c6",
            "",
            "\1\u00c7",
            "\1\56\13\uffff\12\56\7\uffff\32\56\4\uffff\1\56\1\uffff\32\56",
            "\1\56\13\uffff\12\56\7\uffff\32\56\4\uffff\1\56\1\uffff\32\56",
            "\1\u00ca",
            "\1\u00cb",
            "\1\u00cc",
            "\1\56\13\uffff\12\56\7\uffff\32\56\4\uffff\1\56\1\uffff\32\56",
            "",
            "",
            "\1\u00ce",
            "\1\u00cf",
            "\1\56\13\uffff\12\56\7\uffff\32\56\4\uffff\1\56\1\uffff\32\56",
            "",
            "\1\u00d1",
            "\1\u00d2",
            "",
            "\1\56\13\uffff\12\56\7\uffff\32\56\4\uffff\1\56\1\uffff\32\56",
            "\1\56\13\uffff\12\56\7\uffff\32\56\4\uffff\1\56\1\uffff\32\56",
            "",
            ""
    };

    static final short[] DFA14_eot = DFA.unpackEncodedString(DFA14_eotS);
    static final short[] DFA14_eof = DFA.unpackEncodedString(DFA14_eofS);
    static final char[] DFA14_min = DFA.unpackEncodedStringToUnsignedChars(DFA14_minS);
    static final char[] DFA14_max = DFA.unpackEncodedStringToUnsignedChars(DFA14_maxS);
    static final short[] DFA14_accept = DFA.unpackEncodedString(DFA14_acceptS);
    static final short[] DFA14_special = DFA.unpackEncodedString(DFA14_specialS);
    static final short[][] DFA14_transition;

    static {
        int numStates = DFA14_transitionS.length;
        DFA14_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA14_transition[i] = DFA.unpackEncodedString(DFA14_transitionS[i]);
        }
    }

    class DFA14 extends DFA {

        public DFA14(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 14;
            this.eot = DFA14_eot;
            this.eof = DFA14_eof;
            this.min = DFA14_min;
            this.max = DFA14_max;
            this.accept = DFA14_accept;
            this.special = DFA14_special;
            this.transition = DFA14_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( T__20 | T__21 | T__22 | T__23 | T__24 | T__25 | T__26 | T__27 | T__28 | T__29 | T__30 | T__31 | T__32 | T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | RULE_PARAM_KEY | RULE_MACRO_KEY | RULE_FILTER_KEY | RULE_FUNCTION_KEY | RULE_MAPPER_KEY | RULE_VARIABLE_KEY | RULE_INPUT_KEY | RULE_OUTPUT_KEY | RULE_RETURN_KEY | RULE_INT | RULE_ID | RULE_STRING | RULE_ML_COMMENT | RULE_SL_COMMENT | RULE_WS | RULE_ANY_OTHER );";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            IntStream input = _input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA14_0 = input.LA(1);

                        s = -1;
                        if ( (LA14_0=='p') ) {s = 1;}

                        else if ( (LA14_0==';') ) {s = 2;}

                        else if ( (LA14_0=='i') ) {s = 3;}

                        else if ( (LA14_0=='M') ) {s = 4;}

                        else if ( (LA14_0==':') ) {s = 5;}

                        else if ( (LA14_0=='d') ) {s = 6;}

                        else if ( (LA14_0=='-') ) {s = 7;}

                        else if ( (LA14_0=='{') ) {s = 8;}

                        else if ( (LA14_0=='}') ) {s = 9;}

                        else if ( (LA14_0=='S') ) {s = 10;}

                        else if ( (LA14_0=='E') ) {s = 11;}

                        else if ( (LA14_0=='c') ) {s = 12;}

                        else if ( (LA14_0=='m') ) {s = 13;}

                        else if ( (LA14_0=='N') ) {s = 14;}

                        else if ( (LA14_0=='.') ) {s = 15;}

                        else if ( (LA14_0=='=') ) {s = 16;}

                        else if ( (LA14_0=='n') ) {s = 17;}

                        else if ( (LA14_0=='l') ) {s = 18;}

                        else if ( (LA14_0==',') ) {s = 19;}

                        else if ( (LA14_0=='[') ) {s = 20;}

                        else if ( (LA14_0==']') ) {s = 21;}

                        else if ( (LA14_0=='(') ) {s = 22;}

                        else if ( (LA14_0==')') ) {s = 23;}

                        else if ( (LA14_0=='<') ) {s = 24;}

                        else if ( (LA14_0=='>') ) {s = 25;}

                        else if ( (LA14_0=='?') ) {s = 26;}

                        else if ( (LA14_0=='e') ) {s = 27;}

                        else if ( (LA14_0=='&') ) {s = 28;}

                        else if ( (LA14_0=='s') ) {s = 29;}

                        else if ( (LA14_0=='*') ) {s = 30;}

                        else if ( (LA14_0=='P') ) {s = 31;}

                        else if ( (LA14_0=='F') ) {s = 32;}

                        else if ( (LA14_0=='V') ) {s = 33;}

                        else if ( (LA14_0=='I') ) {s = 34;}

                        else if ( (LA14_0=='O') ) {s = 35;}

                        else if ( (LA14_0=='R') ) {s = 36;}

                        else if ( ((LA14_0>='0' && LA14_0<='9')) ) {s = 37;}

                        else if ( (LA14_0=='^') ) {s = 38;}

                        else if ( (LA14_0=='$'||(LA14_0>='A' && LA14_0<='D')||(LA14_0>='G' && LA14_0<='H')||(LA14_0>='J' && LA14_0<='L')||LA14_0=='Q'||(LA14_0>='T' && LA14_0<='U')||(LA14_0>='W' && LA14_0<='Z')||LA14_0=='_'||(LA14_0>='a' && LA14_0<='b')||(LA14_0>='f' && LA14_0<='h')||(LA14_0>='j' && LA14_0<='k')||LA14_0=='o'||(LA14_0>='q' && LA14_0<='r')||(LA14_0>='t' && LA14_0<='z')) ) {s = 39;}

                        else if ( (LA14_0=='\"') ) {s = 40;}

                        else if ( (LA14_0=='\'') ) {s = 41;}

                        else if ( (LA14_0=='/') ) {s = 42;}

                        else if ( ((LA14_0>='\t' && LA14_0<='\n')||LA14_0=='\r'||LA14_0==' ') ) {s = 43;}

                        else if ( ((LA14_0>='\u0000' && LA14_0<='\b')||(LA14_0>='\u000B' && LA14_0<='\f')||(LA14_0>='\u000E' && LA14_0<='\u001F')||LA14_0=='!'||LA14_0=='#'||LA14_0=='%'||LA14_0=='+'||LA14_0=='@'||LA14_0=='\\'||LA14_0=='`'||LA14_0=='|'||(LA14_0>='~' && LA14_0<='\uFFFF')) ) {s = 44;}

                        if ( s>=0 ) return s;
                        break;
            }
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 14, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

}