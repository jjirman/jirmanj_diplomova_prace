package com.gk_software.core.dsl.xmapping.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import com.gk_software.core.dsl.xmapping.services.XMappingGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
@SuppressWarnings("all")
public class InternalXMappingParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_INPUT_KEY", "RULE_OUTPUT_KEY", "RULE_VARIABLE_KEY", "RULE_RETURN_KEY", "RULE_MACRO_KEY", "RULE_FILTER_KEY", "RULE_FUNCTION_KEY", "RULE_MAPPER_KEY", "RULE_PARAM_KEY", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'package'", "';'", "'import'", "'MappingClass'", "':'", "'def'", "'->'", "'{'", "'}'", "'StartMapping'", "'EndMapping'", "'call'", "'::'", "'map'", "'NULL'", "'.'", "'='", "'new'", "'-'", "'let'", "','", "'.*'", "'['", "']'", "'('", "')'", "'=>'", "'<'", "'>'", "'?'", "'extends'", "'&'", "'super'", "'static'", "'extension'", "'*'"
    };
    public static final int T__50=50;
    public static final int RULE_VARIABLE_KEY=9;
    public static final int RULE_OUTPUT_KEY=8;
    public static final int RULE_MACRO_KEY=11;
    public static final int RULE_RETURN_KEY=10;
    public static final int T__55=55;
    public static final int T__51=51;
    public static final int T__52=52;
    public static final int T__53=53;
    public static final int T__54=54;
    public static final int RULE_MAPPER_KEY=14;
    public static final int RULE_ID=4;
    public static final int RULE_INPUT_KEY=7;
    public static final int T__26=26;
    public static final int T__27=27;
    public static final int T__28=28;
    public static final int RULE_INT=6;
    public static final int T__29=29;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=16;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int RULE_PARAM_KEY=15;
    public static final int T__25=25;
    public static final int T__20=20;
    public static final int T__21=21;
    public static final int RULE_FUNCTION_KEY=13;
    public static final int RULE_STRING=5;
    public static final int RULE_SL_COMMENT=17;
    public static final int T__37=37;
    public static final int T__38=38;
    public static final int T__39=39;
    public static final int T__33=33;
    public static final int T__34=34;
    public static final int T__35=35;
    public static final int T__36=36;
    public static final int EOF=-1;
    public static final int T__30=30;
    public static final int T__31=31;
    public static final int T__32=32;
    public static final int RULE_WS=18;
    public static final int RULE_FILTER_KEY=12;
    public static final int RULE_ANY_OTHER=19;
    public static final int T__48=48;
    public static final int T__49=49;
    public static final int T__44=44;
    public static final int T__45=45;
    public static final int T__46=46;
    public static final int T__47=47;
    public static final int T__40=40;
    public static final int T__41=41;
    public static final int T__42=42;
    public static final int T__43=43;

    // delegates
    // delegators


        public InternalXMappingParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalXMappingParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalXMappingParser.tokenNames; }
    public String getGrammarFileName() { return "InternalXMapping.g"; }



    /*
      This grammar contains a lot of empty actions to work around a bug in ANTLR.
      Otherwise the ANTLR tool will create synpreds that cannot be compiled in some rare cases.
    */

     	private XMappingGrammarAccess grammarAccess;

        public InternalXMappingParser(TokenStream input, XMappingGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "Model";
       	}

       	@Override
       	protected XMappingGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleModel"
    // InternalXMapping.g:70:1: entryRuleModel returns [EObject current=null] : iv_ruleModel= ruleModel EOF ;
    public final EObject entryRuleModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleModel = null;


        try {
            // InternalXMapping.g:70:46: (iv_ruleModel= ruleModel EOF )
            // InternalXMapping.g:71:2: iv_ruleModel= ruleModel EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getModelRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleModel=ruleModel();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleModel; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleModel"


    // $ANTLR start "ruleModel"
    // InternalXMapping.g:77:1: ruleModel returns [EObject current=null] : (otherlv_0= 'package' ( (lv_name_1_0= ruleQualifiedName ) ) otherlv_2= ';' ( (lv_namespaceImports_3_0= ruleImport ) )* ( (lv_class_4_0= ruleMappingClass ) ) ) ;
    public final EObject ruleModel() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        AntlrDatatypeRuleToken lv_name_1_0 = null;

        EObject lv_namespaceImports_3_0 = null;

        EObject lv_class_4_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:83:2: ( (otherlv_0= 'package' ( (lv_name_1_0= ruleQualifiedName ) ) otherlv_2= ';' ( (lv_namespaceImports_3_0= ruleImport ) )* ( (lv_class_4_0= ruleMappingClass ) ) ) )
            // InternalXMapping.g:84:2: (otherlv_0= 'package' ( (lv_name_1_0= ruleQualifiedName ) ) otherlv_2= ';' ( (lv_namespaceImports_3_0= ruleImport ) )* ( (lv_class_4_0= ruleMappingClass ) ) )
            {
            // InternalXMapping.g:84:2: (otherlv_0= 'package' ( (lv_name_1_0= ruleQualifiedName ) ) otherlv_2= ';' ( (lv_namespaceImports_3_0= ruleImport ) )* ( (lv_class_4_0= ruleMappingClass ) ) )
            // InternalXMapping.g:85:3: otherlv_0= 'package' ( (lv_name_1_0= ruleQualifiedName ) ) otherlv_2= ';' ( (lv_namespaceImports_3_0= ruleImport ) )* ( (lv_class_4_0= ruleMappingClass ) )
            {
            otherlv_0=(Token)match(input,20,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getModelAccess().getPackageKeyword_0());
              		
            }
            // InternalXMapping.g:89:3: ( (lv_name_1_0= ruleQualifiedName ) )
            // InternalXMapping.g:90:4: (lv_name_1_0= ruleQualifiedName )
            {
            // InternalXMapping.g:90:4: (lv_name_1_0= ruleQualifiedName )
            // InternalXMapping.g:91:5: lv_name_1_0= ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getModelAccess().getNameQualifiedNameParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_4);
            lv_name_1_0=ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getModelRule());
              					}
              					set(
              						current,
              						"name",
              						lv_name_1_0,
              						"com.gk_software.core.dsl.xmapping.XMapping.QualifiedName");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,21,FOLLOW_5); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getModelAccess().getSemicolonKeyword_2());
              		
            }
            // InternalXMapping.g:112:3: ( (lv_namespaceImports_3_0= ruleImport ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==22) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalXMapping.g:113:4: (lv_namespaceImports_3_0= ruleImport )
            	    {
            	    // InternalXMapping.g:113:4: (lv_namespaceImports_3_0= ruleImport )
            	    // InternalXMapping.g:114:5: lv_namespaceImports_3_0= ruleImport
            	    {
            	    if ( state.backtracking==0 ) {

            	      					newCompositeNode(grammarAccess.getModelAccess().getNamespaceImportsImportParserRuleCall_3_0());
            	      				
            	    }
            	    pushFollow(FOLLOW_5);
            	    lv_namespaceImports_3_0=ruleImport();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      					if (current==null) {
            	      						current = createModelElementForParent(grammarAccess.getModelRule());
            	      					}
            	      					add(
            	      						current,
            	      						"namespaceImports",
            	      						lv_namespaceImports_3_0,
            	      						"com.gk_software.core.dsl.xmapping.XMapping.Import");
            	      					afterParserOrEnumRuleCall();
            	      				
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            // InternalXMapping.g:131:3: ( (lv_class_4_0= ruleMappingClass ) )
            // InternalXMapping.g:132:4: (lv_class_4_0= ruleMappingClass )
            {
            // InternalXMapping.g:132:4: (lv_class_4_0= ruleMappingClass )
            // InternalXMapping.g:133:5: lv_class_4_0= ruleMappingClass
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getModelAccess().getClassMappingClassParserRuleCall_4_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_class_4_0=ruleMappingClass();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getModelRule());
              					}
              					set(
              						current,
              						"class",
              						lv_class_4_0,
              						"com.gk_software.core.dsl.xmapping.XMapping.MappingClass");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleModel"


    // $ANTLR start "entryRuleImport"
    // InternalXMapping.g:154:1: entryRuleImport returns [EObject current=null] : iv_ruleImport= ruleImport EOF ;
    public final EObject entryRuleImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImport = null;


        try {
            // InternalXMapping.g:154:47: (iv_ruleImport= ruleImport EOF )
            // InternalXMapping.g:155:2: iv_ruleImport= ruleImport EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getImportRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleImport=ruleImport();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleImport; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImport"


    // $ANTLR start "ruleImport"
    // InternalXMapping.g:161:1: ruleImport returns [EObject current=null] : (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) otherlv_2= ';' ) ;
    public final EObject ruleImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        AntlrDatatypeRuleToken lv_importedNamespace_1_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:167:2: ( (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) otherlv_2= ';' ) )
            // InternalXMapping.g:168:2: (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) otherlv_2= ';' )
            {
            // InternalXMapping.g:168:2: (otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) otherlv_2= ';' )
            // InternalXMapping.g:169:3: otherlv_0= 'import' ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) ) otherlv_2= ';'
            {
            otherlv_0=(Token)match(input,22,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getImportAccess().getImportKeyword_0());
              		
            }
            // InternalXMapping.g:173:3: ( (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard ) )
            // InternalXMapping.g:174:4: (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard )
            {
            // InternalXMapping.g:174:4: (lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard )
            // InternalXMapping.g:175:5: lv_importedNamespace_1_0= ruleQualifiedNameWithWildcard
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getImportAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_4);
            lv_importedNamespace_1_0=ruleQualifiedNameWithWildcard();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getImportRule());
              					}
              					set(
              						current,
              						"importedNamespace",
              						lv_importedNamespace_1_0,
              						"com.gk_software.core.dsl.xmapping.XMapping.QualifiedNameWithWildcard");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,21,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getImportAccess().getSemicolonKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImport"


    // $ANTLR start "entryRuleMappingClass"
    // InternalXMapping.g:200:1: entryRuleMappingClass returns [EObject current=null] : iv_ruleMappingClass= ruleMappingClass EOF ;
    public final EObject entryRuleMappingClass() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMappingClass = null;


        try {
            // InternalXMapping.g:200:53: (iv_ruleMappingClass= ruleMappingClass EOF )
            // InternalXMapping.g:201:2: iv_ruleMappingClass= ruleMappingClass EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMappingClassRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleMappingClass=ruleMappingClass();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMappingClass; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMappingClass"


    // $ANTLR start "ruleMappingClass"
    // InternalXMapping.g:207:1: ruleMappingClass returns [EObject current=null] : (otherlv_0= 'MappingClass' ( (lv_name_1_0= RULE_ID ) ) ( (lv_xsdImport_2_0= ruleXsdImport ) ) ( (lv_block_3_0= ruleMappingBlock ) ) ( (lv_routineBlock_4_0= ruleRoutineBlock ) )* ) ;
    public final EObject ruleMappingClass() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        EObject lv_xsdImport_2_0 = null;

        EObject lv_block_3_0 = null;

        EObject lv_routineBlock_4_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:213:2: ( (otherlv_0= 'MappingClass' ( (lv_name_1_0= RULE_ID ) ) ( (lv_xsdImport_2_0= ruleXsdImport ) ) ( (lv_block_3_0= ruleMappingBlock ) ) ( (lv_routineBlock_4_0= ruleRoutineBlock ) )* ) )
            // InternalXMapping.g:214:2: (otherlv_0= 'MappingClass' ( (lv_name_1_0= RULE_ID ) ) ( (lv_xsdImport_2_0= ruleXsdImport ) ) ( (lv_block_3_0= ruleMappingBlock ) ) ( (lv_routineBlock_4_0= ruleRoutineBlock ) )* )
            {
            // InternalXMapping.g:214:2: (otherlv_0= 'MappingClass' ( (lv_name_1_0= RULE_ID ) ) ( (lv_xsdImport_2_0= ruleXsdImport ) ) ( (lv_block_3_0= ruleMappingBlock ) ) ( (lv_routineBlock_4_0= ruleRoutineBlock ) )* )
            // InternalXMapping.g:215:3: otherlv_0= 'MappingClass' ( (lv_name_1_0= RULE_ID ) ) ( (lv_xsdImport_2_0= ruleXsdImport ) ) ( (lv_block_3_0= ruleMappingBlock ) ) ( (lv_routineBlock_4_0= ruleRoutineBlock ) )*
            {
            otherlv_0=(Token)match(input,23,FOLLOW_6); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getMappingClassAccess().getMappingClassKeyword_0());
              		
            }
            // InternalXMapping.g:219:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalXMapping.g:220:4: (lv_name_1_0= RULE_ID )
            {
            // InternalXMapping.g:220:4: (lv_name_1_0= RULE_ID )
            // InternalXMapping.g:221:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_7); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					newLeafNode(lv_name_1_0, grammarAccess.getMappingClassAccess().getNameIDTerminalRuleCall_1_0());
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getMappingClassRule());
              					}
              					setWithLastConsumed(
              						current,
              						"name",
              						lv_name_1_0,
              						"org.eclipse.xtext.xbase.Xtype.ID");
              				
            }

            }


            }

            // InternalXMapping.g:237:3: ( (lv_xsdImport_2_0= ruleXsdImport ) )
            // InternalXMapping.g:238:4: (lv_xsdImport_2_0= ruleXsdImport )
            {
            // InternalXMapping.g:238:4: (lv_xsdImport_2_0= ruleXsdImport )
            // InternalXMapping.g:239:5: lv_xsdImport_2_0= ruleXsdImport
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getMappingClassAccess().getXsdImportXsdImportParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_8);
            lv_xsdImport_2_0=ruleXsdImport();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getMappingClassRule());
              					}
              					set(
              						current,
              						"xsdImport",
              						lv_xsdImport_2_0,
              						"com.gk_software.core.dsl.xmapping.XMapping.XsdImport");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalXMapping.g:256:3: ( (lv_block_3_0= ruleMappingBlock ) )
            // InternalXMapping.g:257:4: (lv_block_3_0= ruleMappingBlock )
            {
            // InternalXMapping.g:257:4: (lv_block_3_0= ruleMappingBlock )
            // InternalXMapping.g:258:5: lv_block_3_0= ruleMappingBlock
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getMappingClassAccess().getBlockMappingBlockParserRuleCall_3_0());
              				
            }
            pushFollow(FOLLOW_9);
            lv_block_3_0=ruleMappingBlock();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getMappingClassRule());
              					}
              					set(
              						current,
              						"block",
              						lv_block_3_0,
              						"com.gk_software.core.dsl.xmapping.XMapping.MappingBlock");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalXMapping.g:275:3: ( (lv_routineBlock_4_0= ruleRoutineBlock ) )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==25) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalXMapping.g:276:4: (lv_routineBlock_4_0= ruleRoutineBlock )
            	    {
            	    // InternalXMapping.g:276:4: (lv_routineBlock_4_0= ruleRoutineBlock )
            	    // InternalXMapping.g:277:5: lv_routineBlock_4_0= ruleRoutineBlock
            	    {
            	    if ( state.backtracking==0 ) {

            	      					newCompositeNode(grammarAccess.getMappingClassAccess().getRoutineBlockRoutineBlockParserRuleCall_4_0());
            	      				
            	    }
            	    pushFollow(FOLLOW_9);
            	    lv_routineBlock_4_0=ruleRoutineBlock();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      					if (current==null) {
            	      						current = createModelElementForParent(grammarAccess.getMappingClassRule());
            	      					}
            	      					add(
            	      						current,
            	      						"routineBlock",
            	      						lv_routineBlock_4_0,
            	      						"com.gk_software.core.dsl.xmapping.XMapping.RoutineBlock");
            	      					afterParserOrEnumRuleCall();
            	      				
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMappingClass"


    // $ANTLR start "entryRuleXsdImport"
    // InternalXMapping.g:298:1: entryRuleXsdImport returns [EObject current=null] : iv_ruleXsdImport= ruleXsdImport EOF ;
    public final EObject entryRuleXsdImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXsdImport = null;


        try {
            // InternalXMapping.g:298:50: (iv_ruleXsdImport= ruleXsdImport EOF )
            // InternalXMapping.g:299:2: iv_ruleXsdImport= ruleXsdImport EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXsdImportRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleXsdImport=ruleXsdImport();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXsdImport; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXsdImport"


    // $ANTLR start "ruleXsdImport"
    // InternalXMapping.g:305:1: ruleXsdImport returns [EObject current=null] : ( ( (lv_inputImport_0_0= ruleInputImport ) )+ ( (lv_outputImport_1_0= ruleOutputImport ) )+ ) ;
    public final EObject ruleXsdImport() throws RecognitionException {
        EObject current = null;

        EObject lv_inputImport_0_0 = null;

        EObject lv_outputImport_1_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:311:2: ( ( ( (lv_inputImport_0_0= ruleInputImport ) )+ ( (lv_outputImport_1_0= ruleOutputImport ) )+ ) )
            // InternalXMapping.g:312:2: ( ( (lv_inputImport_0_0= ruleInputImport ) )+ ( (lv_outputImport_1_0= ruleOutputImport ) )+ )
            {
            // InternalXMapping.g:312:2: ( ( (lv_inputImport_0_0= ruleInputImport ) )+ ( (lv_outputImport_1_0= ruleOutputImport ) )+ )
            // InternalXMapping.g:313:3: ( (lv_inputImport_0_0= ruleInputImport ) )+ ( (lv_outputImport_1_0= ruleOutputImport ) )+
            {
            // InternalXMapping.g:313:3: ( (lv_inputImport_0_0= ruleInputImport ) )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==RULE_INPUT_KEY) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // InternalXMapping.g:314:4: (lv_inputImport_0_0= ruleInputImport )
            	    {
            	    // InternalXMapping.g:314:4: (lv_inputImport_0_0= ruleInputImport )
            	    // InternalXMapping.g:315:5: lv_inputImport_0_0= ruleInputImport
            	    {
            	    if ( state.backtracking==0 ) {

            	      					newCompositeNode(grammarAccess.getXsdImportAccess().getInputImportInputImportParserRuleCall_0_0());
            	      				
            	    }
            	    pushFollow(FOLLOW_10);
            	    lv_inputImport_0_0=ruleInputImport();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      					if (current==null) {
            	      						current = createModelElementForParent(grammarAccess.getXsdImportRule());
            	      					}
            	      					add(
            	      						current,
            	      						"inputImport",
            	      						lv_inputImport_0_0,
            	      						"com.gk_software.core.dsl.xmapping.XMapping.InputImport");
            	      					afterParserOrEnumRuleCall();
            	      				
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
            	    if (state.backtracking>0) {state.failed=true; return current;}
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);

            // InternalXMapping.g:332:3: ( (lv_outputImport_1_0= ruleOutputImport ) )+
            int cnt4=0;
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==RULE_OUTPUT_KEY) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // InternalXMapping.g:333:4: (lv_outputImport_1_0= ruleOutputImport )
            	    {
            	    // InternalXMapping.g:333:4: (lv_outputImport_1_0= ruleOutputImport )
            	    // InternalXMapping.g:334:5: lv_outputImport_1_0= ruleOutputImport
            	    {
            	    if ( state.backtracking==0 ) {

            	      					newCompositeNode(grammarAccess.getXsdImportAccess().getOutputImportOutputImportParserRuleCall_1_0());
            	      				
            	    }
            	    pushFollow(FOLLOW_11);
            	    lv_outputImport_1_0=ruleOutputImport();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      					if (current==null) {
            	      						current = createModelElementForParent(grammarAccess.getXsdImportRule());
            	      					}
            	      					add(
            	      						current,
            	      						"outputImport",
            	      						lv_outputImport_1_0,
            	      						"com.gk_software.core.dsl.xmapping.XMapping.OutputImport");
            	      					afterParserOrEnumRuleCall();
            	      				
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt4 >= 1 ) break loop4;
            	    if (state.backtracking>0) {state.failed=true; return current;}
                        EarlyExitException eee =
                            new EarlyExitException(4, input);
                        throw eee;
                }
                cnt4++;
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXsdImport"


    // $ANTLR start "entryRuleInputImport"
    // InternalXMapping.g:355:1: entryRuleInputImport returns [EObject current=null] : iv_ruleInputImport= ruleInputImport EOF ;
    public final EObject entryRuleInputImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInputImport = null;


        try {
            // InternalXMapping.g:355:52: (iv_ruleInputImport= ruleInputImport EOF )
            // InternalXMapping.g:356:2: iv_ruleInputImport= ruleInputImport EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInputImportRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleInputImport=ruleInputImport();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInputImport; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInputImport"


    // $ANTLR start "ruleInputImport"
    // InternalXMapping.g:362:1: ruleInputImport returns [EObject current=null] : ( ( (lv_name_0_0= ruleInput ) ) otherlv_1= ':' ( (lv_path_2_0= rulePath ) ) otherlv_3= ';' ) ;
    public final EObject ruleInputImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_name_0_0 = null;

        AntlrDatatypeRuleToken lv_path_2_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:368:2: ( ( ( (lv_name_0_0= ruleInput ) ) otherlv_1= ':' ( (lv_path_2_0= rulePath ) ) otherlv_3= ';' ) )
            // InternalXMapping.g:369:2: ( ( (lv_name_0_0= ruleInput ) ) otherlv_1= ':' ( (lv_path_2_0= rulePath ) ) otherlv_3= ';' )
            {
            // InternalXMapping.g:369:2: ( ( (lv_name_0_0= ruleInput ) ) otherlv_1= ':' ( (lv_path_2_0= rulePath ) ) otherlv_3= ';' )
            // InternalXMapping.g:370:3: ( (lv_name_0_0= ruleInput ) ) otherlv_1= ':' ( (lv_path_2_0= rulePath ) ) otherlv_3= ';'
            {
            // InternalXMapping.g:370:3: ( (lv_name_0_0= ruleInput ) )
            // InternalXMapping.g:371:4: (lv_name_0_0= ruleInput )
            {
            // InternalXMapping.g:371:4: (lv_name_0_0= ruleInput )
            // InternalXMapping.g:372:5: lv_name_0_0= ruleInput
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getInputImportAccess().getNameInputParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_12);
            lv_name_0_0=ruleInput();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getInputImportRule());
              					}
              					set(
              						current,
              						"name",
              						lv_name_0_0,
              						"com.gk_software.core.dsl.xmapping.XMapping.Input");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_1=(Token)match(input,24,FOLLOW_13); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getInputImportAccess().getColonKeyword_1());
              		
            }
            // InternalXMapping.g:393:3: ( (lv_path_2_0= rulePath ) )
            // InternalXMapping.g:394:4: (lv_path_2_0= rulePath )
            {
            // InternalXMapping.g:394:4: (lv_path_2_0= rulePath )
            // InternalXMapping.g:395:5: lv_path_2_0= rulePath
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getInputImportAccess().getPathPathParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_4);
            lv_path_2_0=rulePath();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getInputImportRule());
              					}
              					set(
              						current,
              						"path",
              						lv_path_2_0,
              						"com.gk_software.core.dsl.xmapping.XMapping.Path");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_3=(Token)match(input,21,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getInputImportAccess().getSemicolonKeyword_3());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInputImport"


    // $ANTLR start "entryRuleOutputImport"
    // InternalXMapping.g:420:1: entryRuleOutputImport returns [EObject current=null] : iv_ruleOutputImport= ruleOutputImport EOF ;
    public final EObject entryRuleOutputImport() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOutputImport = null;


        try {
            // InternalXMapping.g:420:53: (iv_ruleOutputImport= ruleOutputImport EOF )
            // InternalXMapping.g:421:2: iv_ruleOutputImport= ruleOutputImport EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOutputImportRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleOutputImport=ruleOutputImport();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOutputImport; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutputImport"


    // $ANTLR start "ruleOutputImport"
    // InternalXMapping.g:427:1: ruleOutputImport returns [EObject current=null] : ( ( (lv_name_0_0= ruleOutput ) ) otherlv_1= ':' ( (lv_path_2_0= rulePath ) ) otherlv_3= ';' ) ;
    public final EObject ruleOutputImport() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_name_0_0 = null;

        AntlrDatatypeRuleToken lv_path_2_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:433:2: ( ( ( (lv_name_0_0= ruleOutput ) ) otherlv_1= ':' ( (lv_path_2_0= rulePath ) ) otherlv_3= ';' ) )
            // InternalXMapping.g:434:2: ( ( (lv_name_0_0= ruleOutput ) ) otherlv_1= ':' ( (lv_path_2_0= rulePath ) ) otherlv_3= ';' )
            {
            // InternalXMapping.g:434:2: ( ( (lv_name_0_0= ruleOutput ) ) otherlv_1= ':' ( (lv_path_2_0= rulePath ) ) otherlv_3= ';' )
            // InternalXMapping.g:435:3: ( (lv_name_0_0= ruleOutput ) ) otherlv_1= ':' ( (lv_path_2_0= rulePath ) ) otherlv_3= ';'
            {
            // InternalXMapping.g:435:3: ( (lv_name_0_0= ruleOutput ) )
            // InternalXMapping.g:436:4: (lv_name_0_0= ruleOutput )
            {
            // InternalXMapping.g:436:4: (lv_name_0_0= ruleOutput )
            // InternalXMapping.g:437:5: lv_name_0_0= ruleOutput
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getOutputImportAccess().getNameOutputParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_12);
            lv_name_0_0=ruleOutput();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getOutputImportRule());
              					}
              					set(
              						current,
              						"name",
              						lv_name_0_0,
              						"com.gk_software.core.dsl.xmapping.XMapping.Output");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_1=(Token)match(input,24,FOLLOW_13); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getOutputImportAccess().getColonKeyword_1());
              		
            }
            // InternalXMapping.g:458:3: ( (lv_path_2_0= rulePath ) )
            // InternalXMapping.g:459:4: (lv_path_2_0= rulePath )
            {
            // InternalXMapping.g:459:4: (lv_path_2_0= rulePath )
            // InternalXMapping.g:460:5: lv_path_2_0= rulePath
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getOutputImportAccess().getPathPathParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_4);
            lv_path_2_0=rulePath();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getOutputImportRule());
              					}
              					set(
              						current,
              						"path",
              						lv_path_2_0,
              						"com.gk_software.core.dsl.xmapping.XMapping.Path");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_3=(Token)match(input,21,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getOutputImportAccess().getSemicolonKeyword_3());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutputImport"


    // $ANTLR start "entryRuleRoutineBlock"
    // InternalXMapping.g:485:1: entryRuleRoutineBlock returns [EObject current=null] : iv_ruleRoutineBlock= ruleRoutineBlock EOF ;
    public final EObject entryRuleRoutineBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRoutineBlock = null;


        try {
            // InternalXMapping.g:485:53: (iv_ruleRoutineBlock= ruleRoutineBlock EOF )
            // InternalXMapping.g:486:2: iv_ruleRoutineBlock= ruleRoutineBlock EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRoutineBlockRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleRoutineBlock=ruleRoutineBlock();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRoutineBlock; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRoutineBlock"


    // $ANTLR start "ruleRoutineBlock"
    // InternalXMapping.g:492:1: ruleRoutineBlock returns [EObject current=null] : (otherlv_0= 'def' ( ( (lv_params_1_0= ruleParamSection ) ) otherlv_2= '->' )? ( (lv_routineName_3_0= ruleRoutine ) ) otherlv_4= '{' ( (lv_routineBlock_5_0= ruleBlock ) ) otherlv_6= '}' ) ;
    public final EObject ruleRoutineBlock() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        EObject lv_params_1_0 = null;

        EObject lv_routineName_3_0 = null;

        EObject lv_routineBlock_5_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:498:2: ( (otherlv_0= 'def' ( ( (lv_params_1_0= ruleParamSection ) ) otherlv_2= '->' )? ( (lv_routineName_3_0= ruleRoutine ) ) otherlv_4= '{' ( (lv_routineBlock_5_0= ruleBlock ) ) otherlv_6= '}' ) )
            // InternalXMapping.g:499:2: (otherlv_0= 'def' ( ( (lv_params_1_0= ruleParamSection ) ) otherlv_2= '->' )? ( (lv_routineName_3_0= ruleRoutine ) ) otherlv_4= '{' ( (lv_routineBlock_5_0= ruleBlock ) ) otherlv_6= '}' )
            {
            // InternalXMapping.g:499:2: (otherlv_0= 'def' ( ( (lv_params_1_0= ruleParamSection ) ) otherlv_2= '->' )? ( (lv_routineName_3_0= ruleRoutine ) ) otherlv_4= '{' ( (lv_routineBlock_5_0= ruleBlock ) ) otherlv_6= '}' )
            // InternalXMapping.g:500:3: otherlv_0= 'def' ( ( (lv_params_1_0= ruleParamSection ) ) otherlv_2= '->' )? ( (lv_routineName_3_0= ruleRoutine ) ) otherlv_4= '{' ( (lv_routineBlock_5_0= ruleBlock ) ) otherlv_6= '}'
            {
            otherlv_0=(Token)match(input,25,FOLLOW_14); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getRoutineBlockAccess().getDefKeyword_0());
              		
            }
            // InternalXMapping.g:504:3: ( ( (lv_params_1_0= ruleParamSection ) ) otherlv_2= '->' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==RULE_PARAM_KEY) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalXMapping.g:505:4: ( (lv_params_1_0= ruleParamSection ) ) otherlv_2= '->'
                    {
                    // InternalXMapping.g:505:4: ( (lv_params_1_0= ruleParamSection ) )
                    // InternalXMapping.g:506:5: (lv_params_1_0= ruleParamSection )
                    {
                    // InternalXMapping.g:506:5: (lv_params_1_0= ruleParamSection )
                    // InternalXMapping.g:507:6: lv_params_1_0= ruleParamSection
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getRoutineBlockAccess().getParamsParamSectionParserRuleCall_1_0_0());
                      					
                    }
                    pushFollow(FOLLOW_15);
                    lv_params_1_0=ruleParamSection();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getRoutineBlockRule());
                      						}
                      						set(
                      							current,
                      							"params",
                      							lv_params_1_0,
                      							"com.gk_software.core.dsl.xmapping.XMapping.ParamSection");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    otherlv_2=(Token)match(input,26,FOLLOW_14); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getRoutineBlockAccess().getHyphenMinusGreaterThanSignKeyword_1_1());
                      			
                    }

                    }
                    break;

            }

            // InternalXMapping.g:529:3: ( (lv_routineName_3_0= ruleRoutine ) )
            // InternalXMapping.g:530:4: (lv_routineName_3_0= ruleRoutine )
            {
            // InternalXMapping.g:530:4: (lv_routineName_3_0= ruleRoutine )
            // InternalXMapping.g:531:5: lv_routineName_3_0= ruleRoutine
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getRoutineBlockAccess().getRoutineNameRoutineParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_16);
            lv_routineName_3_0=ruleRoutine();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getRoutineBlockRule());
              					}
              					set(
              						current,
              						"routineName",
              						lv_routineName_3_0,
              						"com.gk_software.core.dsl.xmapping.XMapping.Routine");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_4=(Token)match(input,27,FOLLOW_17); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_4, grammarAccess.getRoutineBlockAccess().getLeftCurlyBracketKeyword_3());
              		
            }
            // InternalXMapping.g:552:3: ( (lv_routineBlock_5_0= ruleBlock ) )
            // InternalXMapping.g:553:4: (lv_routineBlock_5_0= ruleBlock )
            {
            // InternalXMapping.g:553:4: (lv_routineBlock_5_0= ruleBlock )
            // InternalXMapping.g:554:5: lv_routineBlock_5_0= ruleBlock
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getRoutineBlockAccess().getRoutineBlockBlockParserRuleCall_4_0());
              				
            }
            pushFollow(FOLLOW_18);
            lv_routineBlock_5_0=ruleBlock();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getRoutineBlockRule());
              					}
              					set(
              						current,
              						"routineBlock",
              						lv_routineBlock_5_0,
              						"com.gk_software.core.dsl.xmapping.XMapping.Block");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_6=(Token)match(input,28,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_6, grammarAccess.getRoutineBlockAccess().getRightCurlyBracketKeyword_5());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRoutineBlock"


    // $ANTLR start "entryRuleMappingBlock"
    // InternalXMapping.g:579:1: entryRuleMappingBlock returns [EObject current=null] : iv_ruleMappingBlock= ruleMappingBlock EOF ;
    public final EObject entryRuleMappingBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMappingBlock = null;


        try {
            // InternalXMapping.g:579:53: (iv_ruleMappingBlock= ruleMappingBlock EOF )
            // InternalXMapping.g:580:2: iv_ruleMappingBlock= ruleMappingBlock EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMappingBlockRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleMappingBlock=ruleMappingBlock();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMappingBlock; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMappingBlock"


    // $ANTLR start "ruleMappingBlock"
    // InternalXMapping.g:586:1: ruleMappingBlock returns [EObject current=null] : (otherlv_0= 'StartMapping' ( (lv_block_1_0= ruleBlock ) ) otherlv_2= 'EndMapping' ) ;
    public final EObject ruleMappingBlock() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_block_1_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:592:2: ( (otherlv_0= 'StartMapping' ( (lv_block_1_0= ruleBlock ) ) otherlv_2= 'EndMapping' ) )
            // InternalXMapping.g:593:2: (otherlv_0= 'StartMapping' ( (lv_block_1_0= ruleBlock ) ) otherlv_2= 'EndMapping' )
            {
            // InternalXMapping.g:593:2: (otherlv_0= 'StartMapping' ( (lv_block_1_0= ruleBlock ) ) otherlv_2= 'EndMapping' )
            // InternalXMapping.g:594:3: otherlv_0= 'StartMapping' ( (lv_block_1_0= ruleBlock ) ) otherlv_2= 'EndMapping'
            {
            otherlv_0=(Token)match(input,29,FOLLOW_19); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getMappingBlockAccess().getStartMappingKeyword_0());
              		
            }
            // InternalXMapping.g:598:3: ( (lv_block_1_0= ruleBlock ) )
            // InternalXMapping.g:599:4: (lv_block_1_0= ruleBlock )
            {
            // InternalXMapping.g:599:4: (lv_block_1_0= ruleBlock )
            // InternalXMapping.g:600:5: lv_block_1_0= ruleBlock
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getMappingBlockAccess().getBlockBlockParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_20);
            lv_block_1_0=ruleBlock();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getMappingBlockRule());
              					}
              					set(
              						current,
              						"block",
              						lv_block_1_0,
              						"com.gk_software.core.dsl.xmapping.XMapping.Block");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_2=(Token)match(input,30,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getMappingBlockAccess().getEndMappingKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMappingBlock"


    // $ANTLR start "entryRuleBlock"
    // InternalXMapping.g:625:1: entryRuleBlock returns [EObject current=null] : iv_ruleBlock= ruleBlock EOF ;
    public final EObject entryRuleBlock() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleBlock = null;


        try {
            // InternalXMapping.g:625:46: (iv_ruleBlock= ruleBlock EOF )
            // InternalXMapping.g:626:2: iv_ruleBlock= ruleBlock EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getBlockRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleBlock=ruleBlock();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleBlock; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleBlock"


    // $ANTLR start "ruleBlock"
    // InternalXMapping.g:632:1: ruleBlock returns [EObject current=null] : ( () ( (lv_statement_1_0= ruleStatement ) )* ) ;
    public final EObject ruleBlock() throws RecognitionException {
        EObject current = null;

        EObject lv_statement_1_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:638:2: ( ( () ( (lv_statement_1_0= ruleStatement ) )* ) )
            // InternalXMapping.g:639:2: ( () ( (lv_statement_1_0= ruleStatement ) )* )
            {
            // InternalXMapping.g:639:2: ( () ( (lv_statement_1_0= ruleStatement ) )* )
            // InternalXMapping.g:640:3: () ( (lv_statement_1_0= ruleStatement ) )*
            {
            // InternalXMapping.g:640:3: ()
            // InternalXMapping.g:641:4: 
            {
            if ( state.backtracking==0 ) {

              				/* */
              			
            }
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getBlockAccess().getBlockAction_0(),
              					current);
              			
            }

            }

            // InternalXMapping.g:650:3: ( (lv_statement_1_0= ruleStatement ) )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==RULE_ID||LA6_0==RULE_INPUT_KEY||(LA6_0>=RULE_VARIABLE_KEY && LA6_0<=RULE_RETURN_KEY)||LA6_0==RULE_PARAM_KEY||LA6_0==20||LA6_0==22||LA6_0==25||LA6_0==31||LA6_0==33||LA6_0==39) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // InternalXMapping.g:651:4: (lv_statement_1_0= ruleStatement )
            	    {
            	    // InternalXMapping.g:651:4: (lv_statement_1_0= ruleStatement )
            	    // InternalXMapping.g:652:5: lv_statement_1_0= ruleStatement
            	    {
            	    if ( state.backtracking==0 ) {

            	      					newCompositeNode(grammarAccess.getBlockAccess().getStatementStatementParserRuleCall_1_0());
            	      				
            	    }
            	    pushFollow(FOLLOW_21);
            	    lv_statement_1_0=ruleStatement();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      					if (current==null) {
            	      						current = createModelElementForParent(grammarAccess.getBlockRule());
            	      					}
            	      					add(
            	      						current,
            	      						"statement",
            	      						lv_statement_1_0,
            	      						"com.gk_software.core.dsl.xmapping.XMapping.Statement");
            	      					afterParserOrEnumRuleCall();
            	      				
            	    }

            	    }


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleBlock"


    // $ANTLR start "entryRuleStatement"
    // InternalXMapping.g:673:1: entryRuleStatement returns [EObject current=null] : iv_ruleStatement= ruleStatement EOF ;
    public final EObject entryRuleStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleStatement = null;


        try {
            // InternalXMapping.g:673:50: (iv_ruleStatement= ruleStatement EOF )
            // InternalXMapping.g:674:2: iv_ruleStatement= ruleStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleStatement=ruleStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleStatement"


    // $ANTLR start "ruleStatement"
    // InternalXMapping.g:680:1: ruleStatement returns [EObject current=null] : (this_MapForwardStatement_0= ruleMapForwardStatement | this_MapStatement_1= ruleMapStatement | this_AssignmentStatement_2= ruleAssignmentStatement | this_CallStatement_3= ruleCallStatement ) ;
    public final EObject ruleStatement() throws RecognitionException {
        EObject current = null;

        EObject this_MapForwardStatement_0 = null;

        EObject this_MapStatement_1 = null;

        EObject this_AssignmentStatement_2 = null;

        EObject this_CallStatement_3 = null;



        	enterRule();

        try {
            // InternalXMapping.g:686:2: ( (this_MapForwardStatement_0= ruleMapForwardStatement | this_MapStatement_1= ruleMapStatement | this_AssignmentStatement_2= ruleAssignmentStatement | this_CallStatement_3= ruleCallStatement ) )
            // InternalXMapping.g:687:2: (this_MapForwardStatement_0= ruleMapForwardStatement | this_MapStatement_1= ruleMapStatement | this_AssignmentStatement_2= ruleAssignmentStatement | this_CallStatement_3= ruleCallStatement )
            {
            // InternalXMapping.g:687:2: (this_MapForwardStatement_0= ruleMapForwardStatement | this_MapStatement_1= ruleMapStatement | this_AssignmentStatement_2= ruleAssignmentStatement | this_CallStatement_3= ruleCallStatement )
            int alt7=4;
            alt7 = dfa7.predict(input);
            switch (alt7) {
                case 1 :
                    // InternalXMapping.g:688:3: this_MapForwardStatement_0= ruleMapForwardStatement
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getStatementAccess().getMapForwardStatementParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_MapForwardStatement_0=ruleMapForwardStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_MapForwardStatement_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalXMapping.g:700:3: this_MapStatement_1= ruleMapStatement
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getStatementAccess().getMapStatementParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_MapStatement_1=ruleMapStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_MapStatement_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalXMapping.g:712:3: this_AssignmentStatement_2= ruleAssignmentStatement
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getStatementAccess().getAssignmentStatementParserRuleCall_2());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_AssignmentStatement_2=ruleAssignmentStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_AssignmentStatement_2;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalXMapping.g:724:3: this_CallStatement_3= ruleCallStatement
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getStatementAccess().getCallStatementParserRuleCall_3());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_CallStatement_3=ruleCallStatement();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_CallStatement_3;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleStatement"


    // $ANTLR start "entryRuleCallStatement"
    // InternalXMapping.g:739:1: entryRuleCallStatement returns [EObject current=null] : iv_ruleCallStatement= ruleCallStatement EOF ;
    public final EObject entryRuleCallStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCallStatement = null;


        try {
            // InternalXMapping.g:739:54: (iv_ruleCallStatement= ruleCallStatement EOF )
            // InternalXMapping.g:740:2: iv_ruleCallStatement= ruleCallStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCallStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleCallStatement=ruleCallStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCallStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCallStatement"


    // $ANTLR start "ruleCallStatement"
    // InternalXMapping.g:746:1: ruleCallStatement returns [EObject current=null] : (otherlv_0= 'call' ( ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->' )? ( ( ( ruleQualifiedName ) ) () otherlv_5= '::' ) ( ( ruleName ) ) otherlv_7= '->' ( (lv_outputSection_8_0= ruleOutputSection ) ) otherlv_9= ';' ) ;
    public final EObject ruleCallStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        EObject lv_inputSection_1_0 = null;

        EObject lv_outputSection_8_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:752:2: ( (otherlv_0= 'call' ( ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->' )? ( ( ( ruleQualifiedName ) ) () otherlv_5= '::' ) ( ( ruleName ) ) otherlv_7= '->' ( (lv_outputSection_8_0= ruleOutputSection ) ) otherlv_9= ';' ) )
            // InternalXMapping.g:753:2: (otherlv_0= 'call' ( ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->' )? ( ( ( ruleQualifiedName ) ) () otherlv_5= '::' ) ( ( ruleName ) ) otherlv_7= '->' ( (lv_outputSection_8_0= ruleOutputSection ) ) otherlv_9= ';' )
            {
            // InternalXMapping.g:753:2: (otherlv_0= 'call' ( ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->' )? ( ( ( ruleQualifiedName ) ) () otherlv_5= '::' ) ( ( ruleName ) ) otherlv_7= '->' ( (lv_outputSection_8_0= ruleOutputSection ) ) otherlv_9= ';' )
            // InternalXMapping.g:754:3: otherlv_0= 'call' ( ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->' )? ( ( ( ruleQualifiedName ) ) () otherlv_5= '::' ) ( ( ruleName ) ) otherlv_7= '->' ( (lv_outputSection_8_0= ruleOutputSection ) ) otherlv_9= ';'
            {
            otherlv_0=(Token)match(input,31,FOLLOW_22); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getCallStatementAccess().getCallKeyword_0());
              		
            }
            // InternalXMapping.g:758:3: ( ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->' )?
            int alt8=2;
            alt8 = dfa8.predict(input);
            switch (alt8) {
                case 1 :
                    // InternalXMapping.g:759:4: ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->'
                    {
                    // InternalXMapping.g:759:4: ( (lv_inputSection_1_0= ruleInputSection ) )
                    // InternalXMapping.g:760:5: (lv_inputSection_1_0= ruleInputSection )
                    {
                    // InternalXMapping.g:760:5: (lv_inputSection_1_0= ruleInputSection )
                    // InternalXMapping.g:761:6: lv_inputSection_1_0= ruleInputSection
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getCallStatementAccess().getInputSectionInputSectionParserRuleCall_1_0_0());
                      					
                    }
                    pushFollow(FOLLOW_15);
                    lv_inputSection_1_0=ruleInputSection();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getCallStatementRule());
                      						}
                      						set(
                      							current,
                      							"inputSection",
                      							lv_inputSection_1_0,
                      							"com.gk_software.core.dsl.xmapping.XMapping.InputSection");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    otherlv_2=(Token)match(input,26,FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getCallStatementAccess().getHyphenMinusGreaterThanSignKeyword_1_1());
                      			
                    }

                    }
                    break;

            }

            // InternalXMapping.g:783:3: ( ( ( ruleQualifiedName ) ) () otherlv_5= '::' )
            // InternalXMapping.g:784:4: ( ( ruleQualifiedName ) ) () otherlv_5= '::'
            {
            // InternalXMapping.g:784:4: ( ( ruleQualifiedName ) )
            // InternalXMapping.g:785:5: ( ruleQualifiedName )
            {
            // InternalXMapping.g:785:5: ( ruleQualifiedName )
            // InternalXMapping.g:786:6: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              						/* */
              					
            }
            if ( state.backtracking==0 ) {

              						if (current==null) {
              							current = createModelElement(grammarAccess.getCallStatementRule());
              						}
              					
            }
            if ( state.backtracking==0 ) {

              						newCompositeNode(grammarAccess.getCallStatementAccess().getJavaClassJvmTypeCrossReference_2_0_0());
              					
            }
            pushFollow(FOLLOW_23);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              						afterParserOrEnumRuleCall();
              					
            }

            }


            }

            // InternalXMapping.g:803:4: ()
            // InternalXMapping.g:804:5: 
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					current = forceCreateModelElementAndSet(
              						grammarAccess.getCallStatementAccess().getCallStatementTargetAction_2_1(),
              						current);
              				
            }

            }

            otherlv_5=(Token)match(input,32,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				newLeafNode(otherlv_5, grammarAccess.getCallStatementAccess().getColonColonKeyword_2_2());
              			
            }

            }

            // InternalXMapping.g:818:3: ( ( ruleName ) )
            // InternalXMapping.g:819:4: ( ruleName )
            {
            // InternalXMapping.g:819:4: ( ruleName )
            // InternalXMapping.g:820:5: ruleName
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getCallStatementRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getCallStatementAccess().getJava_methodJvmOperationCrossReference_3_0());
              				
            }
            pushFollow(FOLLOW_15);
            ruleName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_7=(Token)match(input,26,FOLLOW_24); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_7, grammarAccess.getCallStatementAccess().getHyphenMinusGreaterThanSignKeyword_4());
              		
            }
            // InternalXMapping.g:841:3: ( (lv_outputSection_8_0= ruleOutputSection ) )
            // InternalXMapping.g:842:4: (lv_outputSection_8_0= ruleOutputSection )
            {
            // InternalXMapping.g:842:4: (lv_outputSection_8_0= ruleOutputSection )
            // InternalXMapping.g:843:5: lv_outputSection_8_0= ruleOutputSection
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getCallStatementAccess().getOutputSectionOutputSectionParserRuleCall_5_0());
              				
            }
            pushFollow(FOLLOW_4);
            lv_outputSection_8_0=ruleOutputSection();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getCallStatementRule());
              					}
              					set(
              						current,
              						"outputSection",
              						lv_outputSection_8_0,
              						"com.gk_software.core.dsl.xmapping.XMapping.OutputSection");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_9=(Token)match(input,21,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_9, grammarAccess.getCallStatementAccess().getSemicolonKeyword_6());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCallStatement"


    // $ANTLR start "entryRuleMapStatement"
    // InternalXMapping.g:868:1: entryRuleMapStatement returns [EObject current=null] : iv_ruleMapStatement= ruleMapStatement EOF ;
    public final EObject entryRuleMapStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMapStatement = null;


        try {
            // InternalXMapping.g:868:53: (iv_ruleMapStatement= ruleMapStatement EOF )
            // InternalXMapping.g:869:2: iv_ruleMapStatement= ruleMapStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMapStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleMapStatement=ruleMapStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMapStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMapStatement"


    // $ANTLR start "ruleMapStatement"
    // InternalXMapping.g:875:1: ruleMapStatement returns [EObject current=null] : (otherlv_0= 'map' ( ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->' )? ( (lv_routineSection_3_0= ruleRoutineSource ) ) ( (otherlv_4= '->' ( (lv_outputSection_5_0= ruleOutputSection ) ) ) | (otherlv_6= '->' ( (lv_null_7_0= 'NULL' ) ) ) ) otherlv_8= ';' ) ;
    public final EObject ruleMapStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_6=null;
        Token lv_null_7_0=null;
        Token otherlv_8=null;
        EObject lv_inputSection_1_0 = null;

        EObject lv_routineSection_3_0 = null;

        EObject lv_outputSection_5_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:881:2: ( (otherlv_0= 'map' ( ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->' )? ( (lv_routineSection_3_0= ruleRoutineSource ) ) ( (otherlv_4= '->' ( (lv_outputSection_5_0= ruleOutputSection ) ) ) | (otherlv_6= '->' ( (lv_null_7_0= 'NULL' ) ) ) ) otherlv_8= ';' ) )
            // InternalXMapping.g:882:2: (otherlv_0= 'map' ( ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->' )? ( (lv_routineSection_3_0= ruleRoutineSource ) ) ( (otherlv_4= '->' ( (lv_outputSection_5_0= ruleOutputSection ) ) ) | (otherlv_6= '->' ( (lv_null_7_0= 'NULL' ) ) ) ) otherlv_8= ';' )
            {
            // InternalXMapping.g:882:2: (otherlv_0= 'map' ( ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->' )? ( (lv_routineSection_3_0= ruleRoutineSource ) ) ( (otherlv_4= '->' ( (lv_outputSection_5_0= ruleOutputSection ) ) ) | (otherlv_6= '->' ( (lv_null_7_0= 'NULL' ) ) ) ) otherlv_8= ';' )
            // InternalXMapping.g:883:3: otherlv_0= 'map' ( ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->' )? ( (lv_routineSection_3_0= ruleRoutineSource ) ) ( (otherlv_4= '->' ( (lv_outputSection_5_0= ruleOutputSection ) ) ) | (otherlv_6= '->' ( (lv_null_7_0= 'NULL' ) ) ) ) otherlv_8= ';'
            {
            otherlv_0=(Token)match(input,33,FOLLOW_25); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getMapStatementAccess().getMapKeyword_0());
              		
            }
            // InternalXMapping.g:887:3: ( ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->' )?
            int alt9=2;
            alt9 = dfa9.predict(input);
            switch (alt9) {
                case 1 :
                    // InternalXMapping.g:888:4: ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->'
                    {
                    // InternalXMapping.g:888:4: ( (lv_inputSection_1_0= ruleInputSection ) )
                    // InternalXMapping.g:889:5: (lv_inputSection_1_0= ruleInputSection )
                    {
                    // InternalXMapping.g:889:5: (lv_inputSection_1_0= ruleInputSection )
                    // InternalXMapping.g:890:6: lv_inputSection_1_0= ruleInputSection
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getMapStatementAccess().getInputSectionInputSectionParserRuleCall_1_0_0());
                      					
                    }
                    pushFollow(FOLLOW_15);
                    lv_inputSection_1_0=ruleInputSection();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getMapStatementRule());
                      						}
                      						set(
                      							current,
                      							"inputSection",
                      							lv_inputSection_1_0,
                      							"com.gk_software.core.dsl.xmapping.XMapping.InputSection");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    otherlv_2=(Token)match(input,26,FOLLOW_25); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getMapStatementAccess().getHyphenMinusGreaterThanSignKeyword_1_1());
                      			
                    }

                    }
                    break;

            }

            // InternalXMapping.g:912:3: ( (lv_routineSection_3_0= ruleRoutineSource ) )
            // InternalXMapping.g:913:4: (lv_routineSection_3_0= ruleRoutineSource )
            {
            // InternalXMapping.g:913:4: (lv_routineSection_3_0= ruleRoutineSource )
            // InternalXMapping.g:914:5: lv_routineSection_3_0= ruleRoutineSource
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getMapStatementAccess().getRoutineSectionRoutineSourceParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_15);
            lv_routineSection_3_0=ruleRoutineSource();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getMapStatementRule());
              					}
              					set(
              						current,
              						"routineSection",
              						lv_routineSection_3_0,
              						"com.gk_software.core.dsl.xmapping.XMapping.RoutineSource");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalXMapping.g:931:3: ( (otherlv_4= '->' ( (lv_outputSection_5_0= ruleOutputSection ) ) ) | (otherlv_6= '->' ( (lv_null_7_0= 'NULL' ) ) ) )
            int alt10=2;
            int LA10_0 = input.LA(1);

            if ( (LA10_0==26) ) {
                int LA10_1 = input.LA(2);

                if ( (LA10_1==34) ) {
                    alt10=2;
                }
                else if ( (LA10_1==RULE_ID||(LA10_1>=RULE_OUTPUT_KEY && LA10_1<=RULE_RETURN_KEY)||LA10_1==20||LA10_1==22||LA10_1==25||LA10_1==31||LA10_1==33) ) {
                    alt10=1;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 10, 1, input);

                    throw nvae;
                }
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 10, 0, input);

                throw nvae;
            }
            switch (alt10) {
                case 1 :
                    // InternalXMapping.g:932:4: (otherlv_4= '->' ( (lv_outputSection_5_0= ruleOutputSection ) ) )
                    {
                    // InternalXMapping.g:932:4: (otherlv_4= '->' ( (lv_outputSection_5_0= ruleOutputSection ) ) )
                    // InternalXMapping.g:933:5: otherlv_4= '->' ( (lv_outputSection_5_0= ruleOutputSection ) )
                    {
                    otherlv_4=(Token)match(input,26,FOLLOW_24); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_4, grammarAccess.getMapStatementAccess().getHyphenMinusGreaterThanSignKeyword_3_0_0());
                      				
                    }
                    // InternalXMapping.g:937:5: ( (lv_outputSection_5_0= ruleOutputSection ) )
                    // InternalXMapping.g:938:6: (lv_outputSection_5_0= ruleOutputSection )
                    {
                    // InternalXMapping.g:938:6: (lv_outputSection_5_0= ruleOutputSection )
                    // InternalXMapping.g:939:7: lv_outputSection_5_0= ruleOutputSection
                    {
                    if ( state.backtracking==0 ) {

                      							newCompositeNode(grammarAccess.getMapStatementAccess().getOutputSectionOutputSectionParserRuleCall_3_0_1_0());
                      						
                    }
                    pushFollow(FOLLOW_4);
                    lv_outputSection_5_0=ruleOutputSection();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      							if (current==null) {
                      								current = createModelElementForParent(grammarAccess.getMapStatementRule());
                      							}
                      							set(
                      								current,
                      								"outputSection",
                      								lv_outputSection_5_0,
                      								"com.gk_software.core.dsl.xmapping.XMapping.OutputSection");
                      							afterParserOrEnumRuleCall();
                      						
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalXMapping.g:958:4: (otherlv_6= '->' ( (lv_null_7_0= 'NULL' ) ) )
                    {
                    // InternalXMapping.g:958:4: (otherlv_6= '->' ( (lv_null_7_0= 'NULL' ) ) )
                    // InternalXMapping.g:959:5: otherlv_6= '->' ( (lv_null_7_0= 'NULL' ) )
                    {
                    otherlv_6=(Token)match(input,26,FOLLOW_26); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_6, grammarAccess.getMapStatementAccess().getHyphenMinusGreaterThanSignKeyword_3_1_0());
                      				
                    }
                    // InternalXMapping.g:963:5: ( (lv_null_7_0= 'NULL' ) )
                    // InternalXMapping.g:964:6: (lv_null_7_0= 'NULL' )
                    {
                    // InternalXMapping.g:964:6: (lv_null_7_0= 'NULL' )
                    // InternalXMapping.g:965:7: lv_null_7_0= 'NULL'
                    {
                    lv_null_7_0=(Token)match(input,34,FOLLOW_4); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      							newLeafNode(lv_null_7_0, grammarAccess.getMapStatementAccess().getNullNULLKeyword_3_1_1_0());
                      						
                    }
                    if ( state.backtracking==0 ) {

                      							if (current==null) {
                      								current = createModelElement(grammarAccess.getMapStatementRule());
                      							}
                      							setWithLastConsumed(current, "null", lv_null_7_0, "NULL");
                      						
                    }

                    }


                    }


                    }


                    }
                    break;

            }

            otherlv_8=(Token)match(input,21,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_8, grammarAccess.getMapStatementAccess().getSemicolonKeyword_4());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMapStatement"


    // $ANTLR start "entryRuleRoutineSource"
    // InternalXMapping.g:987:1: entryRuleRoutineSource returns [EObject current=null] : iv_ruleRoutineSource= ruleRoutineSource EOF ;
    public final EObject entryRuleRoutineSource() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRoutineSource = null;


        try {
            // InternalXMapping.g:987:54: (iv_ruleRoutineSource= ruleRoutineSource EOF )
            // InternalXMapping.g:988:2: iv_ruleRoutineSource= ruleRoutineSource EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRoutineSourceRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleRoutineSource=ruleRoutineSource();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRoutineSource; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRoutineSource"


    // $ANTLR start "ruleRoutineSource"
    // InternalXMapping.g:994:1: ruleRoutineSource returns [EObject current=null] : ( ( ( ( ruleQualifiedName ) ) () otherlv_2= '.' )? ( ( ruleRoutineName ) ) ) ;
    public final EObject ruleRoutineSource() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;


        	enterRule();

        try {
            // InternalXMapping.g:1000:2: ( ( ( ( ( ruleQualifiedName ) ) () otherlv_2= '.' )? ( ( ruleRoutineName ) ) ) )
            // InternalXMapping.g:1001:2: ( ( ( ( ruleQualifiedName ) ) () otherlv_2= '.' )? ( ( ruleRoutineName ) ) )
            {
            // InternalXMapping.g:1001:2: ( ( ( ( ruleQualifiedName ) ) () otherlv_2= '.' )? ( ( ruleRoutineName ) ) )
            // InternalXMapping.g:1002:3: ( ( ( ruleQualifiedName ) ) () otherlv_2= '.' )? ( ( ruleRoutineName ) )
            {
            // InternalXMapping.g:1002:3: ( ( ( ruleQualifiedName ) ) () otherlv_2= '.' )?
            int alt11=2;
            int LA11_0 = input.LA(1);

            if ( (LA11_0==RULE_ID||LA11_0==20||LA11_0==22||LA11_0==25||LA11_0==31||LA11_0==33) ) {
                alt11=1;
            }
            switch (alt11) {
                case 1 :
                    // InternalXMapping.g:1003:4: ( ( ruleQualifiedName ) ) () otherlv_2= '.'
                    {
                    // InternalXMapping.g:1003:4: ( ( ruleQualifiedName ) )
                    // InternalXMapping.g:1004:5: ( ruleQualifiedName )
                    {
                    // InternalXMapping.g:1004:5: ( ruleQualifiedName )
                    // InternalXMapping.g:1005:6: ruleQualifiedName
                    {
                    if ( state.backtracking==0 ) {

                      						/* */
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getRoutineSourceRule());
                      						}
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getRoutineSourceAccess().getClassMappingClassCrossReference_0_0_0());
                      					
                    }
                    pushFollow(FOLLOW_27);
                    ruleQualifiedName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalXMapping.g:1022:4: ()
                    // InternalXMapping.g:1023:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					/* */
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElementAndSet(
                      						grammarAccess.getRoutineSourceAccess().getRoutineSourceTargetAction_0_1(),
                      						current);
                      				
                    }

                    }

                    otherlv_2=(Token)match(input,35,FOLLOW_14); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getRoutineSourceAccess().getFullStopKeyword_0_2());
                      			
                    }

                    }
                    break;

            }

            // InternalXMapping.g:1037:3: ( ( ruleRoutineName ) )
            // InternalXMapping.g:1038:4: ( ruleRoutineName )
            {
            // InternalXMapping.g:1038:4: ( ruleRoutineName )
            // InternalXMapping.g:1039:5: ruleRoutineName
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getRoutineSourceRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getRoutineSourceAccess().getSourceRoutineCrossReference_1_0());
              				
            }
            pushFollow(FOLLOW_2);
            ruleRoutineName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRoutineSource"


    // $ANTLR start "entryRuleAssignmentStatement"
    // InternalXMapping.g:1060:1: entryRuleAssignmentStatement returns [EObject current=null] : iv_ruleAssignmentStatement= ruleAssignmentStatement EOF ;
    public final EObject entryRuleAssignmentStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssignmentStatement = null;


        try {
            // InternalXMapping.g:1060:60: (iv_ruleAssignmentStatement= ruleAssignmentStatement EOF )
            // InternalXMapping.g:1061:2: iv_ruleAssignmentStatement= ruleAssignmentStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAssignmentStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAssignmentStatement=ruleAssignmentStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAssignmentStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssignmentStatement"


    // $ANTLR start "ruleAssignmentStatement"
    // InternalXMapping.g:1067:1: ruleAssignmentStatement returns [EObject current=null] : ( ( (lv_declaration_0_0= ruleDeclaration ) ) ( (lv_initialization_1_0= ruleInitialization ) )? otherlv_2= ';' ) ;
    public final EObject ruleAssignmentStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject lv_declaration_0_0 = null;

        EObject lv_initialization_1_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:1073:2: ( ( ( (lv_declaration_0_0= ruleDeclaration ) ) ( (lv_initialization_1_0= ruleInitialization ) )? otherlv_2= ';' ) )
            // InternalXMapping.g:1074:2: ( ( (lv_declaration_0_0= ruleDeclaration ) ) ( (lv_initialization_1_0= ruleInitialization ) )? otherlv_2= ';' )
            {
            // InternalXMapping.g:1074:2: ( ( (lv_declaration_0_0= ruleDeclaration ) ) ( (lv_initialization_1_0= ruleInitialization ) )? otherlv_2= ';' )
            // InternalXMapping.g:1075:3: ( (lv_declaration_0_0= ruleDeclaration ) ) ( (lv_initialization_1_0= ruleInitialization ) )? otherlv_2= ';'
            {
            // InternalXMapping.g:1075:3: ( (lv_declaration_0_0= ruleDeclaration ) )
            // InternalXMapping.g:1076:4: (lv_declaration_0_0= ruleDeclaration )
            {
            // InternalXMapping.g:1076:4: (lv_declaration_0_0= ruleDeclaration )
            // InternalXMapping.g:1077:5: lv_declaration_0_0= ruleDeclaration
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getAssignmentStatementAccess().getDeclarationDeclarationParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_28);
            lv_declaration_0_0=ruleDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getAssignmentStatementRule());
              					}
              					set(
              						current,
              						"declaration",
              						lv_declaration_0_0,
              						"com.gk_software.core.dsl.xmapping.XMapping.Declaration");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalXMapping.g:1094:3: ( (lv_initialization_1_0= ruleInitialization ) )?
            int alt12=2;
            int LA12_0 = input.LA(1);

            if ( (LA12_0==36) ) {
                alt12=1;
            }
            switch (alt12) {
                case 1 :
                    // InternalXMapping.g:1095:4: (lv_initialization_1_0= ruleInitialization )
                    {
                    // InternalXMapping.g:1095:4: (lv_initialization_1_0= ruleInitialization )
                    // InternalXMapping.g:1096:5: lv_initialization_1_0= ruleInitialization
                    {
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getAssignmentStatementAccess().getInitializationInitializationParserRuleCall_1_0());
                      				
                    }
                    pushFollow(FOLLOW_4);
                    lv_initialization_1_0=ruleInitialization();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElementForParent(grammarAccess.getAssignmentStatementRule());
                      					}
                      					set(
                      						current,
                      						"initialization",
                      						lv_initialization_1_0,
                      						"com.gk_software.core.dsl.xmapping.XMapping.Initialization");
                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,21,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_2, grammarAccess.getAssignmentStatementAccess().getSemicolonKeyword_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssignmentStatement"


    // $ANTLR start "entryRuleInitialization"
    // InternalXMapping.g:1121:1: entryRuleInitialization returns [EObject current=null] : iv_ruleInitialization= ruleInitialization EOF ;
    public final EObject entryRuleInitialization() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInitialization = null;


        try {
            // InternalXMapping.g:1121:55: (iv_ruleInitialization= ruleInitialization EOF )
            // InternalXMapping.g:1122:2: iv_ruleInitialization= ruleInitialization EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInitializationRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleInitialization=ruleInitialization();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInitialization; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInitialization"


    // $ANTLR start "ruleInitialization"
    // InternalXMapping.g:1128:1: ruleInitialization returns [EObject current=null] : (otherlv_0= '=' ( (lv_assignment_1_0= ruleAssignment ) ) ) ;
    public final EObject ruleInitialization() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_assignment_1_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:1134:2: ( (otherlv_0= '=' ( (lv_assignment_1_0= ruleAssignment ) ) ) )
            // InternalXMapping.g:1135:2: (otherlv_0= '=' ( (lv_assignment_1_0= ruleAssignment ) ) )
            {
            // InternalXMapping.g:1135:2: (otherlv_0= '=' ( (lv_assignment_1_0= ruleAssignment ) ) )
            // InternalXMapping.g:1136:3: otherlv_0= '=' ( (lv_assignment_1_0= ruleAssignment ) )
            {
            otherlv_0=(Token)match(input,36,FOLLOW_29); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getInitializationAccess().getEqualsSignKeyword_0());
              		
            }
            // InternalXMapping.g:1140:3: ( (lv_assignment_1_0= ruleAssignment ) )
            // InternalXMapping.g:1141:4: (lv_assignment_1_0= ruleAssignment )
            {
            // InternalXMapping.g:1141:4: (lv_assignment_1_0= ruleAssignment )
            // InternalXMapping.g:1142:5: lv_assignment_1_0= ruleAssignment
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getInitializationAccess().getAssignmentAssignmentParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_assignment_1_0=ruleAssignment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getInitializationRule());
              					}
              					set(
              						current,
              						"assignment",
              						lv_assignment_1_0,
              						"com.gk_software.core.dsl.xmapping.XMapping.Assignment");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInitialization"


    // $ANTLR start "entryRuleAssignment"
    // InternalXMapping.g:1163:1: entryRuleAssignment returns [EObject current=null] : iv_ruleAssignment= ruleAssignment EOF ;
    public final EObject entryRuleAssignment() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssignment = null;


        try {
            // InternalXMapping.g:1163:51: (iv_ruleAssignment= ruleAssignment EOF )
            // InternalXMapping.g:1164:2: iv_ruleAssignment= ruleAssignment EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAssignmentRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAssignment=ruleAssignment();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAssignment; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssignment"


    // $ANTLR start "ruleAssignment"
    // InternalXMapping.g:1170:1: ruleAssignment returns [EObject current=null] : ( (otherlv_0= 'new' ( (lv_structure_1_0= ruleAssignmentStructure ) ) ) | ( () ( (lv_value_3_0= ruleIntValue ) ) ) | ( () ( (lv_value_5_0= ruleDOUBLE ) ) ) | ( () ( (lv_value_7_0= RULE_STRING ) ) ) ) ;
    public final EObject ruleAssignment() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_value_7_0=null;
        EObject lv_structure_1_0 = null;

        AntlrDatatypeRuleToken lv_value_3_0 = null;

        AntlrDatatypeRuleToken lv_value_5_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:1176:2: ( ( (otherlv_0= 'new' ( (lv_structure_1_0= ruleAssignmentStructure ) ) ) | ( () ( (lv_value_3_0= ruleIntValue ) ) ) | ( () ( (lv_value_5_0= ruleDOUBLE ) ) ) | ( () ( (lv_value_7_0= RULE_STRING ) ) ) ) )
            // InternalXMapping.g:1177:2: ( (otherlv_0= 'new' ( (lv_structure_1_0= ruleAssignmentStructure ) ) ) | ( () ( (lv_value_3_0= ruleIntValue ) ) ) | ( () ( (lv_value_5_0= ruleDOUBLE ) ) ) | ( () ( (lv_value_7_0= RULE_STRING ) ) ) )
            {
            // InternalXMapping.g:1177:2: ( (otherlv_0= 'new' ( (lv_structure_1_0= ruleAssignmentStructure ) ) ) | ( () ( (lv_value_3_0= ruleIntValue ) ) ) | ( () ( (lv_value_5_0= ruleDOUBLE ) ) ) | ( () ( (lv_value_7_0= RULE_STRING ) ) ) )
            int alt13=4;
            switch ( input.LA(1) ) {
            case 37:
                {
                alt13=1;
                }
                break;
            case 38:
                {
                int LA13_2 = input.LA(2);

                if ( (LA13_2==RULE_INT) ) {
                    int LA13_3 = input.LA(3);

                    if ( (LA13_3==EOF||LA13_3==21) ) {
                        alt13=2;
                    }
                    else if ( (LA13_3==35) ) {
                        alt13=3;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 13, 3, input);

                        throw nvae;
                    }
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 13, 2, input);

                    throw nvae;
                }
                }
                break;
            case RULE_INT:
                {
                int LA13_3 = input.LA(2);

                if ( (LA13_3==EOF||LA13_3==21) ) {
                    alt13=2;
                }
                else if ( (LA13_3==35) ) {
                    alt13=3;
                }
                else {
                    if (state.backtracking>0) {state.failed=true; return current;}
                    NoViableAltException nvae =
                        new NoViableAltException("", 13, 3, input);

                    throw nvae;
                }
                }
                break;
            case RULE_STRING:
                {
                alt13=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 13, 0, input);

                throw nvae;
            }

            switch (alt13) {
                case 1 :
                    // InternalXMapping.g:1178:3: (otherlv_0= 'new' ( (lv_structure_1_0= ruleAssignmentStructure ) ) )
                    {
                    // InternalXMapping.g:1178:3: (otherlv_0= 'new' ( (lv_structure_1_0= ruleAssignmentStructure ) ) )
                    // InternalXMapping.g:1179:4: otherlv_0= 'new' ( (lv_structure_1_0= ruleAssignmentStructure ) )
                    {
                    otherlv_0=(Token)match(input,37,FOLLOW_24); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_0, grammarAccess.getAssignmentAccess().getNewKeyword_0_0());
                      			
                    }
                    // InternalXMapping.g:1183:4: ( (lv_structure_1_0= ruleAssignmentStructure ) )
                    // InternalXMapping.g:1184:5: (lv_structure_1_0= ruleAssignmentStructure )
                    {
                    // InternalXMapping.g:1184:5: (lv_structure_1_0= ruleAssignmentStructure )
                    // InternalXMapping.g:1185:6: lv_structure_1_0= ruleAssignmentStructure
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getAssignmentAccess().getStructureAssignmentStructureParserRuleCall_0_1_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_structure_1_0=ruleAssignmentStructure();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getAssignmentRule());
                      						}
                      						set(
                      							current,
                      							"structure",
                      							lv_structure_1_0,
                      							"com.gk_software.core.dsl.xmapping.XMapping.AssignmentStructure");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalXMapping.g:1204:3: ( () ( (lv_value_3_0= ruleIntValue ) ) )
                    {
                    // InternalXMapping.g:1204:3: ( () ( (lv_value_3_0= ruleIntValue ) ) )
                    // InternalXMapping.g:1205:4: () ( (lv_value_3_0= ruleIntValue ) )
                    {
                    // InternalXMapping.g:1205:4: ()
                    // InternalXMapping.g:1206:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					/* */
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElement(
                      						grammarAccess.getAssignmentAccess().getIntTypeAction_1_0(),
                      						current);
                      				
                    }

                    }

                    // InternalXMapping.g:1215:4: ( (lv_value_3_0= ruleIntValue ) )
                    // InternalXMapping.g:1216:5: (lv_value_3_0= ruleIntValue )
                    {
                    // InternalXMapping.g:1216:5: (lv_value_3_0= ruleIntValue )
                    // InternalXMapping.g:1217:6: lv_value_3_0= ruleIntValue
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getAssignmentAccess().getValueIntValueParserRuleCall_1_1_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_value_3_0=ruleIntValue();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getAssignmentRule());
                      						}
                      						set(
                      							current,
                      							"value",
                      							lv_value_3_0,
                      							"com.gk_software.core.dsl.xmapping.XMapping.IntValue");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalXMapping.g:1236:3: ( () ( (lv_value_5_0= ruleDOUBLE ) ) )
                    {
                    // InternalXMapping.g:1236:3: ( () ( (lv_value_5_0= ruleDOUBLE ) ) )
                    // InternalXMapping.g:1237:4: () ( (lv_value_5_0= ruleDOUBLE ) )
                    {
                    // InternalXMapping.g:1237:4: ()
                    // InternalXMapping.g:1238:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					/* */
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElement(
                      						grammarAccess.getAssignmentAccess().getDoubleTypeAction_2_0(),
                      						current);
                      				
                    }

                    }

                    // InternalXMapping.g:1247:4: ( (lv_value_5_0= ruleDOUBLE ) )
                    // InternalXMapping.g:1248:5: (lv_value_5_0= ruleDOUBLE )
                    {
                    // InternalXMapping.g:1248:5: (lv_value_5_0= ruleDOUBLE )
                    // InternalXMapping.g:1249:6: lv_value_5_0= ruleDOUBLE
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getAssignmentAccess().getValueDOUBLEParserRuleCall_2_1_0());
                      					
                    }
                    pushFollow(FOLLOW_2);
                    lv_value_5_0=ruleDOUBLE();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getAssignmentRule());
                      						}
                      						set(
                      							current,
                      							"value",
                      							lv_value_5_0,
                      							"com.gk_software.core.dsl.xmapping.XMapping.DOUBLE");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }


                    }
                    break;
                case 4 :
                    // InternalXMapping.g:1268:3: ( () ( (lv_value_7_0= RULE_STRING ) ) )
                    {
                    // InternalXMapping.g:1268:3: ( () ( (lv_value_7_0= RULE_STRING ) ) )
                    // InternalXMapping.g:1269:4: () ( (lv_value_7_0= RULE_STRING ) )
                    {
                    // InternalXMapping.g:1269:4: ()
                    // InternalXMapping.g:1270:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					/* */
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElement(
                      						grammarAccess.getAssignmentAccess().getStringTypeAction_3_0(),
                      						current);
                      				
                    }

                    }

                    // InternalXMapping.g:1279:4: ( (lv_value_7_0= RULE_STRING ) )
                    // InternalXMapping.g:1280:5: (lv_value_7_0= RULE_STRING )
                    {
                    // InternalXMapping.g:1280:5: (lv_value_7_0= RULE_STRING )
                    // InternalXMapping.g:1281:6: lv_value_7_0= RULE_STRING
                    {
                    lv_value_7_0=(Token)match(input,RULE_STRING,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_value_7_0, grammarAccess.getAssignmentAccess().getValueSTRINGTerminalRuleCall_3_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getAssignmentRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"value",
                      							lv_value_7_0,
                      							"org.eclipse.xtext.xbase.Xtype.STRING");
                      					
                    }

                    }


                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssignment"


    // $ANTLR start "entryRuleIntValue"
    // InternalXMapping.g:1302:1: entryRuleIntValue returns [String current=null] : iv_ruleIntValue= ruleIntValue EOF ;
    public final String entryRuleIntValue() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleIntValue = null;


        try {
            // InternalXMapping.g:1302:48: (iv_ruleIntValue= ruleIntValue EOF )
            // InternalXMapping.g:1303:2: iv_ruleIntValue= ruleIntValue EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getIntValueRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleIntValue=ruleIntValue();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleIntValue.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleIntValue"


    // $ANTLR start "ruleIntValue"
    // InternalXMapping.g:1309:1: ruleIntValue returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= '-' )? this_INT_1= RULE_INT ) ;
    public final AntlrDatatypeRuleToken ruleIntValue() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_INT_1=null;


        	enterRule();

        try {
            // InternalXMapping.g:1315:2: ( ( (kw= '-' )? this_INT_1= RULE_INT ) )
            // InternalXMapping.g:1316:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            {
            // InternalXMapping.g:1316:2: ( (kw= '-' )? this_INT_1= RULE_INT )
            // InternalXMapping.g:1317:3: (kw= '-' )? this_INT_1= RULE_INT
            {
            // InternalXMapping.g:1317:3: (kw= '-' )?
            int alt14=2;
            int LA14_0 = input.LA(1);

            if ( (LA14_0==38) ) {
                alt14=1;
            }
            switch (alt14) {
                case 1 :
                    // InternalXMapping.g:1318:4: kw= '-'
                    {
                    kw=(Token)match(input,38,FOLLOW_30); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(kw);
                      				newLeafNode(kw, grammarAccess.getIntValueAccess().getHyphenMinusKeyword_0());
                      			
                    }

                    }
                    break;

            }

            this_INT_1=(Token)match(input,RULE_INT,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_INT_1);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_INT_1, grammarAccess.getIntValueAccess().getINTTerminalRuleCall_1());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleIntValue"


    // $ANTLR start "entryRuleAssignmentStructure"
    // InternalXMapping.g:1335:1: entryRuleAssignmentStructure returns [EObject current=null] : iv_ruleAssignmentStructure= ruleAssignmentStructure EOF ;
    public final EObject entryRuleAssignmentStructure() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAssignmentStructure = null;


        try {
            // InternalXMapping.g:1335:60: (iv_ruleAssignmentStructure= ruleAssignmentStructure EOF )
            // InternalXMapping.g:1336:2: iv_ruleAssignmentStructure= ruleAssignmentStructure EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAssignmentStructureRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAssignmentStructure=ruleAssignmentStructure();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAssignmentStructure; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAssignmentStructure"


    // $ANTLR start "ruleAssignmentStructure"
    // InternalXMapping.g:1342:1: ruleAssignmentStructure returns [EObject current=null] : ( (lv_source_0_0= ruleOutputAttribute ) ) ;
    public final EObject ruleAssignmentStructure() throws RecognitionException {
        EObject current = null;

        EObject lv_source_0_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:1348:2: ( ( (lv_source_0_0= ruleOutputAttribute ) ) )
            // InternalXMapping.g:1349:2: ( (lv_source_0_0= ruleOutputAttribute ) )
            {
            // InternalXMapping.g:1349:2: ( (lv_source_0_0= ruleOutputAttribute ) )
            // InternalXMapping.g:1350:3: (lv_source_0_0= ruleOutputAttribute )
            {
            // InternalXMapping.g:1350:3: (lv_source_0_0= ruleOutputAttribute )
            // InternalXMapping.g:1351:4: lv_source_0_0= ruleOutputAttribute
            {
            if ( state.backtracking==0 ) {

              				newCompositeNode(grammarAccess.getAssignmentStructureAccess().getSourceOutputAttributeParserRuleCall_0());
              			
            }
            pushFollow(FOLLOW_2);
            lv_source_0_0=ruleOutputAttribute();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				if (current==null) {
              					current = createModelElementForParent(grammarAccess.getAssignmentStructureRule());
              				}
              				set(
              					current,
              					"source",
              					lv_source_0_0,
              					"com.gk_software.core.dsl.xmapping.XMapping.OutputAttribute");
              				afterParserOrEnumRuleCall();
              			
            }

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAssignmentStructure"


    // $ANTLR start "entryRuleDeclaration"
    // InternalXMapping.g:1371:1: entryRuleDeclaration returns [EObject current=null] : iv_ruleDeclaration= ruleDeclaration EOF ;
    public final EObject entryRuleDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleDeclaration = null;


        try {
            // InternalXMapping.g:1371:52: (iv_ruleDeclaration= ruleDeclaration EOF )
            // InternalXMapping.g:1372:2: iv_ruleDeclaration= ruleDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDeclarationRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleDeclaration=ruleDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDeclaration; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDeclaration"


    // $ANTLR start "ruleDeclaration"
    // InternalXMapping.g:1378:1: ruleDeclaration returns [EObject current=null] : (this_CreateAttribute_0= ruleCreateAttribute | this_AttributeReference_1= ruleAttributeReference ) ;
    public final EObject ruleDeclaration() throws RecognitionException {
        EObject current = null;

        EObject this_CreateAttribute_0 = null;

        EObject this_AttributeReference_1 = null;



        	enterRule();

        try {
            // InternalXMapping.g:1384:2: ( (this_CreateAttribute_0= ruleCreateAttribute | this_AttributeReference_1= ruleAttributeReference ) )
            // InternalXMapping.g:1385:2: (this_CreateAttribute_0= ruleCreateAttribute | this_AttributeReference_1= ruleAttributeReference )
            {
            // InternalXMapping.g:1385:2: (this_CreateAttribute_0= ruleCreateAttribute | this_AttributeReference_1= ruleAttributeReference )
            int alt15=2;
            int LA15_0 = input.LA(1);

            if ( (LA15_0==39) ) {
                alt15=1;
            }
            else if ( ((LA15_0>=RULE_VARIABLE_KEY && LA15_0<=RULE_RETURN_KEY)) ) {
                alt15=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 15, 0, input);

                throw nvae;
            }
            switch (alt15) {
                case 1 :
                    // InternalXMapping.g:1386:3: this_CreateAttribute_0= ruleCreateAttribute
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getDeclarationAccess().getCreateAttributeParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_CreateAttribute_0=ruleCreateAttribute();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_CreateAttribute_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalXMapping.g:1398:3: this_AttributeReference_1= ruleAttributeReference
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getDeclarationAccess().getAttributeReferenceParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_AttributeReference_1=ruleAttributeReference();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_AttributeReference_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDeclaration"


    // $ANTLR start "entryRuleCreateAttribute"
    // InternalXMapping.g:1413:1: entryRuleCreateAttribute returns [EObject current=null] : iv_ruleCreateAttribute= ruleCreateAttribute EOF ;
    public final EObject entryRuleCreateAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCreateAttribute = null;


        try {
            // InternalXMapping.g:1413:56: (iv_ruleCreateAttribute= ruleCreateAttribute EOF )
            // InternalXMapping.g:1414:2: iv_ruleCreateAttribute= ruleCreateAttribute EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getCreateAttributeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleCreateAttribute=ruleCreateAttribute();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleCreateAttribute; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCreateAttribute"


    // $ANTLR start "ruleCreateAttribute"
    // InternalXMapping.g:1420:1: ruleCreateAttribute returns [EObject current=null] : (otherlv_0= 'let' ( (lv_new_1_0= ruleNewAttribute ) ) (otherlv_2= ',' ( (lv_new_3_0= ruleNewAttribute ) ) )* ) ;
    public final EObject ruleCreateAttribute() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        EObject lv_new_1_0 = null;

        EObject lv_new_3_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:1426:2: ( (otherlv_0= 'let' ( (lv_new_1_0= ruleNewAttribute ) ) (otherlv_2= ',' ( (lv_new_3_0= ruleNewAttribute ) ) )* ) )
            // InternalXMapping.g:1427:2: (otherlv_0= 'let' ( (lv_new_1_0= ruleNewAttribute ) ) (otherlv_2= ',' ( (lv_new_3_0= ruleNewAttribute ) ) )* )
            {
            // InternalXMapping.g:1427:2: (otherlv_0= 'let' ( (lv_new_1_0= ruleNewAttribute ) ) (otherlv_2= ',' ( (lv_new_3_0= ruleNewAttribute ) ) )* )
            // InternalXMapping.g:1428:3: otherlv_0= 'let' ( (lv_new_1_0= ruleNewAttribute ) ) (otherlv_2= ',' ( (lv_new_3_0= ruleNewAttribute ) ) )*
            {
            otherlv_0=(Token)match(input,39,FOLLOW_31); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getCreateAttributeAccess().getLetKeyword_0());
              		
            }
            // InternalXMapping.g:1432:3: ( (lv_new_1_0= ruleNewAttribute ) )
            // InternalXMapping.g:1433:4: (lv_new_1_0= ruleNewAttribute )
            {
            // InternalXMapping.g:1433:4: (lv_new_1_0= ruleNewAttribute )
            // InternalXMapping.g:1434:5: lv_new_1_0= ruleNewAttribute
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getCreateAttributeAccess().getNewNewAttributeParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_32);
            lv_new_1_0=ruleNewAttribute();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getCreateAttributeRule());
              					}
              					add(
              						current,
              						"new",
              						lv_new_1_0,
              						"com.gk_software.core.dsl.xmapping.XMapping.NewAttribute");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalXMapping.g:1451:3: (otherlv_2= ',' ( (lv_new_3_0= ruleNewAttribute ) ) )*
            loop16:
            do {
                int alt16=2;
                int LA16_0 = input.LA(1);

                if ( (LA16_0==40) ) {
                    alt16=1;
                }


                switch (alt16) {
            	case 1 :
            	    // InternalXMapping.g:1452:4: otherlv_2= ',' ( (lv_new_3_0= ruleNewAttribute ) )
            	    {
            	    otherlv_2=(Token)match(input,40,FOLLOW_31); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_2, grammarAccess.getCreateAttributeAccess().getCommaKeyword_2_0());
            	      			
            	    }
            	    // InternalXMapping.g:1456:4: ( (lv_new_3_0= ruleNewAttribute ) )
            	    // InternalXMapping.g:1457:5: (lv_new_3_0= ruleNewAttribute )
            	    {
            	    // InternalXMapping.g:1457:5: (lv_new_3_0= ruleNewAttribute )
            	    // InternalXMapping.g:1458:6: lv_new_3_0= ruleNewAttribute
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getCreateAttributeAccess().getNewNewAttributeParserRuleCall_2_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_32);
            	    lv_new_3_0=ruleNewAttribute();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getCreateAttributeRule());
            	      						}
            	      						add(
            	      							current,
            	      							"new",
            	      							lv_new_3_0,
            	      							"com.gk_software.core.dsl.xmapping.XMapping.NewAttribute");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop16;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCreateAttribute"


    // $ANTLR start "entryRuleAttributeReference"
    // InternalXMapping.g:1480:1: entryRuleAttributeReference returns [EObject current=null] : iv_ruleAttributeReference= ruleAttributeReference EOF ;
    public final EObject entryRuleAttributeReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttributeReference = null;


        try {
            // InternalXMapping.g:1480:59: (iv_ruleAttributeReference= ruleAttributeReference EOF )
            // InternalXMapping.g:1481:2: iv_ruleAttributeReference= ruleAttributeReference EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAttributeReferenceRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAttributeReference=ruleAttributeReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAttributeReference; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttributeReference"


    // $ANTLR start "ruleAttributeReference"
    // InternalXMapping.g:1487:1: ruleAttributeReference returns [EObject current=null] : ( ( (lv_reference_0_0= ruleUsedAttribute ) ) (otherlv_1= ',' ( (lv_reference_2_0= ruleUsedAttribute ) ) )* () ) ;
    public final EObject ruleAttributeReference() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_reference_0_0 = null;

        EObject lv_reference_2_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:1493:2: ( ( ( (lv_reference_0_0= ruleUsedAttribute ) ) (otherlv_1= ',' ( (lv_reference_2_0= ruleUsedAttribute ) ) )* () ) )
            // InternalXMapping.g:1494:2: ( ( (lv_reference_0_0= ruleUsedAttribute ) ) (otherlv_1= ',' ( (lv_reference_2_0= ruleUsedAttribute ) ) )* () )
            {
            // InternalXMapping.g:1494:2: ( ( (lv_reference_0_0= ruleUsedAttribute ) ) (otherlv_1= ',' ( (lv_reference_2_0= ruleUsedAttribute ) ) )* () )
            // InternalXMapping.g:1495:3: ( (lv_reference_0_0= ruleUsedAttribute ) ) (otherlv_1= ',' ( (lv_reference_2_0= ruleUsedAttribute ) ) )* ()
            {
            // InternalXMapping.g:1495:3: ( (lv_reference_0_0= ruleUsedAttribute ) )
            // InternalXMapping.g:1496:4: (lv_reference_0_0= ruleUsedAttribute )
            {
            // InternalXMapping.g:1496:4: (lv_reference_0_0= ruleUsedAttribute )
            // InternalXMapping.g:1497:5: lv_reference_0_0= ruleUsedAttribute
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getAttributeReferenceAccess().getReferenceUsedAttributeParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_32);
            lv_reference_0_0=ruleUsedAttribute();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getAttributeReferenceRule());
              					}
              					add(
              						current,
              						"reference",
              						lv_reference_0_0,
              						"com.gk_software.core.dsl.xmapping.XMapping.UsedAttribute");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalXMapping.g:1514:3: (otherlv_1= ',' ( (lv_reference_2_0= ruleUsedAttribute ) ) )*
            loop17:
            do {
                int alt17=2;
                int LA17_0 = input.LA(1);

                if ( (LA17_0==40) ) {
                    alt17=1;
                }


                switch (alt17) {
            	case 1 :
            	    // InternalXMapping.g:1515:4: otherlv_1= ',' ( (lv_reference_2_0= ruleUsedAttribute ) )
            	    {
            	    otherlv_1=(Token)match(input,40,FOLLOW_31); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_1, grammarAccess.getAttributeReferenceAccess().getCommaKeyword_1_0());
            	      			
            	    }
            	    // InternalXMapping.g:1519:4: ( (lv_reference_2_0= ruleUsedAttribute ) )
            	    // InternalXMapping.g:1520:5: (lv_reference_2_0= ruleUsedAttribute )
            	    {
            	    // InternalXMapping.g:1520:5: (lv_reference_2_0= ruleUsedAttribute )
            	    // InternalXMapping.g:1521:6: lv_reference_2_0= ruleUsedAttribute
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getAttributeReferenceAccess().getReferenceUsedAttributeParserRuleCall_1_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_32);
            	    lv_reference_2_0=ruleUsedAttribute();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getAttributeReferenceRule());
            	      						}
            	      						add(
            	      							current,
            	      							"reference",
            	      							lv_reference_2_0,
            	      							"com.gk_software.core.dsl.xmapping.XMapping.UsedAttribute");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop17;
                }
            } while (true);

            // InternalXMapping.g:1539:3: ()
            // InternalXMapping.g:1540:4: 
            {
            if ( state.backtracking==0 ) {

              				/* */
              			
            }
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElementAndSet(
              					grammarAccess.getAttributeReferenceAccess().getDeclarationTargetAction_2(),
              					current);
              			
            }

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttributeReference"


    // $ANTLR start "entryRuleUsedAttribute"
    // InternalXMapping.g:1553:1: entryRuleUsedAttribute returns [EObject current=null] : iv_ruleUsedAttribute= ruleUsedAttribute EOF ;
    public final EObject entryRuleUsedAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUsedAttribute = null;


        try {
            // InternalXMapping.g:1553:54: (iv_ruleUsedAttribute= ruleUsedAttribute EOF )
            // InternalXMapping.g:1554:2: iv_ruleUsedAttribute= ruleUsedAttribute EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getUsedAttributeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleUsedAttribute=ruleUsedAttribute();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleUsedAttribute; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUsedAttribute"


    // $ANTLR start "ruleUsedAttribute"
    // InternalXMapping.g:1560:1: ruleUsedAttribute returns [EObject current=null] : ( ( ( ruleVariableName ) ) | ( ( ruleReturnName ) ) ) ;
    public final EObject ruleUsedAttribute() throws RecognitionException {
        EObject current = null;


        	enterRule();

        try {
            // InternalXMapping.g:1566:2: ( ( ( ( ruleVariableName ) ) | ( ( ruleReturnName ) ) ) )
            // InternalXMapping.g:1567:2: ( ( ( ruleVariableName ) ) | ( ( ruleReturnName ) ) )
            {
            // InternalXMapping.g:1567:2: ( ( ( ruleVariableName ) ) | ( ( ruleReturnName ) ) )
            int alt18=2;
            int LA18_0 = input.LA(1);

            if ( (LA18_0==RULE_VARIABLE_KEY) ) {
                alt18=1;
            }
            else if ( (LA18_0==RULE_RETURN_KEY) ) {
                alt18=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 18, 0, input);

                throw nvae;
            }
            switch (alt18) {
                case 1 :
                    // InternalXMapping.g:1568:3: ( ( ruleVariableName ) )
                    {
                    // InternalXMapping.g:1568:3: ( ( ruleVariableName ) )
                    // InternalXMapping.g:1569:4: ( ruleVariableName )
                    {
                    // InternalXMapping.g:1569:4: ( ruleVariableName )
                    // InternalXMapping.g:1570:5: ruleVariableName
                    {
                    if ( state.backtracking==0 ) {

                      					/* */
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getUsedAttributeRule());
                      					}
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getUsedAttributeAccess().getSourceVariableCrossReference_0_0());
                      				
                    }
                    pushFollow(FOLLOW_2);
                    ruleVariableName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalXMapping.g:1588:3: ( ( ruleReturnName ) )
                    {
                    // InternalXMapping.g:1588:3: ( ( ruleReturnName ) )
                    // InternalXMapping.g:1589:4: ( ruleReturnName )
                    {
                    // InternalXMapping.g:1589:4: ( ruleReturnName )
                    // InternalXMapping.g:1590:5: ruleReturnName
                    {
                    if ( state.backtracking==0 ) {

                      					/* */
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getUsedAttributeRule());
                      					}
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getUsedAttributeAccess().getSourceReturnCrossReference_1_0());
                      				
                    }
                    pushFollow(FOLLOW_2);
                    ruleReturnName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUsedAttribute"


    // $ANTLR start "entryRuleNewAttribute"
    // InternalXMapping.g:1611:1: entryRuleNewAttribute returns [EObject current=null] : iv_ruleNewAttribute= ruleNewAttribute EOF ;
    public final EObject entryRuleNewAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleNewAttribute = null;


        try {
            // InternalXMapping.g:1611:53: (iv_ruleNewAttribute= ruleNewAttribute EOF )
            // InternalXMapping.g:1612:2: iv_ruleNewAttribute= ruleNewAttribute EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNewAttributeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleNewAttribute=ruleNewAttribute();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleNewAttribute; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleNewAttribute"


    // $ANTLR start "ruleNewAttribute"
    // InternalXMapping.g:1618:1: ruleNewAttribute returns [EObject current=null] : ( ( (lv_source_0_0= ruleVariable ) ) | ( (lv_source_1_0= ruleReturn ) ) ) ;
    public final EObject ruleNewAttribute() throws RecognitionException {
        EObject current = null;

        EObject lv_source_0_0 = null;

        EObject lv_source_1_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:1624:2: ( ( ( (lv_source_0_0= ruleVariable ) ) | ( (lv_source_1_0= ruleReturn ) ) ) )
            // InternalXMapping.g:1625:2: ( ( (lv_source_0_0= ruleVariable ) ) | ( (lv_source_1_0= ruleReturn ) ) )
            {
            // InternalXMapping.g:1625:2: ( ( (lv_source_0_0= ruleVariable ) ) | ( (lv_source_1_0= ruleReturn ) ) )
            int alt19=2;
            int LA19_0 = input.LA(1);

            if ( (LA19_0==RULE_VARIABLE_KEY) ) {
                alt19=1;
            }
            else if ( (LA19_0==RULE_RETURN_KEY) ) {
                alt19=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 19, 0, input);

                throw nvae;
            }
            switch (alt19) {
                case 1 :
                    // InternalXMapping.g:1626:3: ( (lv_source_0_0= ruleVariable ) )
                    {
                    // InternalXMapping.g:1626:3: ( (lv_source_0_0= ruleVariable ) )
                    // InternalXMapping.g:1627:4: (lv_source_0_0= ruleVariable )
                    {
                    // InternalXMapping.g:1627:4: (lv_source_0_0= ruleVariable )
                    // InternalXMapping.g:1628:5: lv_source_0_0= ruleVariable
                    {
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getNewAttributeAccess().getSourceVariableParserRuleCall_0_0());
                      				
                    }
                    pushFollow(FOLLOW_2);
                    lv_source_0_0=ruleVariable();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElementForParent(grammarAccess.getNewAttributeRule());
                      					}
                      					set(
                      						current,
                      						"source",
                      						lv_source_0_0,
                      						"com.gk_software.core.dsl.xmapping.XMapping.Variable");
                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalXMapping.g:1646:3: ( (lv_source_1_0= ruleReturn ) )
                    {
                    // InternalXMapping.g:1646:3: ( (lv_source_1_0= ruleReturn ) )
                    // InternalXMapping.g:1647:4: (lv_source_1_0= ruleReturn )
                    {
                    // InternalXMapping.g:1647:4: (lv_source_1_0= ruleReturn )
                    // InternalXMapping.g:1648:5: lv_source_1_0= ruleReturn
                    {
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getNewAttributeAccess().getSourceReturnParserRuleCall_1_0());
                      				
                    }
                    pushFollow(FOLLOW_2);
                    lv_source_1_0=ruleReturn();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElementForParent(grammarAccess.getNewAttributeRule());
                      					}
                      					set(
                      						current,
                      						"source",
                      						lv_source_1_0,
                      						"com.gk_software.core.dsl.xmapping.XMapping.Return");
                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleNewAttribute"


    // $ANTLR start "entryRuleMapForwardStatement"
    // InternalXMapping.g:1669:1: entryRuleMapForwardStatement returns [EObject current=null] : iv_ruleMapForwardStatement= ruleMapForwardStatement EOF ;
    public final EObject entryRuleMapForwardStatement() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleMapForwardStatement = null;


        try {
            // InternalXMapping.g:1669:60: (iv_ruleMapForwardStatement= ruleMapForwardStatement EOF )
            // InternalXMapping.g:1670:2: iv_ruleMapForwardStatement= ruleMapForwardStatement EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMapForwardStatementRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleMapForwardStatement=ruleMapForwardStatement();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMapForwardStatement; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMapForwardStatement"


    // $ANTLR start "ruleMapForwardStatement"
    // InternalXMapping.g:1676:1: ruleMapForwardStatement returns [EObject current=null] : ( ( (lv_inputSection_0_0= ruleInputSection ) ) otherlv_1= '->' ( (lv_outputSection_2_0= ruleOutputSection ) ) otherlv_3= ';' ) ;
    public final EObject ruleMapForwardStatement() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_inputSection_0_0 = null;

        EObject lv_outputSection_2_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:1682:2: ( ( ( (lv_inputSection_0_0= ruleInputSection ) ) otherlv_1= '->' ( (lv_outputSection_2_0= ruleOutputSection ) ) otherlv_3= ';' ) )
            // InternalXMapping.g:1683:2: ( ( (lv_inputSection_0_0= ruleInputSection ) ) otherlv_1= '->' ( (lv_outputSection_2_0= ruleOutputSection ) ) otherlv_3= ';' )
            {
            // InternalXMapping.g:1683:2: ( ( (lv_inputSection_0_0= ruleInputSection ) ) otherlv_1= '->' ( (lv_outputSection_2_0= ruleOutputSection ) ) otherlv_3= ';' )
            // InternalXMapping.g:1684:3: ( (lv_inputSection_0_0= ruleInputSection ) ) otherlv_1= '->' ( (lv_outputSection_2_0= ruleOutputSection ) ) otherlv_3= ';'
            {
            // InternalXMapping.g:1684:3: ( (lv_inputSection_0_0= ruleInputSection ) )
            // InternalXMapping.g:1685:4: (lv_inputSection_0_0= ruleInputSection )
            {
            // InternalXMapping.g:1685:4: (lv_inputSection_0_0= ruleInputSection )
            // InternalXMapping.g:1686:5: lv_inputSection_0_0= ruleInputSection
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getMapForwardStatementAccess().getInputSectionInputSectionParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_15);
            lv_inputSection_0_0=ruleInputSection();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getMapForwardStatementRule());
              					}
              					set(
              						current,
              						"inputSection",
              						lv_inputSection_0_0,
              						"com.gk_software.core.dsl.xmapping.XMapping.InputSection");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_1=(Token)match(input,26,FOLLOW_24); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getMapForwardStatementAccess().getHyphenMinusGreaterThanSignKeyword_1());
              		
            }
            // InternalXMapping.g:1707:3: ( (lv_outputSection_2_0= ruleOutputSection ) )
            // InternalXMapping.g:1708:4: (lv_outputSection_2_0= ruleOutputSection )
            {
            // InternalXMapping.g:1708:4: (lv_outputSection_2_0= ruleOutputSection )
            // InternalXMapping.g:1709:5: lv_outputSection_2_0= ruleOutputSection
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getMapForwardStatementAccess().getOutputSectionOutputSectionParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_4);
            lv_outputSection_2_0=ruleOutputSection();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getMapForwardStatementRule());
              					}
              					set(
              						current,
              						"outputSection",
              						lv_outputSection_2_0,
              						"com.gk_software.core.dsl.xmapping.XMapping.OutputSection");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            otherlv_3=(Token)match(input,21,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_3, grammarAccess.getMapForwardStatementAccess().getSemicolonKeyword_3());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMapForwardStatement"


    // $ANTLR start "entryRuleInputSection"
    // InternalXMapping.g:1734:1: entryRuleInputSection returns [EObject current=null] : iv_ruleInputSection= ruleInputSection EOF ;
    public final EObject entryRuleInputSection() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInputSection = null;


        try {
            // InternalXMapping.g:1734:53: (iv_ruleInputSection= ruleInputSection EOF )
            // InternalXMapping.g:1735:2: iv_ruleInputSection= ruleInputSection EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInputSectionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleInputSection=ruleInputSection();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInputSection; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInputSection"


    // $ANTLR start "ruleInputSection"
    // InternalXMapping.g:1741:1: ruleInputSection returns [EObject current=null] : ( ( ( (lv_inputAttributes_0_0= ruleInputAttribute ) ) | ( (lv_inputAttributes_1_0= ruleAttribute ) ) ) (otherlv_2= ',' ( ( (lv_inputAttributes_3_0= ruleInputAttribute ) ) | ( (lv_inputAttributes_4_0= ruleAttribute ) ) ) )* ) ;
    public final EObject ruleInputSection() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject lv_inputAttributes_0_0 = null;

        EObject lv_inputAttributes_1_0 = null;

        EObject lv_inputAttributes_3_0 = null;

        EObject lv_inputAttributes_4_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:1747:2: ( ( ( ( (lv_inputAttributes_0_0= ruleInputAttribute ) ) | ( (lv_inputAttributes_1_0= ruleAttribute ) ) ) (otherlv_2= ',' ( ( (lv_inputAttributes_3_0= ruleInputAttribute ) ) | ( (lv_inputAttributes_4_0= ruleAttribute ) ) ) )* ) )
            // InternalXMapping.g:1748:2: ( ( ( (lv_inputAttributes_0_0= ruleInputAttribute ) ) | ( (lv_inputAttributes_1_0= ruleAttribute ) ) ) (otherlv_2= ',' ( ( (lv_inputAttributes_3_0= ruleInputAttribute ) ) | ( (lv_inputAttributes_4_0= ruleAttribute ) ) ) )* )
            {
            // InternalXMapping.g:1748:2: ( ( ( (lv_inputAttributes_0_0= ruleInputAttribute ) ) | ( (lv_inputAttributes_1_0= ruleAttribute ) ) ) (otherlv_2= ',' ( ( (lv_inputAttributes_3_0= ruleInputAttribute ) ) | ( (lv_inputAttributes_4_0= ruleAttribute ) ) ) )* )
            // InternalXMapping.g:1749:3: ( ( (lv_inputAttributes_0_0= ruleInputAttribute ) ) | ( (lv_inputAttributes_1_0= ruleAttribute ) ) ) (otherlv_2= ',' ( ( (lv_inputAttributes_3_0= ruleInputAttribute ) ) | ( (lv_inputAttributes_4_0= ruleAttribute ) ) ) )*
            {
            // InternalXMapping.g:1749:3: ( ( (lv_inputAttributes_0_0= ruleInputAttribute ) ) | ( (lv_inputAttributes_1_0= ruleAttribute ) ) )
            int alt20=2;
            int LA20_0 = input.LA(1);

            if ( (LA20_0==RULE_ID||LA20_0==RULE_INPUT_KEY||LA20_0==RULE_PARAM_KEY||LA20_0==20||LA20_0==22||LA20_0==25||LA20_0==31||LA20_0==33) ) {
                alt20=1;
            }
            else if ( (LA20_0==RULE_VARIABLE_KEY) ) {
                alt20=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 20, 0, input);

                throw nvae;
            }
            switch (alt20) {
                case 1 :
                    // InternalXMapping.g:1750:4: ( (lv_inputAttributes_0_0= ruleInputAttribute ) )
                    {
                    // InternalXMapping.g:1750:4: ( (lv_inputAttributes_0_0= ruleInputAttribute ) )
                    // InternalXMapping.g:1751:5: (lv_inputAttributes_0_0= ruleInputAttribute )
                    {
                    // InternalXMapping.g:1751:5: (lv_inputAttributes_0_0= ruleInputAttribute )
                    // InternalXMapping.g:1752:6: lv_inputAttributes_0_0= ruleInputAttribute
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getInputSectionAccess().getInputAttributesInputAttributeParserRuleCall_0_0_0());
                      					
                    }
                    pushFollow(FOLLOW_32);
                    lv_inputAttributes_0_0=ruleInputAttribute();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getInputSectionRule());
                      						}
                      						add(
                      							current,
                      							"inputAttributes",
                      							lv_inputAttributes_0_0,
                      							"com.gk_software.core.dsl.xmapping.XMapping.InputAttribute");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalXMapping.g:1770:4: ( (lv_inputAttributes_1_0= ruleAttribute ) )
                    {
                    // InternalXMapping.g:1770:4: ( (lv_inputAttributes_1_0= ruleAttribute ) )
                    // InternalXMapping.g:1771:5: (lv_inputAttributes_1_0= ruleAttribute )
                    {
                    // InternalXMapping.g:1771:5: (lv_inputAttributes_1_0= ruleAttribute )
                    // InternalXMapping.g:1772:6: lv_inputAttributes_1_0= ruleAttribute
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getInputSectionAccess().getInputAttributesAttributeParserRuleCall_0_1_0());
                      					
                    }
                    pushFollow(FOLLOW_32);
                    lv_inputAttributes_1_0=ruleAttribute();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getInputSectionRule());
                      						}
                      						add(
                      							current,
                      							"inputAttributes",
                      							lv_inputAttributes_1_0,
                      							"com.gk_software.core.dsl.xmapping.XMapping.Attribute");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalXMapping.g:1790:3: (otherlv_2= ',' ( ( (lv_inputAttributes_3_0= ruleInputAttribute ) ) | ( (lv_inputAttributes_4_0= ruleAttribute ) ) ) )*
            loop22:
            do {
                int alt22=2;
                int LA22_0 = input.LA(1);

                if ( (LA22_0==40) ) {
                    alt22=1;
                }


                switch (alt22) {
            	case 1 :
            	    // InternalXMapping.g:1791:4: otherlv_2= ',' ( ( (lv_inputAttributes_3_0= ruleInputAttribute ) ) | ( (lv_inputAttributes_4_0= ruleAttribute ) ) )
            	    {
            	    otherlv_2=(Token)match(input,40,FOLLOW_22); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_2, grammarAccess.getInputSectionAccess().getCommaKeyword_1_0());
            	      			
            	    }
            	    // InternalXMapping.g:1795:4: ( ( (lv_inputAttributes_3_0= ruleInputAttribute ) ) | ( (lv_inputAttributes_4_0= ruleAttribute ) ) )
            	    int alt21=2;
            	    int LA21_0 = input.LA(1);

            	    if ( (LA21_0==RULE_ID||LA21_0==RULE_INPUT_KEY||LA21_0==RULE_PARAM_KEY||LA21_0==20||LA21_0==22||LA21_0==25||LA21_0==31||LA21_0==33) ) {
            	        alt21=1;
            	    }
            	    else if ( (LA21_0==RULE_VARIABLE_KEY) ) {
            	        alt21=2;
            	    }
            	    else {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 21, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt21) {
            	        case 1 :
            	            // InternalXMapping.g:1796:5: ( (lv_inputAttributes_3_0= ruleInputAttribute ) )
            	            {
            	            // InternalXMapping.g:1796:5: ( (lv_inputAttributes_3_0= ruleInputAttribute ) )
            	            // InternalXMapping.g:1797:6: (lv_inputAttributes_3_0= ruleInputAttribute )
            	            {
            	            // InternalXMapping.g:1797:6: (lv_inputAttributes_3_0= ruleInputAttribute )
            	            // InternalXMapping.g:1798:7: lv_inputAttributes_3_0= ruleInputAttribute
            	            {
            	            if ( state.backtracking==0 ) {

            	              							newCompositeNode(grammarAccess.getInputSectionAccess().getInputAttributesInputAttributeParserRuleCall_1_1_0_0());
            	              						
            	            }
            	            pushFollow(FOLLOW_32);
            	            lv_inputAttributes_3_0=ruleInputAttribute();

            	            state._fsp--;
            	            if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              							if (current==null) {
            	              								current = createModelElementForParent(grammarAccess.getInputSectionRule());
            	              							}
            	              							add(
            	              								current,
            	              								"inputAttributes",
            	              								lv_inputAttributes_3_0,
            	              								"com.gk_software.core.dsl.xmapping.XMapping.InputAttribute");
            	              							afterParserOrEnumRuleCall();
            	              						
            	            }

            	            }


            	            }


            	            }
            	            break;
            	        case 2 :
            	            // InternalXMapping.g:1816:5: ( (lv_inputAttributes_4_0= ruleAttribute ) )
            	            {
            	            // InternalXMapping.g:1816:5: ( (lv_inputAttributes_4_0= ruleAttribute ) )
            	            // InternalXMapping.g:1817:6: (lv_inputAttributes_4_0= ruleAttribute )
            	            {
            	            // InternalXMapping.g:1817:6: (lv_inputAttributes_4_0= ruleAttribute )
            	            // InternalXMapping.g:1818:7: lv_inputAttributes_4_0= ruleAttribute
            	            {
            	            if ( state.backtracking==0 ) {

            	              							newCompositeNode(grammarAccess.getInputSectionAccess().getInputAttributesAttributeParserRuleCall_1_1_1_0());
            	              						
            	            }
            	            pushFollow(FOLLOW_32);
            	            lv_inputAttributes_4_0=ruleAttribute();

            	            state._fsp--;
            	            if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              							if (current==null) {
            	              								current = createModelElementForParent(grammarAccess.getInputSectionRule());
            	              							}
            	              							add(
            	              								current,
            	              								"inputAttributes",
            	              								lv_inputAttributes_4_0,
            	              								"com.gk_software.core.dsl.xmapping.XMapping.Attribute");
            	              							afterParserOrEnumRuleCall();
            	              						
            	            }

            	            }


            	            }


            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop22;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInputSection"


    // $ANTLR start "entryRuleOutputSection"
    // InternalXMapping.g:1841:1: entryRuleOutputSection returns [EObject current=null] : iv_ruleOutputSection= ruleOutputSection EOF ;
    public final EObject entryRuleOutputSection() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOutputSection = null;


        try {
            // InternalXMapping.g:1841:54: (iv_ruleOutputSection= ruleOutputSection EOF )
            // InternalXMapping.g:1842:2: iv_ruleOutputSection= ruleOutputSection EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOutputSectionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleOutputSection=ruleOutputSection();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOutputSection; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutputSection"


    // $ANTLR start "ruleOutputSection"
    // InternalXMapping.g:1848:1: ruleOutputSection returns [EObject current=null] : ( ( ( (lv_outputAttributes_0_0= ruleOutputAttribute ) ) | ( (lv_outputAttributes_1_0= ruleAttribute ) ) ) (otherlv_2= ',' ( ( (lv_outputAttributes_3_0= ruleOutputAttribute ) ) | ( (lv_outputAttributes_4_0= ruleAttribute ) ) ) )* ) ;
    public final EObject ruleOutputSection() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        EObject lv_outputAttributes_0_0 = null;

        EObject lv_outputAttributes_1_0 = null;

        EObject lv_outputAttributes_3_0 = null;

        EObject lv_outputAttributes_4_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:1854:2: ( ( ( ( (lv_outputAttributes_0_0= ruleOutputAttribute ) ) | ( (lv_outputAttributes_1_0= ruleAttribute ) ) ) (otherlv_2= ',' ( ( (lv_outputAttributes_3_0= ruleOutputAttribute ) ) | ( (lv_outputAttributes_4_0= ruleAttribute ) ) ) )* ) )
            // InternalXMapping.g:1855:2: ( ( ( (lv_outputAttributes_0_0= ruleOutputAttribute ) ) | ( (lv_outputAttributes_1_0= ruleAttribute ) ) ) (otherlv_2= ',' ( ( (lv_outputAttributes_3_0= ruleOutputAttribute ) ) | ( (lv_outputAttributes_4_0= ruleAttribute ) ) ) )* )
            {
            // InternalXMapping.g:1855:2: ( ( ( (lv_outputAttributes_0_0= ruleOutputAttribute ) ) | ( (lv_outputAttributes_1_0= ruleAttribute ) ) ) (otherlv_2= ',' ( ( (lv_outputAttributes_3_0= ruleOutputAttribute ) ) | ( (lv_outputAttributes_4_0= ruleAttribute ) ) ) )* )
            // InternalXMapping.g:1856:3: ( ( (lv_outputAttributes_0_0= ruleOutputAttribute ) ) | ( (lv_outputAttributes_1_0= ruleAttribute ) ) ) (otherlv_2= ',' ( ( (lv_outputAttributes_3_0= ruleOutputAttribute ) ) | ( (lv_outputAttributes_4_0= ruleAttribute ) ) ) )*
            {
            // InternalXMapping.g:1856:3: ( ( (lv_outputAttributes_0_0= ruleOutputAttribute ) ) | ( (lv_outputAttributes_1_0= ruleAttribute ) ) )
            int alt23=2;
            int LA23_0 = input.LA(1);

            if ( (LA23_0==RULE_ID||LA23_0==RULE_OUTPUT_KEY||LA23_0==RULE_RETURN_KEY||LA23_0==20||LA23_0==22||LA23_0==25||LA23_0==31||LA23_0==33) ) {
                alt23=1;
            }
            else if ( (LA23_0==RULE_VARIABLE_KEY) ) {
                alt23=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 23, 0, input);

                throw nvae;
            }
            switch (alt23) {
                case 1 :
                    // InternalXMapping.g:1857:4: ( (lv_outputAttributes_0_0= ruleOutputAttribute ) )
                    {
                    // InternalXMapping.g:1857:4: ( (lv_outputAttributes_0_0= ruleOutputAttribute ) )
                    // InternalXMapping.g:1858:5: (lv_outputAttributes_0_0= ruleOutputAttribute )
                    {
                    // InternalXMapping.g:1858:5: (lv_outputAttributes_0_0= ruleOutputAttribute )
                    // InternalXMapping.g:1859:6: lv_outputAttributes_0_0= ruleOutputAttribute
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getOutputSectionAccess().getOutputAttributesOutputAttributeParserRuleCall_0_0_0());
                      					
                    }
                    pushFollow(FOLLOW_32);
                    lv_outputAttributes_0_0=ruleOutputAttribute();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getOutputSectionRule());
                      						}
                      						add(
                      							current,
                      							"outputAttributes",
                      							lv_outputAttributes_0_0,
                      							"com.gk_software.core.dsl.xmapping.XMapping.OutputAttribute");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalXMapping.g:1877:4: ( (lv_outputAttributes_1_0= ruleAttribute ) )
                    {
                    // InternalXMapping.g:1877:4: ( (lv_outputAttributes_1_0= ruleAttribute ) )
                    // InternalXMapping.g:1878:5: (lv_outputAttributes_1_0= ruleAttribute )
                    {
                    // InternalXMapping.g:1878:5: (lv_outputAttributes_1_0= ruleAttribute )
                    // InternalXMapping.g:1879:6: lv_outputAttributes_1_0= ruleAttribute
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getOutputSectionAccess().getOutputAttributesAttributeParserRuleCall_0_1_0());
                      					
                    }
                    pushFollow(FOLLOW_32);
                    lv_outputAttributes_1_0=ruleAttribute();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getOutputSectionRule());
                      						}
                      						add(
                      							current,
                      							"outputAttributes",
                      							lv_outputAttributes_1_0,
                      							"com.gk_software.core.dsl.xmapping.XMapping.Attribute");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalXMapping.g:1897:3: (otherlv_2= ',' ( ( (lv_outputAttributes_3_0= ruleOutputAttribute ) ) | ( (lv_outputAttributes_4_0= ruleAttribute ) ) ) )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( (LA25_0==40) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // InternalXMapping.g:1898:4: otherlv_2= ',' ( ( (lv_outputAttributes_3_0= ruleOutputAttribute ) ) | ( (lv_outputAttributes_4_0= ruleAttribute ) ) )
            	    {
            	    otherlv_2=(Token)match(input,40,FOLLOW_24); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_2, grammarAccess.getOutputSectionAccess().getCommaKeyword_1_0());
            	      			
            	    }
            	    // InternalXMapping.g:1902:4: ( ( (lv_outputAttributes_3_0= ruleOutputAttribute ) ) | ( (lv_outputAttributes_4_0= ruleAttribute ) ) )
            	    int alt24=2;
            	    int LA24_0 = input.LA(1);

            	    if ( (LA24_0==RULE_ID||LA24_0==RULE_OUTPUT_KEY||LA24_0==RULE_RETURN_KEY||LA24_0==20||LA24_0==22||LA24_0==25||LA24_0==31||LA24_0==33) ) {
            	        alt24=1;
            	    }
            	    else if ( (LA24_0==RULE_VARIABLE_KEY) ) {
            	        alt24=2;
            	    }
            	    else {
            	        if (state.backtracking>0) {state.failed=true; return current;}
            	        NoViableAltException nvae =
            	            new NoViableAltException("", 24, 0, input);

            	        throw nvae;
            	    }
            	    switch (alt24) {
            	        case 1 :
            	            // InternalXMapping.g:1903:5: ( (lv_outputAttributes_3_0= ruleOutputAttribute ) )
            	            {
            	            // InternalXMapping.g:1903:5: ( (lv_outputAttributes_3_0= ruleOutputAttribute ) )
            	            // InternalXMapping.g:1904:6: (lv_outputAttributes_3_0= ruleOutputAttribute )
            	            {
            	            // InternalXMapping.g:1904:6: (lv_outputAttributes_3_0= ruleOutputAttribute )
            	            // InternalXMapping.g:1905:7: lv_outputAttributes_3_0= ruleOutputAttribute
            	            {
            	            if ( state.backtracking==0 ) {

            	              							newCompositeNode(grammarAccess.getOutputSectionAccess().getOutputAttributesOutputAttributeParserRuleCall_1_1_0_0());
            	              						
            	            }
            	            pushFollow(FOLLOW_32);
            	            lv_outputAttributes_3_0=ruleOutputAttribute();

            	            state._fsp--;
            	            if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              							if (current==null) {
            	              								current = createModelElementForParent(grammarAccess.getOutputSectionRule());
            	              							}
            	              							add(
            	              								current,
            	              								"outputAttributes",
            	              								lv_outputAttributes_3_0,
            	              								"com.gk_software.core.dsl.xmapping.XMapping.OutputAttribute");
            	              							afterParserOrEnumRuleCall();
            	              						
            	            }

            	            }


            	            }


            	            }
            	            break;
            	        case 2 :
            	            // InternalXMapping.g:1923:5: ( (lv_outputAttributes_4_0= ruleAttribute ) )
            	            {
            	            // InternalXMapping.g:1923:5: ( (lv_outputAttributes_4_0= ruleAttribute ) )
            	            // InternalXMapping.g:1924:6: (lv_outputAttributes_4_0= ruleAttribute )
            	            {
            	            // InternalXMapping.g:1924:6: (lv_outputAttributes_4_0= ruleAttribute )
            	            // InternalXMapping.g:1925:7: lv_outputAttributes_4_0= ruleAttribute
            	            {
            	            if ( state.backtracking==0 ) {

            	              							newCompositeNode(grammarAccess.getOutputSectionAccess().getOutputAttributesAttributeParserRuleCall_1_1_1_0());
            	              						
            	            }
            	            pushFollow(FOLLOW_32);
            	            lv_outputAttributes_4_0=ruleAttribute();

            	            state._fsp--;
            	            if (state.failed) return current;
            	            if ( state.backtracking==0 ) {

            	              							if (current==null) {
            	              								current = createModelElementForParent(grammarAccess.getOutputSectionRule());
            	              							}
            	              							add(
            	              								current,
            	              								"outputAttributes",
            	              								lv_outputAttributes_4_0,
            	              								"com.gk_software.core.dsl.xmapping.XMapping.Attribute");
            	              							afterParserOrEnumRuleCall();
            	              						
            	            }

            	            }


            	            }


            	            }
            	            break;

            	    }


            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutputSection"


    // $ANTLR start "entryRuleParamSection"
    // InternalXMapping.g:1948:1: entryRuleParamSection returns [EObject current=null] : iv_ruleParamSection= ruleParamSection EOF ;
    public final EObject entryRuleParamSection() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParamSection = null;


        try {
            // InternalXMapping.g:1948:53: (iv_ruleParamSection= ruleParamSection EOF )
            // InternalXMapping.g:1949:2: iv_ruleParamSection= ruleParamSection EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getParamSectionRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleParamSection=ruleParamSection();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleParamSection; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParamSection"


    // $ANTLR start "ruleParamSection"
    // InternalXMapping.g:1955:1: ruleParamSection returns [EObject current=null] : ( ( (lv_paramSection_0_0= ruleParam ) ) (otherlv_1= ',' ( (lv_paramSection_2_0= ruleParam ) ) )* ) ;
    public final EObject ruleParamSection() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_paramSection_0_0 = null;

        EObject lv_paramSection_2_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:1961:2: ( ( ( (lv_paramSection_0_0= ruleParam ) ) (otherlv_1= ',' ( (lv_paramSection_2_0= ruleParam ) ) )* ) )
            // InternalXMapping.g:1962:2: ( ( (lv_paramSection_0_0= ruleParam ) ) (otherlv_1= ',' ( (lv_paramSection_2_0= ruleParam ) ) )* )
            {
            // InternalXMapping.g:1962:2: ( ( (lv_paramSection_0_0= ruleParam ) ) (otherlv_1= ',' ( (lv_paramSection_2_0= ruleParam ) ) )* )
            // InternalXMapping.g:1963:3: ( (lv_paramSection_0_0= ruleParam ) ) (otherlv_1= ',' ( (lv_paramSection_2_0= ruleParam ) ) )*
            {
            // InternalXMapping.g:1963:3: ( (lv_paramSection_0_0= ruleParam ) )
            // InternalXMapping.g:1964:4: (lv_paramSection_0_0= ruleParam )
            {
            // InternalXMapping.g:1964:4: (lv_paramSection_0_0= ruleParam )
            // InternalXMapping.g:1965:5: lv_paramSection_0_0= ruleParam
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getParamSectionAccess().getParamSectionParamParserRuleCall_0_0());
              				
            }
            pushFollow(FOLLOW_32);
            lv_paramSection_0_0=ruleParam();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getParamSectionRule());
              					}
              					add(
              						current,
              						"paramSection",
              						lv_paramSection_0_0,
              						"com.gk_software.core.dsl.xmapping.XMapping.Param");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalXMapping.g:1982:3: (otherlv_1= ',' ( (lv_paramSection_2_0= ruleParam ) ) )*
            loop26:
            do {
                int alt26=2;
                int LA26_0 = input.LA(1);

                if ( (LA26_0==40) ) {
                    alt26=1;
                }


                switch (alt26) {
            	case 1 :
            	    // InternalXMapping.g:1983:4: otherlv_1= ',' ( (lv_paramSection_2_0= ruleParam ) )
            	    {
            	    otherlv_1=(Token)match(input,40,FOLLOW_33); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(otherlv_1, grammarAccess.getParamSectionAccess().getCommaKeyword_1_0());
            	      			
            	    }
            	    // InternalXMapping.g:1987:4: ( (lv_paramSection_2_0= ruleParam ) )
            	    // InternalXMapping.g:1988:5: (lv_paramSection_2_0= ruleParam )
            	    {
            	    // InternalXMapping.g:1988:5: (lv_paramSection_2_0= ruleParam )
            	    // InternalXMapping.g:1989:6: lv_paramSection_2_0= ruleParam
            	    {
            	    if ( state.backtracking==0 ) {

            	      						newCompositeNode(grammarAccess.getParamSectionAccess().getParamSectionParamParserRuleCall_1_1_0());
            	      					
            	    }
            	    pushFollow(FOLLOW_32);
            	    lv_paramSection_2_0=ruleParam();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      						if (current==null) {
            	      							current = createModelElementForParent(grammarAccess.getParamSectionRule());
            	      						}
            	      						add(
            	      							current,
            	      							"paramSection",
            	      							lv_paramSection_2_0,
            	      							"com.gk_software.core.dsl.xmapping.XMapping.Param");
            	      						afterParserOrEnumRuleCall();
            	      					
            	    }

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop26;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParamSection"


    // $ANTLR start "entryRuleAttribute"
    // InternalXMapping.g:2011:1: entryRuleAttribute returns [EObject current=null] : iv_ruleAttribute= ruleAttribute EOF ;
    public final EObject entryRuleAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleAttribute = null;


        try {
            // InternalXMapping.g:2011:50: (iv_ruleAttribute= ruleAttribute EOF )
            // InternalXMapping.g:2012:2: iv_ruleAttribute= ruleAttribute EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getAttributeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleAttribute=ruleAttribute();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleAttribute; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleAttribute"


    // $ANTLR start "ruleAttribute"
    // InternalXMapping.g:2018:1: ruleAttribute returns [EObject current=null] : ( ( ( ruleVariableName ) ) (otherlv_1= ':' ( (lv_xPath_2_0= RULE_STRING ) ) )? ) ;
    public final EObject ruleAttribute() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token lv_xPath_2_0=null;


        	enterRule();

        try {
            // InternalXMapping.g:2024:2: ( ( ( ( ruleVariableName ) ) (otherlv_1= ':' ( (lv_xPath_2_0= RULE_STRING ) ) )? ) )
            // InternalXMapping.g:2025:2: ( ( ( ruleVariableName ) ) (otherlv_1= ':' ( (lv_xPath_2_0= RULE_STRING ) ) )? )
            {
            // InternalXMapping.g:2025:2: ( ( ( ruleVariableName ) ) (otherlv_1= ':' ( (lv_xPath_2_0= RULE_STRING ) ) )? )
            // InternalXMapping.g:2026:3: ( ( ruleVariableName ) ) (otherlv_1= ':' ( (lv_xPath_2_0= RULE_STRING ) ) )?
            {
            // InternalXMapping.g:2026:3: ( ( ruleVariableName ) )
            // InternalXMapping.g:2027:4: ( ruleVariableName )
            {
            // InternalXMapping.g:2027:4: ( ruleVariableName )
            // InternalXMapping.g:2028:5: ruleVariableName
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getAttributeRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getAttributeAccess().getTypeVariableCrossReference_0_0());
              				
            }
            pushFollow(FOLLOW_34);
            ruleVariableName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalXMapping.g:2045:3: (otherlv_1= ':' ( (lv_xPath_2_0= RULE_STRING ) ) )?
            int alt27=2;
            int LA27_0 = input.LA(1);

            if ( (LA27_0==24) ) {
                alt27=1;
            }
            switch (alt27) {
                case 1 :
                    // InternalXMapping.g:2046:4: otherlv_1= ':' ( (lv_xPath_2_0= RULE_STRING ) )
                    {
                    otherlv_1=(Token)match(input,24,FOLLOW_13); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_1, grammarAccess.getAttributeAccess().getColonKeyword_1_0());
                      			
                    }
                    // InternalXMapping.g:2050:4: ( (lv_xPath_2_0= RULE_STRING ) )
                    // InternalXMapping.g:2051:5: (lv_xPath_2_0= RULE_STRING )
                    {
                    // InternalXMapping.g:2051:5: (lv_xPath_2_0= RULE_STRING )
                    // InternalXMapping.g:2052:6: lv_xPath_2_0= RULE_STRING
                    {
                    lv_xPath_2_0=(Token)match(input,RULE_STRING,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_xPath_2_0, grammarAccess.getAttributeAccess().getXPathSTRINGTerminalRuleCall_1_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getAttributeRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"xPath",
                      							lv_xPath_2_0,
                      							"org.eclipse.xtext.xbase.Xtype.STRING");
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleAttribute"


    // $ANTLR start "entryRuleOutputAttribute"
    // InternalXMapping.g:2073:1: entryRuleOutputAttribute returns [EObject current=null] : iv_ruleOutputAttribute= ruleOutputAttribute EOF ;
    public final EObject entryRuleOutputAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOutputAttribute = null;


        try {
            // InternalXMapping.g:2073:56: (iv_ruleOutputAttribute= ruleOutputAttribute EOF )
            // InternalXMapping.g:2074:2: iv_ruleOutputAttribute= ruleOutputAttribute EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOutputAttributeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleOutputAttribute=ruleOutputAttribute();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOutputAttribute; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutputAttribute"


    // $ANTLR start "ruleOutputAttribute"
    // InternalXMapping.g:2080:1: ruleOutputAttribute returns [EObject current=null] : ( ( ( ( ruleQualifiedName ) ) () otherlv_2= '.' )? ( (lv_outputType_3_0= ruleOutputType ) ) (otherlv_4= ':' ( (lv_xPath_5_0= RULE_STRING ) ) )? ) ;
    public final EObject ruleOutputAttribute() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_4=null;
        Token lv_xPath_5_0=null;
        EObject lv_outputType_3_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2086:2: ( ( ( ( ( ruleQualifiedName ) ) () otherlv_2= '.' )? ( (lv_outputType_3_0= ruleOutputType ) ) (otherlv_4= ':' ( (lv_xPath_5_0= RULE_STRING ) ) )? ) )
            // InternalXMapping.g:2087:2: ( ( ( ( ruleQualifiedName ) ) () otherlv_2= '.' )? ( (lv_outputType_3_0= ruleOutputType ) ) (otherlv_4= ':' ( (lv_xPath_5_0= RULE_STRING ) ) )? )
            {
            // InternalXMapping.g:2087:2: ( ( ( ( ruleQualifiedName ) ) () otherlv_2= '.' )? ( (lv_outputType_3_0= ruleOutputType ) ) (otherlv_4= ':' ( (lv_xPath_5_0= RULE_STRING ) ) )? )
            // InternalXMapping.g:2088:3: ( ( ( ruleQualifiedName ) ) () otherlv_2= '.' )? ( (lv_outputType_3_0= ruleOutputType ) ) (otherlv_4= ':' ( (lv_xPath_5_0= RULE_STRING ) ) )?
            {
            // InternalXMapping.g:2088:3: ( ( ( ruleQualifiedName ) ) () otherlv_2= '.' )?
            int alt28=2;
            int LA28_0 = input.LA(1);

            if ( (LA28_0==RULE_ID||LA28_0==20||LA28_0==22||LA28_0==25||LA28_0==31||LA28_0==33) ) {
                alt28=1;
            }
            switch (alt28) {
                case 1 :
                    // InternalXMapping.g:2089:4: ( ( ruleQualifiedName ) ) () otherlv_2= '.'
                    {
                    // InternalXMapping.g:2089:4: ( ( ruleQualifiedName ) )
                    // InternalXMapping.g:2090:5: ( ruleQualifiedName )
                    {
                    // InternalXMapping.g:2090:5: ( ruleQualifiedName )
                    // InternalXMapping.g:2091:6: ruleQualifiedName
                    {
                    if ( state.backtracking==0 ) {

                      						/* */
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getOutputAttributeRule());
                      						}
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getOutputAttributeAccess().getClassMappingClassCrossReference_0_0_0());
                      					
                    }
                    pushFollow(FOLLOW_27);
                    ruleQualifiedName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalXMapping.g:2108:4: ()
                    // InternalXMapping.g:2109:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					/* */
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElementAndSet(
                      						grammarAccess.getOutputAttributeAccess().getOutputAttributeTargetAction_0_1(),
                      						current);
                      				
                    }

                    }

                    otherlv_2=(Token)match(input,35,FOLLOW_24); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getOutputAttributeAccess().getFullStopKeyword_0_2());
                      			
                    }

                    }
                    break;

            }

            // InternalXMapping.g:2123:3: ( (lv_outputType_3_0= ruleOutputType ) )
            // InternalXMapping.g:2124:4: (lv_outputType_3_0= ruleOutputType )
            {
            // InternalXMapping.g:2124:4: (lv_outputType_3_0= ruleOutputType )
            // InternalXMapping.g:2125:5: lv_outputType_3_0= ruleOutputType
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getOutputAttributeAccess().getOutputTypeOutputTypeParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_34);
            lv_outputType_3_0=ruleOutputType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getOutputAttributeRule());
              					}
              					set(
              						current,
              						"outputType",
              						lv_outputType_3_0,
              						"com.gk_software.core.dsl.xmapping.XMapping.OutputType");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalXMapping.g:2142:3: (otherlv_4= ':' ( (lv_xPath_5_0= RULE_STRING ) ) )?
            int alt29=2;
            int LA29_0 = input.LA(1);

            if ( (LA29_0==24) ) {
                alt29=1;
            }
            switch (alt29) {
                case 1 :
                    // InternalXMapping.g:2143:4: otherlv_4= ':' ( (lv_xPath_5_0= RULE_STRING ) )
                    {
                    otherlv_4=(Token)match(input,24,FOLLOW_13); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_4, grammarAccess.getOutputAttributeAccess().getColonKeyword_2_0());
                      			
                    }
                    // InternalXMapping.g:2147:4: ( (lv_xPath_5_0= RULE_STRING ) )
                    // InternalXMapping.g:2148:5: (lv_xPath_5_0= RULE_STRING )
                    {
                    // InternalXMapping.g:2148:5: (lv_xPath_5_0= RULE_STRING )
                    // InternalXMapping.g:2149:6: lv_xPath_5_0= RULE_STRING
                    {
                    lv_xPath_5_0=(Token)match(input,RULE_STRING,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_xPath_5_0, grammarAccess.getOutputAttributeAccess().getXPathSTRINGTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getOutputAttributeRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"xPath",
                      							lv_xPath_5_0,
                      							"org.eclipse.xtext.xbase.Xtype.STRING");
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutputAttribute"


    // $ANTLR start "entryRuleOutputType"
    // InternalXMapping.g:2170:1: entryRuleOutputType returns [EObject current=null] : iv_ruleOutputType= ruleOutputType EOF ;
    public final EObject entryRuleOutputType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOutputType = null;


        try {
            // InternalXMapping.g:2170:51: (iv_ruleOutputType= ruleOutputType EOF )
            // InternalXMapping.g:2171:2: iv_ruleOutputType= ruleOutputType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOutputTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleOutputType=ruleOutputType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOutputType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutputType"


    // $ANTLR start "ruleOutputType"
    // InternalXMapping.g:2177:1: ruleOutputType returns [EObject current=null] : ( ( ( ruleOutputName ) ) | ( ( ruleReturnName ) ) ) ;
    public final EObject ruleOutputType() throws RecognitionException {
        EObject current = null;


        	enterRule();

        try {
            // InternalXMapping.g:2183:2: ( ( ( ( ruleOutputName ) ) | ( ( ruleReturnName ) ) ) )
            // InternalXMapping.g:2184:2: ( ( ( ruleOutputName ) ) | ( ( ruleReturnName ) ) )
            {
            // InternalXMapping.g:2184:2: ( ( ( ruleOutputName ) ) | ( ( ruleReturnName ) ) )
            int alt30=2;
            int LA30_0 = input.LA(1);

            if ( (LA30_0==RULE_OUTPUT_KEY) ) {
                alt30=1;
            }
            else if ( (LA30_0==RULE_RETURN_KEY) ) {
                alt30=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 30, 0, input);

                throw nvae;
            }
            switch (alt30) {
                case 1 :
                    // InternalXMapping.g:2185:3: ( ( ruleOutputName ) )
                    {
                    // InternalXMapping.g:2185:3: ( ( ruleOutputName ) )
                    // InternalXMapping.g:2186:4: ( ruleOutputName )
                    {
                    // InternalXMapping.g:2186:4: ( ruleOutputName )
                    // InternalXMapping.g:2187:5: ruleOutputName
                    {
                    if ( state.backtracking==0 ) {

                      					/* */
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getOutputTypeRule());
                      					}
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getOutputTypeAccess().getOutputRefOutputCrossReference_0_0());
                      				
                    }
                    pushFollow(FOLLOW_2);
                    ruleOutputName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalXMapping.g:2205:3: ( ( ruleReturnName ) )
                    {
                    // InternalXMapping.g:2205:3: ( ( ruleReturnName ) )
                    // InternalXMapping.g:2206:4: ( ruleReturnName )
                    {
                    // InternalXMapping.g:2206:4: ( ruleReturnName )
                    // InternalXMapping.g:2207:5: ruleReturnName
                    {
                    if ( state.backtracking==0 ) {

                      					/* */
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getOutputTypeRule());
                      					}
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getOutputTypeAccess().getReturnRefReturnCrossReference_1_0());
                      				
                    }
                    pushFollow(FOLLOW_2);
                    ruleReturnName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutputType"


    // $ANTLR start "entryRuleInputAttribute"
    // InternalXMapping.g:2228:1: entryRuleInputAttribute returns [EObject current=null] : iv_ruleInputAttribute= ruleInputAttribute EOF ;
    public final EObject entryRuleInputAttribute() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInputAttribute = null;


        try {
            // InternalXMapping.g:2228:55: (iv_ruleInputAttribute= ruleInputAttribute EOF )
            // InternalXMapping.g:2229:2: iv_ruleInputAttribute= ruleInputAttribute EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInputAttributeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleInputAttribute=ruleInputAttribute();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInputAttribute; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInputAttribute"


    // $ANTLR start "ruleInputAttribute"
    // InternalXMapping.g:2235:1: ruleInputAttribute returns [EObject current=null] : ( ( ( ( ruleQualifiedName ) ) () otherlv_2= '.' )? ( (lv_inputType_3_0= ruleInputType ) ) (otherlv_4= ':' ( (lv_xPath_5_0= RULE_STRING ) ) )? ) ;
    public final EObject ruleInputAttribute() throws RecognitionException {
        EObject current = null;

        Token otherlv_2=null;
        Token otherlv_4=null;
        Token lv_xPath_5_0=null;
        EObject lv_inputType_3_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2241:2: ( ( ( ( ( ruleQualifiedName ) ) () otherlv_2= '.' )? ( (lv_inputType_3_0= ruleInputType ) ) (otherlv_4= ':' ( (lv_xPath_5_0= RULE_STRING ) ) )? ) )
            // InternalXMapping.g:2242:2: ( ( ( ( ruleQualifiedName ) ) () otherlv_2= '.' )? ( (lv_inputType_3_0= ruleInputType ) ) (otherlv_4= ':' ( (lv_xPath_5_0= RULE_STRING ) ) )? )
            {
            // InternalXMapping.g:2242:2: ( ( ( ( ruleQualifiedName ) ) () otherlv_2= '.' )? ( (lv_inputType_3_0= ruleInputType ) ) (otherlv_4= ':' ( (lv_xPath_5_0= RULE_STRING ) ) )? )
            // InternalXMapping.g:2243:3: ( ( ( ruleQualifiedName ) ) () otherlv_2= '.' )? ( (lv_inputType_3_0= ruleInputType ) ) (otherlv_4= ':' ( (lv_xPath_5_0= RULE_STRING ) ) )?
            {
            // InternalXMapping.g:2243:3: ( ( ( ruleQualifiedName ) ) () otherlv_2= '.' )?
            int alt31=2;
            int LA31_0 = input.LA(1);

            if ( (LA31_0==RULE_ID||LA31_0==20||LA31_0==22||LA31_0==25||LA31_0==31||LA31_0==33) ) {
                alt31=1;
            }
            switch (alt31) {
                case 1 :
                    // InternalXMapping.g:2244:4: ( ( ruleQualifiedName ) ) () otherlv_2= '.'
                    {
                    // InternalXMapping.g:2244:4: ( ( ruleQualifiedName ) )
                    // InternalXMapping.g:2245:5: ( ruleQualifiedName )
                    {
                    // InternalXMapping.g:2245:5: ( ruleQualifiedName )
                    // InternalXMapping.g:2246:6: ruleQualifiedName
                    {
                    if ( state.backtracking==0 ) {

                      						/* */
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getInputAttributeRule());
                      						}
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getInputAttributeAccess().getClassMappingClassCrossReference_0_0_0());
                      					
                    }
                    pushFollow(FOLLOW_27);
                    ruleQualifiedName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalXMapping.g:2263:4: ()
                    // InternalXMapping.g:2264:5: 
                    {
                    if ( state.backtracking==0 ) {

                      					/* */
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					current = forceCreateModelElementAndSet(
                      						grammarAccess.getInputAttributeAccess().getInputAttributeTargetAction_0_1(),
                      						current);
                      				
                    }

                    }

                    otherlv_2=(Token)match(input,35,FOLLOW_35); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_2, grammarAccess.getInputAttributeAccess().getFullStopKeyword_0_2());
                      			
                    }

                    }
                    break;

            }

            // InternalXMapping.g:2278:3: ( (lv_inputType_3_0= ruleInputType ) )
            // InternalXMapping.g:2279:4: (lv_inputType_3_0= ruleInputType )
            {
            // InternalXMapping.g:2279:4: (lv_inputType_3_0= ruleInputType )
            // InternalXMapping.g:2280:5: lv_inputType_3_0= ruleInputType
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getInputAttributeAccess().getInputTypeInputTypeParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_34);
            lv_inputType_3_0=ruleInputType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getInputAttributeRule());
              					}
              					set(
              						current,
              						"inputType",
              						lv_inputType_3_0,
              						"com.gk_software.core.dsl.xmapping.XMapping.InputType");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalXMapping.g:2297:3: (otherlv_4= ':' ( (lv_xPath_5_0= RULE_STRING ) ) )?
            int alt32=2;
            int LA32_0 = input.LA(1);

            if ( (LA32_0==24) ) {
                alt32=1;
            }
            switch (alt32) {
                case 1 :
                    // InternalXMapping.g:2298:4: otherlv_4= ':' ( (lv_xPath_5_0= RULE_STRING ) )
                    {
                    otherlv_4=(Token)match(input,24,FOLLOW_13); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_4, grammarAccess.getInputAttributeAccess().getColonKeyword_2_0());
                      			
                    }
                    // InternalXMapping.g:2302:4: ( (lv_xPath_5_0= RULE_STRING ) )
                    // InternalXMapping.g:2303:5: (lv_xPath_5_0= RULE_STRING )
                    {
                    // InternalXMapping.g:2303:5: (lv_xPath_5_0= RULE_STRING )
                    // InternalXMapping.g:2304:6: lv_xPath_5_0= RULE_STRING
                    {
                    lv_xPath_5_0=(Token)match(input,RULE_STRING,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						newLeafNode(lv_xPath_5_0, grammarAccess.getInputAttributeAccess().getXPathSTRINGTerminalRuleCall_2_1_0());
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getInputAttributeRule());
                      						}
                      						setWithLastConsumed(
                      							current,
                      							"xPath",
                      							lv_xPath_5_0,
                      							"org.eclipse.xtext.xbase.Xtype.STRING");
                      					
                    }

                    }


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInputAttribute"


    // $ANTLR start "entryRuleInputType"
    // InternalXMapping.g:2325:1: entryRuleInputType returns [EObject current=null] : iv_ruleInputType= ruleInputType EOF ;
    public final EObject entryRuleInputType() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInputType = null;


        try {
            // InternalXMapping.g:2325:50: (iv_ruleInputType= ruleInputType EOF )
            // InternalXMapping.g:2326:2: iv_ruleInputType= ruleInputType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInputTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleInputType=ruleInputType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInputType; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInputType"


    // $ANTLR start "ruleInputType"
    // InternalXMapping.g:2332:1: ruleInputType returns [EObject current=null] : ( ( ( ruleInputName ) ) | ( ( ruleParamName ) ) ) ;
    public final EObject ruleInputType() throws RecognitionException {
        EObject current = null;


        	enterRule();

        try {
            // InternalXMapping.g:2338:2: ( ( ( ( ruleInputName ) ) | ( ( ruleParamName ) ) ) )
            // InternalXMapping.g:2339:2: ( ( ( ruleInputName ) ) | ( ( ruleParamName ) ) )
            {
            // InternalXMapping.g:2339:2: ( ( ( ruleInputName ) ) | ( ( ruleParamName ) ) )
            int alt33=2;
            int LA33_0 = input.LA(1);

            if ( (LA33_0==RULE_INPUT_KEY) ) {
                alt33=1;
            }
            else if ( (LA33_0==RULE_PARAM_KEY) ) {
                alt33=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 33, 0, input);

                throw nvae;
            }
            switch (alt33) {
                case 1 :
                    // InternalXMapping.g:2340:3: ( ( ruleInputName ) )
                    {
                    // InternalXMapping.g:2340:3: ( ( ruleInputName ) )
                    // InternalXMapping.g:2341:4: ( ruleInputName )
                    {
                    // InternalXMapping.g:2341:4: ( ruleInputName )
                    // InternalXMapping.g:2342:5: ruleInputName
                    {
                    if ( state.backtracking==0 ) {

                      					/* */
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getInputTypeRule());
                      					}
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getInputTypeAccess().getInputRefInputCrossReference_0_0());
                      				
                    }
                    pushFollow(FOLLOW_2);
                    ruleInputName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalXMapping.g:2360:3: ( ( ruleParamName ) )
                    {
                    // InternalXMapping.g:2360:3: ( ( ruleParamName ) )
                    // InternalXMapping.g:2361:4: ( ruleParamName )
                    {
                    // InternalXMapping.g:2361:4: ( ruleParamName )
                    // InternalXMapping.g:2362:5: ruleParamName
                    {
                    if ( state.backtracking==0 ) {

                      					/* */
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					if (current==null) {
                      						current = createModelElement(grammarAccess.getInputTypeRule());
                      					}
                      				
                    }
                    if ( state.backtracking==0 ) {

                      					newCompositeNode(grammarAccess.getInputTypeAccess().getParamRefParamCrossReference_1_0());
                      				
                    }
                    pushFollow(FOLLOW_2);
                    ruleParamName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					afterParserOrEnumRuleCall();
                      				
                    }

                    }


                    }


                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInputType"


    // $ANTLR start "entryRuleInputName"
    // InternalXMapping.g:2383:1: entryRuleInputName returns [String current=null] : iv_ruleInputName= ruleInputName EOF ;
    public final String entryRuleInputName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleInputName = null;


        try {
            // InternalXMapping.g:2383:49: (iv_ruleInputName= ruleInputName EOF )
            // InternalXMapping.g:2384:2: iv_ruleInputName= ruleInputName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInputNameRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleInputName=ruleInputName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInputName.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInputName"


    // $ANTLR start "ruleInputName"
    // InternalXMapping.g:2390:1: ruleInputName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_INPUT_KEY_0= RULE_INPUT_KEY (kw= '.' this_Name_2= ruleName )? ) ;
    public final AntlrDatatypeRuleToken ruleInputName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_INPUT_KEY_0=null;
        Token kw=null;
        AntlrDatatypeRuleToken this_Name_2 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2396:2: ( (this_INPUT_KEY_0= RULE_INPUT_KEY (kw= '.' this_Name_2= ruleName )? ) )
            // InternalXMapping.g:2397:2: (this_INPUT_KEY_0= RULE_INPUT_KEY (kw= '.' this_Name_2= ruleName )? )
            {
            // InternalXMapping.g:2397:2: (this_INPUT_KEY_0= RULE_INPUT_KEY (kw= '.' this_Name_2= ruleName )? )
            // InternalXMapping.g:2398:3: this_INPUT_KEY_0= RULE_INPUT_KEY (kw= '.' this_Name_2= ruleName )?
            {
            this_INPUT_KEY_0=(Token)match(input,RULE_INPUT_KEY,FOLLOW_36); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_INPUT_KEY_0);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_INPUT_KEY_0, grammarAccess.getInputNameAccess().getINPUT_KEYTerminalRuleCall_0());
              		
            }
            // InternalXMapping.g:2405:3: (kw= '.' this_Name_2= ruleName )?
            int alt34=2;
            int LA34_0 = input.LA(1);

            if ( (LA34_0==35) ) {
                alt34=1;
            }
            switch (alt34) {
                case 1 :
                    // InternalXMapping.g:2406:4: kw= '.' this_Name_2= ruleName
                    {
                    kw=(Token)match(input,35,FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(kw);
                      				newLeafNode(kw, grammarAccess.getInputNameAccess().getFullStopKeyword_1_0());
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				newCompositeNode(grammarAccess.getInputNameAccess().getNameParserRuleCall_1_1());
                      			
                    }
                    pushFollow(FOLLOW_2);
                    this_Name_2=ruleName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(this_Name_2);
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				afterParserOrEnumRuleCall();
                      			
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInputName"


    // $ANTLR start "entryRuleInput"
    // InternalXMapping.g:2426:1: entryRuleInput returns [EObject current=null] : iv_ruleInput= ruleInput EOF ;
    public final EObject entryRuleInput() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleInput = null;


        try {
            // InternalXMapping.g:2426:46: (iv_ruleInput= ruleInput EOF )
            // InternalXMapping.g:2427:2: iv_ruleInput= ruleInput EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getInputRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleInput=ruleInput();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleInput; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleInput"


    // $ANTLR start "ruleInput"
    // InternalXMapping.g:2433:1: ruleInput returns [EObject current=null] : ( (lv_name_0_0= ruleInputName ) ) ;
    public final EObject ruleInput() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_0_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2439:2: ( ( (lv_name_0_0= ruleInputName ) ) )
            // InternalXMapping.g:2440:2: ( (lv_name_0_0= ruleInputName ) )
            {
            // InternalXMapping.g:2440:2: ( (lv_name_0_0= ruleInputName ) )
            // InternalXMapping.g:2441:3: (lv_name_0_0= ruleInputName )
            {
            // InternalXMapping.g:2441:3: (lv_name_0_0= ruleInputName )
            // InternalXMapping.g:2442:4: lv_name_0_0= ruleInputName
            {
            if ( state.backtracking==0 ) {

              				newCompositeNode(grammarAccess.getInputAccess().getNameInputNameParserRuleCall_0());
              			
            }
            pushFollow(FOLLOW_2);
            lv_name_0_0=ruleInputName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				if (current==null) {
              					current = createModelElementForParent(grammarAccess.getInputRule());
              				}
              				set(
              					current,
              					"name",
              					lv_name_0_0,
              					"com.gk_software.core.dsl.xmapping.XMapping.InputName");
              				afterParserOrEnumRuleCall();
              			
            }

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleInput"


    // $ANTLR start "entryRuleOutputName"
    // InternalXMapping.g:2462:1: entryRuleOutputName returns [String current=null] : iv_ruleOutputName= ruleOutputName EOF ;
    public final String entryRuleOutputName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleOutputName = null;


        try {
            // InternalXMapping.g:2462:50: (iv_ruleOutputName= ruleOutputName EOF )
            // InternalXMapping.g:2463:2: iv_ruleOutputName= ruleOutputName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOutputNameRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleOutputName=ruleOutputName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOutputName.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutputName"


    // $ANTLR start "ruleOutputName"
    // InternalXMapping.g:2469:1: ruleOutputName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_OUTPUT_KEY_0= RULE_OUTPUT_KEY (kw= '.' this_Name_2= ruleName )? ) ;
    public final AntlrDatatypeRuleToken ruleOutputName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_OUTPUT_KEY_0=null;
        Token kw=null;
        AntlrDatatypeRuleToken this_Name_2 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2475:2: ( (this_OUTPUT_KEY_0= RULE_OUTPUT_KEY (kw= '.' this_Name_2= ruleName )? ) )
            // InternalXMapping.g:2476:2: (this_OUTPUT_KEY_0= RULE_OUTPUT_KEY (kw= '.' this_Name_2= ruleName )? )
            {
            // InternalXMapping.g:2476:2: (this_OUTPUT_KEY_0= RULE_OUTPUT_KEY (kw= '.' this_Name_2= ruleName )? )
            // InternalXMapping.g:2477:3: this_OUTPUT_KEY_0= RULE_OUTPUT_KEY (kw= '.' this_Name_2= ruleName )?
            {
            this_OUTPUT_KEY_0=(Token)match(input,RULE_OUTPUT_KEY,FOLLOW_36); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_OUTPUT_KEY_0);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_OUTPUT_KEY_0, grammarAccess.getOutputNameAccess().getOUTPUT_KEYTerminalRuleCall_0());
              		
            }
            // InternalXMapping.g:2484:3: (kw= '.' this_Name_2= ruleName )?
            int alt35=2;
            int LA35_0 = input.LA(1);

            if ( (LA35_0==35) ) {
                alt35=1;
            }
            switch (alt35) {
                case 1 :
                    // InternalXMapping.g:2485:4: kw= '.' this_Name_2= ruleName
                    {
                    kw=(Token)match(input,35,FOLLOW_3); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(kw);
                      				newLeafNode(kw, grammarAccess.getOutputNameAccess().getFullStopKeyword_1_0());
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				newCompositeNode(grammarAccess.getOutputNameAccess().getNameParserRuleCall_1_1());
                      			
                    }
                    pushFollow(FOLLOW_2);
                    this_Name_2=ruleName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(this_Name_2);
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				afterParserOrEnumRuleCall();
                      			
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutputName"


    // $ANTLR start "entryRuleOutput"
    // InternalXMapping.g:2505:1: entryRuleOutput returns [EObject current=null] : iv_ruleOutput= ruleOutput EOF ;
    public final EObject entryRuleOutput() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOutput = null;


        try {
            // InternalXMapping.g:2505:47: (iv_ruleOutput= ruleOutput EOF )
            // InternalXMapping.g:2506:2: iv_ruleOutput= ruleOutput EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getOutputRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleOutput=ruleOutput();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleOutput; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOutput"


    // $ANTLR start "ruleOutput"
    // InternalXMapping.g:2512:1: ruleOutput returns [EObject current=null] : ( (lv_name_0_0= ruleOutputName ) ) ;
    public final EObject ruleOutput() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_0_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2518:2: ( ( (lv_name_0_0= ruleOutputName ) ) )
            // InternalXMapping.g:2519:2: ( (lv_name_0_0= ruleOutputName ) )
            {
            // InternalXMapping.g:2519:2: ( (lv_name_0_0= ruleOutputName ) )
            // InternalXMapping.g:2520:3: (lv_name_0_0= ruleOutputName )
            {
            // InternalXMapping.g:2520:3: (lv_name_0_0= ruleOutputName )
            // InternalXMapping.g:2521:4: lv_name_0_0= ruleOutputName
            {
            if ( state.backtracking==0 ) {

              				newCompositeNode(grammarAccess.getOutputAccess().getNameOutputNameParserRuleCall_0());
              			
            }
            pushFollow(FOLLOW_2);
            lv_name_0_0=ruleOutputName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				if (current==null) {
              					current = createModelElementForParent(grammarAccess.getOutputRule());
              				}
              				set(
              					current,
              					"name",
              					lv_name_0_0,
              					"com.gk_software.core.dsl.xmapping.XMapping.OutputName");
              				afterParserOrEnumRuleCall();
              			
            }

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOutput"


    // $ANTLR start "entryRuleVariableName"
    // InternalXMapping.g:2541:1: entryRuleVariableName returns [String current=null] : iv_ruleVariableName= ruleVariableName EOF ;
    public final String entryRuleVariableName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleVariableName = null;


        try {
            // InternalXMapping.g:2541:52: (iv_ruleVariableName= ruleVariableName EOF )
            // InternalXMapping.g:2542:2: iv_ruleVariableName= ruleVariableName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableNameRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleVariableName=ruleVariableName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariableName.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariableName"


    // $ANTLR start "ruleVariableName"
    // InternalXMapping.g:2548:1: ruleVariableName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_VARIABLE_KEY_0= RULE_VARIABLE_KEY kw= '.' this_Name_2= ruleName ) ;
    public final AntlrDatatypeRuleToken ruleVariableName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_VARIABLE_KEY_0=null;
        Token kw=null;
        AntlrDatatypeRuleToken this_Name_2 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2554:2: ( (this_VARIABLE_KEY_0= RULE_VARIABLE_KEY kw= '.' this_Name_2= ruleName ) )
            // InternalXMapping.g:2555:2: (this_VARIABLE_KEY_0= RULE_VARIABLE_KEY kw= '.' this_Name_2= ruleName )
            {
            // InternalXMapping.g:2555:2: (this_VARIABLE_KEY_0= RULE_VARIABLE_KEY kw= '.' this_Name_2= ruleName )
            // InternalXMapping.g:2556:3: this_VARIABLE_KEY_0= RULE_VARIABLE_KEY kw= '.' this_Name_2= ruleName
            {
            this_VARIABLE_KEY_0=(Token)match(input,RULE_VARIABLE_KEY,FOLLOW_27); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_VARIABLE_KEY_0);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_VARIABLE_KEY_0, grammarAccess.getVariableNameAccess().getVARIABLE_KEYTerminalRuleCall_0());
              		
            }
            kw=(Token)match(input,35,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(kw);
              			newLeafNode(kw, grammarAccess.getVariableNameAccess().getFullStopKeyword_1());
              		
            }
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getVariableNameAccess().getNameParserRuleCall_2());
              		
            }
            pushFollow(FOLLOW_2);
            this_Name_2=ruleName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_Name_2);
              		
            }
            if ( state.backtracking==0 ) {

              			afterParserOrEnumRuleCall();
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariableName"


    // $ANTLR start "entryRuleVariable"
    // InternalXMapping.g:2582:1: entryRuleVariable returns [EObject current=null] : iv_ruleVariable= ruleVariable EOF ;
    public final EObject entryRuleVariable() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleVariable = null;


        try {
            // InternalXMapping.g:2582:49: (iv_ruleVariable= ruleVariable EOF )
            // InternalXMapping.g:2583:2: iv_ruleVariable= ruleVariable EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getVariableRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleVariable=ruleVariable();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleVariable; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleVariable"


    // $ANTLR start "ruleVariable"
    // InternalXMapping.g:2589:1: ruleVariable returns [EObject current=null] : ( (lv_name_0_0= ruleVariableName ) ) ;
    public final EObject ruleVariable() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_0_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2595:2: ( ( (lv_name_0_0= ruleVariableName ) ) )
            // InternalXMapping.g:2596:2: ( (lv_name_0_0= ruleVariableName ) )
            {
            // InternalXMapping.g:2596:2: ( (lv_name_0_0= ruleVariableName ) )
            // InternalXMapping.g:2597:3: (lv_name_0_0= ruleVariableName )
            {
            // InternalXMapping.g:2597:3: (lv_name_0_0= ruleVariableName )
            // InternalXMapping.g:2598:4: lv_name_0_0= ruleVariableName
            {
            if ( state.backtracking==0 ) {

              				newCompositeNode(grammarAccess.getVariableAccess().getNameVariableNameParserRuleCall_0());
              			
            }
            pushFollow(FOLLOW_2);
            lv_name_0_0=ruleVariableName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				if (current==null) {
              					current = createModelElementForParent(grammarAccess.getVariableRule());
              				}
              				set(
              					current,
              					"name",
              					lv_name_0_0,
              					"com.gk_software.core.dsl.xmapping.XMapping.VariableName");
              				afterParserOrEnumRuleCall();
              			
            }

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleVariable"


    // $ANTLR start "entryRuleReturnName"
    // InternalXMapping.g:2618:1: entryRuleReturnName returns [String current=null] : iv_ruleReturnName= ruleReturnName EOF ;
    public final String entryRuleReturnName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleReturnName = null;


        try {
            // InternalXMapping.g:2618:50: (iv_ruleReturnName= ruleReturnName EOF )
            // InternalXMapping.g:2619:2: iv_ruleReturnName= ruleReturnName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getReturnNameRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleReturnName=ruleReturnName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleReturnName.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReturnName"


    // $ANTLR start "ruleReturnName"
    // InternalXMapping.g:2625:1: ruleReturnName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_RETURN_KEY_0= RULE_RETURN_KEY kw= '.' this_INT_2= RULE_INT ) ;
    public final AntlrDatatypeRuleToken ruleReturnName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_RETURN_KEY_0=null;
        Token kw=null;
        Token this_INT_2=null;


        	enterRule();

        try {
            // InternalXMapping.g:2631:2: ( (this_RETURN_KEY_0= RULE_RETURN_KEY kw= '.' this_INT_2= RULE_INT ) )
            // InternalXMapping.g:2632:2: (this_RETURN_KEY_0= RULE_RETURN_KEY kw= '.' this_INT_2= RULE_INT )
            {
            // InternalXMapping.g:2632:2: (this_RETURN_KEY_0= RULE_RETURN_KEY kw= '.' this_INT_2= RULE_INT )
            // InternalXMapping.g:2633:3: this_RETURN_KEY_0= RULE_RETURN_KEY kw= '.' this_INT_2= RULE_INT
            {
            this_RETURN_KEY_0=(Token)match(input,RULE_RETURN_KEY,FOLLOW_27); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_RETURN_KEY_0);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_RETURN_KEY_0, grammarAccess.getReturnNameAccess().getRETURN_KEYTerminalRuleCall_0());
              		
            }
            kw=(Token)match(input,35,FOLLOW_30); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(kw);
              			newLeafNode(kw, grammarAccess.getReturnNameAccess().getFullStopKeyword_1());
              		
            }
            this_INT_2=(Token)match(input,RULE_INT,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_INT_2);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_INT_2, grammarAccess.getReturnNameAccess().getINTTerminalRuleCall_2());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReturnName"


    // $ANTLR start "entryRuleReturn"
    // InternalXMapping.g:2656:1: entryRuleReturn returns [EObject current=null] : iv_ruleReturn= ruleReturn EOF ;
    public final EObject entryRuleReturn() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleReturn = null;


        try {
            // InternalXMapping.g:2656:47: (iv_ruleReturn= ruleReturn EOF )
            // InternalXMapping.g:2657:2: iv_ruleReturn= ruleReturn EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getReturnRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleReturn=ruleReturn();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleReturn; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleReturn"


    // $ANTLR start "ruleReturn"
    // InternalXMapping.g:2663:1: ruleReturn returns [EObject current=null] : ( (lv_name_0_0= ruleReturnName ) ) ;
    public final EObject ruleReturn() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_0_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2669:2: ( ( (lv_name_0_0= ruleReturnName ) ) )
            // InternalXMapping.g:2670:2: ( (lv_name_0_0= ruleReturnName ) )
            {
            // InternalXMapping.g:2670:2: ( (lv_name_0_0= ruleReturnName ) )
            // InternalXMapping.g:2671:3: (lv_name_0_0= ruleReturnName )
            {
            // InternalXMapping.g:2671:3: (lv_name_0_0= ruleReturnName )
            // InternalXMapping.g:2672:4: lv_name_0_0= ruleReturnName
            {
            if ( state.backtracking==0 ) {

              				newCompositeNode(grammarAccess.getReturnAccess().getNameReturnNameParserRuleCall_0());
              			
            }
            pushFollow(FOLLOW_2);
            lv_name_0_0=ruleReturnName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				if (current==null) {
              					current = createModelElementForParent(grammarAccess.getReturnRule());
              				}
              				set(
              					current,
              					"name",
              					lv_name_0_0,
              					"com.gk_software.core.dsl.xmapping.XMapping.ReturnName");
              				afterParserOrEnumRuleCall();
              			
            }

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleReturn"


    // $ANTLR start "entryRuleRoutineName"
    // InternalXMapping.g:2692:1: entryRuleRoutineName returns [String current=null] : iv_ruleRoutineName= ruleRoutineName EOF ;
    public final String entryRuleRoutineName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleRoutineName = null;


        try {
            // InternalXMapping.g:2692:51: (iv_ruleRoutineName= ruleRoutineName EOF )
            // InternalXMapping.g:2693:2: iv_ruleRoutineName= ruleRoutineName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRoutineNameRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleRoutineName=ruleRoutineName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRoutineName.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRoutineName"


    // $ANTLR start "ruleRoutineName"
    // InternalXMapping.g:2699:1: ruleRoutineName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_RoutineType_0= ruleRoutineType kw= '.' this_Name_2= ruleName ) ;
    public final AntlrDatatypeRuleToken ruleRoutineName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_RoutineType_0 = null;

        AntlrDatatypeRuleToken this_Name_2 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2705:2: ( (this_RoutineType_0= ruleRoutineType kw= '.' this_Name_2= ruleName ) )
            // InternalXMapping.g:2706:2: (this_RoutineType_0= ruleRoutineType kw= '.' this_Name_2= ruleName )
            {
            // InternalXMapping.g:2706:2: (this_RoutineType_0= ruleRoutineType kw= '.' this_Name_2= ruleName )
            // InternalXMapping.g:2707:3: this_RoutineType_0= ruleRoutineType kw= '.' this_Name_2= ruleName
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getRoutineNameAccess().getRoutineTypeParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_27);
            this_RoutineType_0=ruleRoutineType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_RoutineType_0);
              		
            }
            if ( state.backtracking==0 ) {

              			afterParserOrEnumRuleCall();
              		
            }
            kw=(Token)match(input,35,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(kw);
              			newLeafNode(kw, grammarAccess.getRoutineNameAccess().getFullStopKeyword_1());
              		
            }
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getRoutineNameAccess().getNameParserRuleCall_2());
              		
            }
            pushFollow(FOLLOW_2);
            this_Name_2=ruleName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_Name_2);
              		
            }
            if ( state.backtracking==0 ) {

              			afterParserOrEnumRuleCall();
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRoutineName"


    // $ANTLR start "entryRuleRoutineType"
    // InternalXMapping.g:2736:1: entryRuleRoutineType returns [String current=null] : iv_ruleRoutineType= ruleRoutineType EOF ;
    public final String entryRuleRoutineType() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleRoutineType = null;


        try {
            // InternalXMapping.g:2736:51: (iv_ruleRoutineType= ruleRoutineType EOF )
            // InternalXMapping.g:2737:2: iv_ruleRoutineType= ruleRoutineType EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRoutineTypeRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleRoutineType=ruleRoutineType();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRoutineType.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRoutineType"


    // $ANTLR start "ruleRoutineType"
    // InternalXMapping.g:2743:1: ruleRoutineType returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_MACRO_KEY_0= RULE_MACRO_KEY | this_FILTER_KEY_1= RULE_FILTER_KEY | this_FUNCTION_KEY_2= RULE_FUNCTION_KEY | this_MAPPER_KEY_3= RULE_MAPPER_KEY ) ;
    public final AntlrDatatypeRuleToken ruleRoutineType() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_MACRO_KEY_0=null;
        Token this_FILTER_KEY_1=null;
        Token this_FUNCTION_KEY_2=null;
        Token this_MAPPER_KEY_3=null;


        	enterRule();

        try {
            // InternalXMapping.g:2749:2: ( (this_MACRO_KEY_0= RULE_MACRO_KEY | this_FILTER_KEY_1= RULE_FILTER_KEY | this_FUNCTION_KEY_2= RULE_FUNCTION_KEY | this_MAPPER_KEY_3= RULE_MAPPER_KEY ) )
            // InternalXMapping.g:2750:2: (this_MACRO_KEY_0= RULE_MACRO_KEY | this_FILTER_KEY_1= RULE_FILTER_KEY | this_FUNCTION_KEY_2= RULE_FUNCTION_KEY | this_MAPPER_KEY_3= RULE_MAPPER_KEY )
            {
            // InternalXMapping.g:2750:2: (this_MACRO_KEY_0= RULE_MACRO_KEY | this_FILTER_KEY_1= RULE_FILTER_KEY | this_FUNCTION_KEY_2= RULE_FUNCTION_KEY | this_MAPPER_KEY_3= RULE_MAPPER_KEY )
            int alt36=4;
            switch ( input.LA(1) ) {
            case RULE_MACRO_KEY:
                {
                alt36=1;
                }
                break;
            case RULE_FILTER_KEY:
                {
                alt36=2;
                }
                break;
            case RULE_FUNCTION_KEY:
                {
                alt36=3;
                }
                break;
            case RULE_MAPPER_KEY:
                {
                alt36=4;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 36, 0, input);

                throw nvae;
            }

            switch (alt36) {
                case 1 :
                    // InternalXMapping.g:2751:3: this_MACRO_KEY_0= RULE_MACRO_KEY
                    {
                    this_MACRO_KEY_0=(Token)match(input,RULE_MACRO_KEY,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_MACRO_KEY_0);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newLeafNode(this_MACRO_KEY_0, grammarAccess.getRoutineTypeAccess().getMACRO_KEYTerminalRuleCall_0());
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalXMapping.g:2759:3: this_FILTER_KEY_1= RULE_FILTER_KEY
                    {
                    this_FILTER_KEY_1=(Token)match(input,RULE_FILTER_KEY,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_FILTER_KEY_1);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newLeafNode(this_FILTER_KEY_1, grammarAccess.getRoutineTypeAccess().getFILTER_KEYTerminalRuleCall_1());
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalXMapping.g:2767:3: this_FUNCTION_KEY_2= RULE_FUNCTION_KEY
                    {
                    this_FUNCTION_KEY_2=(Token)match(input,RULE_FUNCTION_KEY,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_FUNCTION_KEY_2);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newLeafNode(this_FUNCTION_KEY_2, grammarAccess.getRoutineTypeAccess().getFUNCTION_KEYTerminalRuleCall_2());
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalXMapping.g:2775:3: this_MAPPER_KEY_3= RULE_MAPPER_KEY
                    {
                    this_MAPPER_KEY_3=(Token)match(input,RULE_MAPPER_KEY,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_MAPPER_KEY_3);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newLeafNode(this_MAPPER_KEY_3, grammarAccess.getRoutineTypeAccess().getMAPPER_KEYTerminalRuleCall_3());
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRoutineType"


    // $ANTLR start "entryRuleRoutine"
    // InternalXMapping.g:2786:1: entryRuleRoutine returns [EObject current=null] : iv_ruleRoutine= ruleRoutine EOF ;
    public final EObject entryRuleRoutine() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRoutine = null;


        try {
            // InternalXMapping.g:2786:48: (iv_ruleRoutine= ruleRoutine EOF )
            // InternalXMapping.g:2787:2: iv_ruleRoutine= ruleRoutine EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getRoutineRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleRoutine=ruleRoutine();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleRoutine; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRoutine"


    // $ANTLR start "ruleRoutine"
    // InternalXMapping.g:2793:1: ruleRoutine returns [EObject current=null] : ( (lv_name_0_0= ruleRoutineName ) ) ;
    public final EObject ruleRoutine() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_0_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2799:2: ( ( (lv_name_0_0= ruleRoutineName ) ) )
            // InternalXMapping.g:2800:2: ( (lv_name_0_0= ruleRoutineName ) )
            {
            // InternalXMapping.g:2800:2: ( (lv_name_0_0= ruleRoutineName ) )
            // InternalXMapping.g:2801:3: (lv_name_0_0= ruleRoutineName )
            {
            // InternalXMapping.g:2801:3: (lv_name_0_0= ruleRoutineName )
            // InternalXMapping.g:2802:4: lv_name_0_0= ruleRoutineName
            {
            if ( state.backtracking==0 ) {

              				newCompositeNode(grammarAccess.getRoutineAccess().getNameRoutineNameParserRuleCall_0());
              			
            }
            pushFollow(FOLLOW_2);
            lv_name_0_0=ruleRoutineName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				if (current==null) {
              					current = createModelElementForParent(grammarAccess.getRoutineRule());
              				}
              				set(
              					current,
              					"name",
              					lv_name_0_0,
              					"com.gk_software.core.dsl.xmapping.XMapping.RoutineName");
              				afterParserOrEnumRuleCall();
              			
            }

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRoutine"


    // $ANTLR start "entryRuleParamName"
    // InternalXMapping.g:2822:1: entryRuleParamName returns [String current=null] : iv_ruleParamName= ruleParamName EOF ;
    public final String entryRuleParamName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleParamName = null;


        try {
            // InternalXMapping.g:2822:49: (iv_ruleParamName= ruleParamName EOF )
            // InternalXMapping.g:2823:2: iv_ruleParamName= ruleParamName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getParamNameRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleParamName=ruleParamName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleParamName.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParamName"


    // $ANTLR start "ruleParamName"
    // InternalXMapping.g:2829:1: ruleParamName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_PARAM_KEY_0= RULE_PARAM_KEY kw= '.' this_Name_2= ruleName ) ;
    public final AntlrDatatypeRuleToken ruleParamName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_PARAM_KEY_0=null;
        Token kw=null;
        AntlrDatatypeRuleToken this_Name_2 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2835:2: ( (this_PARAM_KEY_0= RULE_PARAM_KEY kw= '.' this_Name_2= ruleName ) )
            // InternalXMapping.g:2836:2: (this_PARAM_KEY_0= RULE_PARAM_KEY kw= '.' this_Name_2= ruleName )
            {
            // InternalXMapping.g:2836:2: (this_PARAM_KEY_0= RULE_PARAM_KEY kw= '.' this_Name_2= ruleName )
            // InternalXMapping.g:2837:3: this_PARAM_KEY_0= RULE_PARAM_KEY kw= '.' this_Name_2= ruleName
            {
            this_PARAM_KEY_0=(Token)match(input,RULE_PARAM_KEY,FOLLOW_27); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_PARAM_KEY_0);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_PARAM_KEY_0, grammarAccess.getParamNameAccess().getPARAM_KEYTerminalRuleCall_0());
              		
            }
            kw=(Token)match(input,35,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(kw);
              			newLeafNode(kw, grammarAccess.getParamNameAccess().getFullStopKeyword_1());
              		
            }
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getParamNameAccess().getNameParserRuleCall_2());
              		
            }
            pushFollow(FOLLOW_2);
            this_Name_2=ruleName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_Name_2);
              		
            }
            if ( state.backtracking==0 ) {

              			afterParserOrEnumRuleCall();
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParamName"


    // $ANTLR start "entryRuleParam"
    // InternalXMapping.g:2863:1: entryRuleParam returns [EObject current=null] : iv_ruleParam= ruleParam EOF ;
    public final EObject entryRuleParam() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleParam = null;


        try {
            // InternalXMapping.g:2863:46: (iv_ruleParam= ruleParam EOF )
            // InternalXMapping.g:2864:2: iv_ruleParam= ruleParam EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getParamRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleParam=ruleParam();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleParam; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleParam"


    // $ANTLR start "ruleParam"
    // InternalXMapping.g:2870:1: ruleParam returns [EObject current=null] : ( (lv_name_0_0= ruleParamName ) ) ;
    public final EObject ruleParam() throws RecognitionException {
        EObject current = null;

        AntlrDatatypeRuleToken lv_name_0_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2876:2: ( ( (lv_name_0_0= ruleParamName ) ) )
            // InternalXMapping.g:2877:2: ( (lv_name_0_0= ruleParamName ) )
            {
            // InternalXMapping.g:2877:2: ( (lv_name_0_0= ruleParamName ) )
            // InternalXMapping.g:2878:3: (lv_name_0_0= ruleParamName )
            {
            // InternalXMapping.g:2878:3: (lv_name_0_0= ruleParamName )
            // InternalXMapping.g:2879:4: lv_name_0_0= ruleParamName
            {
            if ( state.backtracking==0 ) {

              				newCompositeNode(grammarAccess.getParamAccess().getNameParamNameParserRuleCall_0());
              			
            }
            pushFollow(FOLLOW_2);
            lv_name_0_0=ruleParamName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              				if (current==null) {
              					current = createModelElementForParent(grammarAccess.getParamRule());
              				}
              				set(
              					current,
              					"name",
              					lv_name_0_0,
              					"com.gk_software.core.dsl.xmapping.XMapping.ParamName");
              				afterParserOrEnumRuleCall();
              			
            }

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleParam"


    // $ANTLR start "entryRuleMacroName"
    // InternalXMapping.g:2899:1: entryRuleMacroName returns [String current=null] : iv_ruleMacroName= ruleMacroName EOF ;
    public final String entryRuleMacroName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleMacroName = null;


        try {
            // InternalXMapping.g:2899:49: (iv_ruleMacroName= ruleMacroName EOF )
            // InternalXMapping.g:2900:2: iv_ruleMacroName= ruleMacroName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMacroNameRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleMacroName=ruleMacroName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMacroName.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMacroName"


    // $ANTLR start "ruleMacroName"
    // InternalXMapping.g:2906:1: ruleMacroName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_MACRO_KEY_0= RULE_MACRO_KEY kw= '.' this_Name_2= ruleName ) ;
    public final AntlrDatatypeRuleToken ruleMacroName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_MACRO_KEY_0=null;
        Token kw=null;
        AntlrDatatypeRuleToken this_Name_2 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2912:2: ( (this_MACRO_KEY_0= RULE_MACRO_KEY kw= '.' this_Name_2= ruleName ) )
            // InternalXMapping.g:2913:2: (this_MACRO_KEY_0= RULE_MACRO_KEY kw= '.' this_Name_2= ruleName )
            {
            // InternalXMapping.g:2913:2: (this_MACRO_KEY_0= RULE_MACRO_KEY kw= '.' this_Name_2= ruleName )
            // InternalXMapping.g:2914:3: this_MACRO_KEY_0= RULE_MACRO_KEY kw= '.' this_Name_2= ruleName
            {
            this_MACRO_KEY_0=(Token)match(input,RULE_MACRO_KEY,FOLLOW_27); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_MACRO_KEY_0);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_MACRO_KEY_0, grammarAccess.getMacroNameAccess().getMACRO_KEYTerminalRuleCall_0());
              		
            }
            kw=(Token)match(input,35,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(kw);
              			newLeafNode(kw, grammarAccess.getMacroNameAccess().getFullStopKeyword_1());
              		
            }
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getMacroNameAccess().getNameParserRuleCall_2());
              		
            }
            pushFollow(FOLLOW_2);
            this_Name_2=ruleName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_Name_2);
              		
            }
            if ( state.backtracking==0 ) {

              			afterParserOrEnumRuleCall();
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMacroName"


    // $ANTLR start "entryRuleFilterName"
    // InternalXMapping.g:2940:1: entryRuleFilterName returns [String current=null] : iv_ruleFilterName= ruleFilterName EOF ;
    public final String entryRuleFilterName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleFilterName = null;


        try {
            // InternalXMapping.g:2940:50: (iv_ruleFilterName= ruleFilterName EOF )
            // InternalXMapping.g:2941:2: iv_ruleFilterName= ruleFilterName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFilterNameRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleFilterName=ruleFilterName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFilterName.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFilterName"


    // $ANTLR start "ruleFilterName"
    // InternalXMapping.g:2947:1: ruleFilterName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_FILTER_KEY_0= RULE_FILTER_KEY kw= '.' this_Name_2= ruleName ) ;
    public final AntlrDatatypeRuleToken ruleFilterName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_FILTER_KEY_0=null;
        Token kw=null;
        AntlrDatatypeRuleToken this_Name_2 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2953:2: ( (this_FILTER_KEY_0= RULE_FILTER_KEY kw= '.' this_Name_2= ruleName ) )
            // InternalXMapping.g:2954:2: (this_FILTER_KEY_0= RULE_FILTER_KEY kw= '.' this_Name_2= ruleName )
            {
            // InternalXMapping.g:2954:2: (this_FILTER_KEY_0= RULE_FILTER_KEY kw= '.' this_Name_2= ruleName )
            // InternalXMapping.g:2955:3: this_FILTER_KEY_0= RULE_FILTER_KEY kw= '.' this_Name_2= ruleName
            {
            this_FILTER_KEY_0=(Token)match(input,RULE_FILTER_KEY,FOLLOW_27); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_FILTER_KEY_0);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_FILTER_KEY_0, grammarAccess.getFilterNameAccess().getFILTER_KEYTerminalRuleCall_0());
              		
            }
            kw=(Token)match(input,35,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(kw);
              			newLeafNode(kw, grammarAccess.getFilterNameAccess().getFullStopKeyword_1());
              		
            }
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getFilterNameAccess().getNameParserRuleCall_2());
              		
            }
            pushFollow(FOLLOW_2);
            this_Name_2=ruleName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_Name_2);
              		
            }
            if ( state.backtracking==0 ) {

              			afterParserOrEnumRuleCall();
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFilterName"


    // $ANTLR start "entryRuleFuctionName"
    // InternalXMapping.g:2981:1: entryRuleFuctionName returns [String current=null] : iv_ruleFuctionName= ruleFuctionName EOF ;
    public final String entryRuleFuctionName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleFuctionName = null;


        try {
            // InternalXMapping.g:2981:51: (iv_ruleFuctionName= ruleFuctionName EOF )
            // InternalXMapping.g:2982:2: iv_ruleFuctionName= ruleFuctionName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getFuctionNameRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleFuctionName=ruleFuctionName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleFuctionName.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFuctionName"


    // $ANTLR start "ruleFuctionName"
    // InternalXMapping.g:2988:1: ruleFuctionName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_FUNCTION_KEY_0= RULE_FUNCTION_KEY kw= '.' this_Name_2= ruleName ) ;
    public final AntlrDatatypeRuleToken ruleFuctionName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_FUNCTION_KEY_0=null;
        Token kw=null;
        AntlrDatatypeRuleToken this_Name_2 = null;



        	enterRule();

        try {
            // InternalXMapping.g:2994:2: ( (this_FUNCTION_KEY_0= RULE_FUNCTION_KEY kw= '.' this_Name_2= ruleName ) )
            // InternalXMapping.g:2995:2: (this_FUNCTION_KEY_0= RULE_FUNCTION_KEY kw= '.' this_Name_2= ruleName )
            {
            // InternalXMapping.g:2995:2: (this_FUNCTION_KEY_0= RULE_FUNCTION_KEY kw= '.' this_Name_2= ruleName )
            // InternalXMapping.g:2996:3: this_FUNCTION_KEY_0= RULE_FUNCTION_KEY kw= '.' this_Name_2= ruleName
            {
            this_FUNCTION_KEY_0=(Token)match(input,RULE_FUNCTION_KEY,FOLLOW_27); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_FUNCTION_KEY_0);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_FUNCTION_KEY_0, grammarAccess.getFuctionNameAccess().getFUNCTION_KEYTerminalRuleCall_0());
              		
            }
            kw=(Token)match(input,35,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(kw);
              			newLeafNode(kw, grammarAccess.getFuctionNameAccess().getFullStopKeyword_1());
              		
            }
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getFuctionNameAccess().getNameParserRuleCall_2());
              		
            }
            pushFollow(FOLLOW_2);
            this_Name_2=ruleName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_Name_2);
              		
            }
            if ( state.backtracking==0 ) {

              			afterParserOrEnumRuleCall();
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFuctionName"


    // $ANTLR start "entryRuleMapperName"
    // InternalXMapping.g:3022:1: entryRuleMapperName returns [String current=null] : iv_ruleMapperName= ruleMapperName EOF ;
    public final String entryRuleMapperName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleMapperName = null;


        try {
            // InternalXMapping.g:3022:50: (iv_ruleMapperName= ruleMapperName EOF )
            // InternalXMapping.g:3023:2: iv_ruleMapperName= ruleMapperName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getMapperNameRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleMapperName=ruleMapperName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleMapperName.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleMapperName"


    // $ANTLR start "ruleMapperName"
    // InternalXMapping.g:3029:1: ruleMapperName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_MAPPER_KEY_0= RULE_MAPPER_KEY kw= '.' this_Name_2= ruleName ) ;
    public final AntlrDatatypeRuleToken ruleMapperName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_MAPPER_KEY_0=null;
        Token kw=null;
        AntlrDatatypeRuleToken this_Name_2 = null;



        	enterRule();

        try {
            // InternalXMapping.g:3035:2: ( (this_MAPPER_KEY_0= RULE_MAPPER_KEY kw= '.' this_Name_2= ruleName ) )
            // InternalXMapping.g:3036:2: (this_MAPPER_KEY_0= RULE_MAPPER_KEY kw= '.' this_Name_2= ruleName )
            {
            // InternalXMapping.g:3036:2: (this_MAPPER_KEY_0= RULE_MAPPER_KEY kw= '.' this_Name_2= ruleName )
            // InternalXMapping.g:3037:3: this_MAPPER_KEY_0= RULE_MAPPER_KEY kw= '.' this_Name_2= ruleName
            {
            this_MAPPER_KEY_0=(Token)match(input,RULE_MAPPER_KEY,FOLLOW_27); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_MAPPER_KEY_0);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_MAPPER_KEY_0, grammarAccess.getMapperNameAccess().getMAPPER_KEYTerminalRuleCall_0());
              		
            }
            kw=(Token)match(input,35,FOLLOW_3); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(kw);
              			newLeafNode(kw, grammarAccess.getMapperNameAccess().getFullStopKeyword_1());
              		
            }
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getMapperNameAccess().getNameParserRuleCall_2());
              		
            }
            pushFollow(FOLLOW_2);
            this_Name_2=ruleName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_Name_2);
              		
            }
            if ( state.backtracking==0 ) {

              			afterParserOrEnumRuleCall();
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleMapperName"


    // $ANTLR start "entryRuleQualifiedNameWithWildcard"
    // InternalXMapping.g:3063:1: entryRuleQualifiedNameWithWildcard returns [String current=null] : iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF ;
    public final String entryRuleQualifiedNameWithWildcard() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedNameWithWildcard = null;


        try {
            // InternalXMapping.g:3063:65: (iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF )
            // InternalXMapping.g:3064:2: iv_ruleQualifiedNameWithWildcard= ruleQualifiedNameWithWildcard EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getQualifiedNameWithWildcardRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedNameWithWildcard=ruleQualifiedNameWithWildcard();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleQualifiedNameWithWildcard.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedNameWithWildcard"


    // $ANTLR start "ruleQualifiedNameWithWildcard"
    // InternalXMapping.g:3070:1: ruleQualifiedNameWithWildcard returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedNameWithWildcard() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_QualifiedName_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:3076:2: ( (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? ) )
            // InternalXMapping.g:3077:2: (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? )
            {
            // InternalXMapping.g:3077:2: (this_QualifiedName_0= ruleQualifiedName (kw= '.*' )? )
            // InternalXMapping.g:3078:3: this_QualifiedName_0= ruleQualifiedName (kw= '.*' )?
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getQualifiedNameWithWildcardAccess().getQualifiedNameParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_37);
            this_QualifiedName_0=ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_QualifiedName_0);
              		
            }
            if ( state.backtracking==0 ) {

              			afterParserOrEnumRuleCall();
              		
            }
            // InternalXMapping.g:3088:3: (kw= '.*' )?
            int alt37=2;
            int LA37_0 = input.LA(1);

            if ( (LA37_0==41) ) {
                alt37=1;
            }
            switch (alt37) {
                case 1 :
                    // InternalXMapping.g:3089:4: kw= '.*'
                    {
                    kw=(Token)match(input,41,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(kw);
                      				newLeafNode(kw, grammarAccess.getQualifiedNameWithWildcardAccess().getFullStopAsteriskKeyword_1());
                      			
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedNameWithWildcard"


    // $ANTLR start "entryRuleQualifiedName"
    // InternalXMapping.g:3099:1: entryRuleQualifiedName returns [String current=null] : iv_ruleQualifiedName= ruleQualifiedName EOF ;
    public final String entryRuleQualifiedName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedName = null;


        try {
            // InternalXMapping.g:3099:53: (iv_ruleQualifiedName= ruleQualifiedName EOF )
            // InternalXMapping.g:3100:2: iv_ruleQualifiedName= ruleQualifiedName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getQualifiedNameRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedName=ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleQualifiedName.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedName"


    // $ANTLR start "ruleQualifiedName"
    // InternalXMapping.g:3106:1: ruleQualifiedName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_Name_0= ruleName (kw= '.' this_Name_2= ruleName )* ) ;
    public final AntlrDatatypeRuleToken ruleQualifiedName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_Name_0 = null;

        AntlrDatatypeRuleToken this_Name_2 = null;



        	enterRule();

        try {
            // InternalXMapping.g:3112:2: ( (this_Name_0= ruleName (kw= '.' this_Name_2= ruleName )* ) )
            // InternalXMapping.g:3113:2: (this_Name_0= ruleName (kw= '.' this_Name_2= ruleName )* )
            {
            // InternalXMapping.g:3113:2: (this_Name_0= ruleName (kw= '.' this_Name_2= ruleName )* )
            // InternalXMapping.g:3114:3: this_Name_0= ruleName (kw= '.' this_Name_2= ruleName )*
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getQualifiedNameAccess().getNameParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_36);
            this_Name_0=ruleName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_Name_0);
              		
            }
            if ( state.backtracking==0 ) {

              			afterParserOrEnumRuleCall();
              		
            }
            // InternalXMapping.g:3124:3: (kw= '.' this_Name_2= ruleName )*
            loop38:
            do {
                int alt38=2;
                int LA38_0 = input.LA(1);

                if ( (LA38_0==35) ) {
                    int LA38_2 = input.LA(2);

                    if ( (LA38_2==RULE_ID||LA38_2==20||LA38_2==22||LA38_2==25||LA38_2==31||LA38_2==33) ) {
                        alt38=1;
                    }


                }


                switch (alt38) {
            	case 1 :
            	    // InternalXMapping.g:3125:4: kw= '.' this_Name_2= ruleName
            	    {
            	    kw=(Token)match(input,35,FOLLOW_3); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				current.merge(kw);
            	      				newLeafNode(kw, grammarAccess.getQualifiedNameAccess().getFullStopKeyword_1_0());
            	      			
            	    }
            	    if ( state.backtracking==0 ) {

            	      				newCompositeNode(grammarAccess.getQualifiedNameAccess().getNameParserRuleCall_1_1());
            	      			
            	    }
            	    pushFollow(FOLLOW_36);
            	    this_Name_2=ruleName();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				current.merge(this_Name_2);
            	      			
            	    }
            	    if ( state.backtracking==0 ) {

            	      				afterParserOrEnumRuleCall();
            	      			
            	    }

            	    }
            	    break;

            	default :
            	    break loop38;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedName"


    // $ANTLR start "entryRulePath"
    // InternalXMapping.g:3145:1: entryRulePath returns [String current=null] : iv_rulePath= rulePath EOF ;
    public final String entryRulePath() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_rulePath = null;


        try {
            // InternalXMapping.g:3145:44: (iv_rulePath= rulePath EOF )
            // InternalXMapping.g:3146:2: iv_rulePath= rulePath EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getPathRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_rulePath=rulePath();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_rulePath.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePath"


    // $ANTLR start "rulePath"
    // InternalXMapping.g:3152:1: rulePath returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_STRING_0= RULE_STRING ( (kw= '.' | kw= '-' )+ this_STRING_3= RULE_STRING )* ) ;
    public final AntlrDatatypeRuleToken rulePath() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_STRING_0=null;
        Token kw=null;
        Token this_STRING_3=null;


        	enterRule();

        try {
            // InternalXMapping.g:3158:2: ( (this_STRING_0= RULE_STRING ( (kw= '.' | kw= '-' )+ this_STRING_3= RULE_STRING )* ) )
            // InternalXMapping.g:3159:2: (this_STRING_0= RULE_STRING ( (kw= '.' | kw= '-' )+ this_STRING_3= RULE_STRING )* )
            {
            // InternalXMapping.g:3159:2: (this_STRING_0= RULE_STRING ( (kw= '.' | kw= '-' )+ this_STRING_3= RULE_STRING )* )
            // InternalXMapping.g:3160:3: this_STRING_0= RULE_STRING ( (kw= '.' | kw= '-' )+ this_STRING_3= RULE_STRING )*
            {
            this_STRING_0=(Token)match(input,RULE_STRING,FOLLOW_38); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_STRING_0);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_STRING_0, grammarAccess.getPathAccess().getSTRINGTerminalRuleCall_0());
              		
            }
            // InternalXMapping.g:3167:3: ( (kw= '.' | kw= '-' )+ this_STRING_3= RULE_STRING )*
            loop40:
            do {
                int alt40=2;
                int LA40_0 = input.LA(1);

                if ( (LA40_0==35||LA40_0==38) ) {
                    alt40=1;
                }


                switch (alt40) {
            	case 1 :
            	    // InternalXMapping.g:3168:4: (kw= '.' | kw= '-' )+ this_STRING_3= RULE_STRING
            	    {
            	    // InternalXMapping.g:3168:4: (kw= '.' | kw= '-' )+
            	    int cnt39=0;
            	    loop39:
            	    do {
            	        int alt39=3;
            	        int LA39_0 = input.LA(1);

            	        if ( (LA39_0==35) ) {
            	            alt39=1;
            	        }
            	        else if ( (LA39_0==38) ) {
            	            alt39=2;
            	        }


            	        switch (alt39) {
            	    	case 1 :
            	    	    // InternalXMapping.g:3169:5: kw= '.'
            	    	    {
            	    	    kw=(Token)match(input,35,FOLLOW_39); if (state.failed) return current;
            	    	    if ( state.backtracking==0 ) {

            	    	      					current.merge(kw);
            	    	      					newLeafNode(kw, grammarAccess.getPathAccess().getFullStopKeyword_1_0_0());
            	    	      				
            	    	    }

            	    	    }
            	    	    break;
            	    	case 2 :
            	    	    // InternalXMapping.g:3175:5: kw= '-'
            	    	    {
            	    	    kw=(Token)match(input,38,FOLLOW_39); if (state.failed) return current;
            	    	    if ( state.backtracking==0 ) {

            	    	      					current.merge(kw);
            	    	      					newLeafNode(kw, grammarAccess.getPathAccess().getHyphenMinusKeyword_1_0_1());
            	    	      				
            	    	    }

            	    	    }
            	    	    break;

            	    	default :
            	    	    if ( cnt39 >= 1 ) break loop39;
            	    	    if (state.backtracking>0) {state.failed=true; return current;}
            	                EarlyExitException eee =
            	                    new EarlyExitException(39, input);
            	                throw eee;
            	        }
            	        cnt39++;
            	    } while (true);

            	    this_STRING_3=(Token)match(input,RULE_STRING,FOLLOW_38); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				current.merge(this_STRING_3);
            	      			
            	    }
            	    if ( state.backtracking==0 ) {

            	      				newLeafNode(this_STRING_3, grammarAccess.getPathAccess().getSTRINGTerminalRuleCall_1_1());
            	      			
            	    }

            	    }
            	    break;

            	default :
            	    break loop40;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePath"


    // $ANTLR start "entryRuleName"
    // InternalXMapping.g:3193:1: entryRuleName returns [String current=null] : iv_ruleName= ruleName EOF ;
    public final String entryRuleName() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleName = null;


        try {
            // InternalXMapping.g:3193:44: (iv_ruleName= ruleName EOF )
            // InternalXMapping.g:3194:2: iv_ruleName= ruleName EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getNameRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleName=ruleName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleName.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleName"


    // $ANTLR start "ruleName"
    // InternalXMapping.g:3200:1: ruleName returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ValidID_0= ruleValidID ( (kw= '-' )+ this_ValidID_2= ruleValidID )* ) ;
    public final AntlrDatatypeRuleToken ruleName() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_ValidID_0 = null;

        AntlrDatatypeRuleToken this_ValidID_2 = null;



        	enterRule();

        try {
            // InternalXMapping.g:3206:2: ( (this_ValidID_0= ruleValidID ( (kw= '-' )+ this_ValidID_2= ruleValidID )* ) )
            // InternalXMapping.g:3207:2: (this_ValidID_0= ruleValidID ( (kw= '-' )+ this_ValidID_2= ruleValidID )* )
            {
            // InternalXMapping.g:3207:2: (this_ValidID_0= ruleValidID ( (kw= '-' )+ this_ValidID_2= ruleValidID )* )
            // InternalXMapping.g:3208:3: this_ValidID_0= ruleValidID ( (kw= '-' )+ this_ValidID_2= ruleValidID )*
            {
            if ( state.backtracking==0 ) {

              			newCompositeNode(grammarAccess.getNameAccess().getValidIDParserRuleCall_0());
              		
            }
            pushFollow(FOLLOW_40);
            this_ValidID_0=ruleValidID();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_ValidID_0);
              		
            }
            if ( state.backtracking==0 ) {

              			afterParserOrEnumRuleCall();
              		
            }
            // InternalXMapping.g:3218:3: ( (kw= '-' )+ this_ValidID_2= ruleValidID )*
            loop42:
            do {
                int alt42=2;
                int LA42_0 = input.LA(1);

                if ( (LA42_0==38) ) {
                    alt42=1;
                }


                switch (alt42) {
            	case 1 :
            	    // InternalXMapping.g:3219:4: (kw= '-' )+ this_ValidID_2= ruleValidID
            	    {
            	    // InternalXMapping.g:3219:4: (kw= '-' )+
            	    int cnt41=0;
            	    loop41:
            	    do {
            	        int alt41=2;
            	        int LA41_0 = input.LA(1);

            	        if ( (LA41_0==38) ) {
            	            alt41=1;
            	        }


            	        switch (alt41) {
            	    	case 1 :
            	    	    // InternalXMapping.g:3220:5: kw= '-'
            	    	    {
            	    	    kw=(Token)match(input,38,FOLLOW_41); if (state.failed) return current;
            	    	    if ( state.backtracking==0 ) {

            	    	      					current.merge(kw);
            	    	      					newLeafNode(kw, grammarAccess.getNameAccess().getHyphenMinusKeyword_1_0());
            	    	      				
            	    	    }

            	    	    }
            	    	    break;

            	    	default :
            	    	    if ( cnt41 >= 1 ) break loop41;
            	    	    if (state.backtracking>0) {state.failed=true; return current;}
            	                EarlyExitException eee =
            	                    new EarlyExitException(41, input);
            	                throw eee;
            	        }
            	        cnt41++;
            	    } while (true);

            	    if ( state.backtracking==0 ) {

            	      				newCompositeNode(grammarAccess.getNameAccess().getValidIDParserRuleCall_1_1());
            	      			
            	    }
            	    pushFollow(FOLLOW_40);
            	    this_ValidID_2=ruleValidID();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      				current.merge(this_ValidID_2);
            	      			
            	    }
            	    if ( state.backtracking==0 ) {

            	      				afterParserOrEnumRuleCall();
            	      			
            	    }

            	    }
            	    break;

            	default :
            	    break loop42;
                }
            } while (true);


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleName"


    // $ANTLR start "entryRuleValidID"
    // InternalXMapping.g:3241:1: entryRuleValidID returns [String current=null] : iv_ruleValidID= ruleValidID EOF ;
    public final String entryRuleValidID() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleValidID = null;


        try {
            // InternalXMapping.g:3241:47: (iv_ruleValidID= ruleValidID EOF )
            // InternalXMapping.g:3242:2: iv_ruleValidID= ruleValidID EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getValidIDRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleValidID=ruleValidID();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleValidID.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleValidID"


    // $ANTLR start "ruleValidID"
    // InternalXMapping.g:3248:1: ruleValidID returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ID_0= RULE_ID | this_KEYWORD_1= ruleKEYWORD ) ;
    public final AntlrDatatypeRuleToken ruleValidID() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token this_ID_0=null;
        AntlrDatatypeRuleToken this_KEYWORD_1 = null;



        	enterRule();

        try {
            // InternalXMapping.g:3254:2: ( (this_ID_0= RULE_ID | this_KEYWORD_1= ruleKEYWORD ) )
            // InternalXMapping.g:3255:2: (this_ID_0= RULE_ID | this_KEYWORD_1= ruleKEYWORD )
            {
            // InternalXMapping.g:3255:2: (this_ID_0= RULE_ID | this_KEYWORD_1= ruleKEYWORD )
            int alt43=2;
            int LA43_0 = input.LA(1);

            if ( (LA43_0==RULE_ID) ) {
                alt43=1;
            }
            else if ( (LA43_0==20||LA43_0==22||LA43_0==25||LA43_0==31||LA43_0==33) ) {
                alt43=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 43, 0, input);

                throw nvae;
            }
            switch (alt43) {
                case 1 :
                    // InternalXMapping.g:3256:3: this_ID_0= RULE_ID
                    {
                    this_ID_0=(Token)match(input,RULE_ID,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_ID_0);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newLeafNode(this_ID_0, grammarAccess.getValidIDAccess().getIDTerminalRuleCall_0());
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalXMapping.g:3264:3: this_KEYWORD_1= ruleKEYWORD
                    {
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getValidIDAccess().getKEYWORDParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_KEYWORD_1=ruleKEYWORD();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(this_KEYWORD_1);
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleValidID"


    // $ANTLR start "entryRuleDOUBLE"
    // InternalXMapping.g:3278:1: entryRuleDOUBLE returns [String current=null] : iv_ruleDOUBLE= ruleDOUBLE EOF ;
    public final String entryRuleDOUBLE() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleDOUBLE = null;


        try {
            // InternalXMapping.g:3278:46: (iv_ruleDOUBLE= ruleDOUBLE EOF )
            // InternalXMapping.g:3279:2: iv_ruleDOUBLE= ruleDOUBLE EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getDOUBLERule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleDOUBLE=ruleDOUBLE();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleDOUBLE.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleDOUBLE"


    // $ANTLR start "ruleDOUBLE"
    // InternalXMapping.g:3285:1: ruleDOUBLE returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : ( (kw= '-' )? this_INT_1= RULE_INT kw= '.' this_INT_3= RULE_INT ) ;
    public final AntlrDatatypeRuleToken ruleDOUBLE() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        Token this_INT_1=null;
        Token this_INT_3=null;


        	enterRule();

        try {
            // InternalXMapping.g:3291:2: ( ( (kw= '-' )? this_INT_1= RULE_INT kw= '.' this_INT_3= RULE_INT ) )
            // InternalXMapping.g:3292:2: ( (kw= '-' )? this_INT_1= RULE_INT kw= '.' this_INT_3= RULE_INT )
            {
            // InternalXMapping.g:3292:2: ( (kw= '-' )? this_INT_1= RULE_INT kw= '.' this_INT_3= RULE_INT )
            // InternalXMapping.g:3293:3: (kw= '-' )? this_INT_1= RULE_INT kw= '.' this_INT_3= RULE_INT
            {
            // InternalXMapping.g:3293:3: (kw= '-' )?
            int alt44=2;
            int LA44_0 = input.LA(1);

            if ( (LA44_0==38) ) {
                alt44=1;
            }
            switch (alt44) {
                case 1 :
                    // InternalXMapping.g:3294:4: kw= '-'
                    {
                    kw=(Token)match(input,38,FOLLOW_30); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current.merge(kw);
                      				newLeafNode(kw, grammarAccess.getDOUBLEAccess().getHyphenMinusKeyword_0());
                      			
                    }

                    }
                    break;

            }

            this_INT_1=(Token)match(input,RULE_INT,FOLLOW_27); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_INT_1);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_INT_1, grammarAccess.getDOUBLEAccess().getINTTerminalRuleCall_1());
              		
            }
            kw=(Token)match(input,35,FOLLOW_30); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(kw);
              			newLeafNode(kw, grammarAccess.getDOUBLEAccess().getFullStopKeyword_2());
              		
            }
            this_INT_3=(Token)match(input,RULE_INT,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(this_INT_3);
              		
            }
            if ( state.backtracking==0 ) {

              			newLeafNode(this_INT_3, grammarAccess.getDOUBLEAccess().getINTTerminalRuleCall_3());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleDOUBLE"


    // $ANTLR start "entryRuleKEYWORD"
    // InternalXMapping.g:3323:1: entryRuleKEYWORD returns [String current=null] : iv_ruleKEYWORD= ruleKEYWORD EOF ;
    public final String entryRuleKEYWORD() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleKEYWORD = null;


        try {
            // InternalXMapping.g:3323:47: (iv_ruleKEYWORD= ruleKEYWORD EOF )
            // InternalXMapping.g:3324:2: iv_ruleKEYWORD= ruleKEYWORD EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getKEYWORDRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleKEYWORD=ruleKEYWORD();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleKEYWORD.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleKEYWORD"


    // $ANTLR start "ruleKEYWORD"
    // InternalXMapping.g:3330:1: ruleKEYWORD returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= 'map' | kw= 'package' | kw= 'import' | kw= 'def' | kw= 'call' ) ;
    public final AntlrDatatypeRuleToken ruleKEYWORD() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalXMapping.g:3336:2: ( (kw= 'map' | kw= 'package' | kw= 'import' | kw= 'def' | kw= 'call' ) )
            // InternalXMapping.g:3337:2: (kw= 'map' | kw= 'package' | kw= 'import' | kw= 'def' | kw= 'call' )
            {
            // InternalXMapping.g:3337:2: (kw= 'map' | kw= 'package' | kw= 'import' | kw= 'def' | kw= 'call' )
            int alt45=5;
            switch ( input.LA(1) ) {
            case 33:
                {
                alt45=1;
                }
                break;
            case 20:
                {
                alt45=2;
                }
                break;
            case 22:
                {
                alt45=3;
                }
                break;
            case 25:
                {
                alt45=4;
                }
                break;
            case 31:
                {
                alt45=5;
                }
                break;
            default:
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 45, 0, input);

                throw nvae;
            }

            switch (alt45) {
                case 1 :
                    // InternalXMapping.g:3338:3: kw= 'map'
                    {
                    kw=(Token)match(input,33,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getKEYWORDAccess().getMapKeyword_0());
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalXMapping.g:3344:3: kw= 'package'
                    {
                    kw=(Token)match(input,20,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getKEYWORDAccess().getPackageKeyword_1());
                      		
                    }

                    }
                    break;
                case 3 :
                    // InternalXMapping.g:3350:3: kw= 'import'
                    {
                    kw=(Token)match(input,22,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getKEYWORDAccess().getImportKeyword_2());
                      		
                    }

                    }
                    break;
                case 4 :
                    // InternalXMapping.g:3356:3: kw= 'def'
                    {
                    kw=(Token)match(input,25,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getKEYWORDAccess().getDefKeyword_3());
                      		
                    }

                    }
                    break;
                case 5 :
                    // InternalXMapping.g:3362:3: kw= 'call'
                    {
                    kw=(Token)match(input,31,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current.merge(kw);
                      			newLeafNode(kw, grammarAccess.getKEYWORDAccess().getCallKeyword_4());
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleKEYWORD"


    // $ANTLR start "entryRuleJvmTypeReference"
    // InternalXMapping.g:3371:1: entryRuleJvmTypeReference returns [EObject current=null] : iv_ruleJvmTypeReference= ruleJvmTypeReference EOF ;
    public final EObject entryRuleJvmTypeReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleJvmTypeReference = null;


        try {
            // InternalXMapping.g:3371:57: (iv_ruleJvmTypeReference= ruleJvmTypeReference EOF )
            // InternalXMapping.g:3372:2: iv_ruleJvmTypeReference= ruleJvmTypeReference EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getJvmTypeReferenceRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleJvmTypeReference=ruleJvmTypeReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleJvmTypeReference; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleJvmTypeReference"


    // $ANTLR start "ruleJvmTypeReference"
    // InternalXMapping.g:3378:1: ruleJvmTypeReference returns [EObject current=null] : ( (this_JvmParameterizedTypeReference_0= ruleJvmParameterizedTypeReference ( ( ( () ruleArrayBrackets ) )=> ( () ruleArrayBrackets ) )* ) | this_XFunctionTypeRef_3= ruleXFunctionTypeRef ) ;
    public final EObject ruleJvmTypeReference() throws RecognitionException {
        EObject current = null;

        EObject this_JvmParameterizedTypeReference_0 = null;

        EObject this_XFunctionTypeRef_3 = null;



        	enterRule();

        try {
            // InternalXMapping.g:3384:2: ( ( (this_JvmParameterizedTypeReference_0= ruleJvmParameterizedTypeReference ( ( ( () ruleArrayBrackets ) )=> ( () ruleArrayBrackets ) )* ) | this_XFunctionTypeRef_3= ruleXFunctionTypeRef ) )
            // InternalXMapping.g:3385:2: ( (this_JvmParameterizedTypeReference_0= ruleJvmParameterizedTypeReference ( ( ( () ruleArrayBrackets ) )=> ( () ruleArrayBrackets ) )* ) | this_XFunctionTypeRef_3= ruleXFunctionTypeRef )
            {
            // InternalXMapping.g:3385:2: ( (this_JvmParameterizedTypeReference_0= ruleJvmParameterizedTypeReference ( ( ( () ruleArrayBrackets ) )=> ( () ruleArrayBrackets ) )* ) | this_XFunctionTypeRef_3= ruleXFunctionTypeRef )
            int alt47=2;
            int LA47_0 = input.LA(1);

            if ( (LA47_0==RULE_ID||LA47_0==20||LA47_0==22||LA47_0==25||LA47_0==31||LA47_0==33) ) {
                alt47=1;
            }
            else if ( (LA47_0==44||LA47_0==46) ) {
                alt47=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 47, 0, input);

                throw nvae;
            }
            switch (alt47) {
                case 1 :
                    // InternalXMapping.g:3386:3: (this_JvmParameterizedTypeReference_0= ruleJvmParameterizedTypeReference ( ( ( () ruleArrayBrackets ) )=> ( () ruleArrayBrackets ) )* )
                    {
                    // InternalXMapping.g:3386:3: (this_JvmParameterizedTypeReference_0= ruleJvmParameterizedTypeReference ( ( ( () ruleArrayBrackets ) )=> ( () ruleArrayBrackets ) )* )
                    // InternalXMapping.g:3387:4: this_JvmParameterizedTypeReference_0= ruleJvmParameterizedTypeReference ( ( ( () ruleArrayBrackets ) )=> ( () ruleArrayBrackets ) )*
                    {
                    if ( state.backtracking==0 ) {

                      				/* */
                      			
                    }
                    if ( state.backtracking==0 ) {

                      				newCompositeNode(grammarAccess.getJvmTypeReferenceAccess().getJvmParameterizedTypeReferenceParserRuleCall_0_0());
                      			
                    }
                    pushFollow(FOLLOW_42);
                    this_JvmParameterizedTypeReference_0=ruleJvmParameterizedTypeReference();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				current = this_JvmParameterizedTypeReference_0;
                      				afterParserOrEnumRuleCall();
                      			
                    }
                    // InternalXMapping.g:3398:4: ( ( ( () ruleArrayBrackets ) )=> ( () ruleArrayBrackets ) )*
                    loop46:
                    do {
                        int alt46=2;
                        int LA46_0 = input.LA(1);

                        if ( (LA46_0==42) && (synpred56_InternalXMapping())) {
                            alt46=1;
                        }


                        switch (alt46) {
                    	case 1 :
                    	    // InternalXMapping.g:3399:5: ( ( () ruleArrayBrackets ) )=> ( () ruleArrayBrackets )
                    	    {
                    	    // InternalXMapping.g:3405:5: ( () ruleArrayBrackets )
                    	    // InternalXMapping.g:3406:6: () ruleArrayBrackets
                    	    {
                    	    // InternalXMapping.g:3406:6: ()
                    	    // InternalXMapping.g:3407:7: 
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      							/* */
                    	      						
                    	    }
                    	    if ( state.backtracking==0 ) {

                    	      							current = forceCreateModelElementAndSet(
                    	      								grammarAccess.getJvmTypeReferenceAccess().getJvmGenericArrayTypeReferenceComponentTypeAction_0_1_0_0(),
                    	      								current);
                    	      						
                    	    }

                    	    }

                    	    if ( state.backtracking==0 ) {

                    	      						/* */
                    	      					
                    	    }
                    	    if ( state.backtracking==0 ) {

                    	      						newCompositeNode(grammarAccess.getJvmTypeReferenceAccess().getArrayBracketsParserRuleCall_0_1_0_1());
                    	      					
                    	    }
                    	    pushFollow(FOLLOW_42);
                    	    ruleArrayBrackets();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      						afterParserOrEnumRuleCall();
                    	      					
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop46;
                        }
                    } while (true);


                    }


                    }
                    break;
                case 2 :
                    // InternalXMapping.g:3430:3: this_XFunctionTypeRef_3= ruleXFunctionTypeRef
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getJvmTypeReferenceAccess().getXFunctionTypeRefParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_XFunctionTypeRef_3=ruleXFunctionTypeRef();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_XFunctionTypeRef_3;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleJvmTypeReference"


    // $ANTLR start "entryRuleArrayBrackets"
    // InternalXMapping.g:3445:1: entryRuleArrayBrackets returns [String current=null] : iv_ruleArrayBrackets= ruleArrayBrackets EOF ;
    public final String entryRuleArrayBrackets() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleArrayBrackets = null;


        try {
            // InternalXMapping.g:3445:53: (iv_ruleArrayBrackets= ruleArrayBrackets EOF )
            // InternalXMapping.g:3446:2: iv_ruleArrayBrackets= ruleArrayBrackets EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getArrayBracketsRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleArrayBrackets=ruleArrayBrackets();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleArrayBrackets.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleArrayBrackets"


    // $ANTLR start "ruleArrayBrackets"
    // InternalXMapping.g:3452:1: ruleArrayBrackets returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (kw= '[' kw= ']' ) ;
    public final AntlrDatatypeRuleToken ruleArrayBrackets() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalXMapping.g:3458:2: ( (kw= '[' kw= ']' ) )
            // InternalXMapping.g:3459:2: (kw= '[' kw= ']' )
            {
            // InternalXMapping.g:3459:2: (kw= '[' kw= ']' )
            // InternalXMapping.g:3460:3: kw= '[' kw= ']'
            {
            kw=(Token)match(input,42,FOLLOW_43); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(kw);
              			newLeafNode(kw, grammarAccess.getArrayBracketsAccess().getLeftSquareBracketKeyword_0());
              		
            }
            kw=(Token)match(input,43,FOLLOW_2); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			current.merge(kw);
              			newLeafNode(kw, grammarAccess.getArrayBracketsAccess().getRightSquareBracketKeyword_1());
              		
            }

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleArrayBrackets"


    // $ANTLR start "entryRuleXFunctionTypeRef"
    // InternalXMapping.g:3474:1: entryRuleXFunctionTypeRef returns [EObject current=null] : iv_ruleXFunctionTypeRef= ruleXFunctionTypeRef EOF ;
    public final EObject entryRuleXFunctionTypeRef() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXFunctionTypeRef = null;


        try {
            // InternalXMapping.g:3474:57: (iv_ruleXFunctionTypeRef= ruleXFunctionTypeRef EOF )
            // InternalXMapping.g:3475:2: iv_ruleXFunctionTypeRef= ruleXFunctionTypeRef EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXFunctionTypeRefRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleXFunctionTypeRef=ruleXFunctionTypeRef();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXFunctionTypeRef; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXFunctionTypeRef"


    // $ANTLR start "ruleXFunctionTypeRef"
    // InternalXMapping.g:3481:1: ruleXFunctionTypeRef returns [EObject current=null] : ( (otherlv_0= '(' ( ( (lv_paramTypes_1_0= ruleJvmTypeReference ) ) (otherlv_2= ',' ( (lv_paramTypes_3_0= ruleJvmTypeReference ) ) )* )? otherlv_4= ')' )? otherlv_5= '=>' ( (lv_returnType_6_0= ruleJvmTypeReference ) ) ) ;
    public final EObject ruleXFunctionTypeRef() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        EObject lv_paramTypes_1_0 = null;

        EObject lv_paramTypes_3_0 = null;

        EObject lv_returnType_6_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:3487:2: ( ( (otherlv_0= '(' ( ( (lv_paramTypes_1_0= ruleJvmTypeReference ) ) (otherlv_2= ',' ( (lv_paramTypes_3_0= ruleJvmTypeReference ) ) )* )? otherlv_4= ')' )? otherlv_5= '=>' ( (lv_returnType_6_0= ruleJvmTypeReference ) ) ) )
            // InternalXMapping.g:3488:2: ( (otherlv_0= '(' ( ( (lv_paramTypes_1_0= ruleJvmTypeReference ) ) (otherlv_2= ',' ( (lv_paramTypes_3_0= ruleJvmTypeReference ) ) )* )? otherlv_4= ')' )? otherlv_5= '=>' ( (lv_returnType_6_0= ruleJvmTypeReference ) ) )
            {
            // InternalXMapping.g:3488:2: ( (otherlv_0= '(' ( ( (lv_paramTypes_1_0= ruleJvmTypeReference ) ) (otherlv_2= ',' ( (lv_paramTypes_3_0= ruleJvmTypeReference ) ) )* )? otherlv_4= ')' )? otherlv_5= '=>' ( (lv_returnType_6_0= ruleJvmTypeReference ) ) )
            // InternalXMapping.g:3489:3: (otherlv_0= '(' ( ( (lv_paramTypes_1_0= ruleJvmTypeReference ) ) (otherlv_2= ',' ( (lv_paramTypes_3_0= ruleJvmTypeReference ) ) )* )? otherlv_4= ')' )? otherlv_5= '=>' ( (lv_returnType_6_0= ruleJvmTypeReference ) )
            {
            // InternalXMapping.g:3489:3: (otherlv_0= '(' ( ( (lv_paramTypes_1_0= ruleJvmTypeReference ) ) (otherlv_2= ',' ( (lv_paramTypes_3_0= ruleJvmTypeReference ) ) )* )? otherlv_4= ')' )?
            int alt50=2;
            int LA50_0 = input.LA(1);

            if ( (LA50_0==44) ) {
                alt50=1;
            }
            switch (alt50) {
                case 1 :
                    // InternalXMapping.g:3490:4: otherlv_0= '(' ( ( (lv_paramTypes_1_0= ruleJvmTypeReference ) ) (otherlv_2= ',' ( (lv_paramTypes_3_0= ruleJvmTypeReference ) ) )* )? otherlv_4= ')'
                    {
                    otherlv_0=(Token)match(input,44,FOLLOW_44); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_0, grammarAccess.getXFunctionTypeRefAccess().getLeftParenthesisKeyword_0_0());
                      			
                    }
                    // InternalXMapping.g:3494:4: ( ( (lv_paramTypes_1_0= ruleJvmTypeReference ) ) (otherlv_2= ',' ( (lv_paramTypes_3_0= ruleJvmTypeReference ) ) )* )?
                    int alt49=2;
                    int LA49_0 = input.LA(1);

                    if ( (LA49_0==RULE_ID||LA49_0==20||LA49_0==22||LA49_0==25||LA49_0==31||LA49_0==33||LA49_0==44||LA49_0==46) ) {
                        alt49=1;
                    }
                    switch (alt49) {
                        case 1 :
                            // InternalXMapping.g:3495:5: ( (lv_paramTypes_1_0= ruleJvmTypeReference ) ) (otherlv_2= ',' ( (lv_paramTypes_3_0= ruleJvmTypeReference ) ) )*
                            {
                            // InternalXMapping.g:3495:5: ( (lv_paramTypes_1_0= ruleJvmTypeReference ) )
                            // InternalXMapping.g:3496:6: (lv_paramTypes_1_0= ruleJvmTypeReference )
                            {
                            // InternalXMapping.g:3496:6: (lv_paramTypes_1_0= ruleJvmTypeReference )
                            // InternalXMapping.g:3497:7: lv_paramTypes_1_0= ruleJvmTypeReference
                            {
                            if ( state.backtracking==0 ) {

                              							newCompositeNode(grammarAccess.getXFunctionTypeRefAccess().getParamTypesJvmTypeReferenceParserRuleCall_0_1_0_0());
                              						
                            }
                            pushFollow(FOLLOW_45);
                            lv_paramTypes_1_0=ruleJvmTypeReference();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              							if (current==null) {
                              								current = createModelElementForParent(grammarAccess.getXFunctionTypeRefRule());
                              							}
                              							add(
                              								current,
                              								"paramTypes",
                              								lv_paramTypes_1_0,
                              								"org.eclipse.xtext.xbase.Xtype.JvmTypeReference");
                              							afterParserOrEnumRuleCall();
                              						
                            }

                            }


                            }

                            // InternalXMapping.g:3514:5: (otherlv_2= ',' ( (lv_paramTypes_3_0= ruleJvmTypeReference ) ) )*
                            loop48:
                            do {
                                int alt48=2;
                                int LA48_0 = input.LA(1);

                                if ( (LA48_0==40) ) {
                                    alt48=1;
                                }


                                switch (alt48) {
                            	case 1 :
                            	    // InternalXMapping.g:3515:6: otherlv_2= ',' ( (lv_paramTypes_3_0= ruleJvmTypeReference ) )
                            	    {
                            	    otherlv_2=(Token)match(input,40,FOLLOW_46); if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	      						newLeafNode(otherlv_2, grammarAccess.getXFunctionTypeRefAccess().getCommaKeyword_0_1_1_0());
                            	      					
                            	    }
                            	    // InternalXMapping.g:3519:6: ( (lv_paramTypes_3_0= ruleJvmTypeReference ) )
                            	    // InternalXMapping.g:3520:7: (lv_paramTypes_3_0= ruleJvmTypeReference )
                            	    {
                            	    // InternalXMapping.g:3520:7: (lv_paramTypes_3_0= ruleJvmTypeReference )
                            	    // InternalXMapping.g:3521:8: lv_paramTypes_3_0= ruleJvmTypeReference
                            	    {
                            	    if ( state.backtracking==0 ) {

                            	      								newCompositeNode(grammarAccess.getXFunctionTypeRefAccess().getParamTypesJvmTypeReferenceParserRuleCall_0_1_1_1_0());
                            	      							
                            	    }
                            	    pushFollow(FOLLOW_45);
                            	    lv_paramTypes_3_0=ruleJvmTypeReference();

                            	    state._fsp--;
                            	    if (state.failed) return current;
                            	    if ( state.backtracking==0 ) {

                            	      								if (current==null) {
                            	      									current = createModelElementForParent(grammarAccess.getXFunctionTypeRefRule());
                            	      								}
                            	      								add(
                            	      									current,
                            	      									"paramTypes",
                            	      									lv_paramTypes_3_0,
                            	      									"org.eclipse.xtext.xbase.Xtype.JvmTypeReference");
                            	      								afterParserOrEnumRuleCall();
                            	      							
                            	    }

                            	    }


                            	    }


                            	    }
                            	    break;

                            	default :
                            	    break loop48;
                                }
                            } while (true);


                            }
                            break;

                    }

                    otherlv_4=(Token)match(input,45,FOLLOW_47); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_4, grammarAccess.getXFunctionTypeRefAccess().getRightParenthesisKeyword_0_2());
                      			
                    }

                    }
                    break;

            }

            otherlv_5=(Token)match(input,46,FOLLOW_46); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_5, grammarAccess.getXFunctionTypeRefAccess().getEqualsSignGreaterThanSignKeyword_1());
              		
            }
            // InternalXMapping.g:3549:3: ( (lv_returnType_6_0= ruleJvmTypeReference ) )
            // InternalXMapping.g:3550:4: (lv_returnType_6_0= ruleJvmTypeReference )
            {
            // InternalXMapping.g:3550:4: (lv_returnType_6_0= ruleJvmTypeReference )
            // InternalXMapping.g:3551:5: lv_returnType_6_0= ruleJvmTypeReference
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getXFunctionTypeRefAccess().getReturnTypeJvmTypeReferenceParserRuleCall_2_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_returnType_6_0=ruleJvmTypeReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getXFunctionTypeRefRule());
              					}
              					set(
              						current,
              						"returnType",
              						lv_returnType_6_0,
              						"org.eclipse.xtext.xbase.Xtype.JvmTypeReference");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXFunctionTypeRef"


    // $ANTLR start "entryRuleJvmParameterizedTypeReference"
    // InternalXMapping.g:3572:1: entryRuleJvmParameterizedTypeReference returns [EObject current=null] : iv_ruleJvmParameterizedTypeReference= ruleJvmParameterizedTypeReference EOF ;
    public final EObject entryRuleJvmParameterizedTypeReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleJvmParameterizedTypeReference = null;


        try {
            // InternalXMapping.g:3572:70: (iv_ruleJvmParameterizedTypeReference= ruleJvmParameterizedTypeReference EOF )
            // InternalXMapping.g:3573:2: iv_ruleJvmParameterizedTypeReference= ruleJvmParameterizedTypeReference EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getJvmParameterizedTypeReferenceRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleJvmParameterizedTypeReference=ruleJvmParameterizedTypeReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleJvmParameterizedTypeReference; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleJvmParameterizedTypeReference"


    // $ANTLR start "ruleJvmParameterizedTypeReference"
    // InternalXMapping.g:3579:1: ruleJvmParameterizedTypeReference returns [EObject current=null] : ( ( ( ruleQualifiedName ) ) ( ( ( '<' )=>otherlv_1= '<' ) ( (lv_arguments_2_0= ruleJvmArgumentTypeReference ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleJvmArgumentTypeReference ) ) )* otherlv_5= '>' ( ( ( ( () '.' ) )=> ( () otherlv_7= '.' ) ) ( ( ruleValidID ) ) ( ( ( '<' )=>otherlv_9= '<' ) ( (lv_arguments_10_0= ruleJvmArgumentTypeReference ) ) (otherlv_11= ',' ( (lv_arguments_12_0= ruleJvmArgumentTypeReference ) ) )* otherlv_13= '>' )? )* )? ) ;
    public final EObject ruleJvmParameterizedTypeReference() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_9=null;
        Token otherlv_11=null;
        Token otherlv_13=null;
        EObject lv_arguments_2_0 = null;

        EObject lv_arguments_4_0 = null;

        EObject lv_arguments_10_0 = null;

        EObject lv_arguments_12_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:3585:2: ( ( ( ( ruleQualifiedName ) ) ( ( ( '<' )=>otherlv_1= '<' ) ( (lv_arguments_2_0= ruleJvmArgumentTypeReference ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleJvmArgumentTypeReference ) ) )* otherlv_5= '>' ( ( ( ( () '.' ) )=> ( () otherlv_7= '.' ) ) ( ( ruleValidID ) ) ( ( ( '<' )=>otherlv_9= '<' ) ( (lv_arguments_10_0= ruleJvmArgumentTypeReference ) ) (otherlv_11= ',' ( (lv_arguments_12_0= ruleJvmArgumentTypeReference ) ) )* otherlv_13= '>' )? )* )? ) )
            // InternalXMapping.g:3586:2: ( ( ( ruleQualifiedName ) ) ( ( ( '<' )=>otherlv_1= '<' ) ( (lv_arguments_2_0= ruleJvmArgumentTypeReference ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleJvmArgumentTypeReference ) ) )* otherlv_5= '>' ( ( ( ( () '.' ) )=> ( () otherlv_7= '.' ) ) ( ( ruleValidID ) ) ( ( ( '<' )=>otherlv_9= '<' ) ( (lv_arguments_10_0= ruleJvmArgumentTypeReference ) ) (otherlv_11= ',' ( (lv_arguments_12_0= ruleJvmArgumentTypeReference ) ) )* otherlv_13= '>' )? )* )? )
            {
            // InternalXMapping.g:3586:2: ( ( ( ruleQualifiedName ) ) ( ( ( '<' )=>otherlv_1= '<' ) ( (lv_arguments_2_0= ruleJvmArgumentTypeReference ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleJvmArgumentTypeReference ) ) )* otherlv_5= '>' ( ( ( ( () '.' ) )=> ( () otherlv_7= '.' ) ) ( ( ruleValidID ) ) ( ( ( '<' )=>otherlv_9= '<' ) ( (lv_arguments_10_0= ruleJvmArgumentTypeReference ) ) (otherlv_11= ',' ( (lv_arguments_12_0= ruleJvmArgumentTypeReference ) ) )* otherlv_13= '>' )? )* )? )
            // InternalXMapping.g:3587:3: ( ( ruleQualifiedName ) ) ( ( ( '<' )=>otherlv_1= '<' ) ( (lv_arguments_2_0= ruleJvmArgumentTypeReference ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleJvmArgumentTypeReference ) ) )* otherlv_5= '>' ( ( ( ( () '.' ) )=> ( () otherlv_7= '.' ) ) ( ( ruleValidID ) ) ( ( ( '<' )=>otherlv_9= '<' ) ( (lv_arguments_10_0= ruleJvmArgumentTypeReference ) ) (otherlv_11= ',' ( (lv_arguments_12_0= ruleJvmArgumentTypeReference ) ) )* otherlv_13= '>' )? )* )?
            {
            // InternalXMapping.g:3587:3: ( ( ruleQualifiedName ) )
            // InternalXMapping.g:3588:4: ( ruleQualifiedName )
            {
            // InternalXMapping.g:3588:4: ( ruleQualifiedName )
            // InternalXMapping.g:3589:5: ruleQualifiedName
            {
            if ( state.backtracking==0 ) {

              					/* */
              				
            }
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElement(grammarAccess.getJvmParameterizedTypeReferenceRule());
              					}
              				
            }
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getJvmParameterizedTypeReferenceAccess().getTypeJvmTypeCrossReference_0_0());
              				
            }
            pushFollow(FOLLOW_48);
            ruleQualifiedName();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					afterParserOrEnumRuleCall();
              				
            }

            }


            }

            // InternalXMapping.g:3606:3: ( ( ( '<' )=>otherlv_1= '<' ) ( (lv_arguments_2_0= ruleJvmArgumentTypeReference ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleJvmArgumentTypeReference ) ) )* otherlv_5= '>' ( ( ( ( () '.' ) )=> ( () otherlv_7= '.' ) ) ( ( ruleValidID ) ) ( ( ( '<' )=>otherlv_9= '<' ) ( (lv_arguments_10_0= ruleJvmArgumentTypeReference ) ) (otherlv_11= ',' ( (lv_arguments_12_0= ruleJvmArgumentTypeReference ) ) )* otherlv_13= '>' )? )* )?
            int alt55=2;
            int LA55_0 = input.LA(1);

            if ( (LA55_0==47) ) {
                alt55=1;
            }
            switch (alt55) {
                case 1 :
                    // InternalXMapping.g:3607:4: ( ( '<' )=>otherlv_1= '<' ) ( (lv_arguments_2_0= ruleJvmArgumentTypeReference ) ) (otherlv_3= ',' ( (lv_arguments_4_0= ruleJvmArgumentTypeReference ) ) )* otherlv_5= '>' ( ( ( ( () '.' ) )=> ( () otherlv_7= '.' ) ) ( ( ruleValidID ) ) ( ( ( '<' )=>otherlv_9= '<' ) ( (lv_arguments_10_0= ruleJvmArgumentTypeReference ) ) (otherlv_11= ',' ( (lv_arguments_12_0= ruleJvmArgumentTypeReference ) ) )* otherlv_13= '>' )? )*
                    {
                    // InternalXMapping.g:3607:4: ( ( '<' )=>otherlv_1= '<' )
                    // InternalXMapping.g:3608:5: ( '<' )=>otherlv_1= '<'
                    {
                    otherlv_1=(Token)match(input,47,FOLLOW_49); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      					newLeafNode(otherlv_1, grammarAccess.getJvmParameterizedTypeReferenceAccess().getLessThanSignKeyword_1_0());
                      				
                    }

                    }

                    // InternalXMapping.g:3614:4: ( (lv_arguments_2_0= ruleJvmArgumentTypeReference ) )
                    // InternalXMapping.g:3615:5: (lv_arguments_2_0= ruleJvmArgumentTypeReference )
                    {
                    // InternalXMapping.g:3615:5: (lv_arguments_2_0= ruleJvmArgumentTypeReference )
                    // InternalXMapping.g:3616:6: lv_arguments_2_0= ruleJvmArgumentTypeReference
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getJvmParameterizedTypeReferenceAccess().getArgumentsJvmArgumentTypeReferenceParserRuleCall_1_1_0());
                      					
                    }
                    pushFollow(FOLLOW_50);
                    lv_arguments_2_0=ruleJvmArgumentTypeReference();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getJvmParameterizedTypeReferenceRule());
                      						}
                      						add(
                      							current,
                      							"arguments",
                      							lv_arguments_2_0,
                      							"org.eclipse.xtext.xbase.Xtype.JvmArgumentTypeReference");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }

                    // InternalXMapping.g:3633:4: (otherlv_3= ',' ( (lv_arguments_4_0= ruleJvmArgumentTypeReference ) ) )*
                    loop51:
                    do {
                        int alt51=2;
                        int LA51_0 = input.LA(1);

                        if ( (LA51_0==40) ) {
                            alt51=1;
                        }


                        switch (alt51) {
                    	case 1 :
                    	    // InternalXMapping.g:3634:5: otherlv_3= ',' ( (lv_arguments_4_0= ruleJvmArgumentTypeReference ) )
                    	    {
                    	    otherlv_3=(Token)match(input,40,FOLLOW_49); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      					newLeafNode(otherlv_3, grammarAccess.getJvmParameterizedTypeReferenceAccess().getCommaKeyword_1_2_0());
                    	      				
                    	    }
                    	    // InternalXMapping.g:3638:5: ( (lv_arguments_4_0= ruleJvmArgumentTypeReference ) )
                    	    // InternalXMapping.g:3639:6: (lv_arguments_4_0= ruleJvmArgumentTypeReference )
                    	    {
                    	    // InternalXMapping.g:3639:6: (lv_arguments_4_0= ruleJvmArgumentTypeReference )
                    	    // InternalXMapping.g:3640:7: lv_arguments_4_0= ruleJvmArgumentTypeReference
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      							newCompositeNode(grammarAccess.getJvmParameterizedTypeReferenceAccess().getArgumentsJvmArgumentTypeReferenceParserRuleCall_1_2_1_0());
                    	      						
                    	    }
                    	    pushFollow(FOLLOW_50);
                    	    lv_arguments_4_0=ruleJvmArgumentTypeReference();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      							if (current==null) {
                    	      								current = createModelElementForParent(grammarAccess.getJvmParameterizedTypeReferenceRule());
                    	      							}
                    	      							add(
                    	      								current,
                    	      								"arguments",
                    	      								lv_arguments_4_0,
                    	      								"org.eclipse.xtext.xbase.Xtype.JvmArgumentTypeReference");
                    	      							afterParserOrEnumRuleCall();
                    	      						
                    	    }

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop51;
                        }
                    } while (true);

                    otherlv_5=(Token)match(input,48,FOLLOW_36); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_5, grammarAccess.getJvmParameterizedTypeReferenceAccess().getGreaterThanSignKeyword_1_3());
                      			
                    }
                    // InternalXMapping.g:3662:4: ( ( ( ( () '.' ) )=> ( () otherlv_7= '.' ) ) ( ( ruleValidID ) ) ( ( ( '<' )=>otherlv_9= '<' ) ( (lv_arguments_10_0= ruleJvmArgumentTypeReference ) ) (otherlv_11= ',' ( (lv_arguments_12_0= ruleJvmArgumentTypeReference ) ) )* otherlv_13= '>' )? )*
                    loop54:
                    do {
                        int alt54=2;
                        int LA54_0 = input.LA(1);

                        if ( (LA54_0==35) ) {
                            alt54=1;
                        }


                        switch (alt54) {
                    	case 1 :
                    	    // InternalXMapping.g:3663:5: ( ( ( () '.' ) )=> ( () otherlv_7= '.' ) ) ( ( ruleValidID ) ) ( ( ( '<' )=>otherlv_9= '<' ) ( (lv_arguments_10_0= ruleJvmArgumentTypeReference ) ) (otherlv_11= ',' ( (lv_arguments_12_0= ruleJvmArgumentTypeReference ) ) )* otherlv_13= '>' )?
                    	    {
                    	    // InternalXMapping.g:3663:5: ( ( ( () '.' ) )=> ( () otherlv_7= '.' ) )
                    	    // InternalXMapping.g:3664:6: ( ( () '.' ) )=> ( () otherlv_7= '.' )
                    	    {
                    	    // InternalXMapping.g:3670:6: ( () otherlv_7= '.' )
                    	    // InternalXMapping.g:3671:7: () otherlv_7= '.'
                    	    {
                    	    // InternalXMapping.g:3671:7: ()
                    	    // InternalXMapping.g:3672:8: 
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      								/* */
                    	      							
                    	    }
                    	    if ( state.backtracking==0 ) {

                    	      								current = forceCreateModelElementAndSet(
                    	      									grammarAccess.getJvmParameterizedTypeReferenceAccess().getJvmInnerTypeReferenceOuterAction_1_4_0_0_0(),
                    	      									current);
                    	      							
                    	    }

                    	    }

                    	    otherlv_7=(Token)match(input,35,FOLLOW_3); if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      							newLeafNode(otherlv_7, grammarAccess.getJvmParameterizedTypeReferenceAccess().getFullStopKeyword_1_4_0_0_1());
                    	      						
                    	    }

                    	    }


                    	    }

                    	    // InternalXMapping.g:3687:5: ( ( ruleValidID ) )
                    	    // InternalXMapping.g:3688:6: ( ruleValidID )
                    	    {
                    	    // InternalXMapping.g:3688:6: ( ruleValidID )
                    	    // InternalXMapping.g:3689:7: ruleValidID
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      							/* */
                    	      						
                    	    }
                    	    if ( state.backtracking==0 ) {

                    	      							if (current==null) {
                    	      								current = createModelElement(grammarAccess.getJvmParameterizedTypeReferenceRule());
                    	      							}
                    	      						
                    	    }
                    	    if ( state.backtracking==0 ) {

                    	      							newCompositeNode(grammarAccess.getJvmParameterizedTypeReferenceAccess().getTypeJvmTypeCrossReference_1_4_1_0());
                    	      						
                    	    }
                    	    pushFollow(FOLLOW_51);
                    	    ruleValidID();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      							afterParserOrEnumRuleCall();
                    	      						
                    	    }

                    	    }


                    	    }

                    	    // InternalXMapping.g:3706:5: ( ( ( '<' )=>otherlv_9= '<' ) ( (lv_arguments_10_0= ruleJvmArgumentTypeReference ) ) (otherlv_11= ',' ( (lv_arguments_12_0= ruleJvmArgumentTypeReference ) ) )* otherlv_13= '>' )?
                    	    int alt53=2;
                    	    int LA53_0 = input.LA(1);

                    	    if ( (LA53_0==47) ) {
                    	        alt53=1;
                    	    }
                    	    switch (alt53) {
                    	        case 1 :
                    	            // InternalXMapping.g:3707:6: ( ( '<' )=>otherlv_9= '<' ) ( (lv_arguments_10_0= ruleJvmArgumentTypeReference ) ) (otherlv_11= ',' ( (lv_arguments_12_0= ruleJvmArgumentTypeReference ) ) )* otherlv_13= '>'
                    	            {
                    	            // InternalXMapping.g:3707:6: ( ( '<' )=>otherlv_9= '<' )
                    	            // InternalXMapping.g:3708:7: ( '<' )=>otherlv_9= '<'
                    	            {
                    	            otherlv_9=(Token)match(input,47,FOLLOW_49); if (state.failed) return current;
                    	            if ( state.backtracking==0 ) {

                    	              							newLeafNode(otherlv_9, grammarAccess.getJvmParameterizedTypeReferenceAccess().getLessThanSignKeyword_1_4_2_0());
                    	              						
                    	            }

                    	            }

                    	            // InternalXMapping.g:3714:6: ( (lv_arguments_10_0= ruleJvmArgumentTypeReference ) )
                    	            // InternalXMapping.g:3715:7: (lv_arguments_10_0= ruleJvmArgumentTypeReference )
                    	            {
                    	            // InternalXMapping.g:3715:7: (lv_arguments_10_0= ruleJvmArgumentTypeReference )
                    	            // InternalXMapping.g:3716:8: lv_arguments_10_0= ruleJvmArgumentTypeReference
                    	            {
                    	            if ( state.backtracking==0 ) {

                    	              								newCompositeNode(grammarAccess.getJvmParameterizedTypeReferenceAccess().getArgumentsJvmArgumentTypeReferenceParserRuleCall_1_4_2_1_0());
                    	              							
                    	            }
                    	            pushFollow(FOLLOW_50);
                    	            lv_arguments_10_0=ruleJvmArgumentTypeReference();

                    	            state._fsp--;
                    	            if (state.failed) return current;
                    	            if ( state.backtracking==0 ) {

                    	              								if (current==null) {
                    	              									current = createModelElementForParent(grammarAccess.getJvmParameterizedTypeReferenceRule());
                    	              								}
                    	              								add(
                    	              									current,
                    	              									"arguments",
                    	              									lv_arguments_10_0,
                    	              									"org.eclipse.xtext.xbase.Xtype.JvmArgumentTypeReference");
                    	              								afterParserOrEnumRuleCall();
                    	              							
                    	            }

                    	            }


                    	            }

                    	            // InternalXMapping.g:3733:6: (otherlv_11= ',' ( (lv_arguments_12_0= ruleJvmArgumentTypeReference ) ) )*
                    	            loop52:
                    	            do {
                    	                int alt52=2;
                    	                int LA52_0 = input.LA(1);

                    	                if ( (LA52_0==40) ) {
                    	                    alt52=1;
                    	                }


                    	                switch (alt52) {
                    	            	case 1 :
                    	            	    // InternalXMapping.g:3734:7: otherlv_11= ',' ( (lv_arguments_12_0= ruleJvmArgumentTypeReference ) )
                    	            	    {
                    	            	    otherlv_11=(Token)match(input,40,FOLLOW_49); if (state.failed) return current;
                    	            	    if ( state.backtracking==0 ) {

                    	            	      							newLeafNode(otherlv_11, grammarAccess.getJvmParameterizedTypeReferenceAccess().getCommaKeyword_1_4_2_2_0());
                    	            	      						
                    	            	    }
                    	            	    // InternalXMapping.g:3738:7: ( (lv_arguments_12_0= ruleJvmArgumentTypeReference ) )
                    	            	    // InternalXMapping.g:3739:8: (lv_arguments_12_0= ruleJvmArgumentTypeReference )
                    	            	    {
                    	            	    // InternalXMapping.g:3739:8: (lv_arguments_12_0= ruleJvmArgumentTypeReference )
                    	            	    // InternalXMapping.g:3740:9: lv_arguments_12_0= ruleJvmArgumentTypeReference
                    	            	    {
                    	            	    if ( state.backtracking==0 ) {

                    	            	      									newCompositeNode(grammarAccess.getJvmParameterizedTypeReferenceAccess().getArgumentsJvmArgumentTypeReferenceParserRuleCall_1_4_2_2_1_0());
                    	            	      								
                    	            	    }
                    	            	    pushFollow(FOLLOW_50);
                    	            	    lv_arguments_12_0=ruleJvmArgumentTypeReference();

                    	            	    state._fsp--;
                    	            	    if (state.failed) return current;
                    	            	    if ( state.backtracking==0 ) {

                    	            	      									if (current==null) {
                    	            	      										current = createModelElementForParent(grammarAccess.getJvmParameterizedTypeReferenceRule());
                    	            	      									}
                    	            	      									add(
                    	            	      										current,
                    	            	      										"arguments",
                    	            	      										lv_arguments_12_0,
                    	            	      										"org.eclipse.xtext.xbase.Xtype.JvmArgumentTypeReference");
                    	            	      									afterParserOrEnumRuleCall();
                    	            	      								
                    	            	    }

                    	            	    }


                    	            	    }


                    	            	    }
                    	            	    break;

                    	            	default :
                    	            	    break loop52;
                    	                }
                    	            } while (true);

                    	            otherlv_13=(Token)match(input,48,FOLLOW_36); if (state.failed) return current;
                    	            if ( state.backtracking==0 ) {

                    	              						newLeafNode(otherlv_13, grammarAccess.getJvmParameterizedTypeReferenceAccess().getGreaterThanSignKeyword_1_4_2_3());
                    	              					
                    	            }

                    	            }
                    	            break;

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop54;
                        }
                    } while (true);


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleJvmParameterizedTypeReference"


    // $ANTLR start "entryRuleJvmArgumentTypeReference"
    // InternalXMapping.g:3769:1: entryRuleJvmArgumentTypeReference returns [EObject current=null] : iv_ruleJvmArgumentTypeReference= ruleJvmArgumentTypeReference EOF ;
    public final EObject entryRuleJvmArgumentTypeReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleJvmArgumentTypeReference = null;


        try {
            // InternalXMapping.g:3769:65: (iv_ruleJvmArgumentTypeReference= ruleJvmArgumentTypeReference EOF )
            // InternalXMapping.g:3770:2: iv_ruleJvmArgumentTypeReference= ruleJvmArgumentTypeReference EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getJvmArgumentTypeReferenceRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleJvmArgumentTypeReference=ruleJvmArgumentTypeReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleJvmArgumentTypeReference; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleJvmArgumentTypeReference"


    // $ANTLR start "ruleJvmArgumentTypeReference"
    // InternalXMapping.g:3776:1: ruleJvmArgumentTypeReference returns [EObject current=null] : (this_JvmTypeReference_0= ruleJvmTypeReference | this_JvmWildcardTypeReference_1= ruleJvmWildcardTypeReference ) ;
    public final EObject ruleJvmArgumentTypeReference() throws RecognitionException {
        EObject current = null;

        EObject this_JvmTypeReference_0 = null;

        EObject this_JvmWildcardTypeReference_1 = null;



        	enterRule();

        try {
            // InternalXMapping.g:3782:2: ( (this_JvmTypeReference_0= ruleJvmTypeReference | this_JvmWildcardTypeReference_1= ruleJvmWildcardTypeReference ) )
            // InternalXMapping.g:3783:2: (this_JvmTypeReference_0= ruleJvmTypeReference | this_JvmWildcardTypeReference_1= ruleJvmWildcardTypeReference )
            {
            // InternalXMapping.g:3783:2: (this_JvmTypeReference_0= ruleJvmTypeReference | this_JvmWildcardTypeReference_1= ruleJvmWildcardTypeReference )
            int alt56=2;
            int LA56_0 = input.LA(1);

            if ( (LA56_0==RULE_ID||LA56_0==20||LA56_0==22||LA56_0==25||LA56_0==31||LA56_0==33||LA56_0==44||LA56_0==46) ) {
                alt56=1;
            }
            else if ( (LA56_0==49) ) {
                alt56=2;
            }
            else {
                if (state.backtracking>0) {state.failed=true; return current;}
                NoViableAltException nvae =
                    new NoViableAltException("", 56, 0, input);

                throw nvae;
            }
            switch (alt56) {
                case 1 :
                    // InternalXMapping.g:3784:3: this_JvmTypeReference_0= ruleJvmTypeReference
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getJvmArgumentTypeReferenceAccess().getJvmTypeReferenceParserRuleCall_0());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_JvmTypeReference_0=ruleJvmTypeReference();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_JvmTypeReference_0;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;
                case 2 :
                    // InternalXMapping.g:3796:3: this_JvmWildcardTypeReference_1= ruleJvmWildcardTypeReference
                    {
                    if ( state.backtracking==0 ) {

                      			/* */
                      		
                    }
                    if ( state.backtracking==0 ) {

                      			newCompositeNode(grammarAccess.getJvmArgumentTypeReferenceAccess().getJvmWildcardTypeReferenceParserRuleCall_1());
                      		
                    }
                    pushFollow(FOLLOW_2);
                    this_JvmWildcardTypeReference_1=ruleJvmWildcardTypeReference();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      			current = this_JvmWildcardTypeReference_1;
                      			afterParserOrEnumRuleCall();
                      		
                    }

                    }
                    break;

            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleJvmArgumentTypeReference"


    // $ANTLR start "entryRuleJvmWildcardTypeReference"
    // InternalXMapping.g:3811:1: entryRuleJvmWildcardTypeReference returns [EObject current=null] : iv_ruleJvmWildcardTypeReference= ruleJvmWildcardTypeReference EOF ;
    public final EObject entryRuleJvmWildcardTypeReference() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleJvmWildcardTypeReference = null;


        try {
            // InternalXMapping.g:3811:65: (iv_ruleJvmWildcardTypeReference= ruleJvmWildcardTypeReference EOF )
            // InternalXMapping.g:3812:2: iv_ruleJvmWildcardTypeReference= ruleJvmWildcardTypeReference EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getJvmWildcardTypeReferenceRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleJvmWildcardTypeReference=ruleJvmWildcardTypeReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleJvmWildcardTypeReference; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleJvmWildcardTypeReference"


    // $ANTLR start "ruleJvmWildcardTypeReference"
    // InternalXMapping.g:3818:1: ruleJvmWildcardTypeReference returns [EObject current=null] : ( () otherlv_1= '?' ( ( ( (lv_constraints_2_0= ruleJvmUpperBound ) ) ( (lv_constraints_3_0= ruleJvmUpperBoundAnded ) )* ) | ( ( (lv_constraints_4_0= ruleJvmLowerBound ) ) ( (lv_constraints_5_0= ruleJvmLowerBoundAnded ) )* ) )? ) ;
    public final EObject ruleJvmWildcardTypeReference() throws RecognitionException {
        EObject current = null;

        Token otherlv_1=null;
        EObject lv_constraints_2_0 = null;

        EObject lv_constraints_3_0 = null;

        EObject lv_constraints_4_0 = null;

        EObject lv_constraints_5_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:3824:2: ( ( () otherlv_1= '?' ( ( ( (lv_constraints_2_0= ruleJvmUpperBound ) ) ( (lv_constraints_3_0= ruleJvmUpperBoundAnded ) )* ) | ( ( (lv_constraints_4_0= ruleJvmLowerBound ) ) ( (lv_constraints_5_0= ruleJvmLowerBoundAnded ) )* ) )? ) )
            // InternalXMapping.g:3825:2: ( () otherlv_1= '?' ( ( ( (lv_constraints_2_0= ruleJvmUpperBound ) ) ( (lv_constraints_3_0= ruleJvmUpperBoundAnded ) )* ) | ( ( (lv_constraints_4_0= ruleJvmLowerBound ) ) ( (lv_constraints_5_0= ruleJvmLowerBoundAnded ) )* ) )? )
            {
            // InternalXMapping.g:3825:2: ( () otherlv_1= '?' ( ( ( (lv_constraints_2_0= ruleJvmUpperBound ) ) ( (lv_constraints_3_0= ruleJvmUpperBoundAnded ) )* ) | ( ( (lv_constraints_4_0= ruleJvmLowerBound ) ) ( (lv_constraints_5_0= ruleJvmLowerBoundAnded ) )* ) )? )
            // InternalXMapping.g:3826:3: () otherlv_1= '?' ( ( ( (lv_constraints_2_0= ruleJvmUpperBound ) ) ( (lv_constraints_3_0= ruleJvmUpperBoundAnded ) )* ) | ( ( (lv_constraints_4_0= ruleJvmLowerBound ) ) ( (lv_constraints_5_0= ruleJvmLowerBoundAnded ) )* ) )?
            {
            // InternalXMapping.g:3826:3: ()
            // InternalXMapping.g:3827:4: 
            {
            if ( state.backtracking==0 ) {

              				/* */
              			
            }
            if ( state.backtracking==0 ) {

              				current = forceCreateModelElement(
              					grammarAccess.getJvmWildcardTypeReferenceAccess().getJvmWildcardTypeReferenceAction_0(),
              					current);
              			
            }

            }

            otherlv_1=(Token)match(input,49,FOLLOW_52); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_1, grammarAccess.getJvmWildcardTypeReferenceAccess().getQuestionMarkKeyword_1());
              		
            }
            // InternalXMapping.g:3840:3: ( ( ( (lv_constraints_2_0= ruleJvmUpperBound ) ) ( (lv_constraints_3_0= ruleJvmUpperBoundAnded ) )* ) | ( ( (lv_constraints_4_0= ruleJvmLowerBound ) ) ( (lv_constraints_5_0= ruleJvmLowerBoundAnded ) )* ) )?
            int alt59=3;
            int LA59_0 = input.LA(1);

            if ( (LA59_0==50) ) {
                alt59=1;
            }
            else if ( (LA59_0==52) ) {
                alt59=2;
            }
            switch (alt59) {
                case 1 :
                    // InternalXMapping.g:3841:4: ( ( (lv_constraints_2_0= ruleJvmUpperBound ) ) ( (lv_constraints_3_0= ruleJvmUpperBoundAnded ) )* )
                    {
                    // InternalXMapping.g:3841:4: ( ( (lv_constraints_2_0= ruleJvmUpperBound ) ) ( (lv_constraints_3_0= ruleJvmUpperBoundAnded ) )* )
                    // InternalXMapping.g:3842:5: ( (lv_constraints_2_0= ruleJvmUpperBound ) ) ( (lv_constraints_3_0= ruleJvmUpperBoundAnded ) )*
                    {
                    // InternalXMapping.g:3842:5: ( (lv_constraints_2_0= ruleJvmUpperBound ) )
                    // InternalXMapping.g:3843:6: (lv_constraints_2_0= ruleJvmUpperBound )
                    {
                    // InternalXMapping.g:3843:6: (lv_constraints_2_0= ruleJvmUpperBound )
                    // InternalXMapping.g:3844:7: lv_constraints_2_0= ruleJvmUpperBound
                    {
                    if ( state.backtracking==0 ) {

                      							newCompositeNode(grammarAccess.getJvmWildcardTypeReferenceAccess().getConstraintsJvmUpperBoundParserRuleCall_2_0_0_0());
                      						
                    }
                    pushFollow(FOLLOW_53);
                    lv_constraints_2_0=ruleJvmUpperBound();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      							if (current==null) {
                      								current = createModelElementForParent(grammarAccess.getJvmWildcardTypeReferenceRule());
                      							}
                      							add(
                      								current,
                      								"constraints",
                      								lv_constraints_2_0,
                      								"org.eclipse.xtext.xbase.Xtype.JvmUpperBound");
                      							afterParserOrEnumRuleCall();
                      						
                    }

                    }


                    }

                    // InternalXMapping.g:3861:5: ( (lv_constraints_3_0= ruleJvmUpperBoundAnded ) )*
                    loop57:
                    do {
                        int alt57=2;
                        int LA57_0 = input.LA(1);

                        if ( (LA57_0==51) ) {
                            alt57=1;
                        }


                        switch (alt57) {
                    	case 1 :
                    	    // InternalXMapping.g:3862:6: (lv_constraints_3_0= ruleJvmUpperBoundAnded )
                    	    {
                    	    // InternalXMapping.g:3862:6: (lv_constraints_3_0= ruleJvmUpperBoundAnded )
                    	    // InternalXMapping.g:3863:7: lv_constraints_3_0= ruleJvmUpperBoundAnded
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      							newCompositeNode(grammarAccess.getJvmWildcardTypeReferenceAccess().getConstraintsJvmUpperBoundAndedParserRuleCall_2_0_1_0());
                    	      						
                    	    }
                    	    pushFollow(FOLLOW_53);
                    	    lv_constraints_3_0=ruleJvmUpperBoundAnded();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      							if (current==null) {
                    	      								current = createModelElementForParent(grammarAccess.getJvmWildcardTypeReferenceRule());
                    	      							}
                    	      							add(
                    	      								current,
                    	      								"constraints",
                    	      								lv_constraints_3_0,
                    	      								"org.eclipse.xtext.xbase.Xtype.JvmUpperBoundAnded");
                    	      							afterParserOrEnumRuleCall();
                    	      						
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop57;
                        }
                    } while (true);


                    }


                    }
                    break;
                case 2 :
                    // InternalXMapping.g:3882:4: ( ( (lv_constraints_4_0= ruleJvmLowerBound ) ) ( (lv_constraints_5_0= ruleJvmLowerBoundAnded ) )* )
                    {
                    // InternalXMapping.g:3882:4: ( ( (lv_constraints_4_0= ruleJvmLowerBound ) ) ( (lv_constraints_5_0= ruleJvmLowerBoundAnded ) )* )
                    // InternalXMapping.g:3883:5: ( (lv_constraints_4_0= ruleJvmLowerBound ) ) ( (lv_constraints_5_0= ruleJvmLowerBoundAnded ) )*
                    {
                    // InternalXMapping.g:3883:5: ( (lv_constraints_4_0= ruleJvmLowerBound ) )
                    // InternalXMapping.g:3884:6: (lv_constraints_4_0= ruleJvmLowerBound )
                    {
                    // InternalXMapping.g:3884:6: (lv_constraints_4_0= ruleJvmLowerBound )
                    // InternalXMapping.g:3885:7: lv_constraints_4_0= ruleJvmLowerBound
                    {
                    if ( state.backtracking==0 ) {

                      							newCompositeNode(grammarAccess.getJvmWildcardTypeReferenceAccess().getConstraintsJvmLowerBoundParserRuleCall_2_1_0_0());
                      						
                    }
                    pushFollow(FOLLOW_53);
                    lv_constraints_4_0=ruleJvmLowerBound();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      							if (current==null) {
                      								current = createModelElementForParent(grammarAccess.getJvmWildcardTypeReferenceRule());
                      							}
                      							add(
                      								current,
                      								"constraints",
                      								lv_constraints_4_0,
                      								"org.eclipse.xtext.xbase.Xtype.JvmLowerBound");
                      							afterParserOrEnumRuleCall();
                      						
                    }

                    }


                    }

                    // InternalXMapping.g:3902:5: ( (lv_constraints_5_0= ruleJvmLowerBoundAnded ) )*
                    loop58:
                    do {
                        int alt58=2;
                        int LA58_0 = input.LA(1);

                        if ( (LA58_0==51) ) {
                            alt58=1;
                        }


                        switch (alt58) {
                    	case 1 :
                    	    // InternalXMapping.g:3903:6: (lv_constraints_5_0= ruleJvmLowerBoundAnded )
                    	    {
                    	    // InternalXMapping.g:3903:6: (lv_constraints_5_0= ruleJvmLowerBoundAnded )
                    	    // InternalXMapping.g:3904:7: lv_constraints_5_0= ruleJvmLowerBoundAnded
                    	    {
                    	    if ( state.backtracking==0 ) {

                    	      							newCompositeNode(grammarAccess.getJvmWildcardTypeReferenceAccess().getConstraintsJvmLowerBoundAndedParserRuleCall_2_1_1_0());
                    	      						
                    	    }
                    	    pushFollow(FOLLOW_53);
                    	    lv_constraints_5_0=ruleJvmLowerBoundAnded();

                    	    state._fsp--;
                    	    if (state.failed) return current;
                    	    if ( state.backtracking==0 ) {

                    	      							if (current==null) {
                    	      								current = createModelElementForParent(grammarAccess.getJvmWildcardTypeReferenceRule());
                    	      							}
                    	      							add(
                    	      								current,
                    	      								"constraints",
                    	      								lv_constraints_5_0,
                    	      								"org.eclipse.xtext.xbase.Xtype.JvmLowerBoundAnded");
                    	      							afterParserOrEnumRuleCall();
                    	      						
                    	    }

                    	    }


                    	    }
                    	    break;

                    	default :
                    	    break loop58;
                        }
                    } while (true);


                    }


                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleJvmWildcardTypeReference"


    // $ANTLR start "entryRuleJvmUpperBound"
    // InternalXMapping.g:3927:1: entryRuleJvmUpperBound returns [EObject current=null] : iv_ruleJvmUpperBound= ruleJvmUpperBound EOF ;
    public final EObject entryRuleJvmUpperBound() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleJvmUpperBound = null;


        try {
            // InternalXMapping.g:3927:54: (iv_ruleJvmUpperBound= ruleJvmUpperBound EOF )
            // InternalXMapping.g:3928:2: iv_ruleJvmUpperBound= ruleJvmUpperBound EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getJvmUpperBoundRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleJvmUpperBound=ruleJvmUpperBound();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleJvmUpperBound; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleJvmUpperBound"


    // $ANTLR start "ruleJvmUpperBound"
    // InternalXMapping.g:3934:1: ruleJvmUpperBound returns [EObject current=null] : (otherlv_0= 'extends' ( (lv_typeReference_1_0= ruleJvmTypeReference ) ) ) ;
    public final EObject ruleJvmUpperBound() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_typeReference_1_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:3940:2: ( (otherlv_0= 'extends' ( (lv_typeReference_1_0= ruleJvmTypeReference ) ) ) )
            // InternalXMapping.g:3941:2: (otherlv_0= 'extends' ( (lv_typeReference_1_0= ruleJvmTypeReference ) ) )
            {
            // InternalXMapping.g:3941:2: (otherlv_0= 'extends' ( (lv_typeReference_1_0= ruleJvmTypeReference ) ) )
            // InternalXMapping.g:3942:3: otherlv_0= 'extends' ( (lv_typeReference_1_0= ruleJvmTypeReference ) )
            {
            otherlv_0=(Token)match(input,50,FOLLOW_46); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getJvmUpperBoundAccess().getExtendsKeyword_0());
              		
            }
            // InternalXMapping.g:3946:3: ( (lv_typeReference_1_0= ruleJvmTypeReference ) )
            // InternalXMapping.g:3947:4: (lv_typeReference_1_0= ruleJvmTypeReference )
            {
            // InternalXMapping.g:3947:4: (lv_typeReference_1_0= ruleJvmTypeReference )
            // InternalXMapping.g:3948:5: lv_typeReference_1_0= ruleJvmTypeReference
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getJvmUpperBoundAccess().getTypeReferenceJvmTypeReferenceParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_typeReference_1_0=ruleJvmTypeReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getJvmUpperBoundRule());
              					}
              					set(
              						current,
              						"typeReference",
              						lv_typeReference_1_0,
              						"org.eclipse.xtext.xbase.Xtype.JvmTypeReference");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleJvmUpperBound"


    // $ANTLR start "entryRuleJvmUpperBoundAnded"
    // InternalXMapping.g:3969:1: entryRuleJvmUpperBoundAnded returns [EObject current=null] : iv_ruleJvmUpperBoundAnded= ruleJvmUpperBoundAnded EOF ;
    public final EObject entryRuleJvmUpperBoundAnded() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleJvmUpperBoundAnded = null;


        try {
            // InternalXMapping.g:3969:59: (iv_ruleJvmUpperBoundAnded= ruleJvmUpperBoundAnded EOF )
            // InternalXMapping.g:3970:2: iv_ruleJvmUpperBoundAnded= ruleJvmUpperBoundAnded EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getJvmUpperBoundAndedRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleJvmUpperBoundAnded=ruleJvmUpperBoundAnded();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleJvmUpperBoundAnded; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleJvmUpperBoundAnded"


    // $ANTLR start "ruleJvmUpperBoundAnded"
    // InternalXMapping.g:3976:1: ruleJvmUpperBoundAnded returns [EObject current=null] : (otherlv_0= '&' ( (lv_typeReference_1_0= ruleJvmTypeReference ) ) ) ;
    public final EObject ruleJvmUpperBoundAnded() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_typeReference_1_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:3982:2: ( (otherlv_0= '&' ( (lv_typeReference_1_0= ruleJvmTypeReference ) ) ) )
            // InternalXMapping.g:3983:2: (otherlv_0= '&' ( (lv_typeReference_1_0= ruleJvmTypeReference ) ) )
            {
            // InternalXMapping.g:3983:2: (otherlv_0= '&' ( (lv_typeReference_1_0= ruleJvmTypeReference ) ) )
            // InternalXMapping.g:3984:3: otherlv_0= '&' ( (lv_typeReference_1_0= ruleJvmTypeReference ) )
            {
            otherlv_0=(Token)match(input,51,FOLLOW_46); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getJvmUpperBoundAndedAccess().getAmpersandKeyword_0());
              		
            }
            // InternalXMapping.g:3988:3: ( (lv_typeReference_1_0= ruleJvmTypeReference ) )
            // InternalXMapping.g:3989:4: (lv_typeReference_1_0= ruleJvmTypeReference )
            {
            // InternalXMapping.g:3989:4: (lv_typeReference_1_0= ruleJvmTypeReference )
            // InternalXMapping.g:3990:5: lv_typeReference_1_0= ruleJvmTypeReference
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getJvmUpperBoundAndedAccess().getTypeReferenceJvmTypeReferenceParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_typeReference_1_0=ruleJvmTypeReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getJvmUpperBoundAndedRule());
              					}
              					set(
              						current,
              						"typeReference",
              						lv_typeReference_1_0,
              						"org.eclipse.xtext.xbase.Xtype.JvmTypeReference");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleJvmUpperBoundAnded"


    // $ANTLR start "entryRuleJvmLowerBound"
    // InternalXMapping.g:4011:1: entryRuleJvmLowerBound returns [EObject current=null] : iv_ruleJvmLowerBound= ruleJvmLowerBound EOF ;
    public final EObject entryRuleJvmLowerBound() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleJvmLowerBound = null;


        try {
            // InternalXMapping.g:4011:54: (iv_ruleJvmLowerBound= ruleJvmLowerBound EOF )
            // InternalXMapping.g:4012:2: iv_ruleJvmLowerBound= ruleJvmLowerBound EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getJvmLowerBoundRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleJvmLowerBound=ruleJvmLowerBound();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleJvmLowerBound; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleJvmLowerBound"


    // $ANTLR start "ruleJvmLowerBound"
    // InternalXMapping.g:4018:1: ruleJvmLowerBound returns [EObject current=null] : (otherlv_0= 'super' ( (lv_typeReference_1_0= ruleJvmTypeReference ) ) ) ;
    public final EObject ruleJvmLowerBound() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_typeReference_1_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:4024:2: ( (otherlv_0= 'super' ( (lv_typeReference_1_0= ruleJvmTypeReference ) ) ) )
            // InternalXMapping.g:4025:2: (otherlv_0= 'super' ( (lv_typeReference_1_0= ruleJvmTypeReference ) ) )
            {
            // InternalXMapping.g:4025:2: (otherlv_0= 'super' ( (lv_typeReference_1_0= ruleJvmTypeReference ) ) )
            // InternalXMapping.g:4026:3: otherlv_0= 'super' ( (lv_typeReference_1_0= ruleJvmTypeReference ) )
            {
            otherlv_0=(Token)match(input,52,FOLLOW_46); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getJvmLowerBoundAccess().getSuperKeyword_0());
              		
            }
            // InternalXMapping.g:4030:3: ( (lv_typeReference_1_0= ruleJvmTypeReference ) )
            // InternalXMapping.g:4031:4: (lv_typeReference_1_0= ruleJvmTypeReference )
            {
            // InternalXMapping.g:4031:4: (lv_typeReference_1_0= ruleJvmTypeReference )
            // InternalXMapping.g:4032:5: lv_typeReference_1_0= ruleJvmTypeReference
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getJvmLowerBoundAccess().getTypeReferenceJvmTypeReferenceParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_typeReference_1_0=ruleJvmTypeReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getJvmLowerBoundRule());
              					}
              					set(
              						current,
              						"typeReference",
              						lv_typeReference_1_0,
              						"org.eclipse.xtext.xbase.Xtype.JvmTypeReference");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleJvmLowerBound"


    // $ANTLR start "entryRuleJvmLowerBoundAnded"
    // InternalXMapping.g:4053:1: entryRuleJvmLowerBoundAnded returns [EObject current=null] : iv_ruleJvmLowerBoundAnded= ruleJvmLowerBoundAnded EOF ;
    public final EObject entryRuleJvmLowerBoundAnded() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleJvmLowerBoundAnded = null;


        try {
            // InternalXMapping.g:4053:59: (iv_ruleJvmLowerBoundAnded= ruleJvmLowerBoundAnded EOF )
            // InternalXMapping.g:4054:2: iv_ruleJvmLowerBoundAnded= ruleJvmLowerBoundAnded EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getJvmLowerBoundAndedRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleJvmLowerBoundAnded=ruleJvmLowerBoundAnded();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleJvmLowerBoundAnded; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleJvmLowerBoundAnded"


    // $ANTLR start "ruleJvmLowerBoundAnded"
    // InternalXMapping.g:4060:1: ruleJvmLowerBoundAnded returns [EObject current=null] : (otherlv_0= '&' ( (lv_typeReference_1_0= ruleJvmTypeReference ) ) ) ;
    public final EObject ruleJvmLowerBoundAnded() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_typeReference_1_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:4066:2: ( (otherlv_0= '&' ( (lv_typeReference_1_0= ruleJvmTypeReference ) ) ) )
            // InternalXMapping.g:4067:2: (otherlv_0= '&' ( (lv_typeReference_1_0= ruleJvmTypeReference ) ) )
            {
            // InternalXMapping.g:4067:2: (otherlv_0= '&' ( (lv_typeReference_1_0= ruleJvmTypeReference ) ) )
            // InternalXMapping.g:4068:3: otherlv_0= '&' ( (lv_typeReference_1_0= ruleJvmTypeReference ) )
            {
            otherlv_0=(Token)match(input,51,FOLLOW_46); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getJvmLowerBoundAndedAccess().getAmpersandKeyword_0());
              		
            }
            // InternalXMapping.g:4072:3: ( (lv_typeReference_1_0= ruleJvmTypeReference ) )
            // InternalXMapping.g:4073:4: (lv_typeReference_1_0= ruleJvmTypeReference )
            {
            // InternalXMapping.g:4073:4: (lv_typeReference_1_0= ruleJvmTypeReference )
            // InternalXMapping.g:4074:5: lv_typeReference_1_0= ruleJvmTypeReference
            {
            if ( state.backtracking==0 ) {

              					newCompositeNode(grammarAccess.getJvmLowerBoundAndedAccess().getTypeReferenceJvmTypeReferenceParserRuleCall_1_0());
              				
            }
            pushFollow(FOLLOW_2);
            lv_typeReference_1_0=ruleJvmTypeReference();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {

              					if (current==null) {
              						current = createModelElementForParent(grammarAccess.getJvmLowerBoundAndedRule());
              					}
              					set(
              						current,
              						"typeReference",
              						lv_typeReference_1_0,
              						"org.eclipse.xtext.xbase.Xtype.JvmTypeReference");
              					afterParserOrEnumRuleCall();
              				
            }

            }


            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleJvmLowerBoundAnded"


    // $ANTLR start "entryRuleXImportDeclaration"
    // InternalXMapping.g:4095:1: entryRuleXImportDeclaration returns [EObject current=null] : iv_ruleXImportDeclaration= ruleXImportDeclaration EOF ;
    public final EObject entryRuleXImportDeclaration() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXImportDeclaration = null;


        try {
            // InternalXMapping.g:4095:59: (iv_ruleXImportDeclaration= ruleXImportDeclaration EOF )
            // InternalXMapping.g:4096:2: iv_ruleXImportDeclaration= ruleXImportDeclaration EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getXImportDeclarationRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleXImportDeclaration=ruleXImportDeclaration();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleXImportDeclaration; 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXImportDeclaration"


    // $ANTLR start "ruleXImportDeclaration"
    // InternalXMapping.g:4102:1: ruleXImportDeclaration returns [EObject current=null] : (otherlv_0= 'import' ( ( ( (lv_static_1_0= 'static' ) ) ( (lv_extension_2_0= 'extension' ) )? ( ( ruleQualifiedNameInStaticImport ) ) ( ( (lv_wildcard_4_0= '*' ) ) | ( (lv_memberName_5_0= ruleValidID ) ) ) ) | ( ( ruleQualifiedName ) ) | ( (lv_importedNamespace_7_0= ruleQualifiedNameWithWildcard ) ) ) (otherlv_8= ';' )? ) ;
    public final EObject ruleXImportDeclaration() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_static_1_0=null;
        Token lv_extension_2_0=null;
        Token lv_wildcard_4_0=null;
        Token otherlv_8=null;
        AntlrDatatypeRuleToken lv_memberName_5_0 = null;

        AntlrDatatypeRuleToken lv_importedNamespace_7_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:4108:2: ( (otherlv_0= 'import' ( ( ( (lv_static_1_0= 'static' ) ) ( (lv_extension_2_0= 'extension' ) )? ( ( ruleQualifiedNameInStaticImport ) ) ( ( (lv_wildcard_4_0= '*' ) ) | ( (lv_memberName_5_0= ruleValidID ) ) ) ) | ( ( ruleQualifiedName ) ) | ( (lv_importedNamespace_7_0= ruleQualifiedNameWithWildcard ) ) ) (otherlv_8= ';' )? ) )
            // InternalXMapping.g:4109:2: (otherlv_0= 'import' ( ( ( (lv_static_1_0= 'static' ) ) ( (lv_extension_2_0= 'extension' ) )? ( ( ruleQualifiedNameInStaticImport ) ) ( ( (lv_wildcard_4_0= '*' ) ) | ( (lv_memberName_5_0= ruleValidID ) ) ) ) | ( ( ruleQualifiedName ) ) | ( (lv_importedNamespace_7_0= ruleQualifiedNameWithWildcard ) ) ) (otherlv_8= ';' )? )
            {
            // InternalXMapping.g:4109:2: (otherlv_0= 'import' ( ( ( (lv_static_1_0= 'static' ) ) ( (lv_extension_2_0= 'extension' ) )? ( ( ruleQualifiedNameInStaticImport ) ) ( ( (lv_wildcard_4_0= '*' ) ) | ( (lv_memberName_5_0= ruleValidID ) ) ) ) | ( ( ruleQualifiedName ) ) | ( (lv_importedNamespace_7_0= ruleQualifiedNameWithWildcard ) ) ) (otherlv_8= ';' )? )
            // InternalXMapping.g:4110:3: otherlv_0= 'import' ( ( ( (lv_static_1_0= 'static' ) ) ( (lv_extension_2_0= 'extension' ) )? ( ( ruleQualifiedNameInStaticImport ) ) ( ( (lv_wildcard_4_0= '*' ) ) | ( (lv_memberName_5_0= ruleValidID ) ) ) ) | ( ( ruleQualifiedName ) ) | ( (lv_importedNamespace_7_0= ruleQualifiedNameWithWildcard ) ) ) (otherlv_8= ';' )?
            {
            otherlv_0=(Token)match(input,22,FOLLOW_54); if (state.failed) return current;
            if ( state.backtracking==0 ) {

              			newLeafNode(otherlv_0, grammarAccess.getXImportDeclarationAccess().getImportKeyword_0());
              		
            }
            // InternalXMapping.g:4114:3: ( ( ( (lv_static_1_0= 'static' ) ) ( (lv_extension_2_0= 'extension' ) )? ( ( ruleQualifiedNameInStaticImport ) ) ( ( (lv_wildcard_4_0= '*' ) ) | ( (lv_memberName_5_0= ruleValidID ) ) ) ) | ( ( ruleQualifiedName ) ) | ( (lv_importedNamespace_7_0= ruleQualifiedNameWithWildcard ) ) )
            int alt62=3;
            alt62 = dfa62.predict(input);
            switch (alt62) {
                case 1 :
                    // InternalXMapping.g:4115:4: ( ( (lv_static_1_0= 'static' ) ) ( (lv_extension_2_0= 'extension' ) )? ( ( ruleQualifiedNameInStaticImport ) ) ( ( (lv_wildcard_4_0= '*' ) ) | ( (lv_memberName_5_0= ruleValidID ) ) ) )
                    {
                    // InternalXMapping.g:4115:4: ( ( (lv_static_1_0= 'static' ) ) ( (lv_extension_2_0= 'extension' ) )? ( ( ruleQualifiedNameInStaticImport ) ) ( ( (lv_wildcard_4_0= '*' ) ) | ( (lv_memberName_5_0= ruleValidID ) ) ) )
                    // InternalXMapping.g:4116:5: ( (lv_static_1_0= 'static' ) ) ( (lv_extension_2_0= 'extension' ) )? ( ( ruleQualifiedNameInStaticImport ) ) ( ( (lv_wildcard_4_0= '*' ) ) | ( (lv_memberName_5_0= ruleValidID ) ) )
                    {
                    // InternalXMapping.g:4116:5: ( (lv_static_1_0= 'static' ) )
                    // InternalXMapping.g:4117:6: (lv_static_1_0= 'static' )
                    {
                    // InternalXMapping.g:4117:6: (lv_static_1_0= 'static' )
                    // InternalXMapping.g:4118:7: lv_static_1_0= 'static'
                    {
                    lv_static_1_0=(Token)match(input,53,FOLLOW_55); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      							newLeafNode(lv_static_1_0, grammarAccess.getXImportDeclarationAccess().getStaticStaticKeyword_1_0_0_0());
                      						
                    }
                    if ( state.backtracking==0 ) {

                      							if (current==null) {
                      								current = createModelElement(grammarAccess.getXImportDeclarationRule());
                      							}
                      							setWithLastConsumed(current, "static", lv_static_1_0 != null, "static");
                      						
                    }

                    }


                    }

                    // InternalXMapping.g:4130:5: ( (lv_extension_2_0= 'extension' ) )?
                    int alt60=2;
                    int LA60_0 = input.LA(1);

                    if ( (LA60_0==54) ) {
                        alt60=1;
                    }
                    switch (alt60) {
                        case 1 :
                            // InternalXMapping.g:4131:6: (lv_extension_2_0= 'extension' )
                            {
                            // InternalXMapping.g:4131:6: (lv_extension_2_0= 'extension' )
                            // InternalXMapping.g:4132:7: lv_extension_2_0= 'extension'
                            {
                            lv_extension_2_0=(Token)match(input,54,FOLLOW_55); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              							newLeafNode(lv_extension_2_0, grammarAccess.getXImportDeclarationAccess().getExtensionExtensionKeyword_1_0_1_0());
                              						
                            }
                            if ( state.backtracking==0 ) {

                              							if (current==null) {
                              								current = createModelElement(grammarAccess.getXImportDeclarationRule());
                              							}
                              							setWithLastConsumed(current, "extension", lv_extension_2_0 != null, "extension");
                              						
                            }

                            }


                            }
                            break;

                    }

                    // InternalXMapping.g:4144:5: ( ( ruleQualifiedNameInStaticImport ) )
                    // InternalXMapping.g:4145:6: ( ruleQualifiedNameInStaticImport )
                    {
                    // InternalXMapping.g:4145:6: ( ruleQualifiedNameInStaticImport )
                    // InternalXMapping.g:4146:7: ruleQualifiedNameInStaticImport
                    {
                    if ( state.backtracking==0 ) {

                      							/* */
                      						
                    }
                    if ( state.backtracking==0 ) {

                      							if (current==null) {
                      								current = createModelElement(grammarAccess.getXImportDeclarationRule());
                      							}
                      						
                    }
                    if ( state.backtracking==0 ) {

                      							newCompositeNode(grammarAccess.getXImportDeclarationAccess().getImportedTypeJvmDeclaredTypeCrossReference_1_0_2_0());
                      						
                    }
                    pushFollow(FOLLOW_56);
                    ruleQualifiedNameInStaticImport();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      							afterParserOrEnumRuleCall();
                      						
                    }

                    }


                    }

                    // InternalXMapping.g:4163:5: ( ( (lv_wildcard_4_0= '*' ) ) | ( (lv_memberName_5_0= ruleValidID ) ) )
                    int alt61=2;
                    int LA61_0 = input.LA(1);

                    if ( (LA61_0==55) ) {
                        alt61=1;
                    }
                    else if ( (LA61_0==RULE_ID||LA61_0==20||LA61_0==22||LA61_0==25||LA61_0==31||LA61_0==33) ) {
                        alt61=2;
                    }
                    else {
                        if (state.backtracking>0) {state.failed=true; return current;}
                        NoViableAltException nvae =
                            new NoViableAltException("", 61, 0, input);

                        throw nvae;
                    }
                    switch (alt61) {
                        case 1 :
                            // InternalXMapping.g:4164:6: ( (lv_wildcard_4_0= '*' ) )
                            {
                            // InternalXMapping.g:4164:6: ( (lv_wildcard_4_0= '*' ) )
                            // InternalXMapping.g:4165:7: (lv_wildcard_4_0= '*' )
                            {
                            // InternalXMapping.g:4165:7: (lv_wildcard_4_0= '*' )
                            // InternalXMapping.g:4166:8: lv_wildcard_4_0= '*'
                            {
                            lv_wildcard_4_0=(Token)match(input,55,FOLLOW_57); if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              								newLeafNode(lv_wildcard_4_0, grammarAccess.getXImportDeclarationAccess().getWildcardAsteriskKeyword_1_0_3_0_0());
                              							
                            }
                            if ( state.backtracking==0 ) {

                              								if (current==null) {
                              									current = createModelElement(grammarAccess.getXImportDeclarationRule());
                              								}
                              								setWithLastConsumed(current, "wildcard", lv_wildcard_4_0 != null, "*");
                              							
                            }

                            }


                            }


                            }
                            break;
                        case 2 :
                            // InternalXMapping.g:4179:6: ( (lv_memberName_5_0= ruleValidID ) )
                            {
                            // InternalXMapping.g:4179:6: ( (lv_memberName_5_0= ruleValidID ) )
                            // InternalXMapping.g:4180:7: (lv_memberName_5_0= ruleValidID )
                            {
                            // InternalXMapping.g:4180:7: (lv_memberName_5_0= ruleValidID )
                            // InternalXMapping.g:4181:8: lv_memberName_5_0= ruleValidID
                            {
                            if ( state.backtracking==0 ) {

                              								newCompositeNode(grammarAccess.getXImportDeclarationAccess().getMemberNameValidIDParserRuleCall_1_0_3_1_0());
                              							
                            }
                            pushFollow(FOLLOW_57);
                            lv_memberName_5_0=ruleValidID();

                            state._fsp--;
                            if (state.failed) return current;
                            if ( state.backtracking==0 ) {

                              								if (current==null) {
                              									current = createModelElementForParent(grammarAccess.getXImportDeclarationRule());
                              								}
                              								set(
                              									current,
                              									"memberName",
                              									lv_memberName_5_0,
                              									"com.gk_software.core.dsl.xmapping.XMapping.ValidID");
                              								afterParserOrEnumRuleCall();
                              							
                            }

                            }


                            }


                            }
                            break;

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalXMapping.g:4201:4: ( ( ruleQualifiedName ) )
                    {
                    // InternalXMapping.g:4201:4: ( ( ruleQualifiedName ) )
                    // InternalXMapping.g:4202:5: ( ruleQualifiedName )
                    {
                    // InternalXMapping.g:4202:5: ( ruleQualifiedName )
                    // InternalXMapping.g:4203:6: ruleQualifiedName
                    {
                    if ( state.backtracking==0 ) {

                      						/* */
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElement(grammarAccess.getXImportDeclarationRule());
                      						}
                      					
                    }
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getXImportDeclarationAccess().getImportedTypeJvmDeclaredTypeCrossReference_1_1_0());
                      					
                    }
                    pushFollow(FOLLOW_57);
                    ruleQualifiedName();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;
                case 3 :
                    // InternalXMapping.g:4221:4: ( (lv_importedNamespace_7_0= ruleQualifiedNameWithWildcard ) )
                    {
                    // InternalXMapping.g:4221:4: ( (lv_importedNamespace_7_0= ruleQualifiedNameWithWildcard ) )
                    // InternalXMapping.g:4222:5: (lv_importedNamespace_7_0= ruleQualifiedNameWithWildcard )
                    {
                    // InternalXMapping.g:4222:5: (lv_importedNamespace_7_0= ruleQualifiedNameWithWildcard )
                    // InternalXMapping.g:4223:6: lv_importedNamespace_7_0= ruleQualifiedNameWithWildcard
                    {
                    if ( state.backtracking==0 ) {

                      						newCompositeNode(grammarAccess.getXImportDeclarationAccess().getImportedNamespaceQualifiedNameWithWildcardParserRuleCall_1_2_0());
                      					
                    }
                    pushFollow(FOLLOW_57);
                    lv_importedNamespace_7_0=ruleQualifiedNameWithWildcard();

                    state._fsp--;
                    if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      						if (current==null) {
                      							current = createModelElementForParent(grammarAccess.getXImportDeclarationRule());
                      						}
                      						set(
                      							current,
                      							"importedNamespace",
                      							lv_importedNamespace_7_0,
                      							"com.gk_software.core.dsl.xmapping.XMapping.QualifiedNameWithWildcard");
                      						afterParserOrEnumRuleCall();
                      					
                    }

                    }


                    }


                    }
                    break;

            }

            // InternalXMapping.g:4241:3: (otherlv_8= ';' )?
            int alt63=2;
            int LA63_0 = input.LA(1);

            if ( (LA63_0==21) ) {
                alt63=1;
            }
            switch (alt63) {
                case 1 :
                    // InternalXMapping.g:4242:4: otherlv_8= ';'
                    {
                    otherlv_8=(Token)match(input,21,FOLLOW_2); if (state.failed) return current;
                    if ( state.backtracking==0 ) {

                      				newLeafNode(otherlv_8, grammarAccess.getXImportDeclarationAccess().getSemicolonKeyword_2());
                      			
                    }

                    }
                    break;

            }


            }


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXImportDeclaration"


    // $ANTLR start "entryRuleQualifiedNameInStaticImport"
    // InternalXMapping.g:4251:1: entryRuleQualifiedNameInStaticImport returns [String current=null] : iv_ruleQualifiedNameInStaticImport= ruleQualifiedNameInStaticImport EOF ;
    public final String entryRuleQualifiedNameInStaticImport() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleQualifiedNameInStaticImport = null;


        try {
            // InternalXMapping.g:4251:67: (iv_ruleQualifiedNameInStaticImport= ruleQualifiedNameInStaticImport EOF )
            // InternalXMapping.g:4252:2: iv_ruleQualifiedNameInStaticImport= ruleQualifiedNameInStaticImport EOF
            {
            if ( state.backtracking==0 ) {
               newCompositeNode(grammarAccess.getQualifiedNameInStaticImportRule()); 
            }
            pushFollow(FOLLOW_1);
            iv_ruleQualifiedNameInStaticImport=ruleQualifiedNameInStaticImport();

            state._fsp--;
            if (state.failed) return current;
            if ( state.backtracking==0 ) {
               current =iv_ruleQualifiedNameInStaticImport.getText(); 
            }
            match(input,EOF,FOLLOW_2); if (state.failed) return current;

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQualifiedNameInStaticImport"


    // $ANTLR start "ruleQualifiedNameInStaticImport"
    // InternalXMapping.g:4258:1: ruleQualifiedNameInStaticImport returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : (this_ValidID_0= ruleValidID kw= '.' )+ ;
    public final AntlrDatatypeRuleToken ruleQualifiedNameInStaticImport() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;
        AntlrDatatypeRuleToken this_ValidID_0 = null;



        	enterRule();

        try {
            // InternalXMapping.g:4264:2: ( (this_ValidID_0= ruleValidID kw= '.' )+ )
            // InternalXMapping.g:4265:2: (this_ValidID_0= ruleValidID kw= '.' )+
            {
            // InternalXMapping.g:4265:2: (this_ValidID_0= ruleValidID kw= '.' )+
            int cnt64=0;
            loop64:
            do {
                int alt64=2;
                switch ( input.LA(1) ) {
                case RULE_ID:
                    {
                    int LA64_2 = input.LA(2);

                    if ( (LA64_2==35) ) {
                        alt64=1;
                    }


                    }
                    break;
                case 33:
                    {
                    int LA64_3 = input.LA(2);

                    if ( (LA64_3==35) ) {
                        alt64=1;
                    }


                    }
                    break;
                case 20:
                    {
                    int LA64_4 = input.LA(2);

                    if ( (LA64_4==35) ) {
                        alt64=1;
                    }


                    }
                    break;
                case 22:
                    {
                    int LA64_5 = input.LA(2);

                    if ( (LA64_5==35) ) {
                        alt64=1;
                    }


                    }
                    break;
                case 25:
                    {
                    int LA64_6 = input.LA(2);

                    if ( (LA64_6==35) ) {
                        alt64=1;
                    }


                    }
                    break;
                case 31:
                    {
                    int LA64_7 = input.LA(2);

                    if ( (LA64_7==35) ) {
                        alt64=1;
                    }


                    }
                    break;

                }

                switch (alt64) {
            	case 1 :
            	    // InternalXMapping.g:4266:3: this_ValidID_0= ruleValidID kw= '.'
            	    {
            	    if ( state.backtracking==0 ) {

            	      			newCompositeNode(grammarAccess.getQualifiedNameInStaticImportAccess().getValidIDParserRuleCall_0());
            	      		
            	    }
            	    pushFollow(FOLLOW_27);
            	    this_ValidID_0=ruleValidID();

            	    state._fsp--;
            	    if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      			current.merge(this_ValidID_0);
            	      		
            	    }
            	    if ( state.backtracking==0 ) {

            	      			afterParserOrEnumRuleCall();
            	      		
            	    }
            	    kw=(Token)match(input,35,FOLLOW_58); if (state.failed) return current;
            	    if ( state.backtracking==0 ) {

            	      			current.merge(kw);
            	      			newLeafNode(kw, grammarAccess.getQualifiedNameInStaticImportAccess().getFullStopKeyword_1());
            	      		
            	    }

            	    }
            	    break;

            	default :
            	    if ( cnt64 >= 1 ) break loop64;
            	    if (state.backtracking>0) {state.failed=true; return current;}
                        EarlyExitException eee =
                            new EarlyExitException(64, input);
                        throw eee;
                }
                cnt64++;
            } while (true);


            }

            if ( state.backtracking==0 ) {

              	leaveRule();

            }
        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQualifiedNameInStaticImport"

    // $ANTLR start synpred56_InternalXMapping
    public final void synpred56_InternalXMapping_fragment() throws RecognitionException {   
        // InternalXMapping.g:3399:5: ( ( () ruleArrayBrackets ) )
        // InternalXMapping.g:3399:6: ( () ruleArrayBrackets )
        {
        // InternalXMapping.g:3399:6: ( () ruleArrayBrackets )
        // InternalXMapping.g:3400:6: () ruleArrayBrackets
        {
        // InternalXMapping.g:3400:6: ()
        // InternalXMapping.g:3401:6: 
        {
        }

        pushFollow(FOLLOW_2);
        ruleArrayBrackets();

        state._fsp--;
        if (state.failed) return ;

        }


        }
    }
    // $ANTLR end synpred56_InternalXMapping

    // $ANTLR start synpred77_InternalXMapping
    public final void synpred77_InternalXMapping_fragment() throws RecognitionException {   
        // InternalXMapping.g:4201:4: ( ( ( ruleQualifiedName ) ) )
        // InternalXMapping.g:4201:4: ( ( ruleQualifiedName ) )
        {
        // InternalXMapping.g:4201:4: ( ( ruleQualifiedName ) )
        // InternalXMapping.g:4202:5: ( ruleQualifiedName )
        {
        // InternalXMapping.g:4202:5: ( ruleQualifiedName )
        // InternalXMapping.g:4203:6: ruleQualifiedName
        {
        if ( state.backtracking==0 ) {

          						/* */
          					
        }
        pushFollow(FOLLOW_2);
        ruleQualifiedName();

        state._fsp--;
        if (state.failed) return ;

        }


        }


        }
    }
    // $ANTLR end synpred77_InternalXMapping

    // Delegated rules

    public final boolean synpred77_InternalXMapping() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred77_InternalXMapping_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred56_InternalXMapping() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred56_InternalXMapping_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA7 dfa7 = new DFA7(this);
    protected DFA8 dfa8 = new DFA8(this);
    protected DFA9 dfa9 = new DFA9(this);
    protected DFA62 dfa62 = new DFA62(this);
    static final String dfa_1s = "\46\uffff";
    static final String dfa_2s = "\1\4\1\uffff\2\4\1\43\3\uffff\1\4\6\25\2\4\6\25\1\43\1\4\6\25\1\4\6\25";
    static final String dfa_3s = "\1\47\1\uffff\2\46\1\43\3\uffff\1\41\6\50\1\46\1\41\6\50\1\43\1\41\6\50\1\46\6\50";
    static final String dfa_4s = "\1\uffff\1\1\3\uffff\1\3\1\2\1\4\36\uffff";
    static final String dfa_5s = "\46\uffff}>";
    static final String[] dfa_6s = {
            "\1\1\2\uffff\1\1\1\uffff\1\4\1\5\4\uffff\1\1\4\uffff\1\1\1\uffff\1\1\2\uffff\1\1\5\uffff\1\3\1\uffff\1\2\5\uffff\1\5",
            "",
            "\1\6\2\uffff\1\6\1\uffff\1\6\1\uffff\5\6\4\uffff\1\6\1\uffff\1\6\2\uffff\1\6\5\uffff\1\6\1\uffff\1\6\1\uffff\1\1\2\uffff\1\1",
            "\1\7\2\uffff\1\7\1\uffff\1\7\5\uffff\1\7\4\uffff\1\7\1\uffff\1\7\2\uffff\1\7\5\uffff\1\7\1\uffff\1\7\1\uffff\1\1\2\uffff\1\1",
            "\1\10",
            "",
            "",
            "",
            "\1\11\17\uffff\1\13\1\uffff\1\14\2\uffff\1\15\5\uffff\1\16\1\uffff\1\12",
            "\1\5\2\uffff\1\1\1\uffff\1\1\11\uffff\1\5\1\uffff\1\17\1\uffff\1\20",
            "\1\5\2\uffff\1\1\1\uffff\1\1\11\uffff\1\5\1\uffff\1\17\1\uffff\1\20",
            "\1\5\2\uffff\1\1\1\uffff\1\1\11\uffff\1\5\1\uffff\1\17\1\uffff\1\20",
            "\1\5\2\uffff\1\1\1\uffff\1\1\11\uffff\1\5\1\uffff\1\17\1\uffff\1\20",
            "\1\5\2\uffff\1\1\1\uffff\1\1\11\uffff\1\5\1\uffff\1\17\1\uffff\1\20",
            "\1\5\2\uffff\1\1\1\uffff\1\1\11\uffff\1\5\1\uffff\1\17\1\uffff\1\20",
            "\1\21\17\uffff\1\23\1\uffff\1\24\2\uffff\1\25\5\uffff\1\26\1\uffff\1\22\4\uffff\1\17",
            "\1\1\2\uffff\1\1\1\uffff\1\27\1\5\4\uffff\1\1\4\uffff\1\1\1\uffff\1\1\2\uffff\1\1\5\uffff\1\1\1\uffff\1\1",
            "\1\5\2\uffff\1\1\1\uffff\1\1\11\uffff\1\5\1\uffff\1\17\1\uffff\1\20",
            "\1\5\2\uffff\1\1\1\uffff\1\1\11\uffff\1\5\1\uffff\1\17\1\uffff\1\20",
            "\1\5\2\uffff\1\1\1\uffff\1\1\11\uffff\1\5\1\uffff\1\17\1\uffff\1\20",
            "\1\5\2\uffff\1\1\1\uffff\1\1\11\uffff\1\5\1\uffff\1\17\1\uffff\1\20",
            "\1\5\2\uffff\1\1\1\uffff\1\1\11\uffff\1\5\1\uffff\1\17\1\uffff\1\20",
            "\1\5\2\uffff\1\1\1\uffff\1\1\11\uffff\1\5\1\uffff\1\17\1\uffff\1\20",
            "\1\30",
            "\1\31\17\uffff\1\33\1\uffff\1\34\2\uffff\1\35\5\uffff\1\36\1\uffff\1\32",
            "\1\5\2\uffff\1\1\1\uffff\1\1\11\uffff\1\5\1\uffff\1\37\1\uffff\1\20",
            "\1\5\2\uffff\1\1\1\uffff\1\1\11\uffff\1\5\1\uffff\1\37\1\uffff\1\20",
            "\1\5\2\uffff\1\1\1\uffff\1\1\11\uffff\1\5\1\uffff\1\37\1\uffff\1\20",
            "\1\5\2\uffff\1\1\1\uffff\1\1\11\uffff\1\5\1\uffff\1\37\1\uffff\1\20",
            "\1\5\2\uffff\1\1\1\uffff\1\1\11\uffff\1\5\1\uffff\1\37\1\uffff\1\20",
            "\1\5\2\uffff\1\1\1\uffff\1\1\11\uffff\1\5\1\uffff\1\37\1\uffff\1\20",
            "\1\40\17\uffff\1\42\1\uffff\1\43\2\uffff\1\44\5\uffff\1\45\1\uffff\1\41\4\uffff\1\37",
            "\1\5\2\uffff\1\1\1\uffff\1\1\11\uffff\1\5\1\uffff\1\37\1\uffff\1\20",
            "\1\5\2\uffff\1\1\1\uffff\1\1\11\uffff\1\5\1\uffff\1\37\1\uffff\1\20",
            "\1\5\2\uffff\1\1\1\uffff\1\1\11\uffff\1\5\1\uffff\1\37\1\uffff\1\20",
            "\1\5\2\uffff\1\1\1\uffff\1\1\11\uffff\1\5\1\uffff\1\37\1\uffff\1\20",
            "\1\5\2\uffff\1\1\1\uffff\1\1\11\uffff\1\5\1\uffff\1\37\1\uffff\1\20",
            "\1\5\2\uffff\1\1\1\uffff\1\1\11\uffff\1\5\1\uffff\1\37\1\uffff\1\20"
    };

    static final short[] dfa_1 = DFA.unpackEncodedString(dfa_1s);
    static final char[] dfa_2 = DFA.unpackEncodedStringToUnsignedChars(dfa_2s);
    static final char[] dfa_3 = DFA.unpackEncodedStringToUnsignedChars(dfa_3s);
    static final short[] dfa_4 = DFA.unpackEncodedString(dfa_4s);
    static final short[] dfa_5 = DFA.unpackEncodedString(dfa_5s);
    static final short[][] dfa_6 = unpackEncodedStringArray(dfa_6s);

    class DFA7 extends DFA {

        public DFA7(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 7;
            this.eot = dfa_1;
            this.eof = dfa_1;
            this.min = dfa_2;
            this.max = dfa_3;
            this.accept = dfa_4;
            this.special = dfa_5;
            this.transition = dfa_6;
        }
        public String getDescription() {
            return "687:2: (this_MapForwardStatement_0= ruleMapForwardStatement | this_MapStatement_1= ruleMapStatement | this_AssignmentStatement_2= ruleAssignmentStatement | this_CallStatement_3= ruleCallStatement )";
        }
    }
    static final String dfa_7s = "\36\uffff";
    static final String dfa_8s = "\1\4\6\40\1\uffff\2\4\1\uffff\14\40\1\4\6\40";
    static final String dfa_9s = "\1\41\6\46\1\uffff\1\46\1\41\1\uffff\23\46";
    static final String dfa_10s = "\7\uffff\1\1\2\uffff\1\2\23\uffff";
    static final String dfa_11s = "\36\uffff}>";
    static final String[] dfa_12s = {
            "\1\1\2\uffff\1\7\1\uffff\1\7\5\uffff\1\7\4\uffff\1\3\1\uffff\1\4\2\uffff\1\5\5\uffff\1\6\1\uffff\1\2",
            "\1\12\2\uffff\1\11\2\uffff\1\10",
            "\1\12\2\uffff\1\11\2\uffff\1\10",
            "\1\12\2\uffff\1\11\2\uffff\1\10",
            "\1\12\2\uffff\1\11\2\uffff\1\10",
            "\1\12\2\uffff\1\11\2\uffff\1\10",
            "\1\12\2\uffff\1\11\2\uffff\1\10",
            "",
            "\1\13\17\uffff\1\15\1\uffff\1\16\2\uffff\1\17\5\uffff\1\20\1\uffff\1\14\4\uffff\1\10",
            "\1\21\2\uffff\1\7\7\uffff\1\7\4\uffff\1\23\1\uffff\1\24\2\uffff\1\25\5\uffff\1\26\1\uffff\1\22",
            "",
            "\1\12\2\uffff\1\11\2\uffff\1\10",
            "\1\12\2\uffff\1\11\2\uffff\1\10",
            "\1\12\2\uffff\1\11\2\uffff\1\10",
            "\1\12\2\uffff\1\11\2\uffff\1\10",
            "\1\12\2\uffff\1\11\2\uffff\1\10",
            "\1\12\2\uffff\1\11\2\uffff\1\10",
            "\1\12\2\uffff\1\11\2\uffff\1\27",
            "\1\12\2\uffff\1\11\2\uffff\1\27",
            "\1\12\2\uffff\1\11\2\uffff\1\27",
            "\1\12\2\uffff\1\11\2\uffff\1\27",
            "\1\12\2\uffff\1\11\2\uffff\1\27",
            "\1\12\2\uffff\1\11\2\uffff\1\27",
            "\1\30\17\uffff\1\32\1\uffff\1\33\2\uffff\1\34\5\uffff\1\35\1\uffff\1\31\4\uffff\1\27",
            "\1\12\2\uffff\1\11\2\uffff\1\27",
            "\1\12\2\uffff\1\11\2\uffff\1\27",
            "\1\12\2\uffff\1\11\2\uffff\1\27",
            "\1\12\2\uffff\1\11\2\uffff\1\27",
            "\1\12\2\uffff\1\11\2\uffff\1\27",
            "\1\12\2\uffff\1\11\2\uffff\1\27"
    };

    static final short[] dfa_7 = DFA.unpackEncodedString(dfa_7s);
    static final char[] dfa_8 = DFA.unpackEncodedStringToUnsignedChars(dfa_8s);
    static final char[] dfa_9 = DFA.unpackEncodedStringToUnsignedChars(dfa_9s);
    static final short[] dfa_10 = DFA.unpackEncodedString(dfa_10s);
    static final short[] dfa_11 = DFA.unpackEncodedString(dfa_11s);
    static final short[][] dfa_12 = unpackEncodedStringArray(dfa_12s);

    class DFA8 extends DFA {

        public DFA8(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 8;
            this.eot = dfa_7;
            this.eof = dfa_7;
            this.min = dfa_8;
            this.max = dfa_9;
            this.accept = dfa_10;
            this.special = dfa_11;
            this.transition = dfa_12;
        }
        public String getDescription() {
            return "758:3: ( ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->' )?";
        }
    }
    static final String dfa_13s = "\1\4\6\43\2\uffff\2\4\14\43\1\4\6\43";
    static final String dfa_14s = "\1\41\6\46\2\uffff\1\46\1\41\23\46";
    static final String dfa_15s = "\7\uffff\1\1\1\2\25\uffff";
    static final String[] dfa_16s = {
            "\1\1\2\uffff\1\7\1\uffff\1\7\1\uffff\4\10\1\7\4\uffff\1\3\1\uffff\1\4\2\uffff\1\5\5\uffff\1\6\1\uffff\1\2",
            "\1\12\2\uffff\1\11",
            "\1\12\2\uffff\1\11",
            "\1\12\2\uffff\1\11",
            "\1\12\2\uffff\1\11",
            "\1\12\2\uffff\1\11",
            "\1\12\2\uffff\1\11",
            "",
            "",
            "\1\13\17\uffff\1\15\1\uffff\1\16\2\uffff\1\17\5\uffff\1\20\1\uffff\1\14\4\uffff\1\11",
            "\1\21\2\uffff\1\7\3\uffff\4\10\1\7\4\uffff\1\23\1\uffff\1\24\2\uffff\1\25\5\uffff\1\26\1\uffff\1\22",
            "\1\12\2\uffff\1\11",
            "\1\12\2\uffff\1\11",
            "\1\12\2\uffff\1\11",
            "\1\12\2\uffff\1\11",
            "\1\12\2\uffff\1\11",
            "\1\12\2\uffff\1\11",
            "\1\12\2\uffff\1\27",
            "\1\12\2\uffff\1\27",
            "\1\12\2\uffff\1\27",
            "\1\12\2\uffff\1\27",
            "\1\12\2\uffff\1\27",
            "\1\12\2\uffff\1\27",
            "\1\30\17\uffff\1\32\1\uffff\1\33\2\uffff\1\34\5\uffff\1\35\1\uffff\1\31\4\uffff\1\27",
            "\1\12\2\uffff\1\27",
            "\1\12\2\uffff\1\27",
            "\1\12\2\uffff\1\27",
            "\1\12\2\uffff\1\27",
            "\1\12\2\uffff\1\27",
            "\1\12\2\uffff\1\27"
    };
    static final char[] dfa_13 = DFA.unpackEncodedStringToUnsignedChars(dfa_13s);
    static final char[] dfa_14 = DFA.unpackEncodedStringToUnsignedChars(dfa_14s);
    static final short[] dfa_15 = DFA.unpackEncodedString(dfa_15s);
    static final short[][] dfa_16 = unpackEncodedStringArray(dfa_16s);

    class DFA9 extends DFA {

        public DFA9(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 9;
            this.eot = dfa_7;
            this.eof = dfa_7;
            this.min = dfa_13;
            this.max = dfa_14;
            this.accept = dfa_15;
            this.special = dfa_11;
            this.transition = dfa_16;
        }
        public String getDescription() {
            return "887:3: ( ( (lv_inputSection_1_0= ruleInputSection ) ) otherlv_2= '->' )?";
        }
    }
    static final String dfa_17s = "\12\uffff";
    static final String dfa_18s = "\1\4\1\uffff\6\0\2\uffff";
    static final String dfa_19s = "\1\65\1\uffff\6\0\2\uffff";
    static final String dfa_20s = "\1\uffff\1\1\6\uffff\1\2\1\3";
    static final String dfa_21s = "\2\uffff\1\5\1\3\1\0\1\4\1\1\1\2\2\uffff}>";
    static final String[] dfa_22s = {
            "\1\2\17\uffff\1\4\1\uffff\1\5\2\uffff\1\6\5\uffff\1\7\1\uffff\1\3\23\uffff\1\1",
            "",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "\1\uffff",
            "",
            ""
    };

    static final short[] dfa_17 = DFA.unpackEncodedString(dfa_17s);
    static final char[] dfa_18 = DFA.unpackEncodedStringToUnsignedChars(dfa_18s);
    static final char[] dfa_19 = DFA.unpackEncodedStringToUnsignedChars(dfa_19s);
    static final short[] dfa_20 = DFA.unpackEncodedString(dfa_20s);
    static final short[] dfa_21 = DFA.unpackEncodedString(dfa_21s);
    static final short[][] dfa_22 = unpackEncodedStringArray(dfa_22s);

    class DFA62 extends DFA {

        public DFA62(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 62;
            this.eot = dfa_17;
            this.eof = dfa_17;
            this.min = dfa_18;
            this.max = dfa_19;
            this.accept = dfa_20;
            this.special = dfa_21;
            this.transition = dfa_22;
        }
        public String getDescription() {
            return "4114:3: ( ( ( (lv_static_1_0= 'static' ) ) ( (lv_extension_2_0= 'extension' ) )? ( ( ruleQualifiedNameInStaticImport ) ) ( ( (lv_wildcard_4_0= '*' ) ) | ( (lv_memberName_5_0= ruleValidID ) ) ) ) | ( ( ruleQualifiedName ) ) | ( (lv_importedNamespace_7_0= ruleQualifiedNameWithWildcard ) ) )";
        }
        public int specialStateTransition(int s, IntStream _input) throws NoViableAltException {
            TokenStream input = (TokenStream)_input;
        	int _s = s;
            switch ( s ) {
                    case 0 : 
                        int LA62_4 = input.LA(1);

                         
                        int index62_4 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred77_InternalXMapping()) ) {s = 8;}

                        else if ( (true) ) {s = 9;}

                         
                        input.seek(index62_4);
                        if ( s>=0 ) return s;
                        break;
                    case 1 : 
                        int LA62_6 = input.LA(1);

                         
                        int index62_6 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred77_InternalXMapping()) ) {s = 8;}

                        else if ( (true) ) {s = 9;}

                         
                        input.seek(index62_6);
                        if ( s>=0 ) return s;
                        break;
                    case 2 : 
                        int LA62_7 = input.LA(1);

                         
                        int index62_7 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred77_InternalXMapping()) ) {s = 8;}

                        else if ( (true) ) {s = 9;}

                         
                        input.seek(index62_7);
                        if ( s>=0 ) return s;
                        break;
                    case 3 : 
                        int LA62_3 = input.LA(1);

                         
                        int index62_3 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred77_InternalXMapping()) ) {s = 8;}

                        else if ( (true) ) {s = 9;}

                         
                        input.seek(index62_3);
                        if ( s>=0 ) return s;
                        break;
                    case 4 : 
                        int LA62_5 = input.LA(1);

                         
                        int index62_5 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred77_InternalXMapping()) ) {s = 8;}

                        else if ( (true) ) {s = 9;}

                         
                        input.seek(index62_5);
                        if ( s>=0 ) return s;
                        break;
                    case 5 : 
                        int LA62_2 = input.LA(1);

                         
                        int index62_2 = input.index();
                        input.rewind();
                        s = -1;
                        if ( (synpred77_InternalXMapping()) ) {s = 8;}

                        else if ( (true) ) {s = 9;}

                         
                        input.seek(index62_2);
                        if ( s>=0 ) return s;
                        break;
            }
            if (state.backtracking>0) {state.failed=true; return -1;}
            NoViableAltException nvae =
                new NoViableAltException(getDescription(), 62, _s, input);
            error(nvae);
            throw nvae;
        }
    }
 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000282500010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000C00000L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000000080L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000020000000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000002000002L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000000000180L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000000102L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000001000000L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x000000000000F800L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000004000000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000008000000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000008292508690L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000010000000L});
    public static final BitSet FOLLOW_19 = new BitSet(new long[]{0x00000082C2508690L});
    public static final BitSet FOLLOW_20 = new BitSet(new long[]{0x0000000040000000L});
    public static final BitSet FOLLOW_21 = new BitSet(new long[]{0x0000008282508692L});
    public static final BitSet FOLLOW_22 = new BitSet(new long[]{0x0000000282508290L});
    public static final BitSet FOLLOW_23 = new BitSet(new long[]{0x0000000100000000L});
    public static final BitSet FOLLOW_24 = new BitSet(new long[]{0x0000008282508790L});
    public static final BitSet FOLLOW_25 = new BitSet(new long[]{0x000000028250FA90L});
    public static final BitSet FOLLOW_26 = new BitSet(new long[]{0x0000000400000000L});
    public static final BitSet FOLLOW_27 = new BitSet(new long[]{0x0000000800000000L});
    public static final BitSet FOLLOW_28 = new BitSet(new long[]{0x0000001000200000L});
    public static final BitSet FOLLOW_29 = new BitSet(new long[]{0x0000006000000060L});
    public static final BitSet FOLLOW_30 = new BitSet(new long[]{0x0000000000000040L});
    public static final BitSet FOLLOW_31 = new BitSet(new long[]{0x0000008282508690L});
    public static final BitSet FOLLOW_32 = new BitSet(new long[]{0x0000010000000002L});
    public static final BitSet FOLLOW_33 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_34 = new BitSet(new long[]{0x0000000001000002L});
    public static final BitSet FOLLOW_35 = new BitSet(new long[]{0x0000000282508090L});
    public static final BitSet FOLLOW_36 = new BitSet(new long[]{0x0000000800000002L});
    public static final BitSet FOLLOW_37 = new BitSet(new long[]{0x0000020000000002L});
    public static final BitSet FOLLOW_38 = new BitSet(new long[]{0x0000004800000002L});
    public static final BitSet FOLLOW_39 = new BitSet(new long[]{0x0000004800000020L});
    public static final BitSet FOLLOW_40 = new BitSet(new long[]{0x0000004000000002L});
    public static final BitSet FOLLOW_41 = new BitSet(new long[]{0x0000004282500010L});
    public static final BitSet FOLLOW_42 = new BitSet(new long[]{0x0000040000000002L});
    public static final BitSet FOLLOW_43 = new BitSet(new long[]{0x0000080000000000L});
    public static final BitSet FOLLOW_44 = new BitSet(new long[]{0x0000700282500010L});
    public static final BitSet FOLLOW_45 = new BitSet(new long[]{0x0000210000000000L});
    public static final BitSet FOLLOW_46 = new BitSet(new long[]{0x0000500282500010L});
    public static final BitSet FOLLOW_47 = new BitSet(new long[]{0x0000400000000000L});
    public static final BitSet FOLLOW_48 = new BitSet(new long[]{0x0000800000000002L});
    public static final BitSet FOLLOW_49 = new BitSet(new long[]{0x0002500282500010L});
    public static final BitSet FOLLOW_50 = new BitSet(new long[]{0x0001010000000000L});
    public static final BitSet FOLLOW_51 = new BitSet(new long[]{0x0000800800000002L});
    public static final BitSet FOLLOW_52 = new BitSet(new long[]{0x0014000000000002L});
    public static final BitSet FOLLOW_53 = new BitSet(new long[]{0x0008000000000002L});
    public static final BitSet FOLLOW_54 = new BitSet(new long[]{0x0020000282500010L});
    public static final BitSet FOLLOW_55 = new BitSet(new long[]{0x0040000282500010L});
    public static final BitSet FOLLOW_56 = new BitSet(new long[]{0x0080000282500010L});
    public static final BitSet FOLLOW_57 = new BitSet(new long[]{0x0000000000200002L});
    public static final BitSet FOLLOW_58 = new BitSet(new long[]{0x0000000282500012L});

}