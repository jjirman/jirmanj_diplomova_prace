/*
 * generated by Xtext 2.24.0
 */
package com.gk_software.core.dsl.xmapping;

import org.eclipse.xtext.naming.IQualifiedNameConverter;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.resource.IDefaultResourceDescriptionStrategy;
import org.eclipse.xtext.scoping.IScopeProvider;
import org.eclipse.xtext.scoping.impl.AbstractDeclarativeScopeProvider;

import com.gk_software.core.dsl.xmapping.generator.IXMappingGenerator;
import com.gk_software.core.dsl.xmapping.generator.XMappingGenerator;
import com.gk_software.core.dsl.xmapping.interpret.AbstractXMappingInterpret;
import com.gk_software.core.dsl.xmapping.interpret.IXMappingInterpretQueue;
import com.gk_software.core.dsl.xmapping.interpret.XMappingInterpret;
import com.gk_software.core.dsl.xmapping.interpret.XMappingInterpretQueue;
import com.gk_software.core.dsl.xmapping.naming.XMappingQualifiedNameConverter;
import com.gk_software.core.dsl.xmapping.naming.XMappingQualifiedNameProvider;
import com.gk_software.core.dsl.xmapping.scoping.XMappingImportedNamespaceAwareLocalScopeProvider;
import com.gk_software.core.dsl.xmapping.scoping.XMappingResourceDescriptionStrategy;
import com.google.inject.Binder;
import com.google.inject.name.Names;

/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
public class XMappingRuntimeModule extends AbstractXMappingRuntimeModule {
	
	@Override
	public void configureIScopeProviderDelegate(Binder binder) {
		binder.bind(IScopeProvider.class).annotatedWith(Names.named(AbstractDeclarativeScopeProvider.NAMED_DELEGATE)).to(XMappingImportedNamespaceAwareLocalScopeProvider.class);
	}
	
	@Override
	public Class<? extends IQualifiedNameProvider> bindIQualifiedNameProvider() {
		return XMappingQualifiedNameProvider.class;
	}
	
	public Class<? extends IDefaultResourceDescriptionStrategy> bindIDefaultResourceDescriptionStrategy() {
		return XMappingResourceDescriptionStrategy.class;
	}
	
	public Class<? extends IQualifiedNameConverter> bindIQualifiedNameConverter() {
		return XMappingQualifiedNameConverter.class;
	}
	
	public Class<? extends IXMappingGenerator> bindIXMappingGenerator () {
	    return XMappingGenerator.class;
	}
	
	public Class<? extends AbstractXMappingInterpret> bindIXMappingInterpret () {
	    return (Class<? extends AbstractXMappingInterpret>) XMappingInterpret.class;
	} 
	
	public Class<? extends IXMappingInterpretQueue> bindIXMappingInterpretQueue () {
	    return (Class<? extends IXMappingInterpretQueue>) XMappingInterpretQueue.class;
	} 
	
}
