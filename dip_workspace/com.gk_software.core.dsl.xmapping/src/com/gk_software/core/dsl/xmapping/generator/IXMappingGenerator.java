package com.gk_software.core.dsl.xmapping.generator;

import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.xtext.generator.IFileSystemAccess2;
import org.eclipse.xtext.generator.IGenerator2;
import org.eclipse.xtext.generator.IGeneratorContext;

import com.gk_software.core.dsl.xmapping.helpers.classes.IMessageListener;

/**
 * Interface for XMapping generator
 * @author Bc. Jan Jirman
 *
 */
public interface IXMappingGenerator extends IGenerator2{
    /**
     * Main generating method
     * @param resources all available resources
     * @param fsa file system access
     * @param context context
     */
	public void doGenerate(ResourceSet resources, IFileSystemAccess2 fsa, IGeneratorContext context);
	
	/**
	 * Sets messaging listener
	 * @param listener messaging listener
	 */
	public void setListener(IMessageListener listener);
}
