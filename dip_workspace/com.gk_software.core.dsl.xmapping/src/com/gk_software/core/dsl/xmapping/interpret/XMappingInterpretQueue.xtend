package com.gk_software.core.dsl.xmapping.interpret

import org.eclipse.emf.ecore.EObject
import java.util.Queue
import java.util.LinkedList
import javax.inject.Inject
import org.eclipse.xtext.EcoreUtil2
import com.gk_software.core.dsl.xmapping.typing.XMappingTypeProvider
import com.gk_software.core.dsl.xmapping.helpers.util.XMappingInterpretObjectUtil
import com.gk_software.core.dsl.xmapping.helpers.util.XMappingInterpretUtil
import com.gk_software.core.dsl.xmapping.helpers.util.XMappingEObjectUtil
import com.gk_software.core.dsl.xmapping.helpers.classes.InterpretRequest
import com.gk_software.core.dsl.xmapping.typing.Type
import com.gk_software.core.dsl.xmapping.xMapping.Model
import com.gk_software.core.dsl.xmapping.xMapping.Output
import com.gk_software.core.dsl.xmapping.xMapping.Input
import com.gk_software.core.dsl.xmapping.xMapping.Assignment
import com.gk_software.core.dsl.xmapping.helpers.classes.Sources
import com.gk_software.core.dsl.xmapping.helpers.classes.RequestType

class XMappingInterpretQueue implements IXMappingInterpretQueue{
	/** type provider */
	@Inject XMappingTypeProvider typeProvider 
	/** interpret object */
	@Inject extension XMappingInterpretObjectUtil
	/** interpret util */
	@Inject extension XMappingInterpretUtil
	/** object util */
	@Inject XMappingEObjectUtil objectUtil
	
	/** constant for routine call */
	final static String GENERATOR_COMMAND = "//routine"
	/** queue for requests (to make command) */
	Queue<InterpretRequest> requests = new LinkedList<InterpretRequest>();
	
	override getStackSize() {
		this.requests.size
	}
		
	override pushRoutineGenerator() {
		val space = Sources.GENERAL_SPACE.getValue;
 		var request = new InterpretRequest(space + GENERATOR_COMMAND, RequestType.REQUEST_MACRO)
 		request.pushRequest
	}
 	
 	override pushNewImport(String importValue){
 		val importCommand = Sources.CODE_IMPORT.getValue
 		val code = importCommand + importValue
 		
 		var request = new InterpretRequest(code,RequestType.REQUEST_COMMAND)
 		request.pushRequest	
 	}
 	
 	override pushObjectNonPrimitiveType(EObject object, String identifier, Type type){
 		var request = null as InterpretRequest 
 		typeProvider.resetRoutineScope
 		var objectType = type		
 		if(identifier !== null){
 			objectType =typeProvider.methodType(identifier);
 		}else if(object !== null){
 			objectType = typeProvider.typeForObject(object);	
 		}
 		val code = typeProvider.getNonPrimitiveType(objectType);
 		checkImports(code);
 		request = new InterpretRequest(code,RequestType.REQUEST_COMMAND)
 		request.pushRequest	
 	}
 	 	
 	override String pushObjectType(EObject object){		
 		val code = getObjectType(object)
 		if(code !== null){
 			checkImports(code); //add import of object type if is needed
	 		var request = new InterpretRequest(code,RequestType.REQUEST_COMMAND)
	 		request.pushRequest	
 		}
 		//returns code because it can be used in interpret method
 		return code
 	}
 	
 	private def void checkImports(String code){
 		val document = Sources.CODE_DOCUMENT.getValue
 		val file = Sources.CODE_FILE_INPUT.getValue
 		var returnValue = Sources.CODE_RETURN_VALUE.getValue
 		var function = Sources.CODE_FUNCTION.getValue
 		if(code.contains(document)){			
 			pushNewImport(Sources.IMPORT_DOCUMENT_OUTPUT.getValue)
 		}else if(code.contains(returnValue)){
 			pushNewImport(Sources.IMPORT_RETURN_VALUE.getValue)
 		}else if(code.contains(file)){
 			pushNewImport(Sources.IMPORT_FILE_INPUT.getValue)
 		}else if(code.contains(function)){
 			pushNewImport(Sources.IMPORT_FUNCTION.getValue)
 		}
 	}
 	
 	override pushObjectName(EObject object){
 		var request = null as InterpretRequest 
 		var name = getJavaName(objectUtil.getObjectName(object))
 		val basicObject = objectUtil.getBasicEObject(object);
 		if(basicObject instanceof Input || basicObject instanceof Output){
 			var model = EcoreUtil2.getContainerOfType(object,typeof(Model))
			var mappingClass = model.class_.name
			var packageName = model.name
			val path = packageName + Sources.GENERAL_DOT.getValue + mappingClass+ Sources.GENERAL_DOT.getValue	
			name = path + name	
 		}
 		request = new InterpretRequest(name,RequestType.REQUEST_COMMAND)
 		request.pushRequest	
 	}
 	
 	override pushAssignmentValue(Assignment assignment){
 		val code = getAssignmentValue(assignment)
 		var request = new InterpretRequest(code,RequestType.REQUEST_COMMAND)
 		request.pushRequest
 	}
 	
 	override pushEndCommand(){
 		val character = Sources.GENERAL_DELIMITER.getValue
 		var request = new InterpretRequest(character,RequestType.REQUEST_COMMAND)
 		request.pushRequest
 	}
 	
 	override pushCustomCode(String code){
 		var request = new InterpretRequest(code,RequestType.REQUEST_COMMAND)
 		request.pushRequest
 	}
 	
 	override pushNewLine(){
 		val code = '''
 		
 		'''
 		var request = new InterpretRequest(code,RequestType.REQUEST_COMMAND)
 		request.pushRequest
 	}
 	
 	override pushErrorCode(String text){
 		var request = new InterpretRequest(text,RequestType.REQUEST_COMMAND)
 		request.pushRequest
 	}
 	
 	override boolean pushXpath(EObject object){
 		var code = getObjectXPathCode(object)
 		if(code === null){
 			return false
 		}
 		var request = new InterpretRequest(code,RequestType.REQUEST_COMMAND)
 		request.pushRequest
 		return true
 	}
 	
 	override boolean pushRequest(InterpretRequest request){
 		if(request !== null){
 			requests.add(request)	
 			return true	
 		}	
 		return false
 	}
 	
 	override String popRequest(){
 		val request = requests.poll()
 		return request.code
 	}
 	 
}