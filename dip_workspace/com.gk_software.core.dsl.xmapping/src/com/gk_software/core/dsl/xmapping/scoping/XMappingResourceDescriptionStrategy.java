package com.gk_software.core.dsl.xmapping.scoping;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.naming.IQualifiedNameProvider;
import org.eclipse.xtext.naming.QualifiedName;
import org.eclipse.xtext.resource.EObjectDescription;
import org.eclipse.xtext.resource.IEObjectDescription;
import org.eclipse.xtext.resource.impl.DefaultResourceDescriptionStrategy;
import org.eclipse.xtext.util.IAcceptor;

import com.gk_software.core.dsl.xmapping.naming.XMappingQualifiedNameProvider;
import com.google.inject.Inject;

/**
 *  Class which works with indexes
 *  @author Bc. Jan Jirman
 */
public class XMappingResourceDescriptionStrategy extends DefaultResourceDescriptionStrategy {
	
	/** qualifiend name provider */
	@Inject XMappingQualifiedNameProvider qualifiedNameProvider;
	
	@Override
	public IQualifiedNameProvider getQualifiedNameProvider() {
		return qualifiedNameProvider;
	}

	@Override
	public boolean createEObjectDescriptions(EObject eObject, IAcceptor<IEObjectDescription> acceptor) {
		if (getQualifiedNameProvider() == null)
			return false;
		try {
			//providing qualified name
			QualifiedName qualifiedName = getQualifiedNameProvider().getFullyQualifiedName(eObject);
			//if ok, adding into index - accepted
			if (qualifiedName != null) {
				acceptor.accept(EObjectDescription.create(qualifiedName, eObject));
			}
		} catch (Exception exc) {
			exc.getStackTrace();
		}
		return true;
	}
	
	
}
