package com.gk_software.core.dsl.xmapping.helpers.classes;

/**
 * Enum which is associated with generator and interpret - contains frequently used strings.
 * @author Bc. Jan Jirman
 *
 */
public enum Sources {
	
	// ------------------ General ---------------------------
	
	GENERAL_DELIMITER(";"),
	GENERAL_LESS_THAN("<"),
	GENERAL_GREATER_THAN(">"),
	GENERAL_SPACE(" "),
	GENERAL_EQUALS(" = "),
	GENERAL_EMPTY(""),
	GENERAL_LEFT_BRACKET("("),
	GENERAL_RIGHT_BRACKET(")"),
	GENERAL_ARRAYLIST("ArrayList"),
	GENERAL_DOT("."),
	GENERAL_COMMA(", "),
	GENERAL_GENERATING_FILE(".java"),
	GENERAL_MAPPING_FILE(".xmapping"),
	
	// ------------------ Names ---------------------------
	
	NAME_RETURN("return_"),
	
	// ------------------ Code ---------------------------
	
	CODE_IMPORT("import "),
	CODE_RETURN_VALUE("ReturnValue"),
	CODE_DOCUMENT_OUTPUT("DocumentOutput"),
	CODE_FILE_INPUT("FileInput"),
	CODE_FUNCTION("Function"),
	CODE_ROUTINE_COLLECTION_NAME("returns"),
	CODE_MAIN_COLLECTION_NAME("routineReturns"),
	CODE_QUERY_OUTPUT_OUTPUT("XPathResolver.queryOutputOutput("),
	CODE_QUERY_INPUT_OUTPUT("XPathResolver.queryInputOutput("),
	CODE_QUERY_VALUE_OUTPUT("XPathResolver.queryValueOutput("),
	CODE_QUERY_INPUT("XPathResolver.queryInput("),
	CODE_QUERY_FUNCTION_OUTPUT("XPathResolver.queryFunctionOutput("),
	CODE_NULL("null"),
	CODE_DOCUMENT("Document"),
	
	// ------------------ Import ---------------------------
	
	IMPORT_RETURN_VALUE("xpath.ReturnValue"),
	IMPORT_XPATH_RESOLVER("xpath.XPathResolver"),
	IMPORT_ARRAYLIST("java.util.ArrayList"),
	IMPORT_LIST("java.util.List"),
	IMPORT_DOCUMENT_OUTPUT("io.DocumentOutput"),
	IMPORT_FILE_INPUT("io.FileInput"),
	IMPORT_FUNCTION("mapping.Function")
	;
	
	/** value of enum */
    private final String value;

    /**
     * Gets value
     * @return value
     */
    public String getValue() {
		return value;
	}
    
    /**
     * Constructor which creates enum
     * @param value value
     */
	Sources(String value) {
		this.value = value;
	}
}
