package com.gk_software.core.dsl.xmapping.typing;

/**
 * Singleton Structure type
 */
public final class StructureType extends Type{
	
	/** instance */
	public static final StructureType INSTANCE = new StructureType();  
	
	/** name */
	private static final String NAME = "structureType";
	
	/** code name */
	private static final String CODE_TYPE = "NodeList";
	
	/**
	 * Constructor
	 */
	private StructureType() {
		super(NAME, CODE_TYPE);
	}
	
}
