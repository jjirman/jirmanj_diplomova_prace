package com.gk_software.core.dsl.xmapping.typing;

/**
 * Singleton Function type
 */
public final class FunctionType extends Type{
	
	/** instance */
	public static final FunctionType INSTANCE = new FunctionType();  
	
	/** name */
	private static final String NAME = "functionType";
	
	/** code name */
	private static final String CODE_TYPE = "Function";
	
	/**
	 * Constructor
	 */
	private FunctionType() {
		super(NAME, CODE_TYPE);
	}	
}

