package com.gk_software.core.dsl.xmapping.helpers.util

import org.eclipse.emf.ecore.EObject
import com.google.inject.Inject
import com.gk_software.core.dsl.xmapping.typing.XMappingTypeProvider
import com.gk_software.core.dsl.xmapping.xMapping.InputAttribute
import com.gk_software.core.dsl.xmapping.xMapping.OutputAttribute
import com.gk_software.core.dsl.xmapping.xMapping.Attribute
import com.gk_software.core.dsl.xmapping.xMapping.Return
import com.gk_software.core.dsl.xmapping.typing.IntType
import com.gk_software.core.dsl.xmapping.typing.DoubleType
import com.gk_software.core.dsl.xmapping.typing.StringType
import com.gk_software.core.dsl.xmapping.typing.StructureType
import com.gk_software.core.dsl.xmapping.xMapping.Input
import com.gk_software.core.dsl.xmapping.typing.FunctionType
import org.eclipse.xtext.EcoreUtil2
import com.gk_software.core.dsl.xmapping.xMapping.AssignmentStatement

/**
 * Utility which helps interpet with objects
 * @author Bc. Jan Jirman
 *
 */
class XMappingInterpretObjectUtil {

	/** type provider */
	@Inject XMappingTypeProvider typeProvider
	/** object util */
	@Inject XMappingEObjectUtil objectUtil
	
	/**
	 * Generates java name from object name
	 * @param objectName object name
	 * @return object java name
	 */
 	def String getJavaName(String objectName){
 		val index = 0;
 		var betterName = objectName.replace(".","_")
 		return betterName.toFirstLower
 	}
 	
 	/**
 	 * Gets Xpath string from object
 	 * @param object object
 	 * @return charSequence xpath
 	 */
 	def getObjectXPathCode(EObject object){
 		switch(object){
	 		InputAttribute: {
	 			if(object.XPath !== null){
	 				return '''"�object.XPath�"'''
	 			}
			}
			OutputAttribute:{
				if(object.XPath !== null){
					return '''"�object.XPath�"'''
				}
			}
			Attribute: {
				if(object.XPath !== null){
					return '''"�object.XPath�"'''	
				}	
			}
			Return :{
				var statement = EcoreUtil2.getContainerOfType(object,typeof(AssignmentStatement))
				var init = statement.initialization
				if(init !== null){
					var structure = statement.initialization.assignment.structure
					if(structure !== null){
						var source = structure.source
						if(source.XPath !== null){
							return '''"�source.XPath�"'''	
						}
					}
				}
			}
		}
 	}
 	
 	/**
 	 * Gets object type
 	 * @param object object
 	 * @return object type
 	 */
	def String getObjectType(EObject object){
		val basicObject = objectUtil.getBasicEObject(object)
		if(basicObject instanceof Return){
			return	'''ReturnValue'''
		}else{
			val type = typeProvider.typeForObject(object)
			switch(type){
				IntType:'''int'''
				DoubleType:'''double'''
				StringType:'''String'''
				FunctionType: '''Function'''
				StructureType:{
					if(basicObject instanceof Input){
						'''FileInput'''
					}else{
						'''DocumentOutput'''
					}
				}
			}
		}
	}
}
