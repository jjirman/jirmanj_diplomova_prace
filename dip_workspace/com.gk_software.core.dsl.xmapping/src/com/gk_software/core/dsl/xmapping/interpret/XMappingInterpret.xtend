package com.gk_software.core.dsl.xmapping.interpret

import com.google.inject.Inject
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.common.util.EList
import java.util.List
import org.eclipse.xtext.common.types.JvmOperation
import org.eclipse.xtext.common.types.JvmGenericType
import org.eclipse.xtext.common.types.JvmMember
import com.gk_software.core.dsl.xmapping.helpers.util.XMappingInterpretObjectUtil
import com.gk_software.core.dsl.xmapping.helpers.util.XMappingInterpretUtil
import com.gk_software.core.dsl.xmapping.helpers.util.XMappingEObjectUtil
import com.gk_software.core.dsl.xmapping.helpers.util.XMappingStatementUtil
import com.gk_software.core.dsl.xmapping.helpers.util.XMappingGeneratorUtil
import com.gk_software.core.dsl.xmapping.typing.XMappingTypeProvider
import com.gk_software.core.dsl.xmapping.xMapping.Statement
import com.gk_software.core.dsl.xmapping.xMapping.AssignmentStatement
import com.gk_software.core.dsl.xmapping.xMapping.MapForwardStatement
import com.gk_software.core.dsl.xmapping.typing.StructureType
import com.gk_software.core.dsl.xmapping.xMapping.Output
import com.gk_software.core.dsl.xmapping.xMapping.Attribute
import com.gk_software.core.dsl.xmapping.typing.Type
import com.gk_software.core.dsl.xmapping.typing.NotType
import com.gk_software.core.dsl.xmapping.xMapping.Return
import com.gk_software.core.dsl.xmapping.xMapping.CallStatement
import com.gk_software.core.dsl.xmapping.xMapping.MapStatement
import com.gk_software.core.dsl.xmapping.xMapping.InputImport
import com.gk_software.core.dsl.xmapping.xMapping.OutputImport
import com.gk_software.core.dsl.xmapping.typing.FunctionType
import org.eclipse.xtext.common.types.JvmFormalParameter
import com.gk_software.core.dsl.xmapping.helpers.classes.Sources

class XMappingInterpret extends AbstractXMappingInterpret {
		
	/** interpret object */
	@Inject extension XMappingInterpretObjectUtil
	/** interpret util */
	@Inject extension XMappingInterpretUtil
	/** object util */
	@Inject XMappingEObjectUtil objectUtil
	/** object util */
	@Inject XMappingStatementUtil statementUtil
	/** generator util */
	@Inject XMappingGeneratorUtil generatorUtil
	/** type provider */
	@Inject XMappingTypeProvider typeProvider
	/** interpret queue */
	@Inject extension IXMappingInterpretQueue 
	
	/*                                AssignmentCommand
 	 *  ------------------------------------------------------------------------------------
 	 *  ------------------------------------------------------------------------------------  
 	 */
 	 
 	 /**
 	  * Creates type T "<T>"
 	  * @param statement statement
 	  * @param object object
 	  */
 	 def createTType(EObject object, String identifier, Type type){
 	 	pushCustomCode(Sources.GENERAL_LESS_THAN.getValue)
 		pushObjectNonPrimitiveType(object, identifier, type)
 		pushCustomCode(Sources.GENERAL_GREATER_THAN.getValue)
 	 }
 	 
 	 /**
 	  * Creates casting "(type)"
 	  */
 	  def createCastType(EObject object, String identifier, Type type){
 	  	pushCustomCode(Sources.GENERAL_LEFT_BRACKET.getValue);  	
 		pushObjectNonPrimitiveType(object,identifier,type);
 		pushCustomCode(Sources.GENERAL_RIGHT_BRACKET.getValue)	
 	  }
 	  
 	  /**
 	   * Creates new ReturnValue "new ReturnValue<T>([value])"
 	   */
 	   def createNewReturnValue(Statement statement, EObject object){
 	   		pushCustomCode("new "+Sources.CODE_RETURN_VALUE.getValue)
			object.createTType(null,null);
			pushCustomCode(Sources.GENERAL_LEFT_BRACKET.getValue)
			//assignment command 
			if(statement instanceof AssignmentStatement){
				pushAssignmentValue(statement.initialization.assignment)
			}else{
				//TODO
				//pushCustomCode(value);
			}
			pushCustomCode(Sources.GENERAL_RIGHT_BRACKET.getValue)
			pushNewImport(Sources.IMPORT_RETURN_VALUE.getValue)
 	   }
 	 
 	 /**
 	  * Creates assignment return value "[name] = new ReturnValue<T>([value])"
 	  */
 	 def createAssignmentReturnValue(Statement statement, EObject object){
 	 	object.createTType(null,null);
 	 	pushCustomCode(Sources.GENERAL_SPACE.getValue)
		pushObjectName(object)
		pushCustomCode(Sources.GENERAL_EQUALS.getValue)
		statement.createNewReturnValue(object);		
 	 }
	
	override dispatch boolean interpret(AssignmentStatement statement) {	
		//is initialization	
		if(statement.initialization !== null){
			val list = statementUtil.getAssigmnentList(statement.declaration)
			for(EObject decl : list){
				var object = objectUtil.getBasicEObject(decl)
				// 'int <space> variable_name ='
				var initializedInStatement = objectUtil.isEObjectInitializedInStatement(statement,object)
				
				var type = "notType";
				if(initializedInStatement){
					type = pushObjectType(object)
				}else{
					type = getObjectType(object)
				}
				
				//if ReturnValue <space> return_<number>
 				if(type.equals(Sources.CODE_RETURN_VALUE.getValue)){
					// 'new ReturnValue(5/"String", ..)'
					statement.createAssignmentReturnValue(object);			
 				}else{
					pushCustomCode(Sources.GENERAL_SPACE.getValue)
	 				pushObjectName(object)	
					pushCustomCode(Sources.GENERAL_EQUALS.getValue)
 					// '5' / '"String' / getPartOfDocument("XPATH")
 					pushAssignmentValue(statement.initialization.assignment)
 				}
			}
			pushCustomCode(Sources.GENERAL_DELIMITER.getValue)
 		} 
 		true	
 	}
 	
 	//SEM - OK ----- ^^ --------------- ^^ --------- ^^ -------------- ^^ -------------
 	
 	override dispatch boolean interpret(MapForwardStatement statement) {		
 		var inputAttributes = statement.inputSection.inputAttributes
 		var outputAttributes = statement.outputSection.outputAttributes
		for(var i = 0; i < inputAttributes.size; i++){
			//input
			var inputObject = inputAttributes.get(i)
 			//output
 			var outputObject = outputAttributes.get(i)
 			val outputType = typeProvider.typeForObject(outputObject)

			//structure methods
 			if(outputType instanceof StructureType){
 				return createMapForwardStructure(inputObject,outputObject,statement)
 			}else{
 				createMapForwardType(inputObject,outputObject,statement)
 			}
		}
		return true
 	} 
 	
 	/**
 	 * Interprets other types than "structure type" (for Map forward statement)
 	 * @param inputObject input object
 	 * @param outputObject output object
 	 * @param statement statement
 	 */
 	def boolean createMapForwardType(EObject inputObject, EObject outputObject, MapForwardStatement statement){
 		val index = -1
 		return otherTypeCommand(inputObject,outputObject,statement,index)
 	}
 	
 	/**
 	 * Interprets other types than "structure type" (others statements)
 	 * @param index index
 	 * @param outputObject output object
 	 * @param statement statement
 	 */
 	def boolean createRoutineType(int index, EObject outputObject, Statement statement){
 		val object = null as EObject
 		return otherTypeCommand(object,outputObject,statement,index)
 	}
 	
 	/**
 	 * Basic method which interprets other types than Structure type
 	 * @param inputObject input object
 	 * @param index index
 	 * @param outputObject output object
 	 * @param statement statement
 	 * 
 	 */
 	def boolean otherTypeCommand(EObject inputObject, EObject outputObject, Statement statement, int index){
 		//checks initialization in this statement
 		var type = null as String
 		var object = objectUtil.getBasicEObject(outputObject)
 		
 		val isInitialized = isStructureInitialized(outputObject,statement,object);
 		var basicObject = objectUtil.getBasicEObject(outputObject)
 		//is initialized
 		if(!isInitialized){
 			type = pushObjectType(objectUtil.getBasicEObject(outputObject))
		 	pushCustomCode(Sources.GENERAL_SPACE.getValue)			
 		}
 		
 		var typeCheck = getObjectType(basicObject)
 		
		pushObjectName(outputObject)
		val line = '''routineReturns.get(�index�).getValue()'''
		if(type !== null && type.equals(Sources.CODE_RETURN_VALUE.getValue)){
			// = new ReturnValue<TYPE>([object_name]|[routineReturns.get(�index�).getValue()])
			pushCustomCode(Sources.GENERAL_EQUALS.getValue +"new "+Sources.CODE_RETURN_VALUE.getValue)
			pushNewImport(Sources.IMPORT_RETURN_VALUE.getValue)
			var obj = null as EObject
			// <TYPE>
			if(inputObject === null){
				obj = getReturnObjectByIndex(statement,index)
				obj.createTType(null,null)
			}else{
				inputObject.createTType(null,null)
			}
			// (.....)
			pushCustomCode(Sources.GENERAL_LEFT_BRACKET.getValue)
			if(inputObject !== null){
 				pushObjectName(inputObject)
 			}else{
 				obj.createCastType(null,null)
 				pushCustomCode(line)
 			}
			pushCustomCode(Sources.GENERAL_RIGHT_BRACKET.getValue)
		}else if(typeCheck !== null && typeCheck.equals(Sources.CODE_RETURN_VALUE.getValue)){
			val setValueLineStart = '''.setValue(''';
			pushCustomCode(setValueLineStart)
			if(inputObject !== null){
 				pushObjectName(inputObject)
 			}else{
 				pushCustomCode(line)	
 			}
			pushCustomCode(Sources.GENERAL_RIGHT_BRACKET.getValue)
		}else{
			pushCustomCode(Sources.GENERAL_EQUALS.getValue)
			if(inputObject !== null){
 				pushObjectName(inputObject)
 			}else{
 				outputObject.createCastType(null,null)
 				pushCustomCode(line)	
 			}		
		}	
		pushCustomCode(Sources.GENERAL_DELIMITER.getValue)
 		pushNewLine()
		return true
 	}
 	
 	/**
 	 * Interprets other types than "structure type" (others statements)
 	 * @param inputObject input object
 	 * @param outputObject output object
 	 * @param statement statement
 	 */
 	def boolean createMapForwardStructure(EObject inputObject, EObject outputObject, MapForwardStatement statement){
 		val index = -1
 		pushNewImport(Sources.IMPORT_XPATH_RESOLVER.getValue);
 		val succes = structureCommand(inputObject,outputObject,statement,index,false)
 		var inputType = typeProvider.typeForObject(inputObject);
 		if(inputType instanceof FunctionType){
 			pushObjectName(inputObject);
 			pushCustomCode(Sources.GENERAL_RIGHT_BRACKET.getValue + Sources.GENERAL_DELIMITER.getValue);
 		}
 		return succes;
 	}
 	
 	/**
 	 * Interprets other types than "structure type" (for map forward statement)
 	 * @param index index
 	 * @param outputObject output object
 	 * @param statement statement
 	 */
 	def boolean createRoutineStructure(int index, EObject outputObject, Statement statement){
 		val object = null as EObject
 		pushNewImport(Sources.IMPORT_XPATH_RESOLVER.getValue);
 		return structureCommand(object,outputObject,statement,index,false)
 	}
 	
 	/**
 	 * Checks that structure is initialized
 	 */
 	def boolean isStructureInitialized(EObject outputObject, Statement statement, EObject object){
 		if(!(object instanceof Output)){
 			var initializedInStatement = objectUtil.isEObjectInitializedInStatement(statement,outputObject)
			if(initializedInStatement){
				return false;
			}
 		}
 		return true
 	}
 	
 	def boolean createInput(EObject inputObject, Type inputType, int index, boolean onlyInputQuery){
 		val line = ''' routineReturns.get(�index�).getValue()'''
 		//input is structure
 		if(inputType instanceof StructureType){	
			pushNewImport(Sources.IMPORT_XPATH_RESOLVER.getValue);		
			if(onlyInputQuery){
				pushCustomCode(Sources.CODE_QUERY_INPUT.getValue)
			}else
			if(inputObject instanceof Attribute || inputObject instanceof Return){
				//Variable.name:"Xpath" -> Output.name:"Xpath";
				pushCustomCode(Sources.CODE_QUERY_OUTPUT_OUTPUT.getValue) 
			}else{
				//Input.name:"Xpath" -> Output.name:"Xpath";
				pushCustomCode(Sources.CODE_QUERY_INPUT_OUTPUT.getValue)
			}
			if(inputObject instanceof Return){
				createCastType(null,null,inputType)
 				pushCustomCode(line)	
			}else{
				pushObjectName(inputObject)
			}
			
			pushCustomCode(Sources.GENERAL_COMMA.getValue)
			val isXpath = pushXpath(inputObject);
			if(!isXpath){
				return false;
			}
			if(onlyInputQuery){
				pushCustomCode(Sources.GENERAL_RIGHT_BRACKET.getValue);
			}
		
		}else if(inputType instanceof FunctionType){	
			pushNewImport(Sources.IMPORT_XPATH_RESOLVER.getValue);	
			pushNewImport(Sources.IMPORT_FUNCTION.getValue);	
			pushCustomCode(Sources.CODE_QUERY_FUNCTION_OUTPUT.getValue)		
		}else{ //input is not structure
 			pushCustomCode(Sources.CODE_QUERY_VALUE_OUTPUT.getValue)
 			//name or index...
 			if(inputObject !== null && !(inputType instanceof Return)){
 				pushObjectName(inputObject)
 			}else{
 				createCastType(null,null,inputType)
 			//	pushCustomCode(SourcesEnum.GENERAL_LEFT_BRACKET.value + SourcesEnum.CODE_DOCUMENT_OUTPUT.value + SourcesEnum.GENERAL_RIGHT_BRACKET.value)
 				pushCustomCode(line)				
 			}		
		}
		return true;
 	}
 	
 	
 	/**
 	 * Basic method which interprets structure type
 	 * @param inputObject input object
 	 * @param index index
 	 * @param outputObject output object
 	 * @param statement statement
 	 * 
 	 */
 	def boolean structureCommand(EObject inputObject, EObject outputObject, Statement statement, int index, boolean isFunction){
 		var object = objectUtil.getBasicEObject(outputObject)
 		val isInitialized = isStructureInitialized(outputObject,statement,object);
 		var isFunctionVariable = false;
 		var input = inputObject
 		//is structure initialized
 		if(isInitialized){
 			var inputType = typeProvider.typeForObject(inputObject)
			var basicObject = objectUtil.getBasicEObject(outputObject)
 			if(inputType instanceof NotType){
 				var returns = statementUtil.getReturnsFromRoutine(statement)
				inputType = typeProvider.typeForObject(returns.get(index));
				input = returns.get(index);
 			}else if(isFunction || inputType instanceof FunctionType){
 				inputType = FunctionType.INSTANCE
 				isFunctionVariable = true;
 			}
 			val isOK = input.createInput(inputType,index,false);
 			if(!isOK){
 				return false;
 			}
 			if(!isFunctionVariable){
 				pushCustomCode(Sources.GENERAL_COMMA.getValue)
 			}
 			
 			//to get Document / value - call ".getValue() for Return..
			if(basicObject instanceof Return){
				//TODO vyzkou�et
	 			input.createCastType(null,inputType);
				pushObjectName(outputObject)
				val getValueText = ".getValue()" 
				pushCustomCode(getValueText)
			}else{
				pushObjectName(outputObject)
			}
			pushCustomCode(Sources.GENERAL_COMMA.getValue)
			val isXpath =pushXpath(outputObject)
			if(!isXpath){
				return false;
			}
			if(!isFunctionVariable){
				pushCustomCode(Sources.GENERAL_RIGHT_BRACKET.getValue + Sources.GENERAL_DELIMITER.getValue)		
			}else{
				pushCustomCode(Sources.GENERAL_COMMA.getValue);
			}
			return true
 		}
 		return false;
 	}
 	
 	override dispatch boolean interpret(CallStatement statement){
 		var outputSection = statement.outputSection
 		var simple = true;
 		if(outputSection !== null){
 			var outputAttributes = statement.outputSection.outputAttributes
 			val inputSection = statement.target.inputSection
 			var inputAttributes = null as List<EObject>
	 		if(inputSection !== null){
	 			inputAttributes = inputSection.inputAttributes
	 		}
	 		
	 		pushNewLine()
	 		val method = statement.java_method;
			if(method === null){
				return false;
			}
			val clazz = statement.target.javaClass;
			if(clazz === null){
				return false;
			}
			
			if(outputAttributes !== null){
	 			if(outputAttributes.length > 0){
	 				for(var i = 0; i <outputAttributes.length; i++){
						var object = outputAttributes.get(i)
						var objectType = typeProvider.typeForObject(object);
						if(objectType instanceof StructureType){
							structureCommand(object,object,statement,-1,true)
							simple = false;
						}else{
							var initializedInStatement = objectUtil.isEObjectInitializedInStatement(statement,object)
							if(initializedInStatement){
								pushObjectType(object)
								pushCustomCode(Sources.GENERAL_SPACE.getValue)
							}
							pushObjectName(object)
							pushCustomCode(Sources.GENERAL_EQUALS.getValue);
						}	
					}		
	 			}
	 			val line = "new Function(";
	 			val move = 1
	 			pushCustomCode(line);
	 			pushCustomCode(clazz.identifier+".class" + Sources.GENERAL_COMMA.getValue);
	 			pushCustomCode('''"�method.simpleName�"'''+Sources.GENERAL_COMMA.getValue);
	 			
	 			pushCustomCode('''new String[] {''');
	 			var params = method.parameters;
	 			if(params !== null){
	 				var i = 0;
	 				for(JvmFormalParameter param : params){
	 					pushCustomCode('''"�param.parameterType.identifier�"''')
	 					if(i < (params.size - move)){
					 		pushCustomCode(Sources.GENERAL_COMMA.getValue);
					 	}
					 	i++;
	 				}
	 			}
	 			pushCustomCode('''}�Sources.GENERAL_COMMA.getValue�''')
	 			if(inputAttributes !== null){
	 				for(var i = 0; i <inputAttributes.length; i++){
		 				var object = inputAttributes.get(i)
		 				var inputType = typeProvider.typeForObject(object)
		 				if(inputType instanceof StructureType){
		 					val isOK = object.createInput(inputType,-1,true);
				 			if(!isOK){
				 				return false;
				 			}
		 				}else if(inputType instanceof FunctionType){
		 					pushCustomCode(line)
		 					pushObjectName(object);
		 					pushCustomCode(Sources.GENERAL_RIGHT_BRACKET.getValue);
		 				}else{
		 					pushObjectName(object);
		 				}
		 				if(i < (inputAttributes.size - move)){
					 		pushCustomCode(Sources.GENERAL_COMMA.getValue)
					 	}
	 				}
	 			}else{
	 				pushCustomCode('''null''')
	 			}
	 			
	 			pushCustomCode(Sources.GENERAL_RIGHT_BRACKET.getValue);
	 			if(!simple){
	 				pushCustomCode(Sources.GENERAL_RIGHT_BRACKET.getValue)
	 			}
	 			pushCustomCode(Sources.GENERAL_DELIMITER.getValue);
	 			return true;
	 			
	 		}
	 		
 		}
 	}
 	 	
 	/*override dispatch boolean interpret(CallStatement statement){
 		var outputSection = statement.outputSection
 		
 		if(outputSection !== null){
 			var outputAttributes = statement.outputSection.outputAttributes
 			val inputSection = statement.target.inputSection
 			var inputAttributes = null as List<EObject>
	 		if(inputSection !== null){
	 			inputAttributes = inputSection.inputAttributes
	 		}
	 		
 			pushNewLine()
 			val method = statement.java_method;
			if(method === null){
				return false;
			}
 			
	 		if(outputAttributes !== null){
	 			if(outputAttributes.length > 0){
	 				val firstLine = "routineReturns.clear();"
	 				val secondLine = "routineReturns.add("
	 				pushCustomCode(firstLine)
	 				pushNewLine()	
	 				pushNewImport(SourcesEnum.IMPORT_RETURN_VALUE.value)
	 				pushCustomCode(secondLine + "new "+SourcesEnum.CODE_RETURN_VALUE.value)
	 				createTType(null,method.returnType.identifier,null);
	 				pushCustomCode(SourcesEnum.GENERAL_LEFT_BRACKET.value)
	 				createCastType(null,method.returnType.identifier,null)
	 				pushCustomCode(SourcesEnum.GENERAL_SPACE.value);	 		
	 			}
	 		} 

			val methodName = method.simpleName
			var type = statement.target.javaClass;
			if (type instanceof JvmGenericType) {
				pushCustomCode(type.identifier + "." + methodName +"(")
		        var gt = type as JvmGenericType;
		        generateMethod(gt,methodName,inputAttributes)
		    pushCustomCode(SourcesEnum.GENERAL_RIGHT_BRACKET.value);
		    if(outputAttributes !== null){
		    	 pushCustomCode(SourcesEnum.GENERAL_RIGHT_BRACKET.value);
		    }
		    pushCustomCode(SourcesEnum.GENERAL_DELIMITER.value);
		    pushNewLine()
 		}
		if(outputAttributes !== null){
			for(var i = 0; i <outputAttributes.length; i++){
				var obj = outputAttributes.get(i)
				val outputType = typeProvider.typeForObject(obj)
				if(outputType instanceof StructureType){
					createRoutineStructure(i,obj,statement);
				}else{
					createRoutineType(i,obj,statement);
				}
			}
		}
		pushNewLine()
		return true;
	}
	return false;
 	}*/
 	
 	/**
 	 * generate method from call statement
 	 * @param gt generic type
 	 * @param methodName method name
 	 * @param inputAttributes input attribute
 	 */
 	def generateMethod(JvmGenericType gt, String methodName, List<EObject> inputAttributes){
 		for (JvmMember member : gt.getMembers()) {
			if (member instanceof JvmOperation) {
		    	if (methodName.equals(member.simpleName)) {
			  		var params = member.parameters
			  		val size = params.size
			  		val inputSize = (inputAttributes === null) ? 0 : inputAttributes.length
		  			val move = 1
			 		for(var i = 0; i < size; i++){
			 			//if is method(param,param) and inputAttributes(input) .. then call: method(input,null)
			 			if(i < inputSize){
			 				pushObjectName(inputAttributes.get(i));	
		 				}else{
		 					pushCustomCode(Sources.CODE_NULL.getValue)
		 				}
		 				if(i < (size - move)){
				 			pushCustomCode(Sources.GENERAL_COMMA.getValue)
				 		}
		  			}
		        }
		    }
		 }
 	}
 	
 	override dispatch boolean interpret(MapStatement statement) {
 		val inputSection = statement.inputSection
		//
 		var outputSection = statement.outputSection
 		var outputAttributes = null as List<EObject>
 		if(outputSection !== null){
 			outputAttributes = statement.outputSection.outputAttributes
 		}
 		//routine variables...
   		val name = statement.routineSection.source.name
   		if(name === null){
   			return false;
   		}
   		var className = generatorUtil.getCommonClassName(name)
 		var javaRoutineName = className + "." + getJavaName(name)
 		
 		pushNewLine()
 		if(outputAttributes !== null){
 			if(outputAttributes.length > 0){
 				val line = "routineReturns = "
 				pushCustomCode(line)	
 			}
 		} 
 		pushCustomCode(javaRoutineName + Sources.GENERAL_LEFT_BRACKET.getValue)
 		if(inputSection !== null){	
	 		//method call
	 		var inputAttributes = statement.inputSection.inputAttributes	 		
	 		val size = inputAttributes.length
	 		val move = 1
	 		for(var i = 0; i < size; i++){
	 			pushObjectName(inputAttributes.get(i));
	 			if(i < (size - move)){
	 				pushCustomCode(Sources.GENERAL_COMMA.getValue)
	 			}
	 		}
	 	}	
	 	pushCustomCode(Sources.GENERAL_RIGHT_BRACKET.getValue + Sources.GENERAL_DELIMITER.getValue)
 		pushNewLine()
 		if(outputAttributes !== null){
 			for(var i = 0; i <outputAttributes.length; i++){
 				var obj = outputAttributes.get(i)
 				val outputType = typeProvider.typeForObject(obj)
 				if(outputType instanceof StructureType){
					createRoutineStructure(i,obj,statement);
				}else{
					createRoutineType(i,obj,statement);
				}
 			}	
 		}
 		
 		pushRoutineGenerator()	 	
 		return true;		
 	} 
 	
 	
 	override boolean interpretRoutine(Statement statement, List<MapStatement> outerStatements) {
 		typeProvider.activateRoutineScope(outerStatements)
 		var isWorking = statement.interpret
 		typeProvider.deactivateRoutineScope()
 		return isWorking
 	}
 	
 	override dispatch boolean interpret(InputImport inputImport){
 		val input = inputImport.name;
 		if(input !== null){
 			var name = objectUtil.getObjectName(input)
 			name = getJavaName(name);
 			pushCustomCode(name)
 			return true
 		}
 		return false
 	}
 	override dispatch boolean interpret(OutputImport outputImport){
 		val output = outputImport.name;
 		if(output !== null){
 			var name = objectUtil.getObjectName(output)
 			name = getJavaName(name);
 			pushCustomCode(name);
 			return true
 		}
 		return false
 	}
 	
 	override void interpretMethodHeader(MapStatement statement, List<MapStatement> outerStatements){
 		var outputSection = statement.outputSection
 		var outputAttributes = null as EList<EObject>
 		if(outputSection !== null){
 			outputAttributes = statement.outputSection.outputAttributes
 		}
 		val name = statement.routineSection.source.name
 		if(outputAttributes !== null){	
 			val line = "public static ArrayList<"+Sources.CODE_RETURN_VALUE.getValue+"> "
 			pushCustomCode(line)
 		}else{
 			val line = "public static void " 
 			pushCustomCode(line)
 		} 
 		pushNewImport(Sources.IMPORT_RETURN_VALUE.getValue)
 		pushNewImport(Sources.IMPORT_ARRAYLIST.getValue)
 		pushCustomCode(getJavaName(name) + Sources.GENERAL_LEFT_BRACKET.getValue)	
 			
 		
 		if(statement.inputSection !== null){
 			var inputAttributes = statement.inputSection.inputAttributes
 			val size = inputAttributes.length
	 		val move = 1	
	 		
	 		for(var i = 0; i < size; i++){
	 			typeProvider.activateRoutineScope(outerStatements)
	 			pushObjectType(inputAttributes.get(i))
	 			val inputPartName = " input"
	 			pushCustomCode(inputPartName+i)
	 			if(i < (size - move)){
	 				pushCustomCode(Sources.GENERAL_COMMA.getValue)
	 			}
	 			typeProvider.deactivateRoutineScope()
	 		}
 		}
 		
 		pushCustomCode(Sources.GENERAL_RIGHT_BRACKET.getValue + " {")	
 	}

	override String getRequest(){
		return popRequest
	}
	
	override int getRequestsSize(){
		return getStackSize
	}
}
	
	
	
	
