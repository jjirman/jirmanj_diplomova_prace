package com.gk_software.core.dsl.xmapping.typing;

/**
 * Singleton Int type
 */
public final class IntType extends Type{
	
	/** instance */
	public static final IntType INSTANCE = new IntType();  
	
	/** name */
	private static final String NAME = "intType";
	
	/** code name */
	private static final String CODE_TYPE = "int";
	
	/**
	 * Constructor
	 */
	private IntType() {
		super(NAME, CODE_TYPE);
	}
	
}
