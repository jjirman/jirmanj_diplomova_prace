package com.gk_software.core.dsl.xmapping.validation

import org.eclipse.xtext.validation.Check
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.emf.ecore.EObject
import com.google.inject.Inject
import org.eclipse.xtext.naming.IQualifiedNameProvider
import com.gk_software.core.dsl.xmapping.helpers.util.XMappingEObjectUtil
import com.gk_software.core.dsl.xmapping.xMapping.MappingClass
import com.gk_software.core.dsl.xmapping.xMapping.XMappingPackage
import com.gk_software.core.dsl.xmapping.xMapping.Input
import com.gk_software.core.dsl.xmapping.xMapping.Output
import com.gk_software.core.dsl.xmapping.xMapping.Variable
import com.gk_software.core.dsl.xmapping.xMapping.Return
import com.gk_software.core.dsl.xmapping.xMapping.Routine
import com.gk_software.core.dsl.xmapping.xMapping.AssignmentStatement
import com.gk_software.core.dsl.xmapping.xMapping.Block
import com.gk_software.core.dsl.xmapping.xMapping.Model

/**
 * Class which checks duplicates
 * @author Bc. Jan Jirman
 */
class XMappingDuplicateValidator extends AbstractXMappingValidator {
	
	/** name provider */
	@Inject IQualifiedNameProvider nameProvider	
	/** object util*/
	@Inject XMappingEObjectUtil objectUtil
	
	
	/** constant of number of same objects */
	final static int numberOfSameObjects = 1;
		
	/**
	 * Check mapping class and package if they have same names
	 * @param mappingClass mapping class
	 */
	@Check
	def checkMappingClassAndPackage(MappingClass mappingClass){
		val packageName = (mappingClass.eContainer as Model).name
		if(mappingClass.name.equals(packageName)){
			val message = "Mapping class has same name as package name.";
			error(message,null, XMappingPackage.MODEL__NAME);
		}
	}
	
	/**
	 * Check input duplicates
	 * @param input input
	 */
	@Check
	def dispatch checkDuplicate(Input input){
		var xsdImport = input.eContainer.eContainer
		var inputs = EcoreUtil2.getAllContentsOfType(xsdImport,typeof(Input)) as Iterable<Input>;
		inputs = inputs.filter[it |
			var basicObject = objectUtil.getBasicEObject(it);
			(basicObject as Input).name.equals(input.name);
		]
		var size = inputs.sizeOfIterable
		showError(size,input) 		
	}
	
	/**
	 * Check output duplicates
	 * @param output output
	 */
	@Check
	def dispatch checkDuplicate(Output output){
		var xsdImport = output.eContainer.eContainer
		var outputs = EcoreUtil2.getAllContentsOfType(xsdImport,typeof(Output)) as Iterable<Output>;
		outputs = outputs.filter[it |
			var basicObject = objectUtil.getBasicEObject(it);
			(basicObject as Output).name.equals(output.name);
		]
		var size = outputs.sizeOfIterable
		showError(size,output) 
	}
	
	/**
	 * Checks variable duplicates
	 * @param variable variable
	 */
	@Check
	def dispatch checkDuplicate(Variable variable){
		checkduplicateObject(variable)
	}
	
	/**
	 * Checks return duplicates
	 * @param ^return return
	 */
	@Check
	def dispatch checkDuplicate(Return ^return){
		checkduplicateObject(^return)
	}
	
	/**
	 * Check object duplicates
	 * @param object
	 */
	def void checkduplicateObject(EObject object){
		var clazz = object.class
		var declaration = object.checkDeclaredObject(clazz)
		var size = declaration.sizeOfIterable
		showError(size,object) 
	}
	
	/**
	 * Checks routine duplicates
	 */
	@Check
	def dispatch checkDuplicate(Routine routine){
		val mappingClass = EcoreUtil2.getContainerOfType(routine, typeof(MappingClass));
		var routines = EcoreUtil2.getAllContentsOfType(mappingClass, typeof(Routine));
		val sameRoutines = routines.filter[it.name == routine.name]
		if(sameRoutines.size > numberOfSameObjects){
			val message = routine.name +" already exists.";
			error(message,null, XMappingPackage.ROUTINE__NAME);
		}
	}
		
	/**
	 * Shows error
	 * @param <T> extends EObject
	 * @param size size
	 * @param object object
	 */
	private def <T extends EObject> showError(int size, T object){
		val minSize = 0
		//object qualifiedName 
		var qn = nameProvider.getFullyQualifiedName(object)
		var name = qn.lastSegment
			
		if(size === minSize){
			val message = name +" is not declared.";
			if(object instanceof Return){
				error(message,null, XMappingPackage.RETURN__NAME);
			}else if (object instanceof Variable){
				error(message,null, XMappingPackage.VARIABLE__NAME);
			}			
		}else{
			if(size > numberOfSameObjects){
				val message = name +" already exists.";
				error(message,null, XMappingPackage.DECLARATION__NEW);
			}
		}	
	}
	
	/**
	 * Size of "Iterable object"
	 * @param iterable iterable object
	 * @return number
	 */
	private def <T extends EObject> int getSizeOfIterable(Iterable<T> iterable){
		if(iterable === null){
			return 0
		}
		iterable.length
	}
	
	/**
	 * Checks declared objects
	 * @param object
	 * @param clazz class
	 */
	private def <T extends EObject> Iterable<AssignmentStatement> checkDeclaredObject(T object, Class clazz){
		val block = EcoreUtil2.getContainerOfType(object, typeof(Block));
		return EcoreUtil2.getAllContentsOfType(block, typeof(AssignmentStatement))
							.filter[it.declaration.^new !== null && it.declaration.^new.exists[it.source !== null && nameProvider.getFullyQualifiedName(it.source).equals(nameProvider.getFullyQualifiedName(object))]]
	} 
}