package com.gk_software.core.dsl.xmapping.helpers.classes;

import java.io.OutputStream;

import org.eclipse.core.runtime.CoreException;

public interface IMessageListener {
	
	void writeOutput(String message, String where);
	void writeError(String message, String where);
}
