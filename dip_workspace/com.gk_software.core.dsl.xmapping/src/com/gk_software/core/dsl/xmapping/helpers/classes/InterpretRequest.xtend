package com.gk_software.core.dsl.xmapping.helpers.classes

/**
 * Class used in interpret (to make commands and so on..)
 * @author Bc. Jan Jirman
 *
 */
class InterpretRequest {
	
	/** request type */
	RequestType requestType
	/** code to write into template */
	String code
	
	//************************************** CONSTRUCTORS *****************************************//
	
	/**
	 * Constructor which creates InterpretRequest
	 */
	new(String code, RequestType requestType){
		this.code = code
		this.requestType = requestType
	}
	
	//************************************** SETTER & GETTER **************************************//
	
	/**
	 * Gets request type
	 */
	def RequestType getRequestType(){
		return this.requestType
	}
	
	/**
	 * Gets code
	 */
	def String getCode(){
		return this.code	
	}
}
