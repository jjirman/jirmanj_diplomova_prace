package com.gk_software.core.dsl.xmapping.interpret;

import org.eclipse.emf.ecore.EObject;

import com.gk_software.core.dsl.xmapping.helpers.classes.InterpretRequest;
import com.gk_software.core.dsl.xmapping.typing.Type;
import com.gk_software.core.dsl.xmapping.xMapping.Assignment;

public interface IXMappingInterpretQueue {

	void pushRoutineGenerator();
 	void pushNewImport(String importValue);
 	void pushObjectNonPrimitiveType(EObject object, String identifier, Type type);
 	String pushObjectType(EObject object);
 	void pushObjectName(EObject object);
 	void pushAssignmentValue(Assignment assignment);
 	void pushEndCommand();
 	void pushCustomCode(String code);
 	void pushNewLine();
 	void pushErrorCode(String text);
 	boolean pushXpath(EObject object);
 	boolean pushRequest(InterpretRequest request);
 	String popRequest();
 	int getStackSize();
 	
}
