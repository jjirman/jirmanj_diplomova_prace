package com.gk_software.core.dsl.xmapping.helpers.util

import org.eclipse.emf.ecore.EObject
import com.google.inject.Inject
import org.eclipse.xtext.EcoreUtil2
import org.eclipse.xtext.naming.IQualifiedNameProvider
import com.gk_software.core.dsl.xmapping.xMapping.Assignment
import com.gk_software.core.dsl.xmapping.xMapping.AssignmentStructure
import com.gk_software.core.dsl.xmapping.xMapping.Model
import com.gk_software.core.dsl.xmapping.xMapping.Statement

/**
 * Utility which helps interpet in general
 * @author Bc. Jan Jirman
 *
 */
class XMappingInterpretUtil {
	
	/** interpret object */
	@Inject extension XMappingInterpretObjectUtil
	/** object util */
	@Inject XMappingEObjectUtil objectUtil
	/** statement util */
	@Inject XMappingStatementUtil statementUtil
	/** name provider */
	@Inject IQualifiedNameProvider nameProvider	
	
	/**
	 * Gets assignment value from "Assignment object"
	 * @param assignment Assignment object
	 * @return value
	 */
	def String getAssignmentValue(Assignment assignment){
 		switch(assignment){
 			com.gk_software.core.dsl.xmapping.xMapping.IntType: return '''�assignment.value�'''
 			com.gk_software.core.dsl.xmapping.xMapping.DoubleType: return '''�assignment.value�'''
 			com.gk_software.core.dsl.xmapping.xMapping.StringType: return '''"�assignment.value�"'''
 		}
 		if(assignment.structure !== null){
 			return specificAssignmentStructure(assignment.structure)
 		}
 		
 	}
 	
 	/**
 	 * Gets specific assigment structure
 	 * @param assignment assignment object
 	 * @return object java name
 	 */
 	def String specificAssignmentStructure(AssignmentStructure assignmentStructure){
 		if(assignmentStructure instanceof AssignmentStructure){
			var model = EcoreUtil2.getContainerOfType(assignmentStructure,typeof(Model))
			var mappingClass = model.class_.name
			var packageName = model.name
			return '''�packageName�.�mappingClass�.�getJavaName(objectUtil.getObjectName(assignmentStructure.source.outputType.outputRef))�.getPartOfDocument("�assignmentStructure.source.XPath�")'''
 		}
 	}
 	
 	/**
 	 * Gets return from routine by index
 	 * @param statement map statement
 	 * @param index index
 	 */
 	def EObject getReturnObjectByIndex(Statement statement, int index){
 		val returns = statementUtil.getReturnsFromRoutine(statement);
 		val returnsSize = returns.size;
 		if(index < returnsSize){
 			for(EObject returnObject : returns){
 				var name = nameProvider.getFullyQualifiedName(returnObject).lastSegment;
 				var returnToFind = name
 				val regex = "Return.\\d"
 				val nameToFind = "Return."+index
 				returnToFind = returnToFind.replaceAll(regex,nameToFind)
 				if(returnToFind.equals(name)){
 					return returnObject;
 				}
 			}
 			return null;
 		}
 		return null;
 	}
}
