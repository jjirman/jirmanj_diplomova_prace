package com.gk_software.core.dsl.xmapping.typing;

/**
 * Singleton Double type
 */
public final class DoubleType extends Type{
	
	/** instance */
	public static final DoubleType INSTANCE = new DoubleType();  
	
	/** name */
	private static final String NAME = "doubleType";
	
	/** code name */
	private static final String CODE_TYPE = "double";
	
	/**
	 * Constructor
	 */
	private DoubleType() {
		super(NAME, CODE_TYPE);
	}	
}

