package com.gk_software.core.dsl.xmapping.typing

import org.eclipse.xtext.EcoreUtil2
import org.eclipse.emf.ecore.EObject
import com.google.inject.Inject
import java.util.Stack
import java.util.List
import com.gk_software.core.dsl.xmapping.helpers.util.XMappingEObjectUtil
import com.gk_software.core.dsl.xmapping.helpers.util.XMappingStatementUtil
import com.gk_software.core.dsl.xmapping.xMapping.Statement
import com.gk_software.core.dsl.xmapping.xMapping.MapStatement
import com.gk_software.core.dsl.xmapping.xMapping.Variable
import com.gk_software.core.dsl.xmapping.xMapping.Return
import com.gk_software.core.dsl.xmapping.xMapping.Param
import com.gk_software.core.dsl.xmapping.xMapping.Attribute
import com.gk_software.core.dsl.xmapping.xMapping.InputAttribute
import com.gk_software.core.dsl.xmapping.xMapping.Input
import com.gk_software.core.dsl.xmapping.xMapping.Output
import com.gk_software.core.dsl.xmapping.xMapping.RoutineBlock
import com.gk_software.core.dsl.xmapping.xMapping.MapForwardStatement
import com.gk_software.core.dsl.xmapping.xMapping.CallStatement
import com.gk_software.core.dsl.xmapping.xMapping.AssignmentStatement
import com.gk_software.core.dsl.xmapping.xMapping.AssignmentStructure

/**
 * This class provide object type
 * @author Bc. Jan Jirman
 */
class XMappingTypeProvider {
	
	/** statement util */
	@Inject XMappingStatementUtil statementUtil
	/** object util */
	@Inject XMappingEObjectUtil objectUtil
	
	static val STACK_MAX_SIZE = 100; 
	
	/** singleton string type */
	public static val stringType = StringType.INSTANCE
	/** singleton int type */
	public static val intType = IntType.INSTANCE
	/** singleton structure type */
	public static val structureType = StructureType.INSTANCE
	/** singleton not type */
	public static val notType = NotType.INSTANCE
	/** singleton double type */
 	public static val doubleType = DoubleType.INSTANCE
 	/** singleton not specified type */
 	public static val notSpecified = NotSpecified.INSTANCE
 	/** singleton function type */
 	public static val functionType = FunctionType.INSTANCE
 	
 	/** statements stack */
 	static Stack<Statement> statements = new Stack<Statement>();
 	/** routine scope */
 	static boolean routineScope = false;
 	/** outer statement  */
 	static List<MapStatement> outerStatements;
 	
 	/**
 	 * Activates routine scope
 	 * @param list of map statements
 	 */
 	def void activateRoutineScope(List<MapStatement> statements){
 		routineScope = true;
 		outerStatements = statements;		
 	}
 	
 	/**
 	 * Deactivates routine scope
 	 */
 	def void deactivateRoutineScope(){
 		routineScope = false;
 		outerStatements = null;	
 	}
 	
 	/**
 	 * Resets stack with outer statements
 	 */
 	def void resetRoutineScope(){
 		if(routineScope){
 			for(Statement statement : outerStatements){
 				statements.add(statement)
 			}
 			
 		}
 	}

	/**
	 * Gets type of object
	 */
 	def Type typeForObject(EObject object){
 		resetRoutineScope()		
 		var finalObject = objectUtil.getBasicEObject(object)
 		//calls main object type method
 		val type = finalObject.objectType()
 		statements.clear
 		return type
 	}
 	
 	/**
	 * Gets type for specific object - in case of error, then object is notType
	 * @param object object
	 * @return object's type
	 */
 	def Type objectType(EObject object){
 		var statement = null as Statement
 		//types of objects
 		if(object instanceof Variable || object instanceof Return){
 			statement = statementUtil.findObjectInitialization(object)
 		} else if(object instanceof Param){
 			val minSize = 0;
 			if(statements.size > minSize){
 				statement = statements.pop
 			}else{
 				//if statements is 0, then "notSpecified" (used for validation)
 				return notSpecified
 			}
 			
 			//find declared param
 			var declaredObject = statementUtil.findDeclaredParam(statement,object)
 			if(declaredObject !== null){
 				if(declaredObject instanceof Attribute){
 					return declaredObject.type.objectType()	
	 			}else if(declaredObject instanceof InputAttribute){
	 				val obj = declaredObject.inputType
	 				if(obj.inputRef !== null){
	 					return declaredObject.inputType.inputRef.objectType();
	 				}else if(obj.paramRef !== null){
	 					return declaredObject.inputType.paramRef.objectType();
	 				}
	 			}else if(declaredObject instanceof Param){
	 				return declaredObject.objectType();
	 			}
 			}
 			
 		} else if(object instanceof Input || object instanceof Output){
 			return structureType
 		}
 		
 		//find type for object (Variable / Return)
 		if(statement !== null){
 			 return statement.typeFor(object)
 		}
		notType
 	}
 	
 	/**
	 * Finds type for object in MapStatement
	 * @param statement map statement
	 * @param object object
	 * @return object's type
	 */
 	def dispatch Type typeFor(MapStatement statement, EObject object) {
 		statements.push(statement);
 		
 		//recursion check
 		if(statements.size > STACK_MAX_SIZE){
 			return notType
 		}
 		//if statement contains outputSection
 		if(statement.outputSection !== null){
 			var outputAttributes = statement.outputSection.outputAttributes
	 		val outputIndex = objectUtil.getObjectIndexByObject(outputAttributes,object)
	 		//if -1: can not find any object
	 		if(outputIndex != -1){
	 			//gets routine and its block
	 			val routine = statement.routineSection.source
		 		val routineSection = EcoreUtil2.getContainerOfType(routine, typeof(RoutineBlock))
		 		val block = routineSection.routineBlock
		 		val name = "Return." + outputIndex
		 		//searching for return
		 		if(block !== null){
		 			val returnObject = objectUtil.getObjectByName(block, name)
		 			if(returnObject !== null){
		 				//and gets its type
		 				return objectType(returnObject)
		 			}
		 		}
 			}
 		}
 		notType
 	}
 	
 	/**
	 * Finds type for object in MapForwardStatement
	 * @param statement map forward statement
	 * @param object object
	 * @return object's type
	 */
 	def dispatch Type typeFor(MapForwardStatement statement, EObject object) {
 		var outputAttributes = statement.outputSection.outputAttributes
 		//gets object index in map forward statement
 		val outputIndex = objectUtil.getObjectIndexByObject(outputAttributes,object)
 		//if -1: can not find any object
 		if(outputIndex != -1){
 			var inputAttribute = statement.inputSection.inputAttributes.get(outputIndex)
 			//gets input attribute and finds its type
 			if(inputAttribute instanceof InputAttribute){
 				var instance = inputAttribute.inputType
 				if (instance.inputRef !== null){
					return instance.inputRef.objectType
	 			}else if(instance.paramRef !== null){
					return instance.paramRef.objectType
	 			}	
 			}else if (inputAttribute instanceof Attribute){
 				return inputAttribute.type.objectType
 			}	
 		}
 		notType
 	}
 	
 	/**
	 * Finds type for object in CallStatement
	 * @param statement call statement
	 * @param object object
	 * @return object's type
	 */
 	def dispatch Type typeFor(CallStatement statement, EObject object) {
 		/*var javaMethod = statement.java_method;
 		if(javaMethod !== null){
 			val methodReturnType = javaMethod.returnType
 			return methodType(methodReturnType.type.identifier)
 		}*/
 		
 		return functionType;	
 	}
 	
 	def Type methodType(String identifier){
 		//var type = returnReference.type.identifier as String;		
 		switch(identifier){
 			case "io.OutputDocument": return structureType
 			case "java.lang.String": return stringType
 			case "boolean": return stringType
 			case "int": return intType
 			case "double": return doubleType
 			case "void": return notType
 		} 
 		notSpecified
 	}
 
 	/**
	 * Finds type for object in AssignmentStatement
	 * @param statement assignment statement
	 * @param object object
	 * @return object's type
	 */
  	def dispatch Type typeFor(AssignmentStatement statement, EObject object) {
		//gets initialization
		val value = statement.initialization.assignment
 		switch value {
 			com.gk_software.core.dsl.xmapping.xMapping.IntType: return intType
 			com.gk_software.core.dsl.xmapping.xMapping.StringType: return stringType
 			com.gk_software.core.dsl.xmapping.xMapping.DoubleType: return doubleType
	 	}	
	 	if(value.structure !== null){
	 		return structureType
	 	}
 	}
 	
 	/**
 	 * Gets non-primitive type
 	 * @param type
 	 */
 	def String getNonPrimitiveType(Type type){
 		switch(type) {
			case structureType: return "DocumentOutput"
			case intType: return "Integer"
			case stringType: return "String"
			case doubleType: return "Double"
			case functionType: return "DocumentOutput"
 		}
 		return "Object";
 	}
}
