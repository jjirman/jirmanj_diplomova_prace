package com.gk_software.core.dsl.xmapping.interpret;

import com.gk_software.core.dsl.xmapping.xMapping.AssignmentStatement;
import com.gk_software.core.dsl.xmapping.xMapping.CallStatement;
import com.gk_software.core.dsl.xmapping.xMapping.InputImport;
import com.gk_software.core.dsl.xmapping.xMapping.MapForwardStatement;
import com.gk_software.core.dsl.xmapping.xMapping.MapStatement;
import com.gk_software.core.dsl.xmapping.xMapping.OutputImport;
import com.gk_software.core.dsl.xmapping.xMapping.Statement;
import java.util.Arrays;
import java.util.List;
import org.eclipse.emf.ecore.EObject;

@SuppressWarnings("all")
public abstract class AbstractXMappingInterpret {
  /**
   * Interprets an assignment statement
   * @param statement assignment statement
   */
  protected boolean _interpret(final AssignmentStatement statement) {
    return false;
  }
  
  /**
   * Interprets a map forward statement
   * @param statement map forward statement
   */
  protected boolean _interpret(final MapForwardStatement statement) {
    return false;
  }
  
  /**
   * Interprets a call statement
   * @param statement call statement
   */
  protected boolean _interpret(final CallStatement statement) {
    return false;
  }
  
  /**
   * Interprets a map statement
   * @param statement map statement
   */
  protected boolean _interpret(final MapStatement statement) {
    return false;
  }
  
  /**
   * Interprets an input import
   * @param inputImport input import
   */
  protected boolean _interpret(final InputImport inputImport) {
    return false;
  }
  
  /**
   * Interprets an output import
   * @param outputImport output import
   */
  protected boolean _interpret(final OutputImport outputImport) {
    return false;
  }
  
  /**
   * Interprets a method header (routine header
   * @param statement map statement
   * @param outer statements nested statements
   */
  public abstract void interpretMethodHeader(final MapStatement statement, final List<MapStatement> outerStatements);
  
  /**
   * Interprets routine
   * @param statement statement
   * @param outer statements nested statements
   */
  public abstract boolean interpretRoutine(final Statement statement, final List<MapStatement> outerStatements);
  
  public abstract String getRequest();
  
  public abstract int getRequestsSize();
  
  public boolean interpret(final EObject statement) {
    if (statement instanceof AssignmentStatement) {
      return _interpret((AssignmentStatement)statement);
    } else if (statement instanceof CallStatement) {
      return _interpret((CallStatement)statement);
    } else if (statement instanceof MapForwardStatement) {
      return _interpret((MapForwardStatement)statement);
    } else if (statement instanceof MapStatement) {
      return _interpret((MapStatement)statement);
    } else if (statement instanceof InputImport) {
      return _interpret((InputImport)statement);
    } else if (statement instanceof OutputImport) {
      return _interpret((OutputImport)statement);
    } else {
      throw new IllegalArgumentException("Unhandled parameter types: " +
        Arrays.<Object>asList(statement).toString());
    }
  }
}
